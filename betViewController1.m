//
//  betViewController1.m
//  BetMe
//
//  Created by Admin on 7/6/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import "betViewController1.h"
#import "homeViewController.h"
#import "MainViewController.h"
#import "betViewController1.h"
#import "profileViewController.h"
#import "couponViewController.h"
#import "BetMeHelpers.h"
#import <betSelectionViewController.h>
#import "betmevideoservices.h"
//runtime library
#import <objc/message.h>
#import "TGLViewController.h"
#import "parseQueryTableViewController.h"
#import "CameraViewController.h"

// USE WHEN NEEDED
/*
#import "MosaicCell.h"
#import "MosaicData.h"
#import "MosaicLayout.h"
#import "MosaicLayoutDelegate.h"
 */

@interface betViewController1 () <UIAlertViewDelegate>
@property  __block UINavigationController *masterNavigationController;
@property (nonatomic,strong) dispatch_block_t menuBlock;
@property (nonatomic,strong) betMeVideoServices* videoServices;
@end
@implementation betViewController1 


@synthesize sideMenu=_sideMenu;
@synthesize masterNavigationController=_masterNavigationController;
@synthesize menuBlock = _menuBlock;
@synthesize videoServices=_videoServices;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Explore";
    
    
    //menu bar button
    UIImage *menuImage = [UIImage imageNamed:@"menu"];
    CGRect menuFrame = CGRectMake(0, 0, menuImage.size.width/2.5, menuImage.size.height/3);
    UIButton* menuButton = [[UIButton alloc] initWithFrame:menuFrame];
    [menuButton setBackgroundImage:menuImage forState:UIControlStateNormal];
    [menuButton setShowsTouchWhenHighlighted:YES];
    [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* menu = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menu;
    
    
    //record bar button
    UIImage *image = [UIImage imageNamed:@"record"];
    CGRect frame = CGRectMake(0, 0, image.size.width/1.5, image.size.height/2);
    UIButton* button = [[UIButton alloc] initWithFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setShowsTouchWhenHighlighted:YES];
    [button addTarget:self action:@selector(startNewVideo) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* record = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    //wallet bar button
    UIImage *image2 = [UIImage imageNamed:@"wallet"];
    CGRect frame2 = CGRectMake(0, 0, image2.size.width/3, image2.size.height/2);
    UIButton* button2 = [[UIButton alloc] initWithFrame:frame2];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [button2 setShowsTouchWhenHighlighted:YES];
    [button2 addTarget:self action:@selector(showDealsWallet) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* wallet = [[UIBarButtonItem alloc] initWithCustomView:button2];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:wallet ,record,nil];
    
    parseQueryTableViewController*tableViewController=[[parseQueryTableViewController alloc]initWithCoder:nil];
    tableViewController.usedForMultipleSearch=YES;
    [self presentViewController:tableViewController animated:YES completion:nil];
    //[self.view addSubview:tableViewController.tableView];
    
    
}

- (void)showMenu
{
    [self.sideMenuViewController presentMenuViewController];
}

- (void)startNewVideo{
    
    
    CameraViewController*betPage=[[CameraViewController alloc]init];
//    betSelectionViewController*betPage=[[betSelectionViewController alloc]init];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:betPage animated:YES];
    
}
- (void)showDealsWallet{
    
    
    //current way
    NSLog(@"the current user is %@",[PFUser currentUser].objectId);
    
    //get users points
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    [query includeKey:@"deals.fromBusiness"];
    
    //start query
    __block TGLViewController*vc;
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            NSArray*deals=[object objectForKey:@"deals"];
            //NSLog(@"the deals are in dealsinWallet %@",deals);
            
            //load the controller now
            UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
            // [aFlowLayout setItemSize:CGSizeMake(400, 400)];
            [aFlowLayout setItemSize:CGSizeMake(320, 463)];
            [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
            vc = [[TGLViewController alloc]initWithCollectionViewLayout:aFlowLayout];
            vc.dealsInWallet=[[NSMutableArray alloc]initWithArray:deals];;
            
            
            self.navigationController.navigationBarHidden=NO;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else
            NSLog(@"error : %@",error);
    }];
    
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 1)
    {
        // if its logout
        if (buttonIndex==0) {
            
            NSLog(@"hi this is 0");
        }
        
        else if (buttonIndex==1) {
            [PFUser logOut];
            
            NSLog(@"User logged out!");
           // [_sideMenu hide];
            MainViewController*mainViewController=[[MainViewController alloc]init];
            [self.masterNavigationController removeFromParentViewController];
            [self.masterNavigationController dismissViewControllerAnimated:YES completion:nil];

            [self presentViewController:mainViewController animated:YES completion:nil];
            
        }
        
    }
    else if (alertView.tag == 2)
    {
        NSLog(@"this is 2");
        // do something else
    }
    
    // continue for each alertView
    
}
 



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{

    
}
@end
