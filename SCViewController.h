/*
 Copyright 2013 Scott Logic Ltd
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#import <UIKit/UIKit.h>
#import "NZAlertViewDelegate.h"
#import "NZAlertView.h"
#import <Parse/Parse.h>


@interface SCViewController : UIViewController <NZAlertViewDelegate>

@property(nonatomic,strong) NSString * dealID;
@property(nonatomic,strong) PFObject * dealIDPointer;
@property(nonatomic,strong) NSString * smallBusinessID;
@property(nonatomic,strong) PFObject * smallBusinessIDPointer;

//for share
@property(nonatomic,strong) NSString * locationName;
@property(nonatomic,strong) NSString * locationShareURL;

//for notify the remove deal
@property(nonatomic,strong) NSIndexPath* relativeIndexPath;



@end
