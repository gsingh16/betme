//
//  betViewController1.h
//  BetMe
//
//  Created by Admin on 7/6/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <Parse/Parse.h>
#import "VideoPostTableViewCell.h"
#import "APSmartStorage.h"
#import "FBPictureHelper.h"
#import <ALMoviePlayerController.h>
#import <ALMoviePlayerControls.h>
#import "RESideMenu.h"



@interface betViewController1 : UIViewController 

@property (strong, readonly, nonatomic) RESideMenu *sideMenu;

@property (strong, nonatomic) IBOutlet PFQueryTableViewController *searchTableViewController;

-(void)startNewVideo;




@end
