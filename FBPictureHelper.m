//
//  FBPictureHelper.m
//  FacebookPicture
//
//  Created by dasmer on 1/2/14.
//  Copyright (c) 2014 Columbia University. All rights reserved.
//

#import "FBPictureHelper.h"

@implementation FBPictureHelper

+ (NSURL *) urlForProfilePictureSizeLargeForFBID:(NSString *)facebookID{
    return [[self class] urlForProfilePictureOfSize:@"large" ForFBID:facebookID];
}

+ (NSURL *) urlForProfilePictureSizeNormalForFBID: (NSString *)facebookID{
    return [[self class] urlForProfilePictureOfSize:@"normal" ForFBID:facebookID];
}

+ (NSURL *) urlForProfilePictureSizeSmallForFBID:(NSString *)facebookID{
    return [[self class] urlForProfilePictureOfSize:@"small" ForFBID:facebookID];
}

+ (NSURL *) urlForProfilePictureSizeSquareForFBID:(NSString *)facebookID{
    return [[self class] urlForProfilePictureOfSize:@"square" ForFBID:facebookID];
}


+ (NSURL *)urlForProfilePictureWithWidthClosestTo:(NSInteger)pixels ForFBID:(NSString *)facebookID{
    return [[self class] urlForProfilePictureForFBID:facebookID WithParam:[NSString stringWithFormat:@"width=%ld",(long)pixels]];

}

+ (NSURL *)urlForProfilePictureWithHeightClosestTo:(NSInteger)pixels ForFBID:(NSString *)facebookID{
    return [[self class] urlForProfilePictureForFBID:facebookID WithParam:[NSString stringWithFormat:@"height=%ld",(long)pixels]];
    
}


+ (NSURL *) urlForProfilePictureOfSize: (NSString *) size ForFBID: (NSString *) facebookID{
    return [[self class] urlForProfilePictureForFBID:facebookID WithParam:[NSString stringWithFormat:@"type=%@",size]];
}

+ (NSURL *) urlForProfilePictureForFBID: (NSString *) facebookID WithParam: (NSString *) param{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?%@",facebookID,param]];

}


+ (void) requestURLForCoverPhotoSizeNormalForFBID:(NSString *) facebookID completionHandler: (void(^)(NSURL *url, NSError *error)) completion{
    NSString *urlAsString = [NSString stringWithFormat:@"http://graph.facebook.com/%@?fields=cover", facebookID];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSURL *coverURL;
        if (data){
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            coverURL = [NSURL URLWithString:jsonDict[@"cover"][@"source"]];
        }
        completion(coverURL, error);
    }];
}

@end
