//
//  TGLViewController.m
//  TGLStackedViewExample
//
//  Created by Tim Gleue on 07.04.14.
//  Copyright (c) 2014 Tim Gleue ( http://gleue-interactive.com )
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "TGLViewController.h"
#import "TGLCollectionViewCell.h"


@interface UIColor (randomColor)

+ (UIColor *)randomColor;

@end

@implementation UIColor (randomColor)

+ (UIColor *)randomColor {
    
    CGFloat comps[3];
    
    for (int i = 0; i < 3; i++) {
        
        NSUInteger r = arc4random_uniform(256);
        comps[i] = (CGFloat)r/255.f;
    }
    
    return [UIColor colorWithRed:comps[0] green:comps[1] blue:comps[2] alpha:1.0];
}

@end

@interface TGLViewController ()


@end

@implementation TGLViewController

@synthesize dealsInWallet  = _dealsInWallet;

- (void)viewDidLoad {

    [super viewDidLoad];
    
    // Set to NO, to prevent a small number
    // of cards from filling the entire
    // view height evenly and only show
    // their -topReveal amount
    //
    self.stackedLayout.fillHeight = YES;
    
    // Set to NO, to prevent unexposed
    // items at top and bottom from
    // being selectable
    //
    self.unexposedItemsAreSelectable = YES;
    
    self.title=@"Retrieving current points...";
    
    PFQuery *query = [PFUser query];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        if (error) {
            NSLog(@"error fetching user's current points in the TGLVC %@",error);
            [self.navigationController popViewControllerAnimated:YES];
        }else{
        
        int currentPoints= [[object valueForKeyPath:@"points"]intValue];
        
    self.title=[NSString stringWithFormat:@"Available points: %i",currentPoints];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"userSelectedUseDeal"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"userUsedDeal"
                                               object:nil];
            
        }
    }];
    
    
    
    
    
}

- (void)receiveEvent:(NSNotification *)notification {
    // handle event
    NSLog(@"recieved event in TGLViewController  and the object is %@",notification.object);
    
    if ([notification.name isEqualToString:@"userSelectedUseDeal"]) {
        //load qr vc
        [self loadQRViewControllerWithDealDictionary:notification.object];
    }else if ([notification.name isEqualToString:@"userUsedDeal"]){
        //remove deal ui
        NSLog(@"user used deal event fired");
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([notification.name isEqualToString:@"locationSelected"]){
        NSLog(@"locationSelected notification");
    }else if ([notification.name isEqualToString:@"userWantsToSharePost"]){
        
    }
    
    
}


- (UIStatusBarStyle)preferredStatusBarStyle {

    return UIStatusBarStyleLightContent;
}

#pragma mark - Accessors

- (NSMutableArray *)cards {
    NSLog(@"the deals are after block %lu",(unsigned long)self.dealsInWallet.count);
 
    
    if (self.dealsInWallet == nil) {
        //show that YOU HAVE NO DEALS
    }
    
    return self.dealsInWallet;
}

#pragma mark - CollectionViewDataSource protocol

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSLog(@"the count of wallet items are %lu",(unsigned long)self.cards.count);
    return self.cards.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"dealInWalletCell";
    TGLCollectionViewCell *cell ;
    
    if (cell == nil) {
        
        [collectionView registerNib:[UINib nibWithNibName:@"TGLcollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CellIdentifier];
        cell=[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    
    

    NSDictionary *card = self.cards[indexPath.item];
    
    
    NSLog(@"printing potential cell %@",card);
    
    //id
    cell.dealID=[card valueForKeyPath:@"objectId"];

    //direct pointer
    cell.dealIDPointer=self.cards[indexPath.item];
    NSLog(@"the dealIDPointer in tglViewcontroller is %@",cell.dealIDPointer);
    
    //fromlocation
    cell.smallBusinessID=[[card valueForKeyPath:@"fromBusiness"]valueForKey:@"objectId"];

    //try as a pfobject?
    cell.smallBusinessIDPointer=[card valueForKeyPath:@"fromBusiness"];
    
    NSLog(@"the deal id is %@, and from small business is %@",cell.dealID,cell.smallBusinessID);
    
    //nomenclature and share
    cell.locationName=[[card valueForKeyPath:@"fromBusiness"]valueForKey:@"locationName"];
    cell.locationShareURL=[[card valueForKeyPath:@"fromBusiness"]valueForKey:@"moreInfoURL"];
    
    NSLog(@"the location name is %@, and from small business share url is %@",cell.locationName,cell.locationShareURL);
          
    //color
    cell.color = [UIColor whiteColor];

    //title
    cell.nameLabel.text=[card valueForKeyPath:@"dealName"];
    cell.title=[card valueForKeyPath:@"dealName"];
    NSLog(@"the title is %@",cell.title);
    [cell.nameLabel setText:cell.title];
    
    //image
    NSURL*locationImageUrl=[NSURL URLWithString:[[self.cards objectAtIndex:indexPath.row]valueForKey:@"imageURL"]];
    [cell.dealImage setImageWithURL:locationImageUrl];
    //terms and conditions
    cell.dealTermsAndConditions.text=[card valueForKey:@"termsAndConditions"];
    
    // this index path
    cell.relativeIndexPath=indexPath;
    return cell;
}

#pragma mark - Overloaded methods

- (void)moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    // Update data source when moving cards around
    //
    NSDictionary *card = self.cards[fromIndexPath.item];
    
    [self.cards removeObjectAtIndex:fromIndexPath.item];
    [self.cards insertObject:card atIndex:toIndexPath.item];
}


#pragma mark- cell notification
-(void)loadQRViewControllerWithDealDictionary:(NSDictionary*)dict{
    

    
    SCViewController*vc=[[SCViewController alloc]init];
    vc.dealID=[dict valueForKey:@"dealId"];
    vc.dealIDPointer=[dict objectForKey:@"dealIdPointer"];
    vc.smallBusinessID=[dict valueForKey:@"smallBusinessID"];
    vc.smallBusinessIDPointer=[dict objectForKey:@"smallBusinessIDPointer"];
    vc.locationName=[dict valueForKey:@"fromLocationName"];
    vc.locationShareURL=[dict valueForKey:@"shareURL"];
    vc.relativeIndexPath=[dict valueForKey:@"relativeIndexPath"];
    
    NSLog(@"the passed deal object is %@",dict);
    
    [self.navigationController pushViewController:vc animated:YES];
}



@end
