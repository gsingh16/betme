// Copyright 2004-present Facebook. All Rights Reserved.

/* ********************************************************************************
 *  Facebook Anonymous Login [BETA] v0.1
 *  For use with Facebook SDK for iOS v3.15
 *
 * How to Use:
 * 1. Include the provided FBAnonymousLoginBeta.h/.m files in your project
 * 2. Disable ARC for the files (if necessary) by adding the -fno-objc-arc compiler flag
 * 3. Use the new class methods on `FBSession` and also the `FBAnonymousLoginView` button provided by this header.
 *
 * ********************************************************************************
 */


#import <FacebookSDK/FacebookSDK.h>


@protocol FBAnonymousLoginViewDelegate;

@interface FBAbstractLoginView : UIView

/*!
 @abstract
 The login behavior for the active session if the user logs in via this view

 @discussion
 The default value is FBSessionLoginBehaviorWithFallbackToWebView.
 */
@property (nonatomic) FBSessionLoginBehavior loginBehavior;

/*!
 @abstract
 Initializes and returns an `FBAbstractLoginView` object.
 */
- (instancetype)init;

@end

@interface FBSession (AnonymousLoginBeta)
/*!
 @abstract
 This is the method for opening an anonymous session with Facebook. If the user proceeds with an anonymous login
 there will be no permissions granted to the access token. If the user has previously logged in with Facebook,
 and permissions were already granted to the app, this will proceed with a normal Facebook login if allowLoginUI is true.

 @param allowLoginUI    Sometimes it is useful to attempt to open a session, but only if
 no login UI will be required to accomplish the operation. For example, at application startup it may not
 be disirable to transition to login UI for the user, and yet an open session is desired so long as a cached
 token can be used to open the session. Passing NO to this argument, assures the method will not present UI
 to the user in order to open the session.

 @discussion
 Returns YES if the session was opened synchronously without presenting UI to the user. This occurs
 when there is a cached token available from a previous run of the application. If NO is returned, this indicates
 that the session was not immediately opened, via cache. However, if YES was passed as allowLoginUI, then it is
 possible that the user will login, and the session will become open asynchronously. The primary use for
 this return value is to switch-on facebook capabilities in your UX upon startup, in the case where the session
 is opened via cache.

 An anonymous session is represented by an isAnonymous boolean flag to be true on the active access token (and the permissions
 on the token also does not contain 'public_profile'). If such a token were found in cache, it will be load it via the cache
 without opening a request to the server. If a non-anonymous cached token exists (that has the 'public_profile' permission that
 was obtained from logging in socially via Facebook Login), a request will be opened to the server which will prompt the user
 with a UI to continue with their previously granted permissions. In the case where no no cached token was found, a request will
 be opened to prompt the user with a UI to log in anonymously.
 */
+ (BOOL)openActiveAnonymousSessionWithAllowLoginUI:(BOOL)allowLoginUI
                                 completionHandler:(FBSessionStateHandler)handler;
@end


@interface FBAnonymousLoginView : FBAbstractLoginView
/*!
 @abstract
 Initializes and returns an `FBAnonymousLoginView` object.  The underlying session has no permissions (anonymous login).
 */
- (instancetype)init;

@property (nonatomic, assign) IBOutlet id<FBAnonymousLoginViewDelegate> delegate;

@end


/*!
 @protocol

 @abstract
 The `FBAnonymousLoginViewDelegate` protocol defines the methods used to receive event
 notifications from `FBAnonymousLoginView` objects.

 @discussion
 Please note: Since FBAnonymousLoginViewDelegate observes the active session, using multiple FBLoginView or
 FBAnonymousLoginViewDelegate instances in different parts of your app can result in each instance's delegates
 being notified of changes for one event.
 */
@protocol FBAnonymousLoginViewDelegate <NSObject>

@optional

/*!
 @abstract
 Tells the delegate that the view is now in logged in mode with anonymous login

 @param loginView   The login view that transitioned its view mode
 */
- (void)anonymousLoginViewShowingLoggedInUser:(FBAnonymousLoginView *)loginView;

/*!
 @abstract
 Tells the delegate that the view is now in logged in mode with facebook login

 @param loginView   The login view that transitioned its view mode
 */
- (void)anonymousLoginViewShowingLoggedInWithFacebookUser:(FBAnonymousLoginView *)loginView;

/*!
 @abstract
 Tells the delegate that the view has now fetched user info when logged in with facebook login

 @param loginView   The login view that transitioned its view mode

 @param user        The user info object describing the logged in user
 */
- (void)anonymousLoginViewFetchedFacebookUserInfo:(FBAnonymousLoginView *)loginView
                                             user:(id<FBGraphUser>)user;

/*!
 @abstract
 Tells the delegate that the view is now in logged out mode

 @param loginView   The login view that transitioned its view mode
 */
- (void)anonymousLoginViewShowingLoggedOutUser:(FBAnonymousLoginView *)loginView;

/*!
 @abstract
 Tells the delegate that there is a communication or authorization error.

 @param loginView           The login view that transitioned its view mode
 @param error               An error object containing details of the error.
 @discussion See https://developers.facebook.com/docs/technical-guides/iossdk/errors/
 for error handling best practices.
 */
- (void)anonymousLoginView:(FBAnonymousLoginView *)loginView
               handleError:(NSError *)error;

@end
