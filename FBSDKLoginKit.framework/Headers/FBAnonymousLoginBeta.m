// Copyright 2004-present Facebook. All Rights Reserved.

#import "FBAnonymousLoginBeta.h"

@interface FBUtility : NSObject
+ (NSString *)localizedStringForKey:(NSString *)key
                        withDefault:(NSString *)value;
@end

@interface FBAppEvents(Internal)
+ (void)logImplicitEvent:(NSString *)eventName
              valueToSum:(NSNumber *)valueToSum
              parameters:(NSDictionary *)parameters
                 session:(FBSession *)session;
@end

@interface FBSession(Internal)
extern NSString *const FBSessionDidSetActiveSessionNotificationUserInfoIsOpening;
+ (FBSession *)activeSessionIfOpen;
@end

@interface FBRequestConnection (Internal)
- (void)startWithCacheIdentity:(NSString *)cacheIdentity
         skipRoundtripIfCached:(BOOL)consultCache;
@end

@interface FBAppEvents(FBAnonymousLoginBeta)
extern NSString *const FBAppEventNameAnonymousLoginViewUsage;
@end

@implementation FBAppEvents(FBAnonymousLoginBeta)
NSString *const FBAppEventNameAnonymousLoginViewUsage          = @"fb_anonymous_login_view_usage";
@end

@implementation FBSession (FBAnonymousLoginBeta)


+ (BOOL)openActiveAnonymousSessionWithAllowLoginUI:(BOOL)allowLoginUI
                                 completionHandler:(FBSessionStateHandler)handler {
  return [FBSession openActiveSessionWithReadPermissions:@[@"anonymous"]
                                            allowLoginUI:allowLoginUI
                                       completionHandler:handler];
}
@end

@interface FBAbstractLoginView (Internal) <UIActionSheetDelegate>

@property (retain, nonatomic) UILabel *label;
@property (retain, nonatomic) UIButton *button;
@property (retain, nonatomic) FBSession *session;
@property (retain, nonatomic) FBRequestConnection *request;
@property (retain, nonatomic) id<FBGraphUser> user;
@property (copy, nonatomic) FBSessionStateHandler sessionStateHandler;
@property (copy, nonatomic) FBRequestHandler requestHandler;

@property CGSize buttonSize;
@property (readonly) UIImage *buttonImage;
@property (readonly) UIImage *pressedButtonImage;

- (void)initialize;

// Hooks for subclasses to modify behavior
- (void)initializeButton;
- (void)handleButtonPressedWithActiveSession;

- (NSString *)logInText;
- (NSString *)logOutText;
- (NSString *)titleText;

- (NSString*)cacheIdentity;

- (void)informDelegateOfError:(NSError *)error;

- (void)informDelegate:(BOOL)userOnly;

- (void)openSession;

- (NSString *)logLoginEventName;

@end

@interface FBAbstractShadowLabel : UILabel
- (void)drawTextInRect:(CGRect)rect;
@end


@interface FBAbstractLoginView () <UIActionSheetDelegate>

@property (retain, nonatomic) UILabel *label;
@property (retain, nonatomic) UIButton *button;
@property (retain, nonatomic) FBSession *session;
@property (retain, nonatomic) FBRequestConnection *request;
@property (retain, nonatomic) id<FBGraphUser> user;
@property (copy, nonatomic) FBSessionStateHandler sessionStateHandler;
@property (copy, nonatomic) FBRequestHandler requestHandler;


// This enum tracks the session state when the inform delegate is called.
// This is to prevent messaging the delegate again in state transfers that are
// not important (e.g., tokenextended, or createdopening), and also to call the
// relevant delegate on an upgrade from an anonymous login to a facebook login
typedef NS_ENUM(NSInteger, FBLastObservedLoginState) {
    /*! A login state has not yet been established */
    FBLastObservedLoginStateUndefined = 0,
    /*! User is logged out */
    FBLastObservedLoginStateLoggedOut = 1,
    /*! User previously logged in anonymously */
    FBLastObservedLoginStateLoggedInAnonymously = 2,
    /*! User previously logged in with Facebook */
    FBLastObservedLoginStateLoggedInWithFacebook = 3,
};

@property (nonatomic) FBLastObservedLoginState lastObservedLoginState;

@end

// The design calls for 16 pixels of space on the right edge of the button
static const float kButtonEndCapWidth = 16.0;
// The button has a 12 pixel buffer to the right of the f logo
static const float kButtonPaddingWidth = 12.0;

@implementation FBAbstractLoginView

- (instancetype)init {
  self = [super init];
  if (self) {
    [self initialize];
  }
  return self;
}
- (instancetype)initWithFrame:(CGRect)aRect {
  self = [super initWithFrame:aRect];
  if (self) {
    [self initialize];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self initialize];
  }
  return self;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  // As noted in `initializeBlocks`, if we are being dealloc'ed, we
  // need to let our handlers know with the sentinel value of nil
  // to prevent EXC_BAD_ACCESS errors.
  self.sessionStateHandler(nil, FBSessionStateClosed, nil);
  [_sessionStateHandler release];
  [_requestHandler release];

  // removes all observers for self
  [[NSNotificationCenter defaultCenter] removeObserver:self];

  // if we have an outstanding request, cancel
  [self.request cancel];

  // unwire the session to release KVO.
  [self unwireViewForSession];

  [_request release];
  [_label release];
  [_button release];
  [_session release];
  [_user release];

  [super dealloc];
}


- (void)initializeBlocks {
  // Set up our block handlers in a way that supports nil'ing out the weak self reference to
  // prevent EXC_BAD_ACCESS errors if the session invokes the handler after the FBLoginView
  // has been deallocated. Note the handlers are declared as a `copy` property so that
  // the block lives on the heap.
  __block FBAbstractLoginView *weakSelf = self;
  self.sessionStateHandler = ^(FBSession *session, FBSessionState status, NSError *error) {
    if (session == nil) {
      // The nil sentinel value for session indicates both blocks should no-op thereafter.
      weakSelf = nil;
    } else if (error) {
      [weakSelf informDelegateOfError:error];
    }
  };
  self.requestHandler = ^(FBRequestConnection *connection, NSMutableDictionary<FBGraphUser> *result, NSError *error) {
    if (result) {
      weakSelf.user = result;
      [weakSelf informDelegate:YES];
    } else {
      weakSelf.user = nil;
      if (weakSelf.session.isOpen) {
        // Only inform the delegate of errors if the session remains open;
        // since session closure errors will surface through the openActiveSession
        // block.
        [weakSelf informDelegateOfError:error];
      }
    }
    weakSelf.request = nil;
  };
}

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
- (void)initialize {
  // the base class can cause virtual recursion, so
  // to handle this we make initialize idempotent
  if (self.button) {
    return;
  }

  // setup view
  self.autoresizesSubviews = YES;
  self.clipsToBounds = YES;

  [self initializeBlocks];

  if ([FBSession activeSessionIfOpen] == nil) {
    // if our session has a cached token ready, we open it; note that it is important
    // that we open the session before notification wiring is in place
    [FBSession openActiveAnonymousSessionWithAllowLoginUI:NO
                                        completionHandler:self.sessionStateHandler];
  }

  // wire-up the current session to the login view, before adding global session-change handlers
  [self wireViewForSession:FBSession.activeSession userInfo:nil];

  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(handleActiveSessionSetNotifications:)
                                               name:FBSessionDidSetActiveSessionNotification
                                             object:nil];

  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(handleActiveSessionUnsetNotifications:)
                                               name:FBSessionDidUnsetActiveSessionNotification
                                             object:nil];

  // setup button
  self.button = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.button addTarget:self
                  action:@selector(buttonPressed:)
        forControlEvents:UIControlEventTouchUpInside];
  self.button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
  self.button.autoresizingMask = UIViewAutoresizingFlexibleWidth;

  // We want to make sure that when we stretch the image, it includes the curved edges and drop shadow
  // We inset enough pixels to make sure that happens
  UIEdgeInsets imageInsets = UIEdgeInsetsMake(4.0, 40.0, 4.0, 4.0);


  UIImage *image = [self.buttonImage resizableImageWithCapInsets:imageInsets];
  [self.button setBackgroundImage:image forState:UIControlStateNormal];

  image = [self.pressedButtonImage resizableImageWithCapInsets:imageInsets];
  [self.button setBackgroundImage:image forState:UIControlStateHighlighted];

  [self addSubview:self.button];

  // Compute the text size to figure out the overall size of the button
  UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
  float textSizeWidth = MAX([[self logInText] sizeWithFont:font].width, [[self logOutText] sizeWithFont:font].width);

  // We make the button big enough to hold the image, the text, the padding to the right of the f and the end cap
  self.buttonSize = CGSizeMake(image.size.width + textSizeWidth + kButtonPaddingWidth + kButtonEndCapWidth, image.size.height);

  // add a label that will appear over the button
  self.label = [[[FBAbstractShadowLabel alloc] init] autorelease];
  self.label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
#ifdef __IPHONE_6_0
  self.label.textAlignment = NSTextAlignmentCenter;
#else
  self.label.textAlignment = UITextAlignmentCenter;
#endif
  self.label.backgroundColor = [UIColor clearColor];
  self.label.font = font;
  self.label.textColor = [UIColor whiteColor];
  [self addSubview:self.label];

  // We force our height to be the same as the image, but we will let someone make us wider
  // than the default image.
  CGFloat width = MAX(self.frame.size.width, [self buttonSize].width);
  CGRect frame = CGRectMake(self.frame.origin.x, self.frame.origin.y,
                            width, image.size.height);
  self.frame = frame;

  CGRect buttonFrame = CGRectMake(0, 0, width, image.size.height);
  self.button.frame = buttonFrame;

  // This needs to start at an x just to the right of the f in the image, the -1 on both x and y is to account for shadow in the image
  self.label.frame = CGRectMake(image.size.width - kButtonPaddingWidth - 1, -1, width - (image.size.width - kButtonPaddingWidth) - kButtonEndCapWidth, image.size.height);

  self.backgroundColor = [UIColor clearColor];

  if (self.session.isOpen) {
    [self fetchMeInfo];
    [self configureViewForStateLoggedIn:YES];
  } else {
    [self configureViewForStateLoggedIn:NO];
  }

  self.loginBehavior = FBSessionLoginBehaviorWithFallbackToWebView;
}
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

- (CGSize)intrinsicContentSize {
  return self.bounds.size;
}

- (CGSize)buttonSize {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

- (void)setButtonSize:(CGSize)size {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

- (UIImage *)buttonImage {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

- (UIImage *)pressedButtonImage {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

- (NSString *)logInText {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSString *)logOutText {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSString *)titleText {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (void)configureViewForStateLoggedIn:(BOOL)isLoggedIn {
  if (isLoggedIn) {
    self.label.text = [self logOutText];
  } else {
    self.label.text = [self logInText];
    self.user = nil;
  }
}

- (void)fetchMeInfo {
  FBRequest *request = [FBRequest requestForMe];
  [request setSession:self.session];
  self.request = [[[FBRequestConnection alloc] init] autorelease];
  [self.request addRequest:request
         completionHandler:self.requestHandler];
  [self.request startWithCacheIdentity:self.cacheIdentity
                 skipRoundtripIfCached:YES];
}

- (void)informDelegateOfError:(NSError *)error {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

- (void)informDelegate:(BOOL)userOnly {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

- (void)wireViewForSessionWithoutOpening:(FBSession *)session {
  // if there is an outstanding request for the previous session, cancel
  [self.request cancel];
  self.request = nil;

  self.session = session;

  // register a KVO observer
  [self.session addObserver:self
                 forKeyPath:@"state"
                    options:NSKeyValueObservingOptionNew
                    context:nil];
}

- (void)wireViewForSession:(FBSession *)session userInfo:(NSDictionary *)userInfo {
  [self wireViewForSessionWithoutOpening:session];

  // anytime we find that our session is created with an available token
  // we open it on the spot
  if (self.session.state == FBSessionStateCreatedTokenLoaded && (![userInfo[FBSessionDidSetActiveSessionNotificationUserInfoIsOpening] isEqual:@YES])) {
    [self.session openWithBehavior:self.loginBehavior
                 completionHandler:self.sessionStateHandler];
  }
}

- (void)unwireViewForSession {
  // this line of code is the main reason we need to hold on
  // to the session object at all
  [self.session removeObserver:self
                    forKeyPath:@"state"];
  self.session = nil;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
  if (self.session.isOpen) {
    [self fetchMeInfo];
    [self configureViewForStateLoggedIn:YES];
  } else {
    [self configureViewForStateLoggedIn:NO];
  }
  [self informDelegate:NO];
}

- (void)handleActiveSessionSetNotifications:(NSNotification *)notification {
  // NSNotificationCenter is a global channel, so we guard against
  // unexpected uses of this notification the best we can
  if ([notification.object isKindOfClass:[FBSession class]]) {
    [self wireViewForSession:notification.object userInfo:notification.userInfo];
  }
}

- (void)handleActiveSessionUnsetNotifications:(NSNotification *)notification {
  [self unwireViewForSession];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
  if (buttonIndex == 0) { // logout
    [FBSession.activeSession closeAndClearTokenInformation];
  }
}

- (void)buttonPressed:(id)sender {
  if (self.session == FBSession.activeSession) {
    BOOL loggingInLogFlag = NO;
    if (!self.session.isOpen) {
      [self openSession];
      loggingInLogFlag = YES;
    } else { // logout action sheet
      NSString *name = self.user.name;
      NSString *title = nil;
      if (name) {
        title = [NSString stringWithFormat:[FBUtility localizedStringForKey:@"FBLV:LoggedInAs"
                                                                withDefault:@"Logged in as %@"], name];
      } else {
        title = [self titleText];
      }
      NSString *cancelTitle = [FBUtility localizedStringForKey:@"FBLV:CancelAction"
                                                   withDefault:@"Cancel"];
      NSString *logOutTitle = [FBUtility localizedStringForKey:@"FBLV:LogOutAction"
                                                   withDefault:@"Log out"];
      UIActionSheet *sheet = [[[UIActionSheet alloc] initWithTitle:title
                                                          delegate:self
                                                 cancelButtonTitle:cancelTitle
                                            destructiveButtonTitle:logOutTitle
                                                 otherButtonTitles:nil]
                              autorelease];
      // Show the sheet
      [sheet showInView:self];
    }

    [FBAppEvents logImplicitEvent:[self logLoginEventName]
                       valueToSum:nil
                       parameters:@{ @"logging_in" : [NSNumber numberWithBool:loggingInLogFlag] }
                          session:nil];
  } else { // state of view out of sync with active session
    // so resync
    [self unwireViewForSession];
    [self wireViewForSession:FBSession.activeSession userInfo:nil];
    [self configureViewForStateLoggedIn:self.session.isOpen];
    [self informDelegate:NO];
  }
}

// Hooks for subclasses
- (void)initializeButton {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

- (void)handleButtonPressedWithActiveSession {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

- (NSString *)logLoginEventName {
  @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                 reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                               userInfo:nil];
}

@end

@implementation FBAbstractShadowLabel

- (void)drawTextInRect:(CGRect)rect {
  CGSize myShadowOffset = CGSizeMake(0, -1);
  CGFloat myColorValues[] = {0, 0, 0, .3};

  CGContextRef myContext = UIGraphicsGetCurrentContext();
  CGContextSaveGState(myContext);

  CGColorSpaceRef myColorSpace = CGColorSpaceCreateDeviceRGB();
  CGColorRef myColor = CGColorCreate(myColorSpace, myColorValues);
  CGContextSetShadowWithColor (myContext, myShadowOffset, 1, myColor);

  [super drawTextInRect:rect];

  CGColorRelease(myColor);
  CGColorSpaceRelease(myColorSpace);

  CGContextRestoreGState(myContext);
}

@end


@interface FBLoginViewAnonymousButtonPNG : NSObject

+ (UIImage *)image;

@end

@interface FBLoginViewAnonymousButtonPressedPNG : NSObject

+ (UIImage *)image;

@end

static NSString *const FBAnonymousLoginViewCacheIdentity = @"FBAnonymousLoginView";

static CGSize g_buttonSize;

@implementation FBAnonymousLoginView

- (instancetype)init {
  self = [super init];
  if (self) {
    [self initialize];
  }
  return self;
}

- (instancetype)initWithFrame:(CGRect)aRect {
  self = [super initWithFrame:aRect];
  if (self) {
    [self initialize];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self initialize];
  }
  return self;
}

- (void)setDelegate:(id<FBAnonymousLoginViewDelegate>)newValue {
  if (self.delegate != newValue) {
    _delegate = newValue;

    // whenever the delegate value changes, we schedule one initial call to inform the delegate
    // of our current state; we use a delay in order to avoid a callback in a setup or init method
    [self performSelector:@selector(informDelegate:)
               withObject:nil
               afterDelay:.01];
  }
}

- (CGSize)buttonSize {
  return g_buttonSize;
}

- (void)setButtonSize:(CGSize)size {
  g_buttonSize = size;
}

- (UIImage *)buttonImage {
  return [FBLoginViewAnonymousButtonPNG image];
}

- (UIImage *)pressedButtonImage {
  return [FBLoginViewAnonymousButtonPressedPNG image];
}

- (NSString *)logInText {
  return [FBUtility localizedStringForKey:@"FBLV:AnonymousLogInButton" withDefault:@"Log in Anonymously"];
}

- (NSString *)logOutText {
  return [FBUtility localizedStringForKey:@"FBLV:AnonymousLogOutButton" withDefault:@"Log out"];
}

- (NSString *)titleText {
    if (self.session.accessTokenData.isAnonymous) {
        return [FBUtility localizedStringForKey:@"FBLV:LoggedInAnonymously"
                                    withDefault:@"Logged in Anonymously"];
    } else {
        return [FBUtility localizedStringForKey:@"FBLV:LoggedInUsingFacebook"
                                    withDefault:@"Logged in using Facebook"];
    }
}

- (void)informDelegateOfError:(NSError *)error {
  if (error) {
    if ([self.delegate respondsToSelector:@selector(anonymousLoginView:handleError:)]) {
      [self.delegate anonymousLoginView:self
                            handleError:error];
    }
  }
}

- (void)informDelegate:(BOOL)userOnly {
    if (FBSession.activeSession.isOpen) {
        if (self.session.accessTokenData.isAnonymous) {
            if (!userOnly && self.lastObservedLoginState != FBLastObservedLoginStateLoggedInAnonymously) {
                self.lastObservedLoginState = FBLastObservedLoginStateLoggedInAnonymously;
                if ([self.delegate respondsToSelector:@selector(anonymousLoginViewShowingLoggedInUser:)]) {
                    [self.delegate anonymousLoginViewShowingLoggedInUser:self];
                }
            }
        } else {
            // We are logged in with facebook
            if (userOnly) {
                if ([self.delegate respondsToSelector:@selector(anonymousLoginViewFetchedFacebookUserInfo:user:)]) {
                    [self.delegate anonymousLoginViewFetchedFacebookUserInfo:self
                                                                        user:self.user];
                }
            } else {
                if (self.lastObservedLoginState != FBLastObservedLoginStateLoggedInWithFacebook) {
                    self.lastObservedLoginState = FBLastObservedLoginStateLoggedInWithFacebook;
                    if ([self.delegate respondsToSelector:@selector(anonymousLoginViewShowingLoggedInWithFacebookUser:)]) {
                        [self.delegate anonymousLoginViewShowingLoggedInWithFacebookUser:self];
                    }
                }
                // any time we inform/reinform of isOpen event, we want to be sure
                // to repass the user if we have it
                if (self.user) {
                    [self informDelegate:YES];
                }
            }
        }
    } else if (self.lastObservedLoginState != FBLastObservedLoginStateLoggedOut) {
        self.lastObservedLoginState = FBLastObservedLoginStateLoggedOut;

        if ([self.delegate respondsToSelector:@selector(anonymousLoginViewShowingLoggedOutUser:)]) {
            [self.delegate anonymousLoginViewShowingLoggedOutUser:self];
        }
    }
}

- (void)openSession {
  [FBSession openActiveAnonymousSessionWithAllowLoginUI:YES
                                      completionHandler:self.sessionStateHandler];
}

- (NSString*)cacheIdentity {
  return FBAnonymousLoginViewCacheIdentity;
}

- (NSString *)logLoginEventName {
  return FBAppEventNameAnonymousLoginViewUsage;
}

@end

