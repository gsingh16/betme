//
//  betSelectionViewController.m
//  BetMe
//
//  Created by Admin on 9/7/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import "betSelectionViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Parse/Parse.h>
#import "betMeVideoServices.h"
#import "MainViewController.h"
#import "CameraViewController.h"
#import <ALMoviePlayerController.h>
#import "KOPopupView.h"
#import <QuartzCore/QuartzCore.h>
#import "SIAlertView.h"
#import "NZAlertView.h"
#import "YIPopupTextView.h"
#import "YISSTextView.h"
#import "Flurry.h"
#import "FlurryAds.h"

@interface betSelectionViewController () <UITableViewDataSource,
UITableViewDelegate, AKTagCellDelegate,AKTagsListViewDelegate,YIPopupTextViewDelegate>
{
    BOOL userSetDescription;
    YIPopupTextView* popupTextView;
}

@property (nonatomic, strong) KOPopupView *popup;
@property (strong, nonatomic) NSArray* selectedFriends;
@property (nonatomic,assign) NSInteger currentBetValueForParseObject;
@property(nonatomic,strong) NSString* currentBetTitleForParseObject;
@property(nonatomic,strong)NSString*currentVideoVimeoID;
@property(nonatomic,strong) NSString * opponentFirstName;
@property(nonatomic,strong) NSArray * opponentFBID;

@property(nonatomic,strong) NSString* currentUsersFirstName;
@property(nonatomic,strong) NSString* betTitle;
@property(nonatomic,strong) NSString* betDescription;
@property (nonatomic,assign) NSInteger currentBetValue;
@property(nonatomic,strong)NSMutableArray*tags;
@end

@implementation betSelectionViewController
@synthesize selectedFriends=_selectedFriends;
@synthesize currentBetValueForParseObject=_currentBetValueForParseObject;
//@synthesize currentBetDescriptionForParseObject=_currentBetDescriptionForParseObject;
//video nomenclature
@synthesize currentUsersFirstName=_currentUsersFirstName,betTitle=_betTitle,betDescription=_betDescription,tags=_tags,currentVideoVimeoID=_currentVideoVimeoID;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        

        // Custom initialization
        
        //set its delegate to self somewhere
        

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receivedNotification:)
                                                     name:@"recievedBetDescription"
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receivedNotification:)
                                                     name:@"videoUploadedToVimeo"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receivedNotification:)
                                                     name:@"videoUploadedToParse"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receivedNotification:)
                                                     name:@"camViewDismissed"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receivedNotification:)
                                                     name:@"beganEditingTagsView"
                                                   object:nil];
        


        
        //init tags array
        _tags=[[NSMutableArray alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //for navigational controller
    self.title = @"Start Video";
    
    
    
    PFQuery *query = [PFUser query];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        int videosUploadedToday= [[object valueForKeyPath:@"videosUploadedToday"]intValue];
        if (videosUploadedToday >= 2 ) {
            NSLog(@"reched video upload limit for the day :( with count %i",videosUploadedToday);
            //show that they have to wait until midnight
            
            //animate timer saying this much until you can upload again..

            //commenting out but should display pop-up saying you have a video uploading please wait
            SIAlertView*uploadLimitReachedAlert=[[SIAlertView alloc]initWithTitle:@"Daily Limit Reached" andMessage:@"You reached your daily limit of 2, please wait until after midnight, to take any more videos"];
            
            [uploadLimitReachedAlert addButtonWithTitle:@"Ok"
                                                   type:SIAlertViewButtonTypeDestructive
                                                handler:^(SIAlertView *alert) {
                                                    [self.navigationController popViewControllerAnimated:YES];
                                                    
                                                    NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
                                                    [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
                                                    
                                                    NSLog(@"should load ad");
                                                }];
            [uploadLimitReachedAlert show];
            
            return ;
            
        }else{
            
        
            NSLog(@"todays count is %i",videosUploadedToday);
        //check if video is in queu
        NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
        BOOL needsToUpload=[defaults boolForKey:@"needsToUpload"];
        
        if (needsToUpload) {
            NSLog(@"current video waiting in q");
            self.shouldUploadPopUpView.center = CGPointMake(self.view.frame.size.width/2.0,
                                                            self.view.frame.size.height/2.0);
            [self.navigationController.view addSubview:self.shouldUploadPopUpView];
            
            JCRBlurView*blur=[[JCRBlurView alloc]initWithFrame:self.view.frame];
            [blur addSubview:self.shouldUploadPopUpView];
            [self.view addSubview:blur];
            
            [self playTheSavedVideo];
            
            //hide the start button
           // self.startButton.hidden=YES;
        }else{
            //setup textViewDelegate for title
            _titleTextView.delegate=self;
            

            
            //set usersetdescription to no
            userSetDescription=NO;
            
            ////test
            BOOL editable = YES;
            
            popupTextView =
            [[YIPopupTextView alloc] initWithPlaceHolder:@"Enter Your Description:"
                                                maxCount:128
                                             buttonStyle:YIPopupTextViewButtonStyleRightCancel];
            popupTextView.delegate = self;
            popupTextView.caretShiftGestureEnabled = YES;       // default = NO. using YISwipeShiftCaret is recommended.
            popupTextView.text = @"";
            popupTextView.editable = editable;                  // set editable=NO to show without keyboard
            popupTextView.topUIBarMargin=80;
            popupTextView.bottomUIBarMargin=350;
            [popupTextView showInViewController:self];
            NSLog(@"the current frame and location is X: %f and y: %f",popupTextView.frame.origin.x,popupTextView.frame.origin.y);
            }
        }
    }];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"the view did appear in betselectionVC is");
    //if the blur is full, undo it; animated style
    
    //check if video is in queu
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    BOOL needsToUpload=[defaults boolForKey:@"needsToUpload"];
    
    //commenting out but should display pop-up saying you have a video uploading please wait

    
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell*)[tableView
                                               dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.textLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        cell.textLabel.lineBreakMode = UILayoutPriorityFittingSizeLevel;
        cell.textLabel.clipsToBounds = YES;
        
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        cell.detailTextLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:0.4
                                                         green:0.6
                                                          blue:0.8
                                                         alpha:1];
        cell.detailTextLabel.lineBreakMode = UILayoutPriorityFittingSizeLevel;
        cell.detailTextLabel.clipsToBounds = YES;
    }
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Pick A bet";
            cell.detailTextLabel.text = @"or make one";
            cell.imageView.image = [UIImage imageNamed:@"action-photo.png"];
            break;
            /*
             dont need now because of counter
        case 1:
            cell.textLabel.text = @"Set the stakes?";
            cell.detailTextLabel.text = @"easy picking";
            cell.imageView.image = [UIImage imageNamed:@"action-photo.png"];
            break;
            */
        case 1:
            cell.textLabel.text = @"Select Challenger?";
            cell.detailTextLabel.text = @"Select friends";
            cell.imageView.image = [UIImage imageNamed:@"action-people.png"];
            break;
            
            
        default:
            break;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - start recording
- (IBAction)startRecording:(id)sender {
    
    
    

    NSLog(@"helo this is where the start is, so you can use this to track the second one, then dismiss it, then replay ");
    

    
        if (userSetDescription==YES) {
            
            
            //check for any changes made in description for hashtags
            self.tags=[self checkForHashtagwithText:self.betDescription];
            
            CameraViewController*camVC=[[CameraViewController alloc]init];
            [self presentViewController:camVC animated:YES completion:Nil];
            
            //flurry track user wants to record video
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [[PFUser currentUser]username] , @"user",
                                           nil];
            [Flurry logEvent:@"userWantsToRecordVideo" withParameters:articleParams];
            [FBSDKAppEvents logEvent:@"userWantsToRecordVideo" parameters:articleParams];



            
        }else{
            UIAlertView*errorAlertView=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter a short description first." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [errorAlertView show];
        }

    
}

#pragma mark- notifications

- (void) receivedNotification:(NSNotification *) notification
{
    

    if ([[notification name] isEqualToString:@"recievedBetDescription"]){
        NSLog (@"Successfully received the bet description %@",(NSString *)[notification.object valueForKey:@"text"]);
        NSLog (@"Successfully received the object frame %@",[notification.object valueForKey:@"frame"]);
        
        //set bool here
        userSetDescription=YES;

        _betDescription=(NSString *)[notification.object valueForKey:@"text"];
        
        [self.view addSubview:self.startButton];
        
    }else if ([[notification name] isEqualToString:@"videoUploadedToVimeo"]){
        NSLog(@"Got the Uploaded notification with the url ref: %@",notification.object);
        _currentVideoVimeoID=notification.object;
        [self makeParseObject];
        
    }else if ([[notification name] isEqualToString:@"videoUploadedToParse"]){

        NSLog(@"uploaded to parse!");
        
    }
    else if ([[notification name] isEqualToString:@"camViewDismissed"]){
        //[self.navigationController popViewControllerAnimated:YES];
        NSLog(@"recieved the  camViewDismissed notification ");
        //show the popup which gives yes or no option
        
        
        [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            self.shouldUploadPopUpView.center = CGPointMake(self.view.frame.size.width/2.0,
                                                            self.view.frame.size.height/2.0);
            
        } completion:nil];
        

        
        
        
        [self.navigationController.view addSubview:self.shouldUploadPopUpView];
        
        //store values for nsuserdefaults
        NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
        [defaults setValue:_betDescription forKey:@"currentVideoDescription"];
        [defaults setObject:self.tags forKey:@"currentVideoTags"];
        [defaults synchronize];
        
        NSLog(@"the tags are %@",self.tags);
        //play
        
        JCRBlurView*blur=[[JCRBlurView alloc]initWithFrame:self.view.frame];
        [blur addSubview:self.shouldUploadPopUpView];
        [self.view addSubview:blur];
        
        [self playTheSavedVideo];


    }else if ([[notification name]isEqualToString:@"beganEditingTagsView"]) {
        NSLog(@"recieved the  beganEditingTagsView notification in bet selection vc ");
        
    }
}

#pragma rotation for popup
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self.popup willRotateToOrientation:toInterfaceOrientation withDuration:duration];
}

#pragma mark - play video
- (void)playTheSavedVideo{
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    NSURL*currentVideoUrl=[defaults URLForKey:@"currentVideoUrl"];
    NSLog(@"the currentVideoUrl in the play method is %@",currentVideoUrl);
    
    

    // create a movie player
    self.moviePlayer = [[ALMoviePlayerController alloc]initWithContentURL:currentVideoUrl];
    self.moviePlayer.delegate = self; //IMPORTANT!
    
    
    // create the controls
    ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:self.moviePlayer style:ALMoviePlayerControlsStyleNone];
    
    
    // optionally customize the controls here...
    
    [movieControls setBarColor:[UIColor colorWithRed:195/255.0 green:29/255.0 blue:29/255.0 alpha:0.5]];
    [movieControls setTimeRemainingDecrements:NO];
    [movieControls setFadeDelay:0.1];
    [movieControls setBarHeight:1.0f];
    [movieControls setSeekRate:0.f];
    
    
    //frame
    [self.moviePlayer setFrame:self.videoView.frame];
    
    // assign the controls to the  player
    [self.moviePlayer setControls:movieControls];

    
    //set content
    [self.moviePlayer setContentURL:currentVideoUrl];

    //stretch video
    self.moviePlayer.view.clipsToBounds = YES;
    [self.moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
    
    //replay
    self.moviePlayer.repeatMode = MPMovieRepeatModeOne;

    // add movie player to your view
    [self.shouldUploadPopUpView addSubview:self.moviePlayer.view];
    


}


#pragma mark - Delegate Method



#pragma mark - UITextViewDelegates
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"textViewDidBeginEditing:");
    _titleTextView.backgroundColor = [UIColor greenColor];
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    NSLog(@"textViewShouldEndEditing:");
    _titleTextView.backgroundColor = [UIColor whiteColor];
    //update for parse obj
    [[NSNotificationCenter defaultCenter]postNotificationName:@"recievedBetDescription" object:textView];
    self.tags=[self checkForHashtagwithText:textView.text];
    

    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"textViewDidEndEditing:");

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    if (textView.text.length + text.length > 128){
        if (location != NSNotFound){
            [textView resignFirstResponder];
            NSLog(@"exceeded 128 char");
        }
        return NO;
    }
    else if (location != NSNotFound){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    NSLog(@"textViewDidChange: with the length %i",textView.text.length);
    
    if (textView.text.length > 128){
        NSLog(@"exceeded the length");
    }
    
    
}
- (void)textViewDidChangeSelection:(UITextView *)textView{
    NSLog(@"textViewDidChangeSelection:");
}

#pragma mark - Check for hashtags 

- (NSMutableArray*) checkForHashtagwithText:(NSString*)text{
   NSMutableArray*hashtags=[[NSMutableArray alloc]init];
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)];
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:1];
        NSString* word = [text substringWithRange:wordRange];
        NSLog(@"Found tag %@", word);
        [hashtags addObject:word];
    }
    
    return hashtags;
    
    /*
    hashtag = YES;
    
    if (mention &amp; hashtag &amp; comment) {
        complete = YES;
    }
     */
    
}

#pragma mark - YIPopupTextView delegate

- (void)popupTextView:(YIPopupTextView*)textView willDismissWithText:(NSString*)text cancelled:(BOOL)cancelled{
    if (cancelled==YES) {
        NSLog(@"the description was cancelled should clear");
    }else{
        NSLog(@"the description was accepted");

        self.startButton.hidden=NO;
    }
    
}
- (void)popupTextView:(YIPopupTextView*)textView didDismissWithText:(NSString*)text cancelled:(BOOL)cancelled{
    
    
}


#pragma mark - Save to Parse
-(void)makeParseObject{
    
  
    NSString*title=[NSString stringWithFormat:@"%@ vs %@ : %@",_currentUsersFirstName,_opponentFirstName,_betTitle];
    NSString*tags=[_tags componentsJoinedByString:@","];
    NSString*vimeoUrl=[NSString stringWithFormat:@"http://player.vimeo.com/video/%@",_currentVideoVimeoID];
 // Properly makes bet
   
 PFObject *newPost=[[PFObject alloc]initWithClassName:@"bets"];
 [newPost setObject:title forKey:@"betTitle"];
 [newPost setObject:[PFUser currentUser] forKey:@"fromUser"];
 [newPost setObject: vimeoUrl forKey:@"vimeoUrl"];
 [newPost setObject: tags forKey:@"ampersandString"];
}




#pragma mark - Closing / house-cleaning
-(void)viewDidDisappear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}


#pragma mark - upload and discard

- (IBAction)uploadVideo:(id)sender {
    NSLog(@"should UploadVideo");
    
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Confirm" andMessage:@"Are you sure you would like to upload? "];
    //yes
    [alertView addButtonWithTitle:@"Yes"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button1 Clicked");
                              //yes
                              [self.moviePlayer stop];
                              self.moviePlayer=nil;
                              [self.shouldUploadPopUpView removeFromSuperview];
                              
                              betMeVideoServices*videoService=[[betMeVideoServices alloc]init];
                              //should be from nsuserdefaults
                              
                              NSLog(@"the tags in the upload video are %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentVideoTags"]);
                              
                              videoService.descriptionForVideo=[[NSUserDefaults standardUserDefaults]valueForKey:@"currentVideoDescription"];
                              videoService.keyWordsForVideo=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentVideoTags"];
                              [videoService upload];
                              
                              [self.navigationController popViewControllerAnimated:YES];
                              
                              
                          }];
    
    [alertView addButtonWithTitle:@"No"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button2 Clicked");
                              
                          }];

    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    


}

- (IBAction)discardVideo:(id)sender {
    NSLog(@"should discard video");
    
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Confirm" andMessage:@"Are you sure you would like to discard this video?? "];
    //yes
    [alertView addButtonWithTitle:@"Yes"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {

    //nsuserDefaults cleanup
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    
    //clean description and tags
    [defaults setValue:nil forKey:@"currentVideoDescription"];
    [defaults setObject:nil forKey:@"currentVideoTags"];
    
    //Remove any prevouis videos at that path
    [[NSFileManager defaultManager]  removeItemAtURL:[defaults URLForKey:@"currentVideoUrl"] error:nil];
    //allow user to upload again by resetting needsToUpload
    [defaults setBool:NO forKey:@"needsToUpload"];
    
    [defaults synchronize];
    
    [self.moviePlayer stop];
    self.moviePlayer=nil;
    [self.shouldUploadPopUpView removeFromSuperview];
                              
                              SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Video Discarded" andMessage:@"You lost 300 points!  "];
                              //yes
                              alertView.messageColor=[UIColor redColor];
                              
                              [alertView addButtonWithTitle:@"Ok"
                                                       type:SIAlertViewButtonTypeDefault
                                                    handler:^(SIAlertView *alert) {
                                                        NSLog(@"Button1 Clicked");
                                                        //deduct points and deduct from daily limit
                                                        
                                                        PFQuery *query = [PFUser query];
                                                        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                                                            
                                                            if (error) {
                                                                NSLog(@"error fetching user's current points in the betselection vc %@",error);
                                                                [self.navigationController popViewControllerAnimated:YES];
                                                            }else{
                                                        int currentPoints=[[[PFUser currentUser]objectForKey:@"points"]intValue];
                                                        int pointsToBeSubtracted=300;
                                                        int newValueForPoints=currentPoints-pointsToBeSubtracted;
                                                        [[PFUser currentUser]setObject:[NSNumber numberWithInt:newValueForPoints] forKey:@"points"];
                                                        [[PFUser currentUser]incrementKey:@"videosUploadedToday" byAmount:[NSNumber numberWithInt:1]];
                                                        
                                                        [[PFUser currentUser]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                                            if (!error) {
                                                                NSLog(@"updated users new points completely for discard");
                                                                [self.navigationController popViewControllerAnimated:YES];
                                                                
                                                                //flurry track user discarded Video
                                                                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                                               [[PFUser currentUser]username] , @"user",
                                                                                               nil];
                                                                [Flurry logEvent:@"userDiscardedVideo" withParameters:articleParams];
                                                                [FBSDKAppEvents logEvent:@"userDiscardedVideo" parameters:articleParams];

                                                               // NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
                                                               // [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
                                                            }else{
                                                                NSLog(@"the error in updating the users points for discard is %@,",error);
                                                                [Flurry logError:@"errorUpdatingPointsForDiscard" message:error.description error:error];
                                                                [FBSDKAppEvents logEvent:@"errorUpdatingPointsForDiscard" parameters:@{@"error: ":error}];
                                                            }
                                                            
                                                        }];
                                                            }}];
  
                                                    }];
                              [alertView addButtonWithTitle:@"Invite friends to earn 150 points!" type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alertView) {
                                  NSLog(@"Button 2 clicked");
                                  
                                  FBSDKAppInviteContent * content =[[FBSDKAppInviteContent alloc] initWithAppLinkURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/bet-me/id914313598?mt=8"]];
                                  //optionally set previewImageURL
                                  content.previewImageURL = [NSURL URLWithString:@"https://s3.amazonaws.com/webresources.binarybros.betme/BimmeTheBunnyLarge1.jpg"];
                                  
                                  // present the dialog. Assumes self implements protocol `FBSDKAppInviteDialogDelegate`
                                  [FBSDKAppInviteDialog showWithContent:content
                                                               delegate:self];
                                  
                              }];
                              alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                              [alertView show];
                              
                          }];
    [alertView addButtonWithTitle:@"No"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button2 Clicked");
                              
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    
}


#pragma mark- app invite delegate
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results{
    NSLog(@"User did complete app invite with result %@",results);
    //should reward user
    
    
    PFQuery *query = [PFUser query];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        if (error) {
            NSLog(@"error fetching user's current points in the betmeVideoServices %@",error);
        }else{
            
            int currentPoints= [[object valueForKeyPath:@"points"]intValue];
            int pointsToBeAdded=150;
            
            [[PFUser currentUser]incrementKey:@"points" byAmount:[NSNumber numberWithInt:pointsToBeAdded]];
            
            
            [[PFUser currentUser]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    NSLog(@"updated users new points completely");
                    //flurry track user upload Video
                    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [[PFUser currentUser]username] , @"user",results,@"result",
                                                   nil];
                    [Flurry logEvent:@"userCompletedAppInvite" withParameters:articleParams];
                    [FBSDKAppEvents logEvent:@"userCompletedAppInvite" parameters:articleParams];
                    
                    
                }else{
                    NSLog(@"the error in updating the users points for upload is %@,",error);
                    [Flurry logError:@"errorUpdatingPointsForAppInvite" message:error.description error:error];
                    [FBSDKAppEvents logEvent:@"errorUpdatingPointsForAppInvite" parameters:@{@"error:":error}];
                    
                    
                }
            }];
            
        }}];
    
    
    
    
    
}


- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error{
    NSLog(@"User failed to  complete app invite with error %@",error);
    
    [Flurry logError:@"userFailedToCompleteAppInvite" message:error.description error:error];
    [FBSDKAppEvents logEvent:@"userFailedToCompleteAppInvite" parameters:@{@"error:":error}];

    
}
@end
