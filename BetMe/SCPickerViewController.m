// Copyright (c) 2014-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "SCPickerViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <FBSDKLoginKit/FBSDKLoginKit.h>

@implementation SCPickerViewController
{
    NSArray *_results;
    NSArray *_selectedFriends;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSBundle mainBundle] loadNibNamed:@"SCPickerViewController" owner:self options:nil];
    self.tableView.allowsMultipleSelection = self.allowsMultipleSelection;
    
    self.tableView.delegate=self;
    
    self.tableView.tintColor =  [UIColor blackColor];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.requiredPermission && ![[FBSDKAccessToken currentAccessToken] hasGranted:self.requiredPermission])
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[self.requiredPermission]
                                handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if ([result.grantedPermissions containsObject:self.requiredPermission]) {
                [self fetchData];
            } else {
                [self dismissViewControllerAnimated:YES completion:NULL];
            }
        }];
    } else {
        [self fetchData];
    }
}

- (void)fetchData
{
    [self.request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (error) {
            NSLog(@"Picker loading error:%@", error);
            if (!error.userInfo[FBSDKErrorLocalizedDescriptionKey]) {
                [[[UIAlertView alloc] initWithTitle:@"Oops"
                                            message:@"There was a problem fetching the list"
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            _results = result[@"data"];
            [self.tableView reloadData];
        }
    }];
}
- (NSArray *)selection {
    NSMutableArray *result = [NSMutableArray array];
    for (NSIndexPath *index in self.tableView.indexPathsForSelectedRows) {
        [result addObject: @{
                             @"id" : _results[index.row][@"id"],
                             @"name" : _results[index.row][@"name"]
                             }];
        [self.friendResults addObject: @{
                                         @"id" : _results[index.row][@"id"],
                                         @"name" : _results[index.row][@"name"]
                                         }];
    }

    return result;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"the results counts is %lu",(unsigned long)_results.count);
    return _results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    
    UIImageView*checkImage=[[UIImageView alloc]initWithFrame:CGRectMake(280, cell.frame.origin.y+10, 25, 25)];
    [checkImage setImage:[UIImage imageNamed:@"checkSign"]];
    
    [cell addSubview:checkImage];
    [checkImage setHidden:YES];

    cell.textLabel.text = _results[indexPath.row][@"name"];
    NSString *pictureURL = _results[indexPath.row][@"picture"][@"data"][@"url"];
    if (pictureURL) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSData *image = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:pictureURL]];

            //this will set the image when loading is finished
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = [UIImage imageWithData:image];
                [cell setNeedsLayout];
            });
        });
    }
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    //[[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor whiteColor]];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [[cell.subviews objectAtIndex:1] setHidden:YES];

    

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    
    //[[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor orangeColor]];

    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [[cell.subviews objectAtIndex:1] setHidden:NO];


    
}



- (IBAction)done:(id)sender {
    
    _selectedFriends = self.selection;
    NSString *subtitle = nil;
    if (_selectedFriends.count == 1) {
        subtitle = self.selection[0][@"name"];
    } else if (_selectedFriends.count == 2) {
        subtitle = [NSString stringWithFormat:@"%@ and %@", self.selection[0][@"name"], self.selection[1][@"name"]];
    } else if (_selectedFriends.count > 2) {
        subtitle = [NSString stringWithFormat:@"%@ and %lu others", self.selection[0][@"name"], (unsigned long) (_selectedFriends.count - 1)];
    } else if (_selectedFriends == 0) {
        subtitle = nil;
        _selectedFriends = nil;
    }
    NSLog(@"the friend list is %@",subtitle);
    //NSLog(@"the fbId's for parse are %@",[self.selection valueForKey:@"id"]);
}

- (IBAction)cancel:(id)sender {
}
@end
