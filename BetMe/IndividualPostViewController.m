//
//  IndividualPostViewController.m
//  BetMe
//
//  Created by Admin on 5/27/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import "IndividualPostViewController.h"
#import "menuBarView.h"
#import "homeViewController.h"
#import "MainViewController.h"
#import "FlipsideViewController.h"
#import "betViewController1.h"
#import "profileViewController.h"
#import "couponViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "betSelectionViewController.h"
#import "VideoPostTableViewCell.h"
#import "FBPictureHelper.h"
#import "PAImageView.h"
#import "CameraViewController.h"
#import "SIAlertView.h"
//test
#import "TGLViewController.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"

#import "parseQueryTableViewController.h"
#import "MyActivityIndicator.h"

//data chaching
#import "APSmartStorage.h"
#import <NSString+MD5.h>
#import "APFileStorage.h"
#import "NSFileManager+Storage.h"


#import "SettingsViewController.h"
#import "TutorialViewController.h"

//flurry
#import "Flurry.h"
#import "FlurryAds.h"

// fb
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface IndividualPostViewController () <UIAlertViewDelegate>{
    int viewCount;
}

@end

@implementation IndividualPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //observers for notifications
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"profilePictureSelected"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"tagSelected"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MPMoviePlayerPlaybackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"userWantsToSharePost"
                                               object:nil];
    
    //set max count for memory
    // APSmartStorage.sharedInstance.maxObjectCount = 10;
    
       //setup view
    
    
    //thumbnail
    [self.thumbnailImageView sd_setImageWithURL:[[self.postObject objectForKey:@"videoThumbnailImage"]valueForKey:@"url"]];
    
    //name
    NSLog(@"the object so far is %@",self.postObject);
    [self.userNameLabel setText:[[[self.postObject objectForKey:@"fromUser"]fetchIfNeeded]objectForKey:@"displayName"] ];
    
    //date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSString*postDate= [dateFormat stringFromDate:[self.postObject valueForKey:@"createdAt"]];
    NSLog(@"the date of this post is %@",postDate);
    
    [self.dateLabel setText:postDate];
    
    //description
    [self.descriptionTextView setText:[self.postObject objectForKey:@"description"]];
    
    //profile imageview
    NSURL* fbProfileImageURL=[[FBPictureHelper urlForProfilePictureSizeLargeForFBID:[[[self.postObject  objectForKey:@"fromUser"]fetchIfNeeded]objectForKey:@"facebookId"]]absoluteURL];
    
    [self.profilePictureImageView sd_setImageWithURL:fbProfileImageURL];
    //new
    [self.profilePictureImageView sd_setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];
    self.profilePictureImageView.layer.cornerRadius = 27.5;
    self.profilePictureImageView.layer.masksToBounds = YES;
    self.profilePictureImageView.layer.borderWidth = 1;
    self.profilePictureImageView.layer.borderColor = [[UIColor orangeColor] CGColor];
    

    
    
    //start loading indicator
    
    [self.spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:self.spinner]; // spinner is not visible until started
    
    [self.spinner startAnimating];
    
    
    BOOL isLikedByUser = [[self.postObject objectForKey:@"likes"] containsObject:[PFUser currentUser].objectId];
    NSLog(@"the post has been like by the user: %d",isLikedByUser);
    
    for (NSString* item in [self.postObject objectForKey:@"likes"])
    {
        if ([item rangeOfString:[PFUser currentUser].objectId].location != NSNotFound){
            NSLog(@"found object in likes array");
            [self.likeButton setTitle:@"Liked👌" forState:UIControlStateNormal];
            [self.likeButton setUserInteractionEnabled:NO];
            
        }else{
            NSLog(@"did not find object in likes array");
            [self.likeButton setTitle:@"Like" forState:UIControlStateNormal];
            [self.likeButton setUserInteractionEnabled:YES];
            
        }
    }
    
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    //[_moviePlayer.view.layer setZPosition:0];
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    //play movie over the thumbnail
    
    NSLog(@"the video url from the post object is %@",[[self.postObject objectForKey:@"videoFile"]valueForKey:@"url"]);
    NSString*currentCellVideoUrl=[[self.postObject objectForKey:@"videoFile"]valueForKey:@"url"];
    self.videoURL=currentCellVideoUrl;
    // allocate controller
    _moviePlayer = [[PBJVideoPlayerController alloc] init];
    _moviePlayer.delegate = self;
    _moviePlayer.view.frame = self.thumbnailImageView.frame;
    
    // setup media
    _moviePlayer.videoPath = currentCellVideoUrl;
    
    // present
    [self addChildViewController:_moviePlayer];
    [self.view addSubview:_moviePlayer.view];
    [_moviePlayer didMoveToParentViewController:self];
    
    [_moviePlayer playFromBeginning];
    [_moviePlayer setPlaybackLoops:YES];

    

}

#pragma mark- mpmovieplayer/almovieplayer notifications
- (void)MPMoviePlayerPlaybackStateDidChange:(NSNotification *)notification
{
    //NSLog(@"the playback state is %ld",self.moviePlayer.playbackState);
    //are we currently playing?
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying)
    { //yes->do something as we are playing...
        //stop animating spinner
        [self.spinner stopAnimating];
        NSLog(@"started playing");
    }
    else if (self.moviePlayer.playbackState == 5 )
    {
        
        NSLog(@"videoDidEnd with id %@",self.moviePlayer.postObjectID);
        
        //flurry track user discarded Video
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",self.moviePlayer.postObjectID,@"videoID",
                                       nil];
        [Flurry logEvent:@"userViewedVideo" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userViewedVideo" parameters:articleParams];
        
        viewCount++;
        if (viewCount>6) {
            NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
            [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
            NSLog(@"should load ad");
            viewCount=0;
        }
        
        //log view for parse
        [self.moviePlayer.postObject incrementKey:@"views" byAmount:[NSNumber numberWithInt:1]];
        [self.moviePlayer.postObject saveInBackground];
        
    }
}




#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}



#pragma mark - PBJVideoPlayerControllerDelegate

- (void)videoPlayerReady:(PBJVideoPlayerController *)videoPlayer
{
    NSLog(@"Max duration of the video: %f", videoPlayer.maxDuration);
}

- (void)videoPlayerPlaybackStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
    NSLog(@"the videoplayer state did change %ld",(long)videoPlayer.playbackState);
    
}

- (void)videoPlayerPlaybackWillStartFromBeginning:(PBJVideoPlayerController *)videoPlayer
{
    /*
     dont need because im not using the play button
     _playButton.alpha = 1.0f;
     _playButton.hidden = NO;
     
     [UIView animateWithDuration:0.1f animations:^{
     _playButton.alpha = 0.0f;
     } completion:^(BOOL finished) {
     _playButton.hidden = YES;
     }];
     */
    
    NSLog(@"the videoplayer will start from begining %@",videoPlayer);

}

- (void)videoPlayerPlaybackDidEnd:(PBJVideoPlayerController *)videoPlayer
{
    /*
     dont need because im not using the play button
     
     _playButton.hidden = NO;
     
     [UIView animateWithDuration:0.1f animations:^{
     _playButton.alpha = 1.0f;
     } completion:^(BOOL finished) {
     }];
     */
    
    //end all subviews
    NSArray *viewsToRemove = [self.moviePlayer.view subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    
    NSLog(@"videoDidEnd with id %@",videoPlayer.postObjectID);
    
    //flurry track user discarded Video
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",videoPlayer.postObjectID,@"videoID",
                                   nil];
    [Flurry logEvent:@"userViewedVideo" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userViewedVideo" parameters:articleParams];
    
    
    viewCount++;
    if (viewCount>6) {
        NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
        [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
        NSLog(@"should load ad");
        viewCount=0;
    }
    
    //log view for parse
    [videoPlayer.postObject incrementKey:@"views" byAmount:[NSNumber numberWithInt:1]];
    [videoPlayer.postObject saveInBackground];
    
    
    
    
}


#pragma mark - button actions

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)moreOptions:(id)sender {

    NSString *destructiveTitle = @"Report/Flag this post"; //Action Sheet Button Titles
    NSString *deletePostButton = @"Delete Post";//if from user
    //NSString *other1 = @"Other Button 1";
    //NSString *other2 = @"Other Button 2";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:destructiveTitle
                                  otherButtonTitles:deletePostButton, nil];
    actionSheet.tag=1;
    [actionSheet showInView:self.view];
    
    
    
    [FBSDKAppEvents logEvent:@"User wants to see more options"];
    [Flurry logEvent:@"User wants to see more options"];

    
    
}

- (IBAction)share:(id)sender {
    
    //old 11 29 14
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:nil];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to share"];
        [Flurry logEvent:@"User wants to share"];
        
        
        NSString *other1 = @"Share on Facebook";
        NSString *other2 = @"Share on Twitter";
        NSString *other3 = @"Share via Facebook Messenger";
        NSString *other4 = @"Share via E-mail";
        NSString *other5 = @"Copy URL";
        
        //e-mail crashes
        
        NSString *cancelTitle = @"Cancel";
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil
                                      delegate:self
                                      cancelButtonTitle:cancelTitle
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:other1,other2,other3,other5, nil];
        actionSheet.tag=2;
        [actionSheet showInView:self.view];
        
        NSLog(@"user wants to share bear ");
    }
    
    

}

- (IBAction)comment:(id)sender {
    
    MessageViewController*SlkTVC=[[MessageViewController alloc]init];
    SlkTVC.postObject=self.postObject;
    
    NSMutableArray *allObjects = [NSMutableArray array];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"Comments"];
    [query whereKey:@"postID" equalTo:self.postObject];
    [query includeKey:@"fromUser"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded. Add the returned objects to allObjects
            [allObjects addObjectsFromArray:objects];
            //NSLog(@"the object comming in for the comments is %@",objects);
            dispatch_semaphore_signal(sema);
            while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
                [[NSRunLoop currentRunLoop]
                 runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0]];
            }
            SlkTVC.commentObjects=allObjects;
            [self presentViewController:SlkTVC animated:YES completion:nil];
            
            //
        }else{
            NSLog(@"error in query for comments retrieval");
        }}];
    
    
}

- (IBAction)like:(id)sender {
}

- (IBAction)userClickedProfile:(id)sender {
}


#pragma mark- ui actionsheet delegate

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"im recieving the notice that this popup sheet %@ index at %i was selected",popup,buttonIndex);
    
    //for sharing vs more options
    if (popup.tag==1)
    {
        
            switch (buttonIndex)
            {
                case 0:
                {
                    //[self FBShare];
                    NSLog(@"report/flag this post");
                    [self reportPost];
                    
                }
                    break;
                case 1:
                    //[self TwitterShare];
                    NSLog(@"user selected delete post");
                    [self userSelectedDeletePost];
                    break;
                case 2:
                    //[self emailContent];
                    NSLog(@"user selected cancel");
                    break;
                case 3:
                    //[self saveContent];
                    NSLog(@"save content");
                    break;
                case 4:
                    //[self rateAppYes];
                    NSLog(@"rate the app");
                    break;
                default:
                    break;
                    
            }
            
        
        
    }else if (popup.tag==2)
    {//for sharing
        
        
        
        switch (buttonIndex)
        {
            case 0:
            {
                NSLog(@"fb share");
                [self shareOnFacebook];
                [FBSDKAppEvents logEvent:@"User wants to see share on facebook"];
                [Flurry logEvent:@"User wants to share on facebook"];
            }
                break;
            case 1:
            {
                NSLog(@"twitter share");
                [self shareOnTwitter];
                [FBSDKAppEvents logEvent:@"User wants to share on twitter"];
                [Flurry logEvent:@"User wants to share on twitter"];
            }
                break;
            case 2:
            {
                NSLog(@"fbMessenger share");
                [self shareOnFacebookMessenger];
                [FBSDKAppEvents logEvent:@"User wants to share on fbMessenger"];
                [Flurry logEvent:@"User wants to share on fbMessenger"];
            }
                break;
                
            case 3:
            {
                NSLog(@"copy url");
                [self copyVideoUrl];
                [FBSDKAppEvents logEvent:@"User copied the video url"];
                [Flurry logEvent:@"User copied the video url"];
            }
                break;
            case 4://e-mail share should be 4, but buggy atm; so it is cancel
            {
                //NSLog(@"e-mail share");
                //[self shareViaEmail];
                [FBSDKAppEvents logEvent:@"User hit cancel on sharing"];
                [Flurry logEvent:@"User hit cancel on sharing"];
            }
                break;
                
            default:
                break;
                
        }
        
    }
    
    
    
}

#pragma mark- report post
-(void)reportPost{
    IQFeedbackView *feedback = [[IQFeedbackView alloc] initWithTitle:@"Flag/Report" message:nil image:nil cancelButtonTitle:@"Cancel" doneButtonTitle:@"Send"];
    [feedback setCanAddImage:NO];
    [feedback setCanEditText:YES];
    
    [feedback showInViewController:self completionHandler:^(BOOL isCancel, NSString *message, UIImage *image) {
        NSLog(@"The message is %@ with length %lu and isCancel %hhd",message,(unsigned long)[message length],isCancel);
        
        if (isCancel==0 && [message length]>0 ) {
            
            PFObject*feedbackPost=[PFObject objectWithClassName:@"flaggedCuts"];
            [feedbackPost setObject:[PFUser currentUser] forKey:@"fromUser"];
            [feedbackPost setValue:message forKey:@"message"];
            [feedbackPost setObject:[PFObject objectWithoutDataWithClassName:@"Cuts" objectId:self.postObject.objectId] forKey:@"fromPost"];
            
            
            [feedbackPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    //alert thanks
                    
                    [FBSDKAppEvents logEvent:@"User reoported post" parameters:@{@"user: ":[PFUser currentUser],@"postID: ":self.postObject.objectId}];
                    [Flurry logEvent:@"User reported post" withParameters:@{@"user: ":[PFUser currentUser],@"postID: ":self.postObject.objectId}];
                    
                    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Success" andMessage:@"Thank you for your report! "];
                    alertView.messageColor=[UIColor redColor];
                    //yes
                    [alertView addButtonWithTitle:@"Ok"
                                             type:SIAlertViewButtonTypeDefault
                                          handler:^(SIAlertView *alert)
                     {
                         
                     }];
                    
                    [alertView show];
                    
                }else{
                    NSLog(@"the error in saving the feedback post is %@",error);
                    //analytics
                    [FBSDKAppEvents logEvent:@"error with reoporting post" parameters:@{@"user: ":[PFUser currentUser],@"postID: ":self.postObject.objectId}];
                    [Flurry logEvent:@"error with reported post" withParameters:@{@"user: ":[PFUser currentUser],@"postID: ":self.postObject.objectId,@"error: ":error}];
                    
                    
                }
            }];
            
        }else
        {
            NSLog(@"the user cancelled feedback post");
            [FBSDKAppEvents logEvent:@"User cancelled reoported post" parameters:@{@"user: ":[PFUser currentUser],@"postID: ":self.postObject.objectId}];
            [Flurry logEvent:@"User cancelled reported post" withParameters:@{@"user: ":[PFUser currentUser],@"postID: ":self.postObject.objectId}];
        }
        
        
        [feedback dismiss];
    }];
}

#pragma mark- delete post

-(void)userSelectedDeletePost{
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Confirm" andMessage:@"Are you sure you would like to discard this video? You will lose 300 points "];
    //yes
    [alertView addButtonWithTitle:@"Yes"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              
                              
                              PFObject*thisPost=[[PFObject alloc]initWithClassName:@"Cuts"];
                              thisPost.objectId=self.postObject.objectId;
                              [thisPost deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                  if (!error) {
                                      NSLog(@"succesfully deleted object SHOULD ALSO DELETE POINTS 300");
                                      
                                      int currentPoints=[[[PFUser currentUser]objectForKey:@"points"]intValue];
                                      int pointsToBeSubtracted=300;
                                      int newValueForPoints=currentPoints-pointsToBeSubtracted;
                                      [[PFUser currentUser]setObject:[NSNumber numberWithInt:newValueForPoints] forKey:@"points"];
                                      
                                      [[PFUser currentUser]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                          if (!error) {
                                              NSLog(@"updated users new points completely for discard");
                                              
                                              SIAlertView *didDeletePost = [[SIAlertView alloc] initWithTitle:@"OMG" andMessage:@"This Post Has Been Deleted! "];
                                              [didDeletePost addButtonWithTitle:@"Ok" type:SIAlertViewButtonTypeDestructive handler:nil];
                                              [didDeletePost show];
                                              
                                              
                                              //flurry track user delete post
                                              NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                             [[PFUser currentUser]username] , @"user",self.postObject.objectId,@"postObjectID",
                                                                             nil];
                                              [Flurry logEvent:@"userDeletedPost" withParameters:articleParams];
                                              [FBSDKAppEvents logEvent:@"userDeletedPost" parameters:articleParams];
                                              
                                              
                                              
                                          }else
                                              NSLog(@"the error in updating the users points for discard is %@,",error);
                                          [Flurry logError:@"errorUpdatingUserPoints" message:error.description error:error];
                                          [FBSDKAppEvents logEvent:@"errorUpdatingUserPoints" parameters:@{@"error: ":error}];
                                          
                                      }];
                                      
                                      
                                  }else
                                      NSLog(@"the error in trying to delete the object is %@",error);
                                  [Flurry logError:@"errorDeletingVideo" message:error.description error:error];
                                  [FBSDKAppEvents logEvent:@"errorDeletingVideo" parameters:@{@"error: ":error}];
                                  
                                  
                              }];
                              
                              
                              //analytics
                              
                              
                              //notify that it has been deleted
                          }];
    //no
    [alertView addButtonWithTitle:@"No"
                             type:SIAlertViewButtonTypeDestructive
     
                          handler:^(SIAlertView *alert) {
                              NSLog(@"No Clicked");
                              
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    
    
    
}

#pragma mark - share


-(void)shareViaEmail{
    NSLog(@"not doing anything for email share");
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    /*
    NSLog(@"the controller did finish");
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",self.postObjectID,@"postObjectID",
                                   nil];
    
    
    switch (result) {
        case MFMailComposeResultSent:{
            NSLog(@"You sent the email.");
            [FBSDKAppEvents logEvent:@"User sent the email" parameters:articleParams];
            [Flurry logEvent:@"User copied the video url" withParameters:articleParams];
        }
            break;
        case MFMailComposeResultSaved:{
            NSLog(@"You saved a draft of this email");
            [FBSDKAppEvents logEvent:@"User saved a draft of the email" parameters:articleParams];
            [Flurry logEvent:@"User saved a draft of the email" withParameters:articleParams];
        }
            break;
        case MFMailComposeResultCancelled:{
            NSLog(@"You cancelled sending this email.");
            [FBSDKAppEvents logEvent:@"User cancelled sending the email" parameters:articleParams];
            [Flurry logEvent:@"User cancelled sending the email" withParameters:articleParams];
        }
            break;
        case MFMailComposeResultFailed:{
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            [FBSDKAppEvents logEvent:@"error occured in composing the email" parameters:articleParams];
            [Flurry logEvent:@"error occured in composing the email" withParameters:articleParams];
        }
            break;
        default:{
            
        }
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
     */
}



-(void)copyVideoUrl{
    
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.videoURL;
    
    
}

-(void)shareOnFacebookMessenger{
    
    
    if ([FBSDKMessengerSharer messengerPlatformCapabilities] & FBSDKMessengerPlatformCapabilityVideo) {
        
        //might use refrence of video from internal to share to someone vs making call again to parse
        NSString *filepath = [[NSBundle mainBundle] pathForResource:@"selfie_vid" ofType:@"mp4"];
        
        NSData *videoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.videoURL]];
        
        [FBSDKMessengerSharer shareVideo:videoData withOptions:nil];
    } else{
        // Messenger isn't installed. Redirect the person to the App Store.
        NSString *appStoreLink = @"https://itunes.apple.com/us/app/facebook-messenger/id454638411?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];
    }
    
}




-(void)shareOnTwitter{
    //  Create an instance of the Tweet Sheet
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:
                                           SLServiceTypeTwitter];
    
    // Sets the completion handler.  Note that we don't know which thread the
    // block will be called on, so we need to ensure that any required UI
    // updates occur on the main queue
    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
        switch(result) {
                //  This means the user cancelled without sending the Tweet
            case SLComposeViewControllerResultCancelled:
            {
                NSLog(@"tw post share cancelled");
                
                //flurry track cancelled fbpost
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [[PFUser currentUser]username] , @"user", self.videoURL,@"videoURL",self.postObject.objectId,@"postObjectID",
                                               nil];
                [Flurry logEvent:@"userCancelledTwitterPost" withParameters:articleParams];
                [FBSDKAppEvents logEvent:@"userCancelledTwitterPost" parameters:articleParams];
                
            }
                break;
                //  This means the user hit 'Send'
            case SLComposeViewControllerResultDone:
            {
                
                NSLog(@"tw post share done");
                
                //flurry track wants to share on tw
                
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [[PFUser currentUser]username] , @"user", self.videoURL,@"videoURL",self.postObject.objectId,@"postObjectID",
                                               nil];
                [Flurry logEvent:@"userSharedToTwitter" withParameters:articleParams];
                [FBSDKAppEvents logEvent:@"userSharedToTwitter" parameters:articleParams];
                
                
            }
                break;
        }
    };
    
    //  Set the initial body of the Tweet
    [tweetSheet setInitialText:@"Checkout this great 10 second shot!  #BetMe"];
    
    //  Adds an image to the Tweet.  For demo purposes, assume we have an
    //  image named 'larry.png' that we wish to attach
    if (![tweetSheet addImage:self.thumbnailImageView.image]) {
        NSLog(@"Unable to add the image!");
    }
    
    //  Add an URL to the Tweet.  You can add multiple URLs.
    if (![tweetSheet addURL:[NSURL URLWithString:self.videoURL]]){
        NSLog(@"Unable to add the URL!");
    }
    
    //  Presents the Tweet Sheet to the user
    /*
     [self presentViewController:tweetSheet animated:NO completion:^{
     NSLog(@"Tweet sheet has been presented.");
     }];
     */
    
    // [[NSNotificationCenter defaultCenter] postNotificationName:@"userWantsToSharePost" object:tweetSheet];
    [self presentViewController:tweetSheet animated:YES completion:nil];
    
}

-(void)shareOnFacebook{
    
    
    
    FBSDKShareLinkContent*contentLink=[[FBSDKShareLinkContent alloc]init];
    [contentLink setContentURL:[NSURL URLWithString:self.videoURL]];
    [contentLink setImageURL:[NSURL URLWithString:[[self.postObject objectForKey:@"videoThumbnailImage"]valueForKey:@"url"]]];

    [contentLink setContentTitle:@"BetMe Video"];
    [contentLink setContentDescription:self.descriptionTextView.text];
    
    FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
    video.videoURL = [NSURL URLWithString:self.videoURL];
    
    
    FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
    FBSDKSharePhoto *photoContent= [[FBSDKSharePhoto alloc]init];
    [photoContent setImage:self.thumbnailImageView.image];
    
    content.video = video;
    content.previewPhoto=photoContent;
    
    /*  [FBSDKShareDialog showFromViewController:self.fromViewController
     withContent:contentLink
     delegate:self];
     */
    
    [FBSDKShareAPI shareWithContent:content delegate:self];
    
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"userWantsToSharePost" object:content];
    
}

#pragma mark- fbsdk messenger delegate
-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    NSLog(@"sharer did complete with results %@",results);
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user", results,@"postID",
                                   nil];
    [Flurry logEvent:@"userSharedOnFacebook" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userSharedOnFacebook" parameters:articleParams];
    
    
    
}
-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    NSLog(@"sharer did fail with error %@",error);
    
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user", error,@"error",
                                   nil];
    [Flurry logEvent:@"userFailedToSharedOnFacebook" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userFailedSharedOnFacebook" parameters:articleParams];
    
}
-(void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    NSLog(@"sharer did cancel");
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",
                                   nil];
    [Flurry logEvent:@"userCancelledSharedOnFacebook" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userCancelledSharedOnFacebook" parameters:articleParams];
    
}

//------------------------------------

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
