//
//  profileViewController.h
//  BetMe
//
//  Created by Admin on 7/6/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "PAImageView.h"
#import "KIPullToRevealViewController.h"
#import "KIPullToRevealCell.h"
#import <Parse/Parse.h>
#import "VideoPostTableViewCell.h"
#import "JPSThumbnailAnnotation.h"
#import "PBJVideoPlayerController.h"
#import <ALMoviePlayerController.h>
#import "MZRSlideInMenu.h"

@interface profileViewController : KIPullToRevealViewController<PBJVideoPlayerControllerDelegate,ALMoviePlayerControllerDelegate,UIGestureRecognizerDelegate,MZRSlideInMenuDelegate>{
    PBJVideoPlayerController *_videoPlayerController;
    UIImageView *_playButton;


}

@property (strong, readonly, nonatomic) RESideMenu *sideMenu;
@property(nonatomic,strong)NSString* facebookName;
@property(nonatomic,strong)NSString* facebookID;
@property(nonatomic,strong)NSString* parseUserName;
@property(nonatomic,strong)NSString* userImageURL;
@property(nonatomic,strong)NSString*parseUserObjectID;


@property (nonatomic,strong) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UITableView *KITableView;


//activity cell
@property (nonatomic,strong)  UIActivityIndicatorView* spinner;

@property (nonatomic, strong) ALMoviePlayerController *moviePlayer;


//determine if user is viewing his/her profile or another user's
@property(nonatomic,assign) BOOL usedForDisplayingCurrentUser;

//custom views if user does not have videos/check-ins
@property (strong, nonatomic) IBOutlet UIView *userHasNoCheckInsView;
@property (strong, nonatomic) IBOutlet UIView *userHasNoVideosView;


- (IBAction)refreshView:(id)sender;

- (IBAction)seeFollowing:(id)sender;
- (IBAction)seeFollowers:(id)sender;
- (IBAction)seeCheckIns:(id)sender;
- (IBAction)seeVideos:(id)sender;



@end
