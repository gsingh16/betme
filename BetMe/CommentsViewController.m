//
//  CommentsViewController.m
//  
//
//  Created by Admin on 4/8/15.
//
//

#import "CommentsViewController.h"
#import "CommentTableViewCell.h"
#import "SLKTextViewController.h"
#import "MessageViewController.h"
@interface CommentsViewController ()

@end

@implementation CommentsViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title=@"Comments";
    self.parseClassName=@"Comments";
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Comments"];
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    // Order by sport type
    //[query orderByAscending:@"sportType"];
    
    [query whereKey:@"postID" equalTo:self.postObject];
    [query includeKey:@"fromUser"];
    return query;
}


#pragma mark - Table view delegate

// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the textKey in the object,
// and the imageView being the imageKey in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    NSLog(@"somethings going on");
    
    
    static NSString *CellIdentifier = @"commentTableViewCell";
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSLog(@"the cell is null");
        [tableView registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        //[tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"videoPostTableViewCellView"];
        
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    //start customizing
    
    //image view
    //cell.profilePictureButtonImageView setBackgroundImage:<#(UIImage *)#> forState:<#(UIControlState)#>
    NSURL* fbProfileImageURL=[[FBPictureHelper urlForProfilePictureSizeLargeForFBID:[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"]]absoluteURL];
    NSLog(@"the fbProfileImageUrl is %@",fbProfileImageURL);
    
    
    
    //new
    [cell.profilePictureImageView setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];


    cell.profilePictureImageView.layer.cornerRadius = 22.5;
    cell.profilePictureImageView.layer.masksToBounds = YES;
    cell.profilePictureImageView.layer.borderWidth = 1;
    cell.profilePictureImageView.layer.borderColor = [[UIColor orangeColor] CGColor];
    

    
    
    
    //name label
    [cell.nameLabel setText:[[object objectForKey:@"fromUser"]objectForKey:@"displayName"]];
    
    //date label
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSString*postDate= [dateFormat stringFromDate:[object valueForKey:@"createdAt"]];
    NSLog(@"the date of this post is %@",postDate);
    cell.dateLabel.text=postDate;
    
    //comment text
    [cell.commentTextView setText:[object objectForKey:@"comment"]];
    
    
    
    //cell disable selecting ui
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView*footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 45)];
    UIButton*sendButton=[[UIButton alloc]initWithFrame:CGRectMake(274, 2, 40, 40)];
    [sendButton setImage:[UIImage imageNamed:@"sendIcon"] forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(pressedTextButton:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:sendButton];
    footerView.backgroundColor=[UIColor redColor];
    
    
    
    return footerView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 46.0;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)pressedTextButton:(id)sender {
    NSMutableArray *allObjects = [NSMutableArray array];

    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);

    
    [[self queryForTable] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded. Add the returned objects to allObjects
            [allObjects addObjectsFromArray:objects];
            NSLog(@"the object comming in for the comments is %@",objects);
            
            dispatch_semaphore_signal(sema);
//
            
            
        }else{
            NSLog(@"error in query for comments retrieval");
        }}];
    
    while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
        [[NSRunLoop currentRunLoop]
         runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0]];
    }
    MessageViewController*SlkTVC=[[MessageViewController alloc]init];
    SlkTVC.commentObjects=allObjects;
    SlkTVC.postObject=self.postObject;
    [self.navigationController pushViewController:SlkTVC animated:YES];// presentViewController:SlkTVC animated:YES completion:nil];
}







@end
