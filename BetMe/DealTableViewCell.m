//
//  DealTableViewCell.m
//  BetMe
//
//  Created by Admin on 4/19/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import "DealTableViewCell.h"
#import "UIImage+BlurredFrame.h"
#import "ALFSAlert.h"
#import "Flurry.h"
#import <Parse/Parse.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MainViewController.h"
@implementation DealTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)claimDeal:(id)sender {
    
    if ([PFUser currentUser]) {
    
    NSLog(@"the points required are %i",self.pointsRequired.intValue);
    
    ALFSAlert *alert = [[ALFSAlert alloc] initInViewController:self.fromViewController];
    [alert showAlertWithMessage:self.termsAndConditions];
    [alert addButtonWithText:@"OK" forType:ALFSAlertButtonTypeNormal onTap:^{
        NSLog(@"Button");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [alert removeAlert];
        });
        
        NSString*pointsRequired=[NSString stringWithFormat:@" %i points ",self.pointsRequired.intValue];
        NSString*message=[NSString stringWithFormat:@" Agree to the terms & conditions, and claim offer? "];
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:pointsRequired andMessage:message];
        
        [alertView addButtonWithTitle:@"Yes"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [self claim];
                                  
                              }];
        [alertView addButtonWithTitle:@"No"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  
                              }];
        
        //optional recievers
        /*
         alertView.willShowHandler = ^(SIAlertView *alertView) {
         NSLog(@"%@, willShowHandler", alertView);
         };
         alertView.didShowHandler = ^(SIAlertView *alertView) {
         NSLog(@"%@, didShowHandler", alertView);
         };
         alertView.willDismissHandler = ^(SIAlertView *alertView) {
         NSLog(@"%@, willDismissHandler", alertView);
         };
         alertView.didDismissHandler = ^(SIAlertView *alertView) {
         NSLog(@"%@, didDismissHandler", alertView);
         };
         */
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
        
    }];
    
    }else{
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self.fromViewController presentViewController:main animated:YES completion:nil];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }
    

     
}


-(void)claim{
//if user has enough points , deduct points, and add to user's inventory [array]

    NSLog(@"the current user is %@",[PFUser currentUser].objectId);
    
    //get users points
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;

    
    __block PFObject *userObj;
    
    //start query
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            userObj=object;
            NSLog(@"the points are %i",[[userObj objectForKey:@"points"]intValue]);

            //get the points the use has
            self.currentPoints=[[userObj objectForKey:@"points"]intValue];
            
            //points required
            int pointsRequired=self.pointsRequired.intValue;
            
            //difference
            int newPointsForUser=self.currentPoints-pointsRequired;
            
            NSLog(@"the user points are %i and the new points would be %i",self.currentPoints,newPointsForUser);
            
            //check if user has enough
            if ( self.currentPoints >= pointsRequired) {
                
                //deduct from points
                [[PFUser currentUser]setObject:[NSNumber numberWithInt:newPointsForUser] forKey:@"points"];
                
                //add to user
                [[PFUser currentUser]addObject:[PFObject objectWithoutDataWithClassName:@"deals" objectId:self.dealObjectId]forKey:@"deals"];
                
                
                //save so far
                [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (!error) {
                        NSLog(@"succeeded in saving the new user info");
                        //tell user the deal has been added to their dealwallet
                        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Success" andMessage:@"This Deal has been added to your wallet, enjoy!"];
                        
                        
                        
                        [alertView addButtonWithTitle:@"Ok"
                                                 type:SIAlertViewButtonTypeDefault
                                              handler:^(SIAlertView *alert) {
                                                  NSLog(@"Button1 Clicked");
                                                  
                                              }];
                        
                        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                        
                        [alertView show];
                        
                        //flurry track user claimed Deal
                        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                       [[PFUser currentUser]username] , @"user",self.dealObjectId,@"deal",
                                                       nil];
                        [Flurry logEvent:@"userClaimedDeal" withParameters:articleParams];
                        [FBSDKAppEvents logEvent:@"userClaimedDeal" parameters:articleParams];

                        
                        
                    }else{
                        NSLog(@"failed in saving new user info in Deal tableViewCell %@",error);
                    }
                }];
                
                PFQuery *query = [PFQuery queryWithClassName:@"deals"];
                
                // Retrieve the deal object by id
                [query getObjectInBackgroundWithId:self.dealObjectId block:^(PFObject *dealCount, NSError *error) {
                    
                    if (!error) {
                        //decrease quantity
                        [dealCount incrementKey:@"quantity" byAmount:[NSNumber numberWithInt:-1]];
                        [dealCount saveInBackground];
                        
                        
                        
                    }else{
                        NSLog(@"error with decrementing deals %@",error);
                    }
                }];
                
            }else{
                NSLog(@"can't buy");
                SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@":(" andMessage:@"You Need More Points To Claim This Offer"];
                [alertView addButtonWithTitle:@"Ok"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          NSLog(@"Button1 Clicked");
                                      }];
                alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                [alertView show];
                 }
            

        }else{
            NSLog(@"The error in finding object(s) %@",error);
        }
    }];
    
//end query

 
    
}

@end
