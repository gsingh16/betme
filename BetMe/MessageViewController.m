//
//  MessageViewController.m
//  BetMe
//
//  Created by Admin on 4/10/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import "MessageViewController.h"
#import "CommentsViewController.h"
#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import "FBPictureHelper.h"
#import "AFNetworking.h"
#import "CommentTableViewCell.h"
#import "MessageTableViewCell.h"
#import "MessageTextView.h"
#import "Message.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SCPickerViewController.h"
#import "profileViewController.h"
static NSString *MessengerCellIdentifier = @"MessengerCell";
static NSString *AutoCompletionCellIdentifier = @"AutoCompletionCell";



@interface MessageViewController ()
@property (nonatomic, strong) NSArray *users;
@property (nonatomic, strong) NSArray *allUsersFriends;

@property (nonatomic, strong) NSArray *channels;
@property (nonatomic, strong) NSArray *emojis;
@property (nonatomic, strong) NSMutableArray *messages;

@end

@implementation MessageViewController


#pragma mark - Initializer

- (id)init
{
#warning Potentially incomplete method implementation.
    self = [super initWithTableViewStyle:UITableViewStylePlain];
    if (self) {
        [self registerPrefixesForAutoCompletion:@[@"@", @"#", @":"]];
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < 100; i++) {
            NSInteger words = (arc4random() % 40)+1;
            
            Message *message = [Message new];
            message.username = @"random name?";
            message.text = @"random text";
            [array addObject:message];
        }
        
        NSArray *reversed = [[array reverseObjectEnumerator] allObjects];
        self.messages = [[NSMutableArray alloc] initWithArray:reversed];
        
        
        self.users = @[@"Allen", @"Anna", @"Alicia", @"Arnold", @"Armando", @"Antonio", @"Brad", @"Catalaya", @"Christoph", @"Emerson", @"Eric", @"Everyone", @"Steve"];
        self.channels = @[@"General", @"Random", @"iOS", @"Bugs", @"Sports", @"Android", @"UI", @"SSB"];
        self.emojis = @[@"m", @"man", @"machine", @"block-a", @"block-b", @"bowtie", @"boar", @"boat", @"book", @"bookmark", @"neckbeard", @"metal", @"fu", @"feelsgood"];
        
/*
        FBSDKGraphRequest * request=[[FBSDKGraphRequest alloc]initWithGraphPath:@"/me/taggable_friends" parameters:nil];
         
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            // TODO: handle results or error of request.
            NSLog(@"the result from the FBSDKGraphRequest is connection: %@, result: %@, error: %@",connection,request,error);
        }];
  */
        
        
        if ([FBSDKAccessToken currentAccessToken]) {
         [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/taggable_friends?limit=1000"
         parameters:@{ @"fields" : @"id,name,picture.width(100).height(100)"
         }]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error) {
                     //NSLog(@"fetched user friends :%@", [result objectForKey:@"data"]);
                     self.users=[[result objectForKey:@"data"]valueForKey:@"name"];
                     self.allUsersFriends=[[result objectForKey:@"data"]valueForKey:@"name"];
                     
                 }
             }];
        }
        
        
        /*
        SCPickerViewController *vc = [[SCPickerViewController alloc]init];
        vc.requiredPermission = @"user_friends";
        vc.request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/taggable_friends?limit=1000"
                                                       parameters:@{ @"fields" : @"id,name,picture.width(100).height(100)"
                                                                     }];
        vc.allowsMultipleSelection = YES;
        //[self.navigationController pushViewController:vc animated:YES];
        // array=vc.friendResults;
        self.users=[vc selection];
        */

        
        self.bounces = YES;
        self.shakeToClearEnabled = YES;
        self.keyboardPanningEnabled = YES;
        self.shouldScrollToBottomAfterKeyboardShows = NO;
        self.inverted = NO;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.tableView registerClass:[MessageTableViewCell class] forCellReuseIdentifier:MessengerCellIdentifier];
        
        //[self.leftButton setImage:[UIImage imageNamed:@"icn_upload"] forState:UIControlStateNormal];
        //[self.leftButton setTintColor:[UIColor grayColor]];
        
        [self.rightButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
        
        [self.textInputbar.editorTitle setTextColor:[UIColor darkGrayColor]];
        [self.textInputbar.editortLeftButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
        [self.textInputbar.editortRightButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
        
        self.textInputbar.autoHideRightButton = YES;
        self.textInputbar.maxCharCount = 256;
        self.textInputbar.counterStyle = SLKCounterStyleSplit;
        self.textInputbar.counterPosition = SLKCounterPositionTop;
        
        self.typingIndicatorView.canResignByTouch = YES;
        
        [self.autoCompletionView registerClass:[MessageTableViewCell class] forCellReuseIdentifier:AutoCompletionCellIdentifier];
        
        
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height -     self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:YES];
        

        self.title=@"Comments";
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
        

    }
    return self;
}

/*
// Uncomment if you are using Storyboard.
// You don't need to call initWithCoder: anymore
+ (UITableViewStyle)tableViewStyleForCoder:(NSCoder *)decoder
{
    return <#(UITableViewStyle)#>;
}
*/


- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Comments"];
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.commentObjects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    // Order by sport type
    //[query orderByAscending:@"sportType"];
    
    [query whereKey:@"postID" equalTo:self.postObject];
    [query includeKey:@"fromUser"];
    return query;
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do view setup here.
    
    NSLog(@"the number of comments are %lu",(unsigned long)self.commentObjects.count);

    

}

-(void)viewWillAppear:(BOOL)animated{
    
    UIBarButtonItem *editItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_editing"] style:UIBarButtonItemStylePlain target:self action:@selector(editRandomMessage:)];
    
    UIBarButtonItem *typeItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_typing"] style:UIBarButtonItemStylePlain target:self action:@selector(simulateUserTyping:)];
    UIBarButtonItem *appendItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_append"] style:UIBarButtonItemStylePlain target:self action:@selector(fillWithText:)];
    
   // self.navigationItem.rightBarButtonItems = @[editItem, appendItem, typeItem];
}


#pragma mark - SLKTextViewController Events

- (void)didChangeKeyboardStatus:(SLKKeyboardStatus)status
{
    // Notifies the view controller that the keyboard changed status.
    // Calling super does nothing
    NSLog(@"didChangeKeyBoardStatus : ");
    
    CGPoint offset = CGPointMake(0, self.tableView.contentSize.height -     self.tableView.frame.size.height);
    [self.tableView setContentOffset:offset animated:YES];
}

- (void)textWillUpdate
{
    // Notifies the view controller that the text will update.
    // Calling super does nothing
    
    [super textWillUpdate];

}

- (void)textDidUpdate:(BOOL)animated
{
    // Notifies the view controller that the text did update.
    // Must call super
    
    [super textDidUpdate:animated];
    
    NSLog(@"textDidUpdate");
}

- (BOOL)canPressRightButton
{
    // Asks if the right button can be pressed
    
    return [super canPressRightButton];
}

- (void)didPressRightButton:(id)sender
{
    NSLog(@"didPressRightButton with text %@",self.textView.text);
    
    //send update object
    PFObject*comment=[PFObject objectWithClassName:@"Comments"];
    [comment setObject:self.textView.text forKey:@"comment"];
    [comment setObject:[PFUser currentUser] forKey:@"fromUser"];
    [comment setObject:self.postObject forKey:@"postID"];
    [comment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error && succeeded==true) {
            //refresh after post
            
            // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
            // Must call super
            
            // This little trick validates any pending auto-correction or auto-spelling just after hitting the 'Send' button
            [self.textView refreshFirstResponder];
            
            [super didPressRightButton:sender];
            
            [self dismissKeyboard:YES];
            
            
            NSMutableArray *allObjects = [NSMutableArray array];
            
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            
            [[self queryForTable] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded. Add the returned objects to allObjects
                    [allObjects addObjectsFromArray:objects];
                    //NSLog(@"the object comming in for the comments is %@",objects);
                    dispatch_semaphore_signal(sema);
                    while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
                        [[NSRunLoop currentRunLoop]
                         runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0]];
                    }
                    self.commentObjects=allObjects;
                    self.messages =self.commentObjects;
                    [self.tableView reloadData];
                    [super didCommitTextEditing:sender];
                    
                    CGPoint offset = CGPointMake(0, self.tableView.contentSize.height -     self.tableView.frame.size.height);
                    [self.tableView setContentOffset:offset animated:YES];
                }else{
                    NSLog(@"error in query for comments retrieval");
                }}];
            
            //Push Notification
            // Create our Installation query
            PFQuery *pushQuery = [PFInstallation query];
            [pushQuery whereKey:@"user" equalTo:[self.postObject objectForKey:@"fromUser"]];
            
            if (![[[self.postObject objectForKey:@"fromUser"]objectId] isEqual:[PFUser currentUser].objectId]){
            
            // Send push notification to query
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery]; // Set our Installation query
            //String to send
            NSString*currentUserName=[[PFUser currentUser]valueForKey:@"displayName"];
            NSString*stringForPush=[NSString stringWithFormat:@"%@ commented on your post ",currentUserName];
            [push setMessage:stringForPush];
            [push sendPushInBackground];
            }else{
                NSLog(@"this user is commenting on their own post");
                //analytics
                
            }
            
            
        
        }else{
            NSLog(@"error in saving comment %@",error);
            //notify user of error bimmie the bunny
        }}];

}



/*
// Uncomment these methods for aditional events
- (void)didPressLeftButton:(id)sender
{
    // Notifies the view controller when the left button's action has been triggered, manually.
 
    [super didPressLeftButton:sender];
}
 
- (id)keyForTextCaching
{
    // Return any valid key object for enabling text caching while composing in the text view.
    // Calling super does nothing
}

- (void)didPasteMediaContent:(NSDictionary *)userInfo
{
    // Notifies the view controller when a user did paste a media content inside of the text view
    // Calling super does nothing
}

- (void)willRequestUndo
{
    // Notification about when a user did shake the device to undo the typed text
 
    [super willRequestUndo];
}
*/

#pragma mark - SLKTextViewController Edition


// Uncomment these methods to enable edit mode
- (void)didCommitTextEditing:(id)sender
{
    // Notifies the view controller when tapped on the right "Accept" button for commiting the edited text
 
    [super didCommitTextEditing:sender];
    NSLog(@"didCommitTextEditing");

}

- (void)didCancelTextEditing:(id)sender
{
    // Notifies the view controller when tapped on the left "Cancel" button
 
    [super didCancelTextEditing:sender];
    NSLog(@"didCancelTextEditing");

}

#pragma mark - SLKTextViewController Autocompletion

- (CGFloat)heightForAutoCompletionView
{
    CGFloat cellHeight = 50.0;
    return cellHeight*self.searchResult.count;
}



// Uncomment these methods to enable autocompletion mode
- (BOOL)canShowAutoCompletion
{
   // return NO;//temp
    
//Will probably ovveride the table view with a tableview of friends, and a table view of hashtags !!!
//will just make pop-ups for those fields and have a user pick from there, eg: picking friends , and picking hashtags
    
    
    
    // Asks of the autocompletion view should be shown
    
    NSArray *array = nil;
    NSString *prefix = self.foundPrefix;
    NSString *word = self.foundWord;
    
    //self.searchResult = nil;

    
    if ([prefix isEqualToString:@"@"]) {
        if (word.length > 0) {
            array = [self.allUsersFriends filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
            

           // self.searchResult=array;

            NSLog(@"the search result for the word is %@ and the users friends  is %@ and , lastly; the array is %@",word,self.searchResult,array);
            //self.users=array;
            
        }
        else {
            array = self.users;
        }
    }
    else if ([prefix isEqualToString:@"#"] && word.length > 0) {
        array = [self.channels filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
    }
    else if ([prefix isEqualToString:@":"] && word.length > 1) {
        array = [self.emojis filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
    }
    
    if (array.count > 0) {
        array = [array sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    }
    
    self.searchResult = [[NSMutableArray alloc] initWithArray:array];
    //user logic here
    self.users=self.searchResult;
    return self.searchResult.count > 0;
    
}





#pragma mark - <UITableViewDataSource>
#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tableView]) {
        return self.commentObjects.count;
    }
    else {
        return self.searchResult.count;
    }
}


- (void)editCellMessage:(UIGestureRecognizer *)gesture
{
    MessageTableViewCell *cell = (MessageTableViewCell *)gesture.view;
    Message *message = self.messages[cell.indexPath.row];
    
    [self editText:message.text];
    
    [self.tableView scrollToRowAtIndexPath:cell.indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)editLastMessage:(id)sender
{
    if (self.textView.text.length > 0) {
        return;
    }
    
    NSInteger lastSectionIndex = [self.tableView numberOfSections]-1;
    NSInteger lastRowIndex = [self.tableView numberOfRowsInSection:lastSectionIndex]-1;
    
    Message *lastMessage = [self.messages objectAtIndex:lastRowIndex];
    
    [self editText:lastMessage.text];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

// Uncomment these methods to configure the cells


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //if needs header
    if (self.navigationController) {
        return 0;
    }else
    return 55.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView*headerCell=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.height, 55)];
    [headerCell setBackgroundColor:[UIColor orangeColor]];
    
    //close button
    UIButton*closeButton=[[UIButton alloc]initWithFrame:CGRectMake(5, 20, 25, 25)];
    //[closeButton setBackgroundImage:[UIImage imageNamed:@"x button"] forState:UIControlStateNormal];
    [closeButton setBackgroundImage:[UIImage imageNamed:@"x button"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeNotificationController:) forControlEvents:UIControlEventTouchUpInside];
    
    [headerCell addSubview:closeButton];
    
    //Text
    UILabel*textLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.tableView.frame.size.width/3, 20, 100, 35)];
    [textLabel setText:@"Comments"];
    [textLabel setTextColor:[UIColor whiteColor]];
    
    [headerCell addSubview:textLabel];
    
    
    
    return headerCell;
}

- (IBAction)closeNotificationController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView isEqual:self.autoCompletionView]) {
        
        
        MessageTableViewCell*cell= [self autoCompletionCellForRowAtIndexPath:indexPath];
        // Cells must inherit the table view's transform
        // This is very important, since the main table view may be inverted
        //cell.transform = self.tableView.transform;
      //  NSLog(@"all the keys in the array are %@  :",[[[self commentObjects] objectAtIndex:indexPath.row]valueForKey:@"comment"]);
        
        Message *message = self.users[indexPath.row];
       // message.username=[[[[self commentObjects] objectAtIndex:indexPath.row]valueForKey:@"fromUser"]valueForKeyPath:@"displayName"];
        //message.text=[[[self commentObjects] objectAtIndex:indexPath.row]valueForKey:@"comment"];
        
       // NSLog(@"the user %@ and comment is  %@ : the total array is %@",message.username,message.text,[self.commentObjects objectAtIndex:indexPath.row]);
        
      //  cell.titleLabel.text = message.username;
      //  cell.bodyLabel.text = message.text;
        
        cell.titleLabel.text=[self.users objectAtIndex:indexPath.row];
//        cell.titleLabel.text = [self.users objectAtIndex:indexPath.row];
        cell.bodyLabel.text = @"body here";
        

        
        cell.indexPath = indexPath;
        cell.usedForMessage = YES;
        
        if (cell.needsPlaceholder)
        {
   
        }
        
        return cell;
        
    }else{
    
    //refer to object from array
    PFObject*object=[self.commentObjects objectAtIndex:indexPath.row];
    
    
    
    static NSString *CellIdentifier = @"commentTableViewCell";
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSLog(@"the cell is null");
        [tableView registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        //[tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"videoPostTableViewCellView"];
        
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    //start customizing
    
    //image view
    //cell.profilePictureButtonImageView setBackgroundImage:<#(UIImage *)#> forState:<#(UIControlState)#>
    NSURL* fbProfileImageURL=[[FBPictureHelper urlForProfilePictureSizeLargeForFBID:[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"]]absoluteURL];
    NSLog(@"the fbProfileImageUrl is %@",fbProfileImageURL);
    
    //fromUser
    cell.fromUser=[object objectForKey:@"fromUser"];
    
    //from vc
    cell.fromViewController=self;
        
    //new
    [cell.profilePictureImageView setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];
    
    
    cell.profilePictureImageView.layer.cornerRadius = 22.5;
    cell.profilePictureImageView.layer.masksToBounds = YES;
    cell.profilePictureImageView.layer.borderWidth = 1;
    cell.profilePictureImageView.layer.borderColor = [[UIColor orangeColor] CGColor];


    
    //name label
    [cell.nameLabel setText:[[object objectForKey:@"fromUser"]objectForKey:@"displayName"]];
    
    //date label
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSString*postDate= [dateFormat stringFromDate:[object valueForKey:@"createdAt"]];
    NSLog(@"the date of this post is %@",postDate);
    cell.dateLabel.text=postDate;
    
    //comment text
    [cell.commentTextView setText:[object objectForKey:@"comment"]];
    
    
    
    //cell disable selecting ui
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //transform
    cell.transform = self.tableView.transform;
        
    
    
    return cell;
    
    }
    
}

- (MessageTableViewCell *)autoCompletionCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageTableViewCell *cell = (MessageTableViewCell *)[self.autoCompletionView dequeueReusableCellWithIdentifier:AutoCompletionCellIdentifier];
    cell.indexPath = indexPath;
    
    //was search res
    NSString *item = self.searchResult[indexPath.row];
    

    
    if ([self.foundPrefix isEqualToString:@"#"]) {
        item = [NSString stringWithFormat:@"# %@", item];
    }
    else if ([self.foundPrefix isEqualToString:@":"]) {
        item = [NSString stringWithFormat:@":%@:", item];
    }
    
    cell.titleLabel.text = item;
    cell.titleLabel.font = [UIFont systemFontOfSize:14.0];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Returns the height each row
 
    if ([tableView isEqual:self.autoCompletionView]) {
        return 50;
    }
    
    return 68;
}



#pragma mark - <UITableViewDelegate>


// Uncomment this method to handle the cell selection
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tableView]) {

    }
    if ([tableView isEqual:self.autoCompletionView]) {

        //[self acceptAutoCompletionWithString:<#@"any_string"#>];
        NSString *item = self.searchResult[indexPath.row];
        //This one works  ->[self acceptAutoCompletionWithString:item];
        [self.textInputbar.textView setText:[NSString stringWithFormat:@"%@%@",[self.textInputbar.textView.text substringToIndex:self.textInputbar.textView.text.length-1] ,item]];
        
        
    }
    
    //maybe this should be in the above block
    //[self dismissKeyboard:YES];
}



#pragma mark - View lifeterm

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    
}

@end
