//
//  MainViewController.h
//  BetMe
//
//  Created by Admin on 6/24/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import "FlipsideViewController.h"
#import <Parse/Parse.h>
#import <CoreData/CoreData.h>
#import <AVFoundation/AVFoundation.h>
#import <ParseUI/ParseUI.h>



@interface MainViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, PFSubclassing >

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


//login Button
@property (strong, nonatomic) IBOutlet UIButton *loginWithFacebookButton;
- (IBAction)loginWithFacebook:(id)sender;

- (IBAction)loginAnonymously:(id)sender;

//terms and connditions
- (IBAction)showTermsOfUse:(id)sender;

//privacy Policy
- (IBAction)showPrivacyPolicy:(id)sender;

@end
