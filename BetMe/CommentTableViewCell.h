//
//  CommentTableViewCell.h
//  BetMe
//
//  Created by Admin on 4/10/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBPictureHelper.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import <Parse/Parse.h>

@interface CommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (nonatomic,strong) PFObject*fromUser;
- (IBAction)profilePictureSelected:(id)sender;

@property (nonatomic,strong) UIViewController* fromViewController;


@end
