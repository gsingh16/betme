//
//  AppDelegate.h
//  BetMe
//
//  Created by Admin on 6/24/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
@class MainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,RESideMenuDelegate,FBSDKMessengerURLHandlerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@property (strong, nonatomic) MainViewController *mainViewController;
@property (nonatomic,strong) FBSDKMessengerURLHandler* messengerUrlHandler ;
@end
