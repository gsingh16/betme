//
//  MessageViewController.h
//  BetMe
//
//  Created by Admin on 4/10/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import "SLKTextViewController.h"
#import <Parse/Parse.h>

@interface MessageViewController : SLKTextViewController


@property(nonatomic,strong)NSMutableArray*commentObjects;
@property(nonatomic,strong)NSArray*searchResult;
@property(nonatomic,strong)PFObject*postObject;
@end
