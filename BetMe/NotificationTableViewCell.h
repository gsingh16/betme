//
//  NotificationTableViewCell.h
//  BetMe
//
//  Created by Admin on 5/17/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface NotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;
@property (nonatomic,strong) PFObject * postObject;
@property (nonatomic,strong) NSString * notificationType;
@end
