//
//  CommentTableViewCell.m
//  BetMe
//
//  Created by Admin on 4/10/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "profileViewController.h"


@implementation CommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.commentTextView setEditable:NO];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)profilePictureSelected:(id)sender {
    
    
    if ([[PFUser currentUser].objectId isEqualToString:self.fromUser.objectId]) {
        NSLog(@"user wants to view themself");
        
    }else{
        profileViewController*profile=[[profileViewController alloc]initWithCoder:nil];
        profile.usedForDisplayingCurrentUser=NO;
        profile.facebookName=[self.fromUser objectForKey:@"displayName"];
        profile.facebookID=[self.fromUser objectForKey:@"facebookId"];
        profile.parseUserName=[self.fromUser objectForKey:@"username"];
        profile.parseUserObjectID=self.fromUser.objectId;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.fromViewController.navigationController pushViewController:profile animated:YES];
        });
    }
    
    //analytics
    
}
@end
