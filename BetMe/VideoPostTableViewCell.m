//
//  videoPostTableViewCell.m
//  BetMe
//
//  Created by Admin on 3/21/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import "VideoPostTableViewCell.h"
#import "profileViewController.h"
#import "parseQueryTableViewController.h"
#import "SIAlertView.h"
#import <Social/Social.h>
#import "IQFeedbackView.h"
#import "Flurry.h"
#import <Parse/Parse.h>
#import <MessageUI/MessageUI.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "CommentsViewController.h"
#import "MessageViewController.h"
#import "POP.h"
#import <POP.h>
#import "POPSpringAnimation.h"
#import "POPBasicAnimation.h"
@implementation VideoPostTableViewCell{
    int xTrack;
    

}
@synthesize videoImageView;
@synthesize userDisplayName;
@synthesize descriptionLabel;
@synthesize dateLabel;
@synthesize tagsForTagView;
@synthesize profilePictureView;
@synthesize videoURL;
@synthesize postObjectID;
@synthesize postObject;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSLog(@"the initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier is being used");
        self.likeControl.objectID=self.postObjectID;

        
    }
    return self;
}

-(void)handleTap{
    NSLog(@"detected the tap from within the tableviewcell");
}

- (IBAction)clickedOnProfilePicture:(id)sender {
    
    if ([[PFUser currentUser].objectId isEqualToString:self.parseUserObjectID]) {
        NSLog(@"user wants to view themself");
        
    }else{
    NSLog(@"clicked on the profile pic, the constructor for the profileVC will have facebookID as %@  , and parse user-id as %@",self.facebookID,self.parseUserName);
    profileViewController*profile=[[profileViewController alloc]initWithCoder:nil];
    profile.usedForDisplayingCurrentUser=NO;
    profile.facebookName=self.facebookName;
    profile.facebookID=self.facebookID;
    profile.parseUserName=self.parseUserName;
    profile.parseUserObjectID=self.parseUserObjectID;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"profilePictureSelected" object:profile];
    });
    }
    
}

- (IBAction)comment:(id)sender {
    
    if ([PFUser currentUser]) {
    
    MessageViewController*SlkTVC=[[MessageViewController alloc]init];
    SlkTVC.postObject=self.postObject;
    
    NSMutableArray *allObjects = [NSMutableArray array];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"Comments"];
    [query whereKey:@"postID" equalTo:self.postObject];
    [query includeKey:@"fromUser"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded. Add the returned objects to allObjects
            [allObjects addObjectsFromArray:objects];
            NSLog(@"the object comming in for the comments is %@",objects);
            dispatch_semaphore_signal(sema);
            while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
                [[NSRunLoop currentRunLoop]
                 runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0]];
            }
            SlkTVC.commentObjects=allObjects;
            [self.fromViewController.navigationController pushViewController:SlkTVC animated:YES];
            
            
            //
        }else{
            NSLog(@"error in query for comments retrieval");
        }}];
    
    }else{
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self.fromViewController presentViewController:main animated:YES completion:nil];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }

}

- (IBAction)like:(id)sender {
    
    //animation (if liked & not liked)
    
    /*
    POPSpringAnimation *spin = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerRotation];
    spin.fromValue = @(M_PI / 4);
    spin.toValue = @(0);
    spin.springBounciness = 20;
    spin.velocity = @(10);
    [self.likeButton.layer pop_addAnimation:spin forKey:@"likeAnimation"];
    [self.likeButton setTitle:@"Liked👌" forState:UIControlStateNormal];
    */
    
    /*
    POPSpringAnimation *sprintAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    sprintAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(0.9, 0.9)];
    sprintAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    sprintAnimation.springBounciness = 20.f;
    [self.likeButton.layer pop_addAnimation:sprintAnimation forKey:@"springAnimation"];
    
    [self.likeButton setTitle:@"Liked👌" forState:UIControlStateNormal];
    */
    
    //make sure user isn't anon
    if ([PFUser currentUser]) {
    
    
    
    if (self.isLikedByUser==NO) {
    
    // Create the like activity and save it
    PFObject *likeActivity = [PFObject objectWithClassName:@"Likes"];
    [likeActivity setObject:[PFUser currentUser] forKey:@"fromUser"];
    [likeActivity setObject:postObject forKey:@"post"];
    
    [likeActivity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error && succeeded) {
            NSLog(@"saved the like activity successfully");
            
            //update like for post in bg
            [self.postObject addObject:[PFUser currentUser].objectId forKey:@"likes"];
            [self.postObject saveInBackground];
            
            //add new notification
            PFObject*notification=[PFObject objectWithClassName:@"Notifications"];
            [notification setObject:[PFUser currentUser] forKey:@"fromUser"];
            [notification setObject:self.postObject forKey:@"fromPost"];
            [notification setObject:self.postAuthor forKey:@"toUser"];
            [notification setObject:@"newLike" forKey:@"notificationType"];
            NSString*currentUserName=[[PFUser currentUser]objectForKey:@"displayName"];
            [notification setObject:[NSString stringWithFormat:@"%@ liked your post",currentUserName] forKey:@"withMessage"];
            [notification saveInBackground];
            
            //push
            //Push Notification
            // Create our Installation query
            PFQuery *pushQuery = [PFInstallation query];
            [pushQuery whereKey:@"user" equalTo:[self.postObject objectForKey:@"fromUser"]];
            //PFUser*reciepientUser=
            
            
            if (![[[self.postObject objectForKey:@"fromUser"]objectId] isEqual:[PFUser currentUser].objectId]){
                
                
                // Send push notification to query
                PFPush *push = [[PFPush alloc] init];
                [push setQuery:pushQuery]; // Set our Installation query
                //String to send
                NSString*stringForPush=[NSString stringWithFormat:@"%@ liked your post ",currentUserName];
                [push setMessage:stringForPush];
                
                [push sendPushInBackground];
                
            }else{
                NSLog(@"this user is liking on their own post");
                //analytics
                
                }
            

            
            //animation
            POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
            animation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
            animation.toValue = [NSValue valueWithCGPoint:CGPointMake(0.9, 0.9)];
            animation.springSpeed = 15;
            animation.springBounciness = 20;
            [self.likeButton.layer pop_addAnimation:animation forKey:@"animation"];
            [self.likeButton setTitle:@"Liked👌" forState:UIControlStateNormal];
            [self.likeButton setUserInteractionEnabled:NO];
            
            //fb stuff here
            
            //analytics
            //flurry track user upload Video
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [[PFUser currentUser]username] , @"user",self.postObjectID,@"post",
                                           nil];
            [Flurry logEvent:@"userLikedPost" withParameters:articleParams];
            [FBSDKAppEvents logEvent:@"userLikedPost" parameters:articleParams];
            
            
        }else{
            NSLog(@"error in saving like activity %@ ",error);
        }
    }];
    
    }else{
        //if liked by user -- display already liked and disable UI
        [self.likeButton setTitle:@"Liked👌" forState:UIControlStateNormal];
        [self.likeButton setUserInteractionEnabled:NO];
        
    }
    
    }else{
        NSLog(@"user is anon");
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self.fromViewController presentViewController:main animated:YES completion:nil];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
 
    //NSLog(@"setSelected method");
    
    
    /*
     [self.tagsView clear];
     self.tagsView.usedForDisplay=true;
     [self.tagsView addTags:self.tagsForTagView];
     */

    /*
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"videoPostCellSelected" object:self.descriptionLabel.text];
    });
     */
    
    

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //Change the selected background view of the cell.

    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSLog(@"did select row at index path %@",indexPath);

}


- (IBAction)userSelectedShare:(id)sender {
    //cell sharing
    
    //old 11 29 14
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self.fromViewController presentViewController:main animated:YES completion:nil];

                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to share"];
        [Flurry logEvent:@"User wants to share"];
    

    NSString *other1 = @"Share on Facebook";
    NSString *other2 = @"Share on Twitter";
    NSString *other3 = @"Share via Facebook Messenger";
    NSString *other4 = @"Share via E-mail";
    NSString *other5 = @"Copy URL";

    //e-mail crashes
    
        NSString *cancelTitle = @"Cancel";
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil
                                      delegate:self
                                      cancelButtonTitle:cancelTitle
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:other1,other2,other3,other5, nil];
        actionSheet.tag=2;
        [actionSheet showInView:self];
    
    NSLog(@"user wants to share bear ");
        }
  

 
    
}

- (IBAction)userSelectedMoreOptions:(id)sender {
    
    if ([PFUser currentUser]) {
    
  //  NSString *actionSheetTitle = @"Action Sheet Demo"; //Action Sheet Title
    NSString *destructiveTitle = @"Report/Flag this post"; //Action Sheet Button Titles
    NSString *deletePostButton = @"Delete Post";//if from user
    //NSString *other1 = @"Other Button 1";
    //NSString *other2 = @"Other Button 2";
    //NSString *other3 = @"Other Button 3";
    
    
    if ([[PFUser currentUser].objectId isEqualToString:self.parseUserObjectID]) {
        
        NSString *cancelTitle = @"Cancel";
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil
                                      delegate:self
                                      cancelButtonTitle:cancelTitle
                                      destructiveButtonTitle:destructiveTitle
                                      otherButtonTitles:deletePostButton, nil];
        actionSheet.tag=1;
        [actionSheet showInView:self];
    }else{

    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:destructiveTitle
                                  otherButtonTitles:nil];
        actionSheet.tag=1;
    [actionSheet showInView:self];
    }
    

        
        [FBSDKAppEvents logEvent:@"User wants to see more options"];
        [Flurry logEvent:@"User wants to see more options"];
    }else{
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self.fromViewController presentViewController:main animated:YES completion:nil];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }
    
}
#pragma mark- ui actionsheet delegate

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"im recieving the notice that this popup sheet %@ index at %i was selected",popup,buttonIndex);

    //for sharing vs more options
    if (popup.tag==1)
    {
    
    if ([[PFUser currentUser].objectId isEqualToString:self.parseUserObjectID]) {
        
        switch (buttonIndex)
        {
            case 0:
            {
                //[self FBShare];
                NSLog(@"report/flag this post");
                [self reportPost];
                
            }
                break;
            case 1:
                //[self TwitterShare];
                NSLog(@"user selected delete post");
                [self userSelectedDeletePost];
                break;
            case 2:
                //[self emailContent];
                NSLog(@"user selected cancel");
                break;
            case 3:
                //[self saveContent];
                NSLog(@"save content");
                break;
            case 4:
                //[self rateAppYes];
                NSLog(@"rate the app");
                break;
            default:
                break;
                
        }
        
    }else{

            switch (buttonIndex) {
                case 0:
                {
                    //[self FBShare];
                    NSLog(@"report/flag this post");
                    [self reportPost];
                }
                    break;
                case 1:
                    //[self TwitterShare];
                    NSLog(@"user selected cancel");
                    break;
                case 2:
                    //[self emailContent];
                    NSLog(@"Email Share");
                    break;
                case 3:
                    //[self saveContent];
                    NSLog(@"save content");
                    break;
                case 4:
                    //[self rateAppYes];
                    NSLog(@"rate the app");
                    break;
                default:
                    break;

             }
        
          }
    }else if (popup.tag==2)
    {//for sharing
    
        
        
        switch (buttonIndex)
        {
            case 0:
            {
                NSLog(@"fb share");
                [self shareOnFacebook];
                [FBSDKAppEvents logEvent:@"User wants to see share on facebook"];
                [Flurry logEvent:@"User wants to share on facebook"];
            }
                break;
            case 1:
            {
                NSLog(@"twitter share");
                [self shareOnTwitter];
                [FBSDKAppEvents logEvent:@"User wants to share on twitter"];
                [Flurry logEvent:@"User wants to share on twitter"];
            }
                break;
            case 2:
            {
                NSLog(@"fbMessenger share");
                [self shareOnFacebookMessenger];
                [FBSDKAppEvents logEvent:@"User wants to share on fbMessenger"];
                [Flurry logEvent:@"User wants to share on fbMessenger"];
            }
                break;

            case 3:
            {
                NSLog(@"copy url");
                [self copyVideoUrl];
                [FBSDKAppEvents logEvent:@"User copied the video url"];
                [Flurry logEvent:@"User copied the video url"];
            }
                break;
            case 4://e-mail share should be 4, but buggy atm; so it is cancel
            {
                //NSLog(@"e-mail share");
                //[self shareViaEmail];
                [FBSDKAppEvents logEvent:@"User hit cancel on sharing"];
                [Flurry logEvent:@"User hit cancel on sharing"];
            }
                break;
                
            default:
                break;
                
        }
        
    }
    
    
    
}

#pragma mark- report post
-(void)reportPost{
    IQFeedbackView *feedback = [[IQFeedbackView alloc] initWithTitle:@"Flag/Report" message:nil image:nil cancelButtonTitle:@"Cancel" doneButtonTitle:@"Send"];
    [feedback setCanAddImage:NO];
    [feedback setCanEditText:YES];
    
    [feedback showInViewController:self.fromViewController completionHandler:^(BOOL isCancel, NSString *message, UIImage *image) {
        NSLog(@"The message is %@ with length %lu and isCancel %hhd",message,(unsigned long)[message length],isCancel);
        
        if (isCancel==0 && [message length]>0 ) {
            
            PFObject*feedbackPost=[PFObject objectWithClassName:@"flaggedCuts"];
            [feedbackPost setObject:[PFUser currentUser] forKey:@"fromUser"];
            [feedbackPost setValue:message forKey:@"message"];
            [feedbackPost setObject:[PFObject objectWithoutDataWithClassName:@"Cuts" objectId:self.postObjectID] forKey:@"fromPost"];
            
            
            [feedbackPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    //alert thanks
                    
                    [FBSDKAppEvents logEvent:@"User reoported post" parameters:@{@"user: ":[PFUser currentUser],@"postID: ":postObjectID}];
                    [Flurry logEvent:@"User reported post" withParameters:@{@"user: ":[PFUser currentUser],@"postID: ":postObjectID}];
                    
                    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Success" andMessage:@"Thank you for your report! "];
                    alertView.messageColor=[UIColor redColor];
                    //yes
                    [alertView addButtonWithTitle:@"Ok"
                                             type:SIAlertViewButtonTypeDefault
                                          handler:^(SIAlertView *alert)
                     {
                         
                     }];
                    
                    [alertView show];
                    
                }else{
                    NSLog(@"the error in saving the feedback post is %@",error);
                    //analytics
                    [FBSDKAppEvents logEvent:@"error with reoporting post" parameters:@{@"user: ":[PFUser currentUser],@"postID: ":postObjectID}];
                    [Flurry logEvent:@"error with reported post" withParameters:@{@"user: ":[PFUser currentUser],@"postID: ":postObjectID,@"error: ":error}];
                    
                    
                }
            }];
            
        }else
        {
            NSLog(@"the user cancelled feedback post");
            [FBSDKAppEvents logEvent:@"User cancelled reoported post" parameters:@{@"user: ":[PFUser currentUser],@"postID: ":postObjectID}];
            [Flurry logEvent:@"User cancelled reported post" withParameters:@{@"user: ":[PFUser currentUser],@"postID: ":postObjectID}];
        }
        
        
        [feedback dismiss];
    }];
}

#pragma mark- delete post

-(void)userSelectedDeletePost{
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Confirm" andMessage:@"Are you sure you would like to discard this video? You will lose 300 points "];
    //yes
    [alertView addButtonWithTitle:@"Yes"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              
                              
                              PFObject*thisPost=[[PFObject alloc]initWithClassName:@"Cuts"];
                              thisPost.objectId=self.postObjectID;
                              [thisPost deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                  if (!error) {
                                      NSLog(@"succesfully deleted object SHOULD ALSO DELETE POINTS 300");
                                      
                                      int currentPoints=[[[PFUser currentUser]objectForKey:@"points"]intValue];
                                      int pointsToBeSubtracted=300;
                                      int newValueForPoints=currentPoints-pointsToBeSubtracted;
                                      [[PFUser currentUser]setObject:[NSNumber numberWithInt:newValueForPoints] forKey:@"points"];
                                      
                                      [[PFUser currentUser]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                          if (!error) {
                                              NSLog(@"updated users new points completely for discard");
                                              
                                              SIAlertView *didDeletePost = [[SIAlertView alloc] initWithTitle:@"OMG" andMessage:@"This Post Has Been Deleted! "];
                                              [didDeletePost addButtonWithTitle:@"Ok" type:SIAlertViewButtonTypeDestructive handler:nil];
                                              [didDeletePost show];
                                              
                                              
                                              //flurry track user delete post
                                              NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                             [[PFUser currentUser]username] , @"user", self.videoURL,@"videoURL",self.postObjectID,@"postObjectID",
                                                                             nil];
                                              [Flurry logEvent:@"userDeletedPost" withParameters:articleParams];
                                              [FBSDKAppEvents logEvent:@"userDeletedPost" parameters:articleParams];

                                              
                                              
                                          }else
                                              NSLog(@"the error in updating the users points for discard is %@,",error);
                                          [Flurry logError:@"errorUpdatingUserPoints" message:error.description error:error];
                                          [FBSDKAppEvents logEvent:@"errorUpdatingUserPoints" parameters:@{@"error: ":error}];

                                      }];

                                      
                                  }else
                                      NSLog(@"the error in trying to delete the object is %@",error);
                                  [Flurry logError:@"errorDeletingVideo" message:error.description error:error];
                                  [FBSDKAppEvents logEvent:@"errorDeletingVideo" parameters:@{@"error: ":error}];


                              }];
                              
                              
                              //analytics
                              
                              
                              //notify that it has been deleted
                          }];
    //no
    [alertView addButtonWithTitle:@"No"
                             type:SIAlertViewButtonTypeDestructive
     
                          handler:^(SIAlertView *alert) {
                              NSLog(@"No Clicked");
                              
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    
    
    
}

#pragma mark - share


-(void)shareViaEmail{

    if ([MFMailComposeViewController canSendMail])
    {
        
        NSString*messageBody=[NSString stringWithFormat:@"%@   \n Discovered on BetMe  -  www.BetMe.NYC ",self.videoURL];
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] initWithRootViewController:self.fromViewController.navigationController];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"Checkout this BetMe Video"];
        [mail setMessageBody:messageBody isHTML:NO];
        [mail setToRecipients:@[@"Share with all your fiends :)"]];
        
        [self.fromViewController presentViewController:mail animated:NO completion:NULL];
    
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"the controller did finish");
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user", self.videoURL,@"videoURL",self.postObjectID,@"postObjectID",
                                   nil];
    
    
    switch (result) {
        case MFMailComposeResultSent:{
            NSLog(@"You sent the email.");
            [FBSDKAppEvents logEvent:@"User sent the email" parameters:articleParams];
            [Flurry logEvent:@"User copied the video url" withParameters:articleParams];
        }
            break;
        case MFMailComposeResultSaved:{
            NSLog(@"You saved a draft of this email");
            [FBSDKAppEvents logEvent:@"User saved a draft of the email" parameters:articleParams];
            [Flurry logEvent:@"User saved a draft of the email" withParameters:articleParams];
        }
            break;
        case MFMailComposeResultCancelled:{
            NSLog(@"You cancelled sending this email.");
            [FBSDKAppEvents logEvent:@"User cancelled sending the email" parameters:articleParams];
            [Flurry logEvent:@"User cancelled sending the email" withParameters:articleParams];
        }
            break;
        case MFMailComposeResultFailed:{
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            [FBSDKAppEvents logEvent:@"error occured in composing the email" parameters:articleParams];
            [Flurry logEvent:@"error occured in composing the email" withParameters:articleParams];
        }
            break;
        default:{

        }
            break;
    }
    
    [self.fromViewController dismissViewControllerAnimated:YES completion:NULL];
}



-(void)copyVideoUrl{

    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.videoURL;

    
}

-(void)shareOnFacebookMessenger{
    

    if ([FBSDKMessengerSharer messengerPlatformCapabilities] & FBSDKMessengerPlatformCapabilityVideo) {
        
        //might use refrence of video from internal to share to someone vs making call again to parse
        NSString *filepath = [[NSBundle mainBundle] pathForResource:@"selfie_vid" ofType:@"mp4"];
        
        NSData *videoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.videoURL]];
        
        [FBSDKMessengerSharer shareVideo:videoData withOptions:nil];
    } else{
        // Messenger isn't installed. Redirect the person to the App Store.
        NSString *appStoreLink = @"https://itunes.apple.com/us/app/facebook-messenger/id454638411?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];
    }
    
}
    
    
    

-(void)shareOnTwitter{
    //  Create an instance of the Tweet Sheet
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:
                                           SLServiceTypeTwitter];
    
    // Sets the completion handler.  Note that we don't know which thread the
    // block will be called on, so we need to ensure that any required UI
    // updates occur on the main queue
    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
        switch(result) {
                //  This means the user cancelled without sending the Tweet
            case SLComposeViewControllerResultCancelled:
            {
                NSLog(@"tw post share cancelled");
                
                //flurry track cancelled fbpost
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [[PFUser currentUser]username] , @"user", self.videoURL,@"videoURL",self.postObjectID,@"postObjectID",
                                               nil];
                [Flurry logEvent:@"userCancelledTwitterPost" withParameters:articleParams];
                [FBSDKAppEvents logEvent:@"userCancelledTwitterPost" parameters:articleParams];

            }
                break;
                //  This means the user hit 'Send'
            case SLComposeViewControllerResultDone:
            {
                
                NSLog(@"tw post share done");
                
                //flurry track wants to share on tw
                
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [[PFUser currentUser]username] , @"user", self.videoURL,@"videoURL",self.postObjectID,@"postObjectID",
                                               nil];
                [Flurry logEvent:@"userSharedToTwitter" withParameters:articleParams];
                [FBSDKAppEvents logEvent:@"userSharedToTwitter" parameters:articleParams];

                
            }
                break;
        }
    };
    
    //  Set the initial body of the Tweet
    [tweetSheet setInitialText:@"Checkout this great 10 second shot!  #BetMe"];
    
    //  Adds an image to the Tweet.  For demo purposes, assume we have an
    //  image named 'larry.png' that we wish to attach
    if (![tweetSheet addImage:videoImageView.image]) {
        NSLog(@"Unable to add the image!");
    }
    
    //  Add an URL to the Tweet.  You can add multiple URLs.
    if (![tweetSheet addURL:[NSURL URLWithString:self.videoURL]]){
        NSLog(@"Unable to add the URL!");
    }
    
    //  Presents the Tweet Sheet to the user
    /*
    [self presentViewController:tweetSheet animated:NO completion:^{
        NSLog(@"Tweet sheet has been presented.");
    }];
    */
    
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"userWantsToSharePost" object:tweetSheet];
    [self.fromViewController presentViewController:tweetSheet animated:YES completion:nil];
    
}

-(void)shareOnFacebook{

    
    
    FBSDKShareLinkContent*contentLink=[[FBSDKShareLinkContent alloc]init];
    [contentLink setContentURL:[NSURL URLWithString:self.videoURL]];
    [contentLink setImageURL:[NSURL URLWithString:self.videoThumnailImageURL]];
    [contentLink setContentTitle:@"BetMe Video"];
    [contentLink setContentDescription:self.descriptionLabel.text];
    
    FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
    video.videoURL = [NSURL URLWithString:self.videoURL];
    
    
    FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
    FBSDKSharePhoto *photoContent= [[FBSDKSharePhoto alloc]init];
    [photoContent setImage:self.imageView.image];
    
    content.video = video;
    content.previewPhoto=photoContent;
    
  /*  [FBSDKShareDialog showFromViewController:self.fromViewController
                                 withContent:contentLink
                                    delegate:self];
    */
    
    [FBSDKShareAPI shareWithContent:content delegate:self];

    
   //[[NSNotificationCenter defaultCenter] postNotificationName:@"userWantsToSharePost" object:content];
    
}

#pragma mark- fbsdk messenger delegate
-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    NSLog(@"sharer did complete with results %@",results);
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user", results,@"postID",
                                   nil];
    [Flurry logEvent:@"userSharedOnFacebook" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userSharedOnFacebook" parameters:articleParams];
    
    
    
}
-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    NSLog(@"sharer did fail with error %@",error);
    
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user", error,@"error",
                                   nil];
    [Flurry logEvent:@"userFailedToSharedOnFacebook" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userFailedSharedOnFacebook" parameters:articleParams];
    
}
-(void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    NSLog(@"sharer did cancel");
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",
                                   nil];
    [Flurry logEvent:@"userCancelledSharedOnFacebook" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userCancelledSharedOnFacebook" parameters:articleParams];
    
}

    //------------------------------------
    
    // A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
        NSArray *pairs = [query componentsSeparatedByString:@"&"];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        for (NSString *pair in pairs) {
            NSArray *kv = [pair componentsSeparatedByString:@"="];
            NSString *val =
            [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            params[kv[0]] = val;
        }
        return params;
    }




@end
