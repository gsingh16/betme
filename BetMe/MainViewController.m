
//
//  MainViewController.m
//  BetMe
//
//  Created by Admin on 6/24/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import "MainViewController.h"
#import "DDAlertPrompt.h"
#import "homeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <Parse/Parse.h>
#import <Accounts/Accounts.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "DEMOMenuViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "MDBrowser.h"
#import "Flurry.h"
#import "FlurryAds.h"
//#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import "TutorialViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface MainViewController () <RESideMenuDelegate,BrowserViewDelegate>
@property (nonatomic,strong) NSString*logIn;
@property(nonatomic,strong)  NSString*pass;
@property (nonatomic,strong) PFLogInViewController*logInViewController;
@property(nonatomic, strong) PFSignUpViewController*signUpViewController;
@property(nonatomic,strong) UINavigationController*navController;
@end

@implementation MainViewController

@synthesize logInViewController=_logInViewController;
@synthesize signUpViewController=_signUpViewController;
@synthesize navController=_navController;

/*
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    [self viewDidAppear:true];
    
}
*/
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.avplayer pause];
}

 -(void)viewDidAppear:(BOOL)animated{
       [super viewDidAppear:animated];
    
    
     NSString*currentAppVersion=[NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
     NSLog(@"the current app version is %@",currentAppVersion);
     
     //check if needed to force upgrade
     [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
         double latestAppVersion = [config[@"latestAppVersion"]doubleValue];
         NSLog(@"the current version is %@ and the latest version is %f",currentAppVersion,latestAppVersion);
        
         if (currentAppVersion.doubleValue < latestAppVersion) {
             //send user to app store
             NSLog(@"user is being sent to app store to force update");
             NSString *iTunesLink = @"itms://itunes.apple.com/us/app/bet-me/id914313598?mt=8";
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
             [Flurry logEvent:@"user sent to appstore to update" ];
             [FBSDKAppEvents logEvent:@"user sent to appstore to update"];
             
             
         }else{
             NSLog(@"user is at current version");
             
             BOOL shouldShowTutorial= [[NSUserDefaults standardUserDefaults]boolForKey:@"userCompletedTutorials"];
             
             if (!shouldShowTutorial) {
                 NSLog(@"going to show tutorial");
                 TutorialViewController*tutorial=[[TutorialViewController alloc]init];
                 [self presentViewController:tutorial animated:YES completion:^{
                     [tutorial showBetMeTutorial];
                 }];
                 
             }else
                 NSLog(@"user already saw tutorial");

             
             
         }
     }];
     
     
     
    if (![PFUser currentUser]) { // No user logged in
        // Create the log in view controller
        _logInViewController = [[PFLogInViewController alloc] init];
        [_logInViewController setDelegate:self]; // Set ourselves as the delegate
        

        
        _logInViewController.delegate=self;
        _logInViewController.signUpController.delegate=self;
        _logInViewController.fields=PFLogInFieldsFacebook;
        
        
        //set custom UI here
        
        
         NSBundle *bundle = [NSBundle mainBundle];
         NSString *moviePath = [bundle pathForResource:@"xx" ofType:@"mp4"];
         NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
        
        
         AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
         AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
         self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
         AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
         [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
         [avPlayerLayer setFrame:self.view.frame];
         [self.movieView.layer addSublayer:avPlayerLayer];
         [self.avplayer seekToTime:kCMTimeZero];
         [self.avplayer setVolume:0.0f];
         [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
        
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(playerItemDidReachEnd:)
         name:AVPlayerItemDidPlayToEndTimeNotification
         object:[self.avplayer currentItem]];
        
         CAGradientLayer *gradient = [CAGradientLayer layer];
         gradient.frame = self.gradientView.bounds;
         gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
         [self.gradientView.layer insertSublayer:gradient atIndex:0];
        
        [self.avplayer play];


    }else{
        NSLog(@"logged in");
        [self goToHomeViewController];
        
    }
    
    
    
}



// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    NSLog(@"did log in");
 /*
    if(user.isNew){
        //if user is new
        
        //Tutorial?
        
    }
    else
    {
        NSLog(@"the current user pfinstall is %@",[[PFInstallation currentInstallation]objectForKey:@"installationId"]);
    }
    
    //[PFUser
    
    //store local copy of facebokInfro
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 
                 //if(user.isNew){ .. MUST DO HERE AND ADD TO PFPUSHJ CURRENT INSATLL
                 
                 // Set user's information
                 NSString *facebookId = [result objectForKey:@"id"];
                 NSString *facebookName = [result objectForKey:@"name"];
                 
                 if (facebookName && facebookName.length != 0) {
                     [[PFUser currentUser] setObject:facebookName forKey:@"displayName"];
                     [defaults setObject:facebookName forKey:@"displayName"];
                     //set first name of user
                     
                 }
                 if (facebookId && facebookId.length != 0) {
                     [[PFUser currentUser] setObject:facebookId forKey:@"facebookId"];
                     [defaults setObject:facebookId forKey:@"facebookId"];
                 }
                 
                 
                 [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                     if (!error) {
                         NSLog(@"saved users info");
                         //proceed
                         [self goToHomeViewController];
                         
                     }else{
                         NSLog(@"error in saving users info to parse");
                         [Flurry logEvent:@"Error in saving current user to parse after login " withParameters:@{@"error":error}];
                         [FBSDKAppEvents logEvent:@"Error in saving current user to parse after login" parameters:@{@"error":error}];
                         
                     }
                 }];
                 
             } else {
                 [self showErrorAlert];
                 NSLog(@"the error in saving the current user is %@",error);
                 [Flurry logEvent:@"Error in saving current user after login " withParameters:@{@"error":error}];
                 [FBSDKAppEvents logEvent:@"Error in saving current user after login" parameters:@{@"error":error}];
                 
             }
         }];
    }else{
        NSLog(@"Current access token is bad %@",[FBSDKAccessToken currentAccessToken]);
    }
    
    
    //WAS NOT USING BEFORE FROM HERE
    // Get user's personal information
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // Set user's information
            NSString *facebookId = [result objectForKey:@"id"];
            NSString *facebookName = [result objectForKey:@"name"];
            
            if (facebookName && facebookName.length != 0) {
                [[PFUser currentUser] setObject:facebookName forKey:@"displayName"];
                [defaults setObject:facebookName forKey:@"displayName"];
                //set first name of user

            }
            if (facebookId && facebookId.length != 0) {
                [[PFUser currentUser] setObject:facebookId forKey:@"facebookId"];
                [defaults setObject:facebookId forKey:@"facebookId"];
            }
            
            [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    NSLog(@"saved users info");
                    //proceed
                    [self goToHomeViewController];
                    
                }else{
                    NSLog(@"error in saving users info to parse");
                    [Flurry logEvent:@"Error in saving current user to parse after login " withParameters:@{@"error":error}];
                    [FBSDKAppEvents logEvent:@"Error in saving current user to parse after login" parameters:@{@"error":error}];

                }
            }];
        } else {
            [self showErrorAlert];
            NSLog(@"the error in saving the current user is %@",error);
            [Flurry logEvent:@"Error in saving current user after login " withParameters:@{@"error":error}];
            [FBSDKAppEvents logEvent:@"Error in saving current user after login" parameters:@{@"error":error}];

        }
    }];
     
    
    
    
    
    
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        // TODO: publish content.
        NSLog(@"Publish_actions is enabled!");
        
    } else {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithPublishPermissions:@[@"publish_actions"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            //TODO: process error or result.
            NSLog(@"the error is %@",error);
            NSLog(@"the result is %@",result);
            
            
            
            
        }];
    }
    [self goToHomeViewController];
    
    //TO HERE
*/
    

}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in... the error is %@",error);
    if(error)
    {
        NSLog(@"Session error with error %@",error);
        [self fbResync];
}
}




// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"hiii");
    [self dismissViewControllerAnimated:YES completion:NULL];

    
    
 //   [_logInViewController removeFromParentViewController];
//    [_logInViewController dismissViewControllerAnimated:YES completion:nil];
   // [self.logInViewController removeFromParentViewController];

}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)fbResync
{

    ACAccountStore *accountStore;
    ACAccountType *accountTypeFB;
    if ((accountStore = [[ACAccountStore alloc] init]) && (accountTypeFB = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook] ) ){
        
        NSArray *fbAccounts = [accountStore accountsWithAccountType:accountTypeFB];
        id account;
        if (fbAccounts && [fbAccounts count] > 0 && (account = [fbAccounts objectAtIndex:0])){

            [accountStore renewCredentialsForAccount:account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
                //we don't actually need to inspect renewResult or error.
                if (error){
                    NSLog(@"there was a deeper error, please see %@",error);
                    
                    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   error , @"error",
                                                   nil];
                    
                    
                    [Flurry logEvent:@"there was a deeper error, please see" withParameters:articleParams];
                    [FBSDKAppEvents logEvent:@"there was a deeper error, please see" parameters:articleParams];
                    
                }
            }];
        }
    }
}

    
-(void)goToHomeViewController{
    
    NSLog(@"Good Login with current user %@",[[PFUser currentUser]objectId]);
    
    //set current user ID
    [Flurry setUserID:[[PFUser currentUser]objectId]];

    //flurry analytic - logged in user
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",
                                   nil];
    
    [Flurry logEvent:@"userLoggedIn" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userLoggedIn" parameters:articleParams];

    
    homeViewController*home=[[homeViewController alloc]initWithCoder:Nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:home];
    [self presentViewController:navigationController animated:YES completion:^{
        NSLog(@"successfully presented home view controller from mainview controller");
        
        //store local copy of facebokInfro
        NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
        
        
        
        if ([FBSDKAccessToken currentAccessToken]) {
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error) {
                     
                     
                     // Set user's information
                     NSString *facebookId = [result objectForKey:@"id"];
                     NSString *facebookName = [result objectForKey:@"name"];
                     
                     if (facebookName && facebookName.length != 0) {
                         [[PFUser currentUser] setObject:facebookName forKey:@"displayName"];
                         [defaults setObject:facebookName forKey:@"displayName"];
                         //set first name of user
                         
                     }
                     if (facebookId && facebookId.length != 0) {
                         [[PFUser currentUser] setObject:facebookId forKey:@"facebookId"];
                         [defaults setObject:facebookId forKey:@"facebookId"];
                     }
                     
                     [[PFInstallation currentInstallation]setObject:facebookName forKey:@"userDisplayName"];
                     [[PFInstallation currentInstallation]setObject:[PFUser currentUser] forKey:@"user"];
                     [[PFInstallation currentInstallation]saveInBackground];
                     
                     
                     [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                         if (!error) {
                             NSLog(@"saved users info");
                             //proceed
                             //[self goToHomeViewController];
                             
                         }else{
                             NSLog(@"error in saving users info to parse");
                             [Flurry logEvent:@"Error in saving current user to parse after login " withParameters:@{@"error":error}];
                             [FBSDKAppEvents logEvent:@"Error in saving current user to parse after login" parameters:@{@"error":error}];
                         }
                     }];
                 } else {
                     [self showErrorAlert];
                     NSLog(@"the error in saving the current user is %@",error);
                     [Flurry logEvent:@"Error in saving current user after login " withParameters:@{@"error":error}];
                     [FBSDKAppEvents logEvent:@"Error in saving current user after login" parameters:@{@"error":error}];
                 }}];
            
            
            /*
            // Request new Publish Permissions
            [PFFacebookUtils linkUserInBackground:[PFUser currentUser] withPublishPermissions:@[ @"publish_actions"] block:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    NSLog(@"User now has read and publish permissions!");
                }else{
                    NSLog(@"the error in linking the publish actions is : %@",error);
                }
            }];
            */
            
            
        }else{
            NSLog(@"FBSDKAccessToken is not there : %@",[FBSDKAccessToken currentAccessToken]);
        }
        
    }];
    
}


-(void)removeNavController{
    [_navController removeFromParentViewController];
    
}


#pragma mark login failure

- (void)showErrorAlert {
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:@"We were not able to create your profile. Please try again."
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    
    //flurry track fail
    [Flurry logEvent:@"Something went wrong logging the user in" withParameters:nil];
    [FBSDKAppEvents logEvent:@"Something went wrong logging the user in"];

}



#pragma video delegates

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

#pragma mark - login

- (IBAction)loginWithFacebook:(id)sender {
    NSLog(@"user wants to login with facebook");
    
    
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[@"public_profile", @"email", @"user_location"];
    
    

    
    
    
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        
        if (!user) {
            NSString *errorMessage = nil;
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                errorMessage = @"Uh oh. The user cancelled the Facebook login.";
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                errorMessage = [error localizedDescription];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
            
            //flurry track fail
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           errorMessage , @"reason",
                                           nil];
            [Flurry logEvent:@"Something went wrong logging the user in" withParameters:articleParams];
            [FBSDKAppEvents logEvent:@"Something went wrong logging the user in" parameters:articleParams];
            
            //direct user to settings to fix
            if ([[[error userInfo] objectForKey:@"com.facebook.sdk:ErrorLoginFailedReason"] isEqualToString:@"com.facebook.sdk:SystemLoginDisallowedWithoutError"]){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error, Please go to settings > Facebook and allow this app to run"
                                                                message:errorMessage
                                                               delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }else{
                NSLog(@"was not a user settings error");
                 }
            
        } else {
            if (user.isNew) {
                NSLog(@"User with facebook signed up and logged in!");
                
            } else {
                NSLog(@"User with facebook logged in!");
                //[self logInViewController:_logInViewController didLogInUser:user];
                [self goToHomeViewController];

            }
            
            
            /*
            // Request new Publish Permissions
            [PFFacebookUtils linkUserInBackground:[PFUser currentUser] withPublishPermissions:@[ @"publish_actions"] block:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    NSLog(@"User now has read and publish permissions!");
                }else{
                    NSLog(@"the error in linking the publish actions is : %@",error);
                }
                
                NSLog(@"in the linkuserinBGmethod %@",error);
            }];
            */
            [PFFacebookUtils logInInBackgroundWithPublishPermissions:@[@"publish_actions"] block:^(PFUser *user, NSError *error){
                
                if (!error) {
                    NSLog(@"logging in user with publish actions");
                }else{
                    NSLog(@"error in logging user w/ publish actions is %@",error);
                }
                
                
            }];

            
            //idk soething
           // [self logInViewController:_logInViewController didLogInUser:user];
        }
    }];

    
    

    
    //[Pare style
    
    /*
    
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[@"public_profile", @"email",@"user_friends",@"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
    
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        
        if (!user) {
            NSString *errorMessage = nil;
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                errorMessage = @"Uh oh. The user cancelled the Facebook login.";
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                errorMessage = [error localizedDescription];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
            
            //flurry track fail
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           errorMessage , @"reason",
                                           nil];
            [Flurry logEvent:@"Something went wrong logging the user in" withParameters:articleParams];
            [FBSDKAppEvents logEvent:@"Something went wrong logging the user in" parameters:articleParams];
            
            //direct user to settings to fix
            if ([[[error userInfo] objectForKey:@"com.facebook.sdk:ErrorLoginFailedReason"] isEqualToString:@"com.facebook.sdk:SystemLoginDisallowedWithoutError"]){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error, Please go to settings > Facebook and allow this app to run"
                                                                message:errorMessage
                                                               delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }else{
                NSLog(@"was not a user settings error");
            }
            
        } else {
            if (user.isNew) {
                NSLog(@"User with facebook signed up and logged in!");
                if([PFUser currentUser]){

                [[PFInstallation currentInstallation]setObject:[user objectForKey:@"displayName"] forKey:@"userDisplayName"];
                [[PFInstallation currentInstallation]setObject:[PFUser currentUser] forKey:@"user"];
                [[PFInstallation currentInstallation]saveInBackground];
                NSLog(@"the current user name from the push token is %@",[[PFInstallation currentInstallation]objectForKey:@"userDisplayName"]);
                }
                [self goToHomeViewController];
                
                //new user ; so tutorial?
                
                
                
            } else {
                NSLog(@"User with facebook logged in!");
                NSLog(@"the current user name from the push token is %@",[[PFInstallation currentInstallation]objectForKey:@"userDisplayName"]);
                //[self logInViewController:_logInViewController didLogInUser:user];
               //
                
                if([PFUser currentUser]){
                [[PFInstallation currentInstallation]setObject:[user objectForKey:@"displayName"] forKey:@"userDisplayName"];
                [[PFInstallation currentInstallation]setObject:[PFUser currentUser] forKey:@"user"];
                [[PFInstallation currentInstallation]saveInBackground];
                }
                [self goToHomeViewController];
                
            }
        }
        
        
    }];
    */
    
    
    
    //new fb style?
    /*
     
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        // TODO: publish content.
        NSLog(@"Publish_actions is enabled!");
        
    } else {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        loginManager.defaultAudience=2;
        [loginManager logInWithPublishPermissions:@[@"publish_actions",@"public_profile", @"email",@"user_friends",@"user_about_me", @"user_relationships", @"user_birthday", @"user_location"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            //TODO: process error or result.
            NSLog(@"the error is %@",error);
            NSLog(@"the result is %@",result );
        }];
    }
    
     
     */
    
    

        
}


- (IBAction)loginAnonymously:(id)sender {
    //track event
    /*
    [FBSession openActiveAnonymousSessionWithAllowLoginUI:YES
                                        completionHandler:^(FBSession *session,                                                       FBSessionState status,                                                       NSError *error) {     // Handle session state changes }];
                                            if (error==NULL){
                                                [FBSDKAppEvents logEvent:@"Something went wrong logging the user in " parameters:@{@"data: ":session}];
                                                NSLog(@"got in with anon login with session %@",session);
                                                [self goToHomeViewController];
                                                
                                                  }else{
                                                  NSLog(@"error with anon login %@",error);
                                                  //track error
                                                      //flurry track fail
                                                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                                     error , @"reason",
                                                                                     nil];
                                                [Flurry logEvent:@"Something went wrong logging the user in anonymously" withParameters:articleParams];
                                                [FBSDKAppEvents logEvent:@"Something went wrong logging the user in " parameters:articleParams];
                                                  }
    }];
     */
    [PFUser logOut];
    [Flurry logEvent:@"User wants to login anonymously" withParameters:nil];
    [FBSDKAppEvents logEvent:@"User wants to login anonymously " parameters:Nil];

    homeViewController*home=[[homeViewController alloc]initWithCoder:Nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:home];
    [self presentViewController:navigationController animated:YES completion:nil];
        
    
}


- (IBAction)showTermsOfUse:(id)sender {
    NSLog(@"user selected terms and conditions button");
    MDBrowser* browser = [[MDBrowser alloc] initWithFrame:CGRectMake(0, 75, self.view.bounds.size.width, self.view.bounds.size.height-100)];
    browser.delegate = self;
    [browser ShowInView:self.view AddOverLayToSuperView:YES withAnimationType:MDBrowserPresetationAnimationTypePopUp];
    [browser LoadUrl:[NSURL URLWithString:@"https://s3-us-west-2.amazonaws.com/binarybros.legal/termsAndConditions.html"]];
    [browser setButtonsHidden:YES];
    
    //flurry track viewed terms of use
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",
                                   nil];
    [Flurry logEvent:@"userViewedTermsOfUse" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userViewedTermsOfUse" parameters:articleParams];

    
    
}
- (IBAction)showPrivacyPolicy:(id)sender {
    NSLog(@"user selected privacy policy button");
    MDBrowser* browser = [[MDBrowser alloc] initWithFrame:CGRectMake(0, 75, self.view.bounds.size.width, self.view.bounds.size.height-100)];
    browser.delegate = self;
    [browser ShowInView:self.view AddOverLayToSuperView:YES withAnimationType:MDBrowserPresetationAnimationTypePopUp];
    [browser LoadUrl:[NSURL URLWithString:@"https://s3-us-west-2.amazonaws.com/binarybros.legal/privacyPolicy.html"]];
    [browser setButtonsHidden:YES];
    
    //flurry track privacy policy
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",
                                   nil];
    [Flurry logEvent:@"userViewedPrivacyPolicy" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userViewedPrivacyPolicy" parameters:articleParams];

    
}
@end
