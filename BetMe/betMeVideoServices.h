//
//  betMeVideoServices.h
//  BetMe
//
//  Created by Admin on 12/27/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "GTMOAuth2Authentication.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

#import <MessageUI/MessageUI.h>
@interface betMeVideoServices : NSObject < GTMFetcherAuthorizationProtocol,GTMHTTPFetcherServiceProtocol,FBSDKSharingDelegate>{
}

@property (nonatomic,strong) NSString*descriptionForVideo;//describe the challenge
@property (nonatomic,strong) NSMutableArray*keyWordsForVideo;//amphersands
@property (retain,strong) NSURL* videoURL;
@property (retain,strong) NSData* videoData;
@property(nonatomic,strong)NSString*fileName;
@property (retain) GTMOAuth2Authentication *signedAuth;

@property(nonatomic,strong)NSUserDefaults*defaults;

@property (retain) NSString *currentTicketID;
@property (retain) NSString *currentVideoID;
@property(nonatomic,strong)NSString*completeURI;
@property(nonatomic,strong)NSString*VimeoUploadSecureURL;

@property (assign) BOOL isUploading;
@property (retain) GTMHTTPFetcher *currentFetcher;
@property(nonatomic,strong)MBProgressHUD *hud;
@property(nonatomic,strong) UIWindow *currentWindow;
@property(nonatomic,strong) UIView* baseView;


-(void)upload;

@end
 

