//
//  TableViewController.h
//  BetMe
//
//  Created by Admin on 4/16/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KIImagePager.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>
#import "MDBrowser.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import <SDWebImageDownloader.h>


@interface DealsTableViewController : UIViewController <BrowserViewDelegate,UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,UIAlertViewDelegate,KIImagePagerDataSource,KIImagePagerDelegate>{
    
    IBOutlet KIImagePager *_imagePager;
    IBOutlet UITableView *_tableView;
        NSString*CellIdentifier;
    
    IBOutlet UILabel *_locationAdressLabel;
    
    IBOutlet UIButton *navigateToLocationButton;
    IBOutlet UIButton *callLocationButton;
    IBOutlet UIButton *moreInfoButton;
    
}

@property(nonatomic,strong)NSArray*locationImageURLsArray;
@property(nonatomic,strong)NSString*locationName;
@property(nonatomic,strong)NSString*locationAdress;
@property(nonatomic,strong)NSString*locationPhoneNumber;
@property(nonatomic,strong)NSString*locationDistance;
@property(nonatomic,strong)NSArray*locationDeals;
@property(nonatomic,strong)NSString*locationMoreInfoURL;
@property(nonatomic,strong)PFGeoPoint*locationCoordinate;


- (IBAction)callLocation:(id)sender;
- (IBAction)moreInfo:(id)sender;
- (IBAction)navigateToLocation:(id)sender;




@end


//for The Cell

