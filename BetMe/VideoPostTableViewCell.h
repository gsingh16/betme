//
//  videoPostTableViewCell.h
//  BetMe
//
//  Created by Admin on 3/21/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <ALMoviePlayerControls.h>
#import <ALMoviePlayerController.h>
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "AGMedallionView.h"
#import "PAImageView.h"
#import <Parse/Parse.h>
#import "HKKTagWriteView.h"
#import "MainViewController.h"
#import "SIAlertView.h"
//tags
#import "AKTagsDefines.h"
#import "AKTagsInputView.h"
#import "AKTagCell.h"
#import "AKTagTextFieldCell.h"
#import "AKTextField.h"

//sharing is caring
#import "HMSideMenu.h"

//flag
#import "IQFeedbackView.h"

//fb
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

#import <MessageUI/MessageUI.h>
@interface VideoPostTableViewCell : UITableViewCell <UIActionSheetDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FBSDKSharingDelegate,FBSDKSharingDialog,FBSDKSharingContent>

//from vc
@property (nonatomic,strong) UIViewController* fromViewController;

@property (strong, nonatomic) IBOutlet UIImageView *videoImageView;
@property (strong, nonatomic) IBOutlet UIImageView *profilePictureView;
@property (strong, nonatomic) IBOutlet UIButton *profilePictureButton;
@property (strong, nonatomic) IBOutlet UILabel *userDisplayName;
@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;


@property (nonatomic, strong) NSURLSessionDataTask *imageDownloadTask;
@property (strong, nonatomic) IBOutlet FBSDKLikeButton *likeControl;
@property (nonatomic, strong) HMSideMenu *sideMenu;


@property (nonatomic, assign) BOOL isVideoThumnailSet;

//object id (to manipulate)
@property(nonatomic,strong)NSString*postObjectID;

//object itself
@property(nonatomic,strong)PFObject*postObject;

//post author
@property(nonatomic,strong)PFObject* postAuthor;


//array of tags
@property(nonatomic,strong)NSArray*tagsForTagView;

//video URL
@property(nonatomic,strong)NSString*videoURL;

//thumbnail URL
@property(nonatomic,strong)NSString*videoThumnailImageURL;

//video data?
@property(nonatomic,strong)NSData* videoFileData;

//name
@property(nonatomic,strong)NSString*facebookName;

//is like by user
@property(nonatomic,assign) BOOL isLikedByUser;

//to load user's profile pic
@property(nonatomic,strong)NSString*parseUserName;
@property(nonatomic,strong)NSString*facebookID;
@property(nonatomic,strong)NSString*parseUserObjectID;

-(void)playVideoWithURL:(NSURL*)url;



- (IBAction)clickedOnProfilePicture:(id)sender;
- (IBAction)comment:(id)sender;
- (IBAction)like:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;

@end
