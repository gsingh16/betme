//
//  NotificationTableViewController.m
//  BetMe
//
//  Created by Admin on 4/25/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import "NotificationTableViewController.h"
#import "FBPictureHelper.h"
#import "TGLViewController.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface NotificationTableViewController ()

@end

@implementation NotificationTableViewController
@synthesize headerView=_headerView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.parseClassName = @"Notifications";
    
    // The key of the PFObject to display in the label of the default cell style
    self.textKey = @"withMessage";
    
    // Whether the built-in pull-to-refresh is enabled
    self.pullToRefreshEnabled = YES;
    
    // Whether the built-in pagination is enabled
    self.paginationEnabled = YES;
    
    // The number of objects to show per page
    self.objectsPerPage = 10;
    

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

}


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Notifications"];
    // If Pull To Refresh is enabled, query against the network by default.
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    [query orderByDescending:@"createdAt"];
    //adding user obj
    [query includeKey:@"fromUser"];
    [query includeKey:@"fromPost"];
    [query whereKey:@"toUser" equalTo:[PFUser currentUser]];
    [query whereKey:@"fromUser" notEqualTo:[PFUser currentUser]];
    
    
    //limit count
    [query setLimit:10];
    
    return query;
}



#pragma mark - Table view delegate



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 55.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView*headerCell=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.height, 55)];
    [headerCell setBackgroundColor:[UIColor orangeColor]];

    //close button
    UIButton*closeButton=[[UIButton alloc]initWithFrame:CGRectMake(5, 20, 25, 25)];
    //[closeButton setBackgroundImage:[UIImage imageNamed:@"x button"] forState:UIControlStateNormal];
    [closeButton setBackgroundImage:[UIImage imageNamed:@"x button"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeNotificationController:) forControlEvents:UIControlEventTouchUpInside];
    
    [headerCell addSubview:closeButton];
    
    //Text
    UILabel*textLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.tableView.frame.size.width/3, 20, 100, 35)];
    [textLabel setText:@"Notifications"];
    [textLabel setTextColor:[UIColor whiteColor]];
    
    [headerCell addSubview:textLabel];
    

    
    return headerCell;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    NSLog(@"the number of objects are: %i",self.objects.count);
    return self.objects.count;
}

// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the textKey in the object,
// and the imageView being the imageKey in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object{
    
    static NSString *simpleTableIdentifier = @"notificationTableViewCell";
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        NSLog(@"the cell is null");
        [tableView registerNib:[UINib nibWithNibName:@"NotificationTableViewCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
        //[tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"videoPostTableViewCellView"];
        
        cell=[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    }
    
    NSLog(@"the notification type is %@",[object valueForKey:@"notificationType"]);
    NSString*notificationType=[object valueForKey:@"notificationType"];
    
    cell.postObject=[object objectForKey:@"fromPost"];
    cell.notificationType=notificationType;
    
    if ([notificationType isEqualToString:@"newComment"]) {
        //Build newComment Cell
        
        //cell profile image
        NSURL* fbProfileImageURL=[[FBPictureHelper urlForProfilePictureSizeSquareForFBID:[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"]]absoluteURL];
        NSLog(@"the fbProfileImageUrl is %@",fbProfileImageURL);
        
        //Left Image
        //UIImageView*leftImage=[[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 40, 40)];
        //[leftImage setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];
        //[cell addSubview:leftImage];
        
        
        [cell.leftImageView sd_setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];
        //round
        cell.leftImageView.layer.cornerRadius = cell.leftImageView.frame.size.width / 2;
        cell.leftImageView.clipsToBounds = YES;
        //border
        cell.leftImageView.layer.borderWidth = 1.0f;
        cell.leftImageView.layer.borderColor = [UIColor orangeColor].CGColor;
        
        
        //Right Image
        NSString*commentThumbnailURL=[[[object objectForKey:@"fromPost"]objectForKey:@"videoThumbnailImage"]valueForKey:@"url"];
        NSLog(@"the commentThumbnailURL is %@",commentThumbnailURL);
        
        [cell.rightImageView sd_setImageWithURL:[NSURL URLWithString:commentThumbnailURL] placeholderImage:[UIImage imageNamed:@"grey"]];
        
        //Text
        cell.textLabel.font = [UIFont fontWithName:@"ArialMT" size:8];
        [cell.textLabel setText:[object valueForKey:@"withMessage"]];
        
        
        
    }
    else if ([notificationType isEqualToString:@"newLike"]){
        //Build Like notification cell
        //cell profile image
        NSURL* fbProfileImageURL=[[FBPictureHelper urlForProfilePictureSizeSquareForFBID:[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"]]absoluteURL];
        NSLog(@"the fbProfileImageUrl is %@",fbProfileImageURL);
        
        //Left Image
        //UIImageView*leftImage=[[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 40, 40)];
        //[leftImage setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];
        //[cell addSubview:leftImage];
        
        
        [cell.leftImageView sd_setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];
        //round
        cell.leftImageView.layer.cornerRadius = cell.leftImageView.frame.size.width / 2;
        cell.leftImageView.clipsToBounds = YES;
        //border
        cell.leftImageView.layer.borderWidth = 1.0f;
        cell.leftImageView.layer.borderColor = [UIColor orangeColor].CGColor;
        
        
        //Right Image
        NSString*commentThumbnailURL=[[[object objectForKey:@"fromPost"]objectForKey:@"videoThumbnailImage"]valueForKey:@"url"];
        NSLog(@"the commentThumbnailURL is %@",commentThumbnailURL);
        
        [cell.rightImageView sd_setImageWithURL:[NSURL URLWithString:commentThumbnailURL] placeholderImage:[UIImage imageNamed:@"grey"]];
        
        //Text
        cell.textLabel.font = [UIFont fontWithName:@"ArialMT" size:8];
        [cell.textLabel setText:[object valueForKey:@"withMessage"]];
        
    }
    
    else if ([notificationType isEqualToString:@"generalNotification"]){
        //Build general notification cell
    }
    

    

    //cell disable selecting ui
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46;
    
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //open object f
    
    NSLog(@"the tapped notification type is %@",[[tableView cellForRowAtIndexPath:indexPath]valueForKey:@"notificationType"]);
    //should remove 

    //for like
    if ([[[tableView cellForRowAtIndexPath:indexPath]valueForKey:@"notificationType"] isEqualToString:@"newLike"]){
        
    IndividualPostViewController*standAlonePostViewController=[[IndividualPostViewController alloc]init];
    standAlonePostViewController.postObject=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObject"];
    [self presentViewController:standAlonePostViewController animated:YES completion:nil];
    }
    
    //for comment, go straight to comment
    if ([[[tableView cellForRowAtIndexPath:indexPath]valueForKey:@"notificationType"] isEqualToString:@"newComment"]) {
        //go to comment directly
        MessageViewController*SlkTVC=[[MessageViewController alloc]init];
        SlkTVC.postObject=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObject"];
        
        NSMutableArray *allObjects = [NSMutableArray array];
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        
        PFQuery *query = [PFQuery queryWithClassName:@"Comments"];
        [query whereKey:@"postID" equalTo:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObject"]];
        [query includeKey:@"fromUser"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded. Add the returned objects to allObjects
                [allObjects addObjectsFromArray:objects];
                NSLog(@"the object comming in for the comments is %@",objects);
                dispatch_semaphore_signal(sema);
                while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
                    [[NSRunLoop currentRunLoop]
                     runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0]];
                }
                SlkTVC.commentObjects=allObjects;
                [self presentViewController:SlkTVC animated:YES completion:nil];
                }
        }];
    }
    
}




#pragma mark- get thumnail image for video
- (UIImage*)loadThumnbailWithURL:(NSURL*)url {
    
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    
    return [[UIImage alloc] initWithCGImage:imgRef];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark button actions
- (IBAction)closeNotificationController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
