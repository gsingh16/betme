//
//  DEMOMenuViewController.m
//  RESideMenuExample
//
//  Created by Roman Efimov on 10/10/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOMenuViewController.h"
#import "homeViewController.h"
#import "betViewController1.h"
#import "profileViewController.h"
#import "couponViewController.h"
#import "KIPullToRevealViewController.h"
#import "SettingsViewController.h"
@interface DEMOMenuViewController ()

@property (strong, readwrite, nonatomic) UITableView *tableView;

//viewControllers
@property(nonatomic,strong)UIViewController*homeVC;
@property(nonatomic,strong)UIViewController*betVC;
@property(nonatomic,strong)UIViewController*profileVC;
@property(nonatomic,strong)UIViewController*couponVC;
@property(nonatomic,strong)UIViewController*settingsVC;

@end

@implementation DEMOMenuViewController
@synthesize homeVC=_homeVC,betVC=_betVC,profileVC=_profileVC,couponVC=_couponVC,settingsVC=_settingsVC;

//ran once for init

-(id)init{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"userWantsToSharePost"
                                               object:nil];
    
    
    
    if (!_homeVC) {
        homeViewController*home=[[homeViewController alloc]initWithCoder:nil];
        UINavigationController*homeVC=[[UINavigationController alloc] initWithRootViewController:home];
        _homeVC=homeVC;
    }
    
      /* --> 7-8-14 deciding to not include search funct in the first release of the app beacuse it makes sense to have a lot of videos already in the DB before the need to search
    if (!_betVC) {
        betViewController1*cuts=[[betViewController1 alloc]init];
        UINavigationController*cutsVC=[[UINavigationController alloc] initWithRootViewController:cuts];
        _betVC=cutsVC;
    }
    */
    
    if (!_profileVC) {
        
        profileViewController*profile=[[profileViewController alloc] initWithCoder:nil];
        profile.usedForDisplayingCurrentUser=YES;
        profile.facebookName=[[PFUser currentUser]objectForKey:@"displayName"];
        profile.facebookID=[[PFUser currentUser]objectForKey:@"facebookId"];
        profile.parseUserName=[[PFUser currentUser]objectForKey:@"username"];
        profile.parseUserObjectID=[PFUser currentUser].objectId;
        

        
        UINavigationController*profileVC=[[UINavigationController alloc] initWithRootViewController:profile];
        _profileVC=profileVC;
    }
    
    if (!_couponVC) {
        couponViewController*coupons=[[couponViewController alloc]initWithCoder:nil];
        UINavigationController*couponsVC=[[UINavigationController alloc] initWithRootViewController:coupons];
        _couponVC=couponsVC;
    }
    if (!_settingsVC) {
        SettingsViewController*settings=[[SettingsViewController alloc]initWithCoder:nil];
        UINavigationController*settingsVC=[[UINavigationController alloc] initWithRootViewController:settings];
        _settingsVC=settingsVC;
        
    }
    
    //always set No/0 for _usedForDisplayingCurrentUser

    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height - 54 * 5) / 2.0f, self.view.frame.size.width, 54 * 5) style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        
        tableView.backgroundView = nil;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    
}

#pragma mark - nsnotification
- (void)receiveEvent:(NSNotification *)notification {
    
if ([notification.name isEqual:@"userWantsToSharePost"]){
    NSLog(@"sdasdasdasdasdasd");
    [self presentViewController:notification.object animated:YES completion:nil];
}else if ([notification.name isEqual:@"locationSelected"]){
    NSLog(@"user hit location selected :)");
}

}
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            if (!_homeVC) {
                self.sideMenuViewController.contentViewController = _homeVC;
                [self.sideMenuViewController hideMenuViewController];
                NSLog(@"case 0 - 1");
            }else{
                self.sideMenuViewController.contentViewController=_homeVC;
                [self.sideMenuViewController hideMenuViewController];
                NSLog(@"case 0 - 2");

            }
            

            break;
            
            
            /*
             
    //nslog  --> 7-8-14 deciding to not include search funct in the first release of the app beacuse it makes sense to have a lot of videos already in the DB before the need to search
             
        case 1:
            if (!_betVC) {
                [self.sideMenuViewController setContentViewController:_betVC];
                [self.sideMenuViewController hideMenuViewController];
                NSLog(@"case 1 - 1");

            }else{
                self.sideMenuViewController.contentViewController=_betVC;
                [self.sideMenuViewController hideMenuViewController];

                NSLog(@"case 1 - 2");

            }
            
            
            break;
             */
            
        case 1:
            if (!_profileVC) {
                
                [self.sideMenuViewController setContentViewController:_profileVC];
                [self.sideMenuViewController hideMenuViewController];
                NSLog(@"case 2 - 1");

            }else{
                [self.sideMenuViewController setContentViewController:_profileVC];
                [self.sideMenuViewController hideMenuViewController];

                NSLog(@"case 2 - 2");

            }
            break;
        case 2:
            if (!_couponVC) {
                self.sideMenuViewController.contentViewController=_couponVC;
                [self.sideMenuViewController hideMenuViewController];
                NSLog(@"case 3 - 1");

            }else{
                self.sideMenuViewController.contentViewController=_couponVC;
                [self.sideMenuViewController hideMenuViewController];
                NSLog(@"case 3 - 2");

            }
            break;
        case 3:
            if (!_settingsVC) {
                self.sideMenuViewController.contentViewController=_settingsVC;
                [self.sideMenuViewController hideMenuViewController];
                NSLog(@"case 4 - 1");
                
            }else{
                self.sideMenuViewController.contentViewController=_settingsVC;
                [self.sideMenuViewController hideMenuViewController];
                NSLog(@"case 4 - 2");
                
            }
            
            break;
            
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:21];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    
    
    //nslog  --> 7-8-14 deciding to not include search funct in the first release of the app beacuse it makes sense to have a lot of videos already in the DB before the need to search
    //NSArray *titles = @[@"Home", @"Explore", @"Profile", @"Deals", @"Settings"];

    
    NSArray *titles = @[@"Home",@"Profile", @"Deals", @"Settings"];
    NSArray *images = @[@"IconHome", @"IconProfile", @"couponTag", @"IconSettings"];
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
