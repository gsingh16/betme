//
//  TutorialViewController.m
//  BetMe
//
//  Created by Admin on 8/29/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import "TutorialViewController.h"
#import "MLPSpotlight.h"
#import "Canvas.h"
#import "CSAnimation.h"
#import "SIAlertView.h"
#import "Flurry.h"
#import <Parse/Parse.h>
#import "FlurryAds.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
static NSString * const sampleDescription1 = @"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
static NSString * const sampleDescription2 = @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.";
static NSString * const sampleDescription3 = @"Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.";
static NSString * const sampleDescription4 = @"Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit.";



@interface TutorialViewController (){
    BOOL didFinishVideoTutorial;
    BOOL didFinishDealsTutorial;
    BOOL didFinishIdeasTutorial;
    MPMoviePlayerController * moviePlayer;
    int currentTutorialVideoIndex;

}

@end

@implementation TutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title=@"Tutorials";
    
}

-(void)showVideoTutorial{
    /*
    EAIntroPage *page1 = [EAIntroPage pageWithCustomView:self.videoTutorialView1];
    
    EAIntroPage *page2 = [EAIntroPage pageWithCustomView:self.videoTutorialView2];
    [MLPSpotlight addSpotlightInView:page2.customView atPoint:CGPointMake(280, 20)];
    
    EAIntroPage *page3 = [EAIntroPage pageWithCustomView:self.videoTutorialView3];
    [MLPSpotlight addSpotlightInView:page3.customView atPoint:CGPointMake(160 , 345)];

    
    EAIntroPage *page4 = [EAIntroPage pageWithCustomView:self.videoTutorialView4];
    [MLPSpotlight addSpotlightInView:page4.customView atPoint:CGPointMake(160 , 420)];
    
    EAIntroPage *page5 = [EAIntroPage pageWithCustomView:self.videoTutorialView5];
    [MLPSpotlight addSpotlightInView:page5.customView atPoint:CGPointMake(160 , 480)];

//    EAIntroPage *page6 = [EAIntroPage pageWithCustomView:self.ideasTutorialView1];

    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3,page4,page5]];
    [intro setDelegate:self];
    
    intro.tag=1;
    
    [intro showInView:self.view animateDuration:0.3];
    */
    [self showBetMeTutorial];
    
}

-(void)showDealTutorial{

    EAIntroPage *page1 = [EAIntroPage pageWithCustomView:self.dealTutorialView1];
    
    EAIntroPage *page2 = [EAIntroPage pageWithCustomView:self.dealTutorialView2];
    [MLPSpotlight addSpotlightInView:page2.customView atPoint:CGPointMake(160, 200)];
    
    EAIntroPage *page3 = [EAIntroPage pageWithCustomView:self.dealTutorialView3];
    [MLPSpotlight addSpotlightInView:page3.customView atPoint:CGPointMake(160 , 345)];
    
    
    EAIntroPage *page4 = [EAIntroPage pageWithCustomView:self.dealTutorialView4];
    [MLPSpotlight addSpotlightInView:page4.customView atPoint:CGPointMake(160 , 420)];
    

    
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3,page4]];
    [intro setDelegate:self];
    
    intro.tag=2;
    
    [intro showInView:self.view animateDuration:0.3];
    
    

}

-(void)showIdeasTutorial{
    
    EAIntroPage *page1 = [EAIntroPage pageWithCustomView:self.ideasTutorialView1];
    
    EAIntroPage *page2 = [EAIntroPage pageWithCustomView:self.ideasTutorialView2];
    
    EAIntroPage *page3 = [EAIntroPage pageWithCustomView:self.ideasTutorialView3];

    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3]];
    [intro setDelegate:self];
    
    intro.tag=3;
    
    [intro showInView:self.view animateDuration:0.3];
    
}


-(void)showBetMeTutorial{

    /*
    EAIntroPage *page1 = [EAIntroPage pageWithCustomView:self.betMeTutorial1];
    
    EAIntroPage *page2 = [EAIntroPage pageWithCustomView:self.betMeTutorial2];
    
    EAIntroPage *page3 = [EAIntroPage pageWithCustomView:self.betMeTutorial3];

    EAIntroPage *page4 = [EAIntroPage pageWithCustomView:self.betMeTutorial4];

    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3,page4]];
    [intro setDelegate:self];
    
    intro.tag=4;
    
    [intro showInView:self.view animateDuration:0.3];
     */
    
    currentTutorialVideoIndex=1;

    
    NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"danceBboy.mp4" ofType:nil];
    NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
    
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    [[moviePlayer view]setFrame:[[UIScreen mainScreen] bounds]];
    [moviePlayer setScalingMode:MPMovieScalingModeFill];
    [self.view addSubview:moviePlayer.view];
    [moviePlayer setRepeatMode:MPMovieRepeatModeOne];
    moviePlayer.fullscreen = NO;
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [swipeGesture setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    swipeGesture.delegate=self;
    
    UIView *aView = [[UIView alloc] initWithFrame:moviePlayer.view.bounds];
    [aView addGestureRecognizer:swipeGesture];
    [moviePlayer.view addSubview:aView];
    
    
    //textview
    self.overlayText=[[UILabel alloc]initWithFrame:CGRectMake(0, 25, 320, 50)];
    [self.overlayText setFontName:@"Helvetica"];
    [self.overlayText setText:@"Welcome to BetMe"];
    [self.overlayText setTextColor:[UIColor whiteColor]];
    [self.overlayText setFont:[UIFont systemFontOfSize:32.0f]];
    self.overlayText.lineBreakMode = NSLineBreakByWordWrapping;
    self.overlayText.numberOfLines = 1;
    
    self.bottomOverlayText=[[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-100, 320, 80)];
    [self.bottomOverlayText setFontName:@"Helvetica"];
    [self.bottomOverlayText setText:@"Take 10-seond uninterrupted videos"];
    [self.bottomOverlayText setTextColor:[UIColor whiteColor]];
    [self.bottomOverlayText setFont:[UIFont systemFontOfSize:24.0f]];
    self.bottomOverlayText.lineBreakMode = NSLineBreakByWordWrapping;
    self.bottomOverlayText.numberOfLines = 5;
    
    
    [moviePlayer.view addSubview:self.overlayText];
    [moviePlayer.view addSubview:self.bottomOverlayText];
    
    
    //setup first video text
    
    [moviePlayer play];
    [moviePlayer.view setHidden:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    UIImageView*swipeLeftIcon=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height, 100, 100)];
    [swipeLeftIcon setImage:[UIImage imageNamed:@"swipeLeft.png"]];
    
    [UIView animateWithDuration:0.6 animations:^{  // animate the following:
    [swipeLeftIcon setFrame:CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height/2, 100, 100)];
    }completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.8 animations:^{  // animate the following:
            [swipeLeftIcon setFrame:CGRectMake(-120, self.view.frame.size.height/2, 100, 100)];
        }];
        
    }];
    [moviePlayer.view addSubview:swipeLeftIcon];
    

    
    
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {

    
    currentTutorialVideoIndex++;
    NSLog(@"Swipe received. the currentTutorialVideoIndex is %i ",currentTutorialVideoIndex);
    
    [moviePlayer stop];

    
    if (currentTutorialVideoIndex==2) {
        [self.overlayText setText:@""];
        [self.bottomOverlayText setText:@"Earn points by uploading"];
        
        
        NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"dogSuccess.mp4" ofType:nil];
        NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
        [moviePlayer setContentURL:fileURL];
        [moviePlayer play];
    }
    if (currentTutorialVideoIndex==3) {
        [self.overlayText setText:@""];
        [self.bottomOverlayText setText:@"Lose points by discarding"];
        
        NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"dogFail.mp4" ofType:nil];
        NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
        [moviePlayer setContentURL:fileURL];
        [moviePlayer play];
    }
    if (currentTutorialVideoIndex==4) {
        [self.overlayText setText:@"Discover and enjoy"];
        [self.bottomOverlayText setText:@"The best deals from our local merchants with your points👍"];
        
        NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"drivingBroadway.mp4" ofType:nil];
        NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
        [moviePlayer setContentURL:fileURL];
        [moviePlayer play];
    }
    if (currentTutorialVideoIndex==5) {
        [self.overlayText setText:@"To claim the deal:"];
        [self.bottomOverlayText setText:@"Scan the QR code from your deals wallet at the location"];
        
        NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"BetMeTutorialVideoScanQr.mp4" ofType:nil];
        NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
        [moviePlayer setContentURL:fileURL];
        [moviePlayer play];
    }
    if (currentTutorialVideoIndex==6) {
        [self.overlayText setText:@""];
        [moviePlayer stop];
        [moviePlayer.view setHidden:YES];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [self.navigationController setNavigationBarHidden:NO];
        [self done:nil];

    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startVideoTutorial:(id)sender {
    NSLog(@"user wants to see video tutorial");

    
    //hide status bar
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.navigationController.navigationBarHidden=YES;
    
    
    [self showVideoTutorial];

}
- (IBAction)startDealsTutorial:(id)sender {
    NSLog(@"user wants to see deal tutorial");
    
    //hide status bar
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.navigationController.navigationBarHidden=YES;

    [self showDealTutorial];
    
    
}


#pragma mark- eaintroView delegates
- (void)introDidFinish:(EAIntroView *)introView{
    
    NSLog(@"introDidFinish");
    //reset view
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.navigationController.navigationBarHidden=NO;
    
    //analytics
    
    if (introView.tag==1 ) {
        didFinishVideoTutorial=YES;
        
        //flurry track viewed Tutorial
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",
                                       nil];
        [Flurry logEvent:@"userViewedVideoTutorial" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userViewedVideoTutorial" parameters:articleParams];

    }
    if (introView.tag==2) {
        didFinishDealsTutorial=YES;
        
        //flurry track viewed deal Tutorial
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",
                                       nil];
        [Flurry logEvent:@"userViewedDealTutorial" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userViewedDealTutorial" parameters:articleParams];

    }
    if (introView.tag==3 ) {
        didFinishIdeasTutorial=YES;
        self.doneButton.hidden=NO;
        
        
        //flurry track viewed ideas Tutorial
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",
                                       nil];
        [Flurry logEvent:@"userViewedIdeasTutorial" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userViewedIdeasTutorial" parameters:articleParams];

        
        NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
        [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
        
    }
    
    if (introView.tag==4) {
        NSLog(@"the BetMeTutorial is playing");
        
        //flurry track viewed betMe Tutorial
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",
                                       nil];
        [Flurry logEvent:@"userViewedBetMeTutorial" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userViewedBetMeTutorial" parameters:articleParams];
    }
    
    
    
    
}

- (void)intro:(EAIntroView *)introView pageAppeared:(EAIntroPage *)page withIndex:(NSInteger)pageIndex{
    NSLog(@"intro view %@  page appeared  %@   at index %li",introView,page,(long)pageIndex);
    
    if (introView.tag==1 && pageIndex==5) {
        //[page.customView startCanvasAnimation];
    }
    if (introView.tag==2) {
        //[page.customView startCanvasAnimation];
    }
    if (introView.tag==3 ) {
       [page.customView startCanvasAnimation];
        
    }
    if (introView.tag==4 && pageIndex == 0 ) {
        NSLog(@"the BetMeTutorial is playing and has the page %@  and the index  (add one) %ld",page,(long)pageIndex);
        //play first one
        
        NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"BetMeTutorialIntro.mp4" ofType:nil];
        NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
    
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
        [[moviePlayer view]setFrame:page.customView.frame];
        [moviePlayer setScalingMode:MPMovieScalingModeFill];
        [self.view addSubview:moviePlayer.view];
        
        moviePlayer.fullscreen = NO;
        moviePlayer.controlStyle = MPMovieControlStyleNone;

        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
        [swipeGesture setDirection:(UISwipeGestureRecognizerDirectionRight)];
        
        UIView *aView = [[UIView alloc] initWithFrame:moviePlayer.view.bounds];
        [aView addGestureRecognizer:swipeGesture];
        [moviePlayer.view addSubview:aView];
        
        
        [moviePlayer play];
        
        
    }
    if (introView.tag==4 && pageIndex == 1 ) {
        NSLog(@"the BetMeTutorial is playing and has the page %@  and the index  (add one) %ld",page,(long)pageIndex);
        //playy second one
        
        NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"BetMeTutorialVideoSuccess2.mov" ofType:nil];
        NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
        [moviePlayer setContentURL:fileURL];
        [[moviePlayer view]setFrame:page.customView.frame];
        [moviePlayer setScalingMode:MPMovieScalingModeFill];
        [page.customView addSubview:moviePlayer.view];
        moviePlayer.fullscreen = NO;
        moviePlayer.controlStyle = MPMovieControlStyleNone;
        
        [moviePlayer play];
    }
    if (introView.tag==4 && pageIndex == 2 ) {
        NSLog(@"the BetMeTutorial is playing and has the page %@  and the index  (add one) %ld",page,(long)pageIndex);
        
        NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"BetMeTutorialVideoFail3.mp4" ofType:nil];
        NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
        
        [moviePlayer setContentURL:fileURL];
        [[moviePlayer view]setFrame:page.customView.frame];
        [moviePlayer setScalingMode:MPMovieScalingModeFill];
        [page.customView addSubview:moviePlayer.view];
        moviePlayer.fullscreen = NO;
        moviePlayer.controlStyle = MPMovieControlStyleNone;
        
        [moviePlayer play];
        
    }
    if (introView.tag==4 && pageIndex == 3 ) {
        NSLog(@"the BetMeTutorial is playing and has the page %@  and the index  (add one) %ld",page,(long)pageIndex);
        
        NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"BetMeTutorialVideoScanQr.mp4" ofType:nil];
        NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
        [moviePlayer setContentURL:fileURL];
        [[moviePlayer view]setFrame:page.customView.frame];
        [moviePlayer setScalingMode:MPMovieScalingModeFill];
        [page.customView addSubview:moviePlayer.view];
        moviePlayer.fullscreen = NO;
        moviePlayer.controlStyle = MPMovieControlStyleNone;
        
        [moviePlayer play];
    }
    
    
}


- (IBAction)startIdeasTutorial:(id)sender {
    //hide status bar
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.navigationController.navigationBarHidden=YES;
    
    
    [self showIdeasTutorial];
}

//quit
- (IBAction)done:(id)sender {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        SIAlertView *didDeletePost = [[SIAlertView alloc] initWithTitle:@"Remember" andMessage:@"You can go to settings>tutorials to view this again "];
        [didDeletePost addButtonWithTitle:@"Ok" type:SIAlertViewButtonTypeDefault handler:nil];
        [didDeletePost show];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"userCompletedTutorials"];

        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
