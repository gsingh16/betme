//
//  betSelectionViewController.h
//  BetMe
//
//  Created by Admin on 9/7/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPVerticalStepper.h"
#import <ALMoviePlayerController.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AVCaptureManager.h"
#import <AVFoundation/AVFoundation.h>
#import "NZAlertView.h"
#import "NZAlertViewDelegate.h"
#import "AKTagsDefines.h"
#import "AKTagsInputView.h"
#import "JCRBlurView.h"
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface betSelectionViewController : UIViewController<RPVerticalStepperDelegate,ALMoviePlayerControllerDelegate,UITextViewDelegate,NZAlertViewDelegate,AKTagCellDelegate,AKTagsListViewDelegate,FBSDKAppInviteDialogDelegate>

@property (strong, nonatomic) IBOutlet UITableView *menuTableView;
@property (nonatomic,strong) JCRBlurView *blurView;


//should upload pop-up view
@property (strong, nonatomic) IBOutlet UIView *shouldUploadPopUpView;
//replay video and buttons
@property (strong, nonatomic) IBOutlet UIView *videoView;
- (IBAction)uploadVideo:(id)sender;
- (IBAction)discardVideo:(id)sender;





@property (nonatomic, strong) ALMoviePlayerController *moviePlayer;

@property (strong, nonatomic) IBOutlet UITextView *titleTextView;

@property (strong, nonatomic) IBOutlet UIButton *startButton;

- (IBAction)startRecording:(id)sender;


@end
