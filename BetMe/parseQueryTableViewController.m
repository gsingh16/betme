//
//  parseQueryTableViewController.m
//  BetMe
//
//  Created by Admin on 6/27/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import "parseQueryTableViewController.h"

@interface parseQueryTableViewController () <UISearchDisplayDelegate, UISearchBarDelegate> {

}
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UISearchDisplayController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@end

@interface parseQueryTableViewController ()

@end

@implementation parseQueryTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"made it to initWithCoder");
        // The className to query on
        self.parseClassName = @"Cuts";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"description";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = NO;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 10;
        
        
        
        //for the player
        //setup movie controller
        // create a movie player
        self.moviePlayer=[[ALMoviePlayerController alloc]init];
        self.moviePlayer.delegate=self;
        self.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
        //no fullscreen
        [self.moviePlayer setFullscreen:NO];
        
        self.moviePlayer.view.clipsToBounds = YES;
        [self.moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
        
        // add movie player to your view
        //[videoView addSubview:self.moviePlayer.view];
        
        ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:self.moviePlayer style:ALMoviePlayerControlsStyleNone];
        
        // assign the controls to the movie player
        [self.moviePlayer setControls:movieControls];
        
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (self.usedForMultipleSearch==NO) {
        self.title=self.passedTag;
    }
    self.pullToRefreshEnabled = NO;

    // allow search bar for multiple use
    if (self.usedForMultipleSearch==YES) {
        
        
        self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        
        self.tableView.tableHeaderView = self.searchBar;
        
        self.searchController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
        
        self.searchController.searchResultsDataSource = self;
        self.searchController.searchResultsDelegate = self;
        self.searchController.delegate = self;
        //
        CGPoint offset = CGPointMake(0, self.searchBar.frame.size.height);
        self.tableView.contentOffset = offset;
    }

    

    
    //[self clear];
    
    self.searchResults = [NSMutableArray array];
    
    if (self.passedTag) {
        [self filterResults:self.passedTag];
    }else{
        NSLog(@"no passed tag");
    }
    

    

}


#pragma mark - table view methods

-(void)filterResults:(NSString *)searchTerm {
    
    [self.searchResults removeAllObjects];
    //either search for passed tag from homeview controller, check by username from explore, and the tag again i guess from explore?
    
    
    
    PFQuery *query = [PFQuery queryWithClassName: self.parseClassName];
    NSLog(@"the query parseClassName is %@",self.parseClassName);
    [query includeKey:@"fromUser"];

    //[query whereKeyExists:@"firstName"];  //this is based on whatever query you are trying to accomplish
    //[query whereKeyExists:@"lastName"]; //this is based on whatever query you are trying to accomplish
    
    //for tags in ampersand array containing search term
    [query whereKey:@"ampersandArray" equalTo:[NSString stringWithFormat:@"%@",searchTerm]];
    //[query whereKey: @"ampersandArray" matchesRegex:[NSString stringWithFormat:@"%@",searchTerm]];
    //[query whereKey:@"ampersandArray" containsString:[NSString stringWithFormat:@"%@",searchTerm]];

    //[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      //  if (!error) {
            self.searchResults=[query findObjects];
            NSLog(@"The result(s) are %@", self.searchResults);
            NSLog(@"The number of results are %u", self.searchResults.count);
            NSLog(@"the searchTerm is %@",searchTerm);
            [self.searchResults addObjectsFromArray:self.searchResults];
            //[self loadObjects];
//            [[self.searchController searchResultsTableView] reloadData];
            
       // }
    //}];
    

    
}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterResults:searchString];
    NSLog(@"the searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString is being called");
    return YES;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        //if (tableView == self.searchDisplayController.searchResultsTableView) {
        NSLog(@"returning self.objects.count");
        return self.objects.count;
        
    } else {
        NSLog(@"returning self.searchResults.count");

        return self.searchResults.count;
        
    }
    
}



// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Cuts"];
    // If Pull To Refresh is enabled, query against the network by default.
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    [query orderByDescending:@"createdAt"];
    //adding user obj
    [query includeKey:@"fromUser"];
    
    //if using tags
    if (! [self.passedTag length] == 0) {
        [query whereKey:@"ampersandArray" equalTo:[NSString stringWithFormat:@"%@",self.passedTag]];
        NSLog(@"this should be logged tags are being passed with the passed tag %@",self.passedTag);

    }

    NSLog(@"the query called queryForTable  is running.");
    
    return query;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 442;
    
}



// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the textKey in the object,
// and the imageView being the imageKey in the object.
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
 NSLog(@"somethings going on");
 
 
 static NSString *CellIdentifier = @"postTableViewCell";
 VideoPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 
 if (cell == nil) {
 NSLog(@"the cell is null");
 [tableView registerNib:[UINib nibWithNibName:@"videoPostTableViewCellView" bundle:nil] forCellReuseIdentifier:CellIdentifier];
 cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 }
 
 // Configure the cell
 
 
 
 
 
 //should use cache!
 
  cell.postObjectID=object.objectId;
  
     
  //from vc
  cell.fromViewController=self;

 //cell video file
 
 NSLog(@"the video file url is %@,",[[object objectForKey:@"videoFile"]valueForKey:@"url"]);
 cell.videoURL=[[object objectForKey:@"videoFile"]valueForKey:@"url"];
 
 
 //cell thumbnail
 
 NSString *videoThumbnailURL= [[object objectForKey:@"videoThumbnailImage"]valueForKey:@"url"];
 NSLog(@"testing to see if i need the pffile or not with this URL! %@",[[object objectForKey:@"videoThumbnailImage"]valueForKey:@"url"]);
 
 NSLog(@"the thumnbail image is being called and allocated with the url %@",videoThumbnailURL);
 
 

[cell.videoImageView setImageWithURL:[NSURL URLWithString:videoThumbnailURL] placeholderImage:[UIImage imageNamed:@"Janet_Chicken"]];



//cell description
cell.descriptionLabel.text = [object objectForKey:@"description"];



//cell user name

cell.userDisplayName.text = [[object objectForKey:@"fromUser"]objectForKey:@"displayName"];


//cell profile image

NSURL* fbProfileImageURL=[[FBPictureHelper urlForProfilePictureSizeSmallForFBID:[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"]]absoluteURL];
NSLog(@"the fbProfileImageUrl is %@",fbProfileImageURL);


[cell.profilePictureView setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"Janet_Chicken"]];
cell.profilePictureView.layer.cornerRadius = 37.5;
cell.profilePictureView.layer.masksToBounds = YES;


//send fb and parse id to the cell, to overload the profileVC so it is ready
cell.facebookName=[[object objectForKey:@"fromUser"]objectForKey:@"displayName"];
cell.facebookID=[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"];
cell.parseUserName=[[object objectForKey:@"fromUser"]objectForKey:@"username"];
cell.parseUserObjectID=[[object objectForKey:@"fromUser"]valueForKey:@"objectId"];

NSLog(@"the parseUserObjectID for this user is in home vc  %@  with name %@" ,cell.parseUserObjectID,cell.facebookName);


//cell tags
if (![cell.tagsForTagView isEqualToArray:[object objectForKey:@"ampersandArray"]]) {
    NSLog(@"there is no cell tags the current value is %@",cell.tagsForTagView);
    cell.tagsForTagView=[object objectForKey:@"ampersandArray"];
}


//cell disable selecting ui
[cell setSelectionStyle:UITableViewCellSelectionStyleNone];

return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    //if not parse cell
    //NSLog(@"the cell's load more... is %@",[tableView cellForRowAtIndexPath:indexPath].textLabel.text);
    if (![[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"Load more..."]) {
        
        NSLog(@"the current table view cell is %@",[tableView cellForRowAtIndexPath:indexPath]);
        
        // setup data parsing block
        APSmartStorage.sharedInstance.parsingBlock = ^(NSData *data, NSURL *url)
        {
            return data;
        };
        
        
        [[APSmartStorage sharedInstance]loadObjectWithURL:[NSURL URLWithString:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoURL"]] storeInMemory:NO completion:^(id object, NSError * error) {
            //
            if (!error) {
                NSLog(@"the object is obtained and is ");
                //ADD VIDEO FILE TO CELL HERE FROM CACHE! OR REFRENCE!!
                [self.moviePlayer stop];
                
                NSString*fileName=[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoURL"]lastPathComponent];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
                
                if(![[NSFileManager defaultManager] fileExistsAtPath: path]){
                    
                    [object writeToFile:path atomically:YES];
                    NSLog(@"file does not exist at path, therefore writing it now");
                }else{
                    NSLog(@"file does  exist at path, therefore; not writing it");
                }
                
                NSURL *moveUrl = [NSURL fileURLWithPath:path];
                NSLog(@"the movie path is %@",moveUrl);
                [self.moviePlayer setContentURL:moveUrl];
                [self.moviePlayer setFrame:CGRectMake([tableView cellForRowAtIndexPath:indexPath].frame.origin.x, [tableView cellForRowAtIndexPath:indexPath].frame.origin.y, 320, 301)];
                
                [self.view addSubview:self.moviePlayer.view];
                
                //dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                
            }else
                NSLog(@"the error is  %@",error);
        }];
        
        NSLog(@"the table cell's url is %@",[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoURL"]);
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
