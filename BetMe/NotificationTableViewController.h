//
//  NotificationTableViewController.h
//  BetMe
//
//  Created by Admin on 4/25/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import <ParseUI/ParseUI.h>
#import <UIKit/UIKit.h>
#import "IndividualPostViewController.h"

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <Parse/Parse.h>
#import "NotificationTableViewCell.h"
@interface NotificationTableViewController : PFQueryTableViewController

@property (strong, nonatomic) IBOutlet UIView *headerView;
- (IBAction)closeNotificationController:(id)sender;

@end
