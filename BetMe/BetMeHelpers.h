//
//  BetMeHelpers.h
//  BetMeFinal
//
//  Created by Admin on 7/17/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RESideMenu.h"
#import <UIKit/UIKit.h>


@interface BetMeHelpers : NSObject  < UIAlertViewDelegate>

@property (strong, readonly, nonatomic) RESideMenu *sideMenu;
@property  __block UINavigationController *masterNavigationController;
@property (nonatomic,strong) dispatch_block_t menuBlock;
@property (nonatomic,strong) UIViewController* viewControllerFromClass;

-(id)initWithViewController:(UIViewController*)viewController;

-(dispatch_block_t)menuReturnForNavController:(UINavigationController*)controller;
@end
