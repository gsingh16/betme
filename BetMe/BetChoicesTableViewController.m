//
//  JGTestViewController.m
//  JGScrollableTableViewCell Examples
//
//  Created by Jonas Gessner on 03.11.13.
//  Copyright (c) 2013 Jonas Gessner. All rights reserved.
//

#import "BetChoicesTableViewController.h"
#import "JGScrollableTableViewCell.h"
#import "JGScrollableTableViewCellAccessoryButton.h"
#import "KNThirdViewController.h"
#import "UIViewController+KNSemiModal.h"

@interface BetChoicesTableViewController () <JGScrollableTableViewCellDelegate> {
    NSIndexPath *_openedIndexPath;
    BOOL _left;
    KNThirdViewController * semiVC;

}
@property(nonatomic,assign)int currentRowItemIntValue;
@property(nonatomic,strong)NSMutableDictionary* betChoicesKeyValue;
@property(nonatomic,strong)NSString*betTitle;
@property(nonatomic,strong)NSString*betDescription;

@end

@implementation BetChoicesTableViewController
@synthesize currentRowItemIntValue=_currentRowItemIntValue;
@synthesize betChoicesKeyValue=_betChoicesKeyValue;
@synthesize betTitle=_betTitle;
@synthesize betDescription=_betDescription;

- (instancetype)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {

        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [self.tableView registerClass:[JGScrollableTableViewCell class] forCellReuseIdentifier:@"ScrollCell"];
        
        UISwitch *s = [[UISwitch alloc] init];
        [s addTarget:self action:@selector(switched:) forControlEvents:UIControlEventValueChanged];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:s];
        
        self.title = @"Pick a Challenge";
        
        printf("NOTE: Real world implementations should use subclasses of JGScrollableTableViewCell to manage and display more content in the cell\n");
        
        //creates bet number index to descrition
        
        NSMutableDictionary*betDict = [[NSMutableDictionary alloc]init];
        
        [betDict setValue:@"Bet with your friend to see who can do more push-ups, or who can hit a certain amount of push-ups in a given time, faster." forKey:@"1"];
        [betDict setValue:@"Bet with your friend to see who can hold their breath longer without breathing." forKey:@"2"];
        [betDict setValue:@"Bet with your friend and see who gets a desired outcome from flipping a two-faced coin." forKey:@"3"];
        [betDict setValue:@"Bet with your friend to see who make more free-throws out of a number of tries, or who can make more freethrows in a given time." forKey:@"4"];
        [betDict setValue:@"Bet with your friend to see who can eat more of something, or who can eat the food, faster." forKey:@"5"];
        [betDict setValue:@"Bet with your friend to see who can get a stranger to give them a high-five" forKey:@"6"];
        [betDict setValue:@"Bet your friend to see who can build a paper-plane faster." forKey:@"7"];
        [betDict setValue:@"Bet your friend and see if they can lick their elbow." forKey:@"8"];
        
        _betChoicesKeyValue=betDict;
    
        
    }
    return self;
}


- (void)switched:(UISwitch *)sender {
    _left = sender.on;
    [self.tableView reloadData];
}


#pragma mark - JGScrollableTableViewCellDelegate

- (void)cellDidBeginScrolling:(JGScrollableTableViewCell *)cell {
    [JGScrollableTableViewCellManager closeAllCellsWithExceptionOf:cell stopAfterFirst:YES];
    // do stuff here cell;
}

- (void)cellDidScroll:(JGScrollableTableViewCell *)cell {
    [JGScrollableTableViewCellManager closeAllCellsWithExceptionOf:cell stopAfterFirst:YES];
}

- (void)cellDidEndScrolling:(JGScrollableTableViewCell *)cell {
    if (cell.optionViewVisible) {
        _openedIndexPath = [self.tableView indexPathForCell:cell];
        NSLog(@"THE CURRENT ROW IS %i",_openedIndexPath.row);
        
        _currentRowItemIntValue=_openedIndexPath.row;
    }
    else {
        _openedIndexPath = nil;
    }
    
    [JGScrollableTableViewCellManager closeAllCellsWithExceptionOf:cell stopAfterFirst:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 99;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const CellIdentifier = @"ScrollCell";
    

    
    JGScrollableTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //this is actually a terrible implementation of JGScrollableTableViewCell, it kills performance. But it shows nicely how to use the class. In real implementations use custom subclasses of JGScrollableTableViewCell that handle content views & properties internally.
    cell.respectiveToTableViewIndexValue=indexPath.item;
    //test
   // cell.textLabel.text=@"adobo";

    
    [cell setScrollViewBackgroundColor:[UIColor colorWithWhite:0.975f alpha:1.0f]];
    [cell setScrollViewInsets:UIEdgeInsetsMake(0.0f, 1.0f, 1.0f, 1.0f)];
    cell.contentView.backgroundColor = [UIColor colorWithWhite:0.7f alpha:1.0f];
    
    //adds display units
    NSString* currentBet=[NSString stringWithFormat:@"bet%i",indexPath.item+1];
    UIImage *image = [UIImage imageNamed:currentBet];
    UIImageView*cellView=[[UIImageView alloc]initWithImage:image];

    _betTitle=currentBet;
    
    [cell addContentView:cellView];
   
    
    JGScrollableTableViewCellAccessoryButton *actionView = [JGScrollableTableViewCellAccessoryButton button];
    [actionView addTarget:self action:@selector(pickCellButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [actionView setButtonColor:[UIColor colorWithRed:0.975f green:0.0f blue:0.0f alpha:1.0f] forState:UIControlStateNormal];
    [actionView setButtonColor:[UIColor colorWithRed:0.8f green:0.1f blue:0.1f alpha:1.0f] forState:UIControlStateHighlighted];
    
    [actionView setTitle:@"Pick" forState:UIControlStateNormal];
    
    actionView.frame = CGRectMake(80.0f, 0.0f, 80.0f, 0.0f); //width is the only frame parameter that needs to be set on the option view
    actionView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    
    JGScrollableTableViewCellAccessoryButton *moreView = [JGScrollableTableViewCellAccessoryButton button];
    [moreView addTarget:self action:@selector(moreCellButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [moreView setButtonColor:[UIColor colorWithWhite:0.8f alpha:1.0f] forState:UIControlStateNormal];
    [moreView setButtonColor:[UIColor colorWithWhite:0.65f alpha:1.0f] forState:UIControlStateHighlighted];
    
    [moreView setTitle:@"More" forState:UIControlStateNormal];
    
    moreView.frame = CGRectMake(0.0f, 0.0f, 80.0f, 0.0f); //width is the only frame parameter that needs to be set on the option view
    moreView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    
    UIView *optionView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 0.0f)];
    
    [optionView addSubview:moreView];
    [optionView addSubview:actionView];
    
    if ((indexPath.row % 1) != 0) {
        UIView *grabber = [[UIView alloc] initWithFrame:(CGRect){CGPointZero, {20.0f, 35.0f}}];
        
        UIView *dot1 = [[UIView alloc] initWithFrame:(CGRect){{10.0f, 5.0f}, {5.0f, 5.0f}}];
        
        UIView *dot2 = [[UIView alloc] initWithFrame:(CGRect){{10.0f, 15.0f}, {5.0f, 5.0f}}];
        
        UIView *dot3 = [[UIView alloc] initWithFrame:(CGRect){{10.0f, 25.0f}, {5.0f, 5.0f}}];
        
        dot1.backgroundColor = [UIColor colorWithWhite:0.6f alpha:1.0f];
        dot2.backgroundColor = [UIColor colorWithWhite:0.6f alpha:1.0f];
        dot3.backgroundColor = [UIColor colorWithWhite:0.6f alpha:1.0f];
        
        [grabber addSubview:dot1];
        [grabber addSubview:dot2];
        [grabber addSubview:dot3];
        
        [cell setGrabberView:grabber];
    }
    else {
        [cell setGrabberView:nil];
    }
    
    [cell setOptionView:optionView side:(_left ? JGScrollableTableViewCellSideLeft : JGScrollableTableViewCellSideRight)];
    
    cell.scrollDelegate = self;

    [cell setOptionViewVisible:[_openedIndexPath isEqual:indexPath]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Selected Index Path %i", indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)pickCellButtonPressed:(int)atIntexPath{
    NSLog(@"pick button cell pressed at index %i",_currentRowItemIntValue);
    //send delegate to previous view controller about bet
    NSString* currentBetDescription=[_betChoicesKeyValue valueForKey:[NSString stringWithFormat:@"%i",_currentRowItemIntValue+1]];
    
    _betDescription=currentBetDescription;
    _betTitle=@"sample title";

    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"recievedBetDescription"
     object:_betDescription];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"recievedBetTitle"
     object:_betTitle];
    
}



-(void)moreCellButtonPressed:(int)atIntexPath{
    
    NSLog(@"moreCellButton Pressed at index %i ", _currentRowItemIntValue);
    //Handle showing description regarding bet
    
    // You can also present a UIViewController with complex views in it
    // and optionally containing an explicit dismiss button for semi modal
    semiVC = [[KNThirdViewController alloc] initWithNibName:@"KNThirdViewController" bundle:nil];
    
   
    
    NSString* currentBetDescription=[_betChoicesKeyValue valueForKey:[NSString stringWithFormat:@"%i",_currentRowItemIntValue+1]];
    semiVC.helpLabelText=currentBetDescription;
    

    [self.navigationController presentSemiViewController:semiVC withOptions:@{
                                                         KNSemiModalOptionKeys.pushParentBack    : @(YES),
                                                         KNSemiModalOptionKeys.animationDuration : @(.6),
                                                         KNSemiModalOptionKeys.shadowOpacity     : @(0.5),
                                                         }];
}




//PFQUERYTABLEVIEWVONTROLLER STUFF
#pragma mark-PFQUERYTABLEVIEWVONTROLLER STUFF
/*

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // This table displays items in the Todo class
        self.className = @"Todo";
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = NO;
        self.objectsPerPage = 25;
    }
    return self;
}

- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:self.className];
    
    // If no objects are loaded in memory, we look to the cache
    // first to fill the table and then subsequently do a query
    // against the network.
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByDescending:@"createdAt"];
    
    return query;
}


 - (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                        object:(PFObject *)object {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell to show todo item with a priority at the bottom
    cell.textLabel.text = [object objectForKey:@"text"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Priority: %@",
                                 [object objectForKey:@"priority"]];
    
    return cell;
}
*/


@end
