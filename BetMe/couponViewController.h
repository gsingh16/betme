//
//  couponViewController.h
//  BetMe
//
//  Created by Admin on 7/6/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "StyledPullableView.h"
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>
#import "TableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import <SDWebImageDownloader.h>
#import "MZRSlideInMenu.h"
#import "SettingsViewController.h"


@interface couponViewController : PFQueryTableViewController<PullableViewDelegate,MKMapViewDelegate,UITableViewDataSource, UITableViewDelegate,CLLocationManagerDelegate,MZRSlideInMenuDelegate> {
    //StyledPullableView *pullDownView;
    
    StyledPullableView *pullUpView;
    UILabel *pullUpLabel;
    
}

@property (strong, readonly, nonatomic) RESideMenu *sideMenu;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

//@property (strong, nonatomic) IBOutlet UITableView *couponTableView;

- (void)receiveEvent:(NSNotification *)notification;

@end
