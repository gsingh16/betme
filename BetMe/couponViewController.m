//
//  couponViewController.m
//  BetMe
//
//  Created by Admin on 7/6/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


#import "couponViewController.h"
#import "homeViewController.h"
#import "MainViewController.h"
#import "FlipsideViewController.h"
#include "RESideMenu.h"
#import "RESideMenu.h"
#import "betViewController1.h"
#import "profileViewController.h"
#import "couponViewController.h"
#import "betSelectionViewController.h"
#import "JPSThumbnailAnnotation.h"
#import "TGLViewController.h"
#import "DealsTableViewController.h"
#import "CameraViewController.h"
#import "SIAlertView.h"
#import "Flurry.h"
@interface couponViewController  () <UIAlertViewDelegate>{
    CLLocationManager *locationManager;
    

}
@property  __block UINavigationController *masterNavigationController;
@property (nonatomic,strong) dispatch_block_t menuBlock;
@property (nonatomic,strong) UIButton* recordButton;
//viewControllers
@property(nonatomic,strong)homeViewController*homeVC;
@property(nonatomic,strong)profileViewController*profileVC;
@property(nonatomic,strong)couponViewController*couponVC;
@property(nonatomic,strong)SettingsViewController*settingsVC;

@end


@implementation couponViewController
@synthesize sideMenu=_sideMenu;
@synthesize homeVC=_homeVC,profileVC=_profileVC,couponVC=_couponVC,settingsVC=_settingsVC;
@synthesize recordButton;


- (id)initWithCoder:(NSCoder *)aDecoder
{
    //self = [super initWithClassName:@"SmallBusiness"];
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"made it to initWithCoder of coupon vc");
        // The className to query on
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 15;
        

        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nibb

    self.title = @"Deals";
    
    UIColor *white = [UIColor whiteColor];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:white forKey:NSForegroundColorAttributeName ];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    //status bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //menu bar button
    UIImage *menuImage = [UIImage imageNamed:@"menu"];
    CGRect menuFrame = CGRectMake(0, 0, menuImage.size.width/2.5, menuImage.size.height/3);
    UIButton* menuButton = [[UIButton alloc] initWithFrame:menuFrame];
    [menuButton setBackgroundImage:menuImage forState:UIControlStateNormal];
    [menuButton setShowsTouchWhenHighlighted:YES];
    [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* menu = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menu;
    
    
    //record bar button
    UIImage *image = [UIImage imageNamed:@"notificationIconWhite"];
    CGRect frame = CGRectMake(0, 0, 35, 30);
    UIButton* button = [[UIButton alloc] initWithFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setShowsTouchWhenHighlighted:YES];
    [button addTarget:self action:@selector(openNotifications) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* record = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    //wallet bar button
    UIImage *image2 = [UIImage imageNamed:@"wallet"];
    CGRect frame2 = CGRectMake(0, 0, 20, 20);
    UIButton* button2 = [[UIButton alloc] initWithFrame:frame2];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [button2 setShowsTouchWhenHighlighted:YES];
    [button2 addTarget:self action:@selector(showDealsWallet) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* wallet = [[UIBarButtonItem alloc] initWithCustomView:button2];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:wallet ,record,nil];
    
    
    
    //record icon
    recordButton=[[UIButton alloc]initWithFrame:CGRectMake(130, 700, 50, 50)];
    [recordButton setBackgroundImage:[UIImage imageNamed:@"centerVideoIcon"] forState:UIControlStateNormal];
    [recordButton addTarget:self action:@selector(startNewVideo) forControlEvents:UIControlEventTouchDown];
    [self.parentViewController.view addSubview:recordButton];
    
    [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [recordButton setFrame:CGRectMake(130, self.view.frame.size.height-30, 50, 50)];
        
    } completion:^(BOOL finished) {
        
    }];
    
    
    
    //color
    self.navigationController.navigationBar.barTintColor = [UIColor orangeColor];
    self.navigationController.navigationBar.translucent=YES;
    
    
    
    CGFloat xOffset = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        xOffset = 224;
    }
    
    
    /*
    //not using for now, tableview first, then when flipped, use below
    [self performSelector:@selector(zoomInToMyLocation)
               withObject:nil
               afterDelay:2]; //will zoom in after 5 seconds
    */
    
    
    //initLocation manager
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    
    if(IS_OS_8_OR_LATER) {
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    
    
    [locationManager startUpdatingLocation];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"locationSelected"
                                               object:nil];

    
    [self loadObjects];
}


- (void)receiveEvent:(NSNotification *)notification {
    // handle event
    
    if ([[notification name]isEqualToString:@"locationSelected"]) {
        NSLog(@"location selected in coupon vc :) <3 ");
        NSLog(@"got event %@", notification);
       [self.navigationController pushViewController:notification.object animated:YES];
        
    }

}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
}


#pragma mark - menu buttons
#pragma mark- menu buttons

- (void)showMenu
{
    //[self.sideMenuViewController presentMenuViewController];
    MZRSlideInMenu *menu = [[MZRSlideInMenu alloc] init];
    [menu setDelegate:self];
    [menu addMenuItemWithTitle:@"Home"];
    [menu addMenuItemWithTitle:@"Profile"];
    [menu addMenuItemWithTitle:@"Deals"];
    [menu addMenuItemWithTitle:@"Settings"];
    [menu showMenuFromLeft];
}

- (void)openNotifications{
    NotificationTableViewController*notificationTBVC=[[NotificationTableViewController alloc]init];
    notificationTBVC.parseClassName=@"Notifications";
    [self presentViewController:notificationTBVC animated:YES completion:nil];
}



- (void)slideInMenu:(MZRSlideInMenu *)menuView didSelectAtIndex:(NSUInteger)index
{
    NSString *buttonTitle = [menuView buttonTitleAtIndex:index];
    
    
    
    switch ((long)index) {
        case 0:
        {
            //0
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if(!_homeVC){
                
                homeViewController*home=[[homeViewController alloc]initWithCoder:nil];
                UINavigationController*homeVC=[[UINavigationController alloc] initWithRootViewController:home];
                _homeVC=homeVC;
                NSLog(@"homeVC is being created");
                
            }
            
            /*
             [self.navigationController setViewControllers:@[_homeVC]];
             [self.navigationController popViewControllerAnimated:YES];
             */
            [self presentViewController:_homeVC animated:YES completion:nil];
            
            
        }
            break;
            
        case 1:
        {
            
            if ([PFUser currentUser]==NULL) {
                NSLog(@"current user is anon %@",[PFUser currentUser]);
                SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
                [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
                [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];

                //yes
                [alertView addButtonWithTitle:@"Ok"
                                         type:SIAlertViewButtonTypeDestructive
                                      handler:^(SIAlertView *alert) {
                                          NSLog(@"Button1 Clicked");
                                          [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                          [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                          //yes
                                          //analytics
                                          MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                          // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                          [self presentViewController:main animated:YES completion:^{
                                              NSLog(@"successfully presented home view controller from mainview controller");
                                          }];
                                          
                                          
                                          
                                      }];
                
                [alertView addButtonWithTitle:@"Cancel"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          NSLog(@"Button2 Clicked");
                                          //analytics
                                      }];
                
                alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                
                [alertView show];
            }else{
                if (!_profileVC) {
                    NSLog(@"profileVC is being created");
                    
                    [FBSDKAppEvents logEvent:@"User Wants to see their profile"];
                    [Flurry logEvent:@"User Wants to see their profile"];
                    
                    
                    profileViewController*profile=[[profileViewController alloc] initWithCoder:nil];
                    profile.usedForDisplayingCurrentUser=YES;
                    profile.facebookName=[[PFUser currentUser]objectForKey:@"displayName"];
                    profile.facebookID=[[PFUser currentUser]objectForKey:@"facebookId"];
                    profile.parseUserName=[[PFUser currentUser]objectForKey:@"username"];
                    profile.parseUserObjectID=[PFUser currentUser].objectId;
                    UINavigationController*profileVC=[[UINavigationController alloc] initWithRootViewController:profile];
                    _profileVC=profileVC;
                    
                }
                
                
                [self presentViewController:_profileVC animated:YES completion:nil];
            }
        }
            break;
            
        case 2:
        {
            //2
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            /*
            if (!_couponVC) {
             
                _couponVC=[[couponViewController alloc]initWithCoder:nil];
                UINavigationController*couponsVC=[[UINavigationController alloc] initWithRootViewController:_couponVC];
                _couponVC=couponsVC;
                
                NSLog(@"couponsVC is being created");
                
            }
            
            [self presentViewController:_couponVC animated:YES completion:nil];
            // [self.navigationController setViewControllers:@[_couponVC]];
            // [self.navigationController popViewControllerAnimated:YES];
            //[self.navigationController pushViewController:_couponVC animated:YES];
            */
        }
            
            break;
        case 3:
        {
            //3
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if (!_settingsVC) {
                
                [FBSDKAppEvents logEvent:@"User Wants to see the settings page"];
                [Flurry logEvent:@"User Wants to see the settings page"];
                
                SettingsViewController*settings=[[SettingsViewController alloc]initWithCoder:nil];
                UINavigationController*settingsVC=[[UINavigationController alloc] initWithRootViewController:settings];
                _settingsVC=settingsVC;
                NSLog(@"settingsVC is being created");
                
            }
            /*
             [self.navigationController setViewControllers:@[_settingsVC]];
             [self.navigationController popViewControllerAnimated:YES];
             */
            [self presentViewController:_settingsVC animated:YES completion:nil];
            
            
        }
            break;
            
        default:
            
            break;
    }
    
}

- (void)startNewVideo{
    //check for anon
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:^{
                                      NSLog(@"successfully presented home view controller from mainview controller");
                                  }];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to start a new video"];
        [Flurry logEvent:@"User wants to start a new video"];
        
        
        CameraViewController*betPage=[[CameraViewController alloc]init];
        //    betSelectionViewController*betPage=[[betSelectionViewController alloc]init];
        self.navigationController.navigationBarHidden = NO;
        //    [self.navigationController prese:betPage animated:YES];
        [self.navigationController presentViewController:betPage animated:YES completion:nil];
    }
}






- (void)showDealsWallet{
    //check for anon
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:^{
                                      NSLog(@"successfully presented home view controller from mainview controller");
                                  }];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to see their deal wallet"];
        [Flurry logEvent:@"User wants to see their deal wallet"];
        
        //current way
        NSLog(@"the current user is %@",[PFUser currentUser].objectId);
        
        //get users points
        PFQuery *query = [PFQuery queryWithClassName:@"_User"];
        [query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
        query.cachePolicy = kPFCachePolicyNetworkElseCache;
        [query includeKey:@"deals.fromBusiness"];
        
        
        //start query
        __block TGLViewController*vc;
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                NSArray*deals=[object objectForKey:@"deals"];
                //NSLog(@"the deals are in dealsinWallet %@",deals);
                
                //load the controller now
                UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
                // [aFlowLayout setItemSize:CGSizeMake(400, 400)];
                [aFlowLayout setItemSize:CGSizeMake(320, 463)];
                [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                vc = [[TGLViewController alloc]initWithCollectionViewLayout:aFlowLayout];
                vc.dealsInWallet=[[NSMutableArray alloc]initWithArray:deals];;
                
                
                self.navigationController.navigationBarHidden=NO;
                [self.navigationController pushViewController:vc animated:YES];
                
            }else
                NSLog(@"error : %@",error);
        }];
        
    }
    
    
}



#pragma mark -generate Annotations


-(void)zoomInToMyLocation
{
    /*
     MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
     region.center.latitude = 51.502729 ;
     region.center.longitude = -0.071948;
     region.span.longitudeDelta = 0.15f;
     region.span.latitudeDelta = 0.15f;
     */
    self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    
    MKUserLocation *userLocation = self.mapView.userLocation;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (
                                                                    userLocation.location.coordinate, 100, 100);
    region.span = MKCoordinateSpanMake(0.1, 0.1);
    
    
    [self.mapView setRegion:region animated:YES];
    
    //generate annotations
    [self.mapView addAnnotations:[self generateAnnotations]];

    [self.view addSubview:self.mapView];

    NSLog(@"the current view is %@",self.view);
}


 -(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
 {
 MKCoordinateRegion mapRegion;
 mapRegion.center = mapView.userLocation.coordinate;
 mapRegion.span.latitudeDelta = 0.1;
 mapRegion.span.longitudeDelta = 0.1;
 
 [mapView setRegion:mapRegion animated: YES];
 NSLog(@"did update user location");
 }



- (NSArray *)generateAnnotations {
    NSMutableArray *annotations = [[NSMutableArray alloc] initWithCapacity:20];
    
    
    // Empire State Building
    JPSThumbnail *empire = [[JPSThumbnail alloc] init];
    empire.image = [UIImage imageNamed:@"empire.jpg"];
    empire.title = @"Empire State Building";
    empire.subtitle = @"NYC Landmark";
    empire.coordinate = CLLocationCoordinate2DMake(40.75, -73.99);
    empire.disclosureBlock = ^{ NSLog(@"selected Empire"); };
    
    [annotations addObject:[[JPSThumbnailAnnotation alloc] initWithThumbnail:empire]];
    
    // Kanes Diner
    JPSThumbnail *kanesDiner = [[JPSThumbnail alloc] init];
    kanesDiner.image = [UIImage imageNamed:@"kanesDinerImage.png"];
    kanesDiner.title = @"Kane's Diner";
    kanesDiner.subtitle = @"Best Steak and Eggs";
    kanesDiner.coordinate = CLLocationCoordinate2DMake(40.753323, -73.832379);
    kanesDiner.disclosureBlock = ^{ NSLog(@"selected Kanes Diner"); };
    
    [annotations addObject:[[JPSThumbnailAnnotation alloc] initWithThumbnail:kanesDiner]];
    
    
    // New Tokyo
    JPSThumbnail *newTokyo = [[JPSThumbnail alloc] init];
    newTokyo.image = [UIImage imageNamed:@"newTokyoImage.png"];
    newTokyo.title = @"New Tokyo Restaurant";
    newTokyo.subtitle = @"Best Sushi around";
    newTokyo.coordinate = CLLocationCoordinate2DMake(40.721122, -73.805292);
    newTokyo.disclosureBlock = ^{ NSLog(@"selected New Tokyo Restaurant"); };
    
    [annotations addObject:[[JPSThumbnailAnnotation alloc] initWithThumbnail:newTokyo]];
    
    
    // Apple HQ
    JPSThumbnail *apple = [[JPSThumbnail alloc] init];
    apple.image = [UIImage imageNamed:@"apple.jpg"];
    apple.title = @"Apple HQ";
    apple.subtitle = @"Apple Headquarters";
    apple.coordinate = CLLocationCoordinate2DMake(37.33, -122.03);
    apple.disclosureBlock = ^{ NSLog(@"selected Appple"); };
    
    [annotations addObject:[[JPSThumbnailAnnotation alloc] initWithThumbnail:apple]];
    
    // Parliament of Canada
    JPSThumbnail *ottawa = [[JPSThumbnail alloc] init];
    ottawa.image = [UIImage imageNamed:@"ottawa.jpg"];
    ottawa.title = @"Parliament of Canada";
    ottawa.subtitle = @"Oh Canada!";
    ottawa.coordinate = CLLocationCoordinate2DMake(45.43, -75.70);
    ottawa.disclosureBlock = ^{ NSLog(@"selected Ottawa"); };
    
    [annotations addObject:[[JPSThumbnailAnnotation alloc] initWithThumbnail:ottawa]];
    
    return annotations;
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didSelectAnnotationViewInMap:mapView];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation conformsToProtocol:@protocol(JPSThumbnailAnnotationProtocol)]) {
        return [((NSObject<JPSThumbnailAnnotationProtocol> *)annotation) annotationViewInMap:mapView];
    }
    return nil;
}






#pragma mark- uitableview delegate method override
#pragma mark - PFQueryTableViewController

- (void)objectsWillLoad {
    [super objectsWillLoad];
    // This method is called before a PFQuery is fired to get more objects
    NSLog(@"the objects will load is running");

}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    // This method is called every time objects are loaded from Parse via the PFQuery
    NSLog(@"the objects did load is running");
}



// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    
    NSLog(@"the query for table is running inside the coupon vc");
    PFQuery *query = [PFQuery queryWithClassName:@"SmallBusiness"];
    // If Pull To Refresh is enabled, query against the network by default.
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyNetworkElseCache;
        NSLog(@"the object count in coupon vc is 0");
    }
   // [query orderByDescending:@"createdAt"];
    [query whereKey:@"Location" nearGeoPoint:[PFGeoPoint geoPointWithLocation:locationManager.location]];
    //[query whereKey:@"isCurrentlyLive" equalTo:[NSNumber numberWithInt:1]]; //true
    [query whereKey:@"isCurrentlyLive" equalTo:@1];
    [query includeKey:@"deals.fromBusiness"];
    
    
    return query;
}



// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the textKey in the object,
// and the imageView being the imageKey in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    
    
    
    static NSString *CellIdentifier = @"smallBusinessCell";
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"TableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    //location cover image
    NSString* url = [object valueForKey:@"coverPictureURL"];
    NSURL* NSUrl = [NSURL URLWithString:url];
    [cell.coverImage setImageWithURL:NSUrl];
    
    //round img
    cell.coverImage.layer.cornerRadius = 20;
    cell.coverImage.layer.masksToBounds = YES;

    //location object ref
    cell.locationObjectID=[object valueForKeyPath:@"objectId"];
    
    //location background images
    cell.backgroundImages=[object objectForKey:@"locationImageURLs"];
    
    //location adress
    cell.locationAdress=[object objectForKey:@"locationAdress"];
    
    //location deals
    cell.deals=[object objectForKey:@"deals"];
    
    //location phone number
    cell.locationPhoneNumber=[object objectForKey:@"locationPhoneNumber"];
    
    cell.locationNeighborhoodLabel.text=[object objectForKey:@"locationNeighborhood"];
    
  //  NSLog(@"the deals are %@",cell.deals);
    
    //location name
    cell.locationNameLabel.text = [object objectForKey:@"locationName"];
    
    //location type
    cell.locationTypeLabel.text = [object objectForKey:@"locationType"];
    
    //location
    PFGeoPoint*location=[object objectForKey:@"Location"];
    cell.locationCoordinate=location;
    NSString* locationDistance=[self returnLocationWithLatitude:location.latitude andLongitude:location.longitude];
    
    
    NSLog(@"the location distance is %@ ",locationDistance);
    cell.locationDistanceLabel.text=[NSString stringWithFormat:@"%@",locationDistance];
    
    //more info url
    cell.locationMoreInfoURL=[object objectForKey:@"moreInfoURL"];
    //NSLog(@"the from user data is %@, and the whole object is %@", [object objectForKey:@"fromUser"],object);
    
    //GETTING THE FROM USER NAME POINTER RELATION   NSLog(@"the fromUser is %@",[[object objectForKey:@"fromUser"] );
    
//also add each obj to addAnnotations here
    //eg: jpsthumnail.objectID= ....
    
    
    //is live?
    NSLog(@"the bool value for isCurrentlyLive is %@",[object valueForKey:@"isCurrentlyLive"]);
    
    //cell disable ui
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (![[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"Load more..."]) {

    UITableViewCell*thisCell=[tableView cellForRowAtIndexPath:indexPath];
    
    DealsTableViewController*locationTableViewController=[[DealsTableViewController alloc]initWithNibName:@"DealsTableViewController" bundle:nil];
    locationTableViewController.locationImageURLsArray=[thisCell valueForKeyPath:@"backgroundImages"];
    locationTableViewController.locationName=[[thisCell valueForKeyPath:@"locationNameLabel"]text];
    locationTableViewController.locationAdress=[thisCell valueForKeyPath:@"locationAdress"];
    locationTableViewController.locationDistance=[[thisCell valueForKeyPath:@"locationDistanceLabel"]text];
    locationTableViewController.locationDeals=[thisCell valueForKeyPath:@"deals"];;
    locationTableViewController.locationCoordinate=[thisCell valueForKeyPath:@"locationCoordinate"];
    locationTableViewController.locationMoreInfoURL=[thisCell valueForKeyPath:@"locationMoreInfoURL"];
    locationTableViewController.locationPhoneNumber=[thisCell valueForKeyPath:@"locationPhoneNumber"];;
    
    [self.navigationController pushViewController:locationTableViewController animated:YES];
    }else
    {
        NSLog(@"user wants to load more cells in the coupon vc");
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
    
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
    
}


//footer


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

    
    UIView*bottomView=[[UIView alloc]initWithFrame:CGRectMake(0, tableView.frame.size.height, 320, 50)];
    [bottomView setBackgroundColor:[UIColor orangeColor]];

    return bottomView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    
    //changig to 0 so not using
    return 25;
}



#pragma mark- calculate distance
- (NSString*)returnLocationWithLatitude:(double)latitude
                        andLongitude:(double)longitude{
    
    CLLocation *currentLoc = locationManager.location;
    NSLog(@"the current location is %@",currentLoc);
    
    CLLocation *restaurnatLoc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    //NSLog(@"the restaurant location is %@",restaurnatLoc);

    CLLocationDistance meters = [restaurnatLoc distanceFromLocation:currentLoc];
    
    NSString*calculatedMiles=[NSString stringWithFormat:@"%.1fmi",(meters/1609.344)];
   // NSLog(@"calculated miles are %@", calculatedMiles);
    
    return calculatedMiles;
}

- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
}


#pragma mark - cleanup
#pragma mark- cleanup

- (void)didReceiveMemoryWarning
{
    NSLog(@"getting memory warning ~ %s",__PRETTY_FUNCTION__);
    
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    NSLog(@"viewDidUnload");
}


- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    NSLog(@"viewWillDissaper in couponvc");
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [self.recordButton setFrame:CGRectMake(130, self.view.frame.size.height+90, 50, 50)];
        
    } completion:^(BOOL finished) {
        
    }];

    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDissapear in coupon vc");
    
    //[self.navigationController removeFromParentViewController];
   // [self.navigationController popViewControllerAnimated:YES];
    
    [APSmartStorage.sharedInstance removeAllFromMemory];
    //self.tableView=nil;
    //[self clear];
    
}


@end
