//
//  CommentsViewController.h
//  
//
//  Created by Admin on 4/8/15.
//
//

#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import "FBPictureHelper.h"
#import "AFNetworking.h"


@interface CommentsViewController : PFQueryTableViewController <UITableViewDelegate>

@property PFObject*postObject;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@end
