//
//  SettingsViewController.h
//  BetMe
//
//  Created by Admin on 8/3/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KHFlatButton.h>
#import "MZRSlideInMenu.h"
#import "MainViewController.h"
#import "betViewController1.h"
#import "profileViewController.h"
#import "couponViewController.h"
#import "betSelectionViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "couponViewController.h"
#import "homeViewController.h"
@class KHFlatButton;


@interface SettingsViewController : UIViewController<MZRSlideInMenuDelegate>




@property (strong, nonatomic) IBOutlet KHFlatButton *feedBackButton;
@property (strong, nonatomic) IBOutlet KHFlatButton *termsAndConditionsButton;
@property (strong, nonatomic) IBOutlet KHFlatButton *privacyPolicyButton;
@property (strong, nonatomic) IBOutlet KHFlatButton *attributionButton;
@property (strong, nonatomic) IBOutlet KHFlatButton *tutorialsButton;
@property (strong, nonatomic) IBOutlet KHFlatButton *logOutButton;

//icon
@property (strong, nonatomic) IBOutlet UIButton *appIcon;

- (IBAction)feedback:(id)sender;
- (IBAction)termsAndConditions:(id)sender;
- (IBAction)privacyPolicy:(id)sender;
- (IBAction)attribution:(id)sender;
- (IBAction)tutorials:(id)sender;
- (IBAction)logOut:(id)sender;




@end
