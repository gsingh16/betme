//
//  IndividualPostViewController.h
//  BetMe
//
//  Created by Admin on 5/27/15.
//  Copyright (c) 2015 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndividualPostViewController.h"
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "RESideMenu.h"
#import "MessageViewController.h"
#include "RESideMenu.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Parse/Parse.h>
#import <ALMoviePlayerController.h>
#import <ALMoviePlayerControls.h>
#import "MZRSlideInMenu.h"
#import "couponViewController.h"
#import "SettingsViewController.h"
#import "PBJVideoPlayerController.h"
#import "APFileStorage.h"
#import "HMSideMenu.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "NotificationTableViewController.h"
#import <Parse/Parse.h>
#import "FBPictureHelper.h"
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface IndividualPostViewController : UIViewController<ALMoviePlayerControllerDelegate,PBJVideoPlayerControllerDelegate,MZRSlideInMenuDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIActionSheetDelegate, FBSDKShareOpenGraphValueContaining,FBSDKSharingDelegate,UITabBarDelegate>{
    BOOL hasProfileImageView;
    UIImageView *_playButton;
}

//Properties for table view cell
@property (nonatomic, strong) PBJVideoPlayerController *moviePlayer;

@property (nonatomic, strong) AVPlayer* videoPlayer;
@property (nonatomic, strong) AVPlayerLayer * videolayer;


//post
@property (nonatomic,strong)PFObject*postObject;

@property(nonatomic,strong)NSString*videoURL;



//activity cell
@property (nonatomic,strong)  UIActivityIndicatorView* spinner;


@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;

- (IBAction)close:(id)sender;


- (IBAction)moreOptions:(id)sender;
- (IBAction)share:(id)sender;
- (IBAction)comment:(id)sender;
- (IBAction)like:(id)sender;
- (IBAction)userClickedProfile:(id)sender;


@end
