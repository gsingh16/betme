//
//  TableViewController.m
//  BetMe
//
//  Created by Admin on 4/16/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import "DealsTableViewController.h"
#import "DealTableViewCell.h"
#import "UIImage+BlurredFrame.h"
#import "TGLViewController.h"
#import "Flurry.h"
//data chaching
#import "APSmartStorage.h"
#import <NSString+MD5.h>
#import "APFileStorage.h"
#import "NSFileManager+Storage.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MainViewController.h"

@interface DealsTableViewController (){

}

@end

@implementation DealsTableViewController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIColor *white = [UIColor whiteColor];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:white forKey:NSForegroundColorAttributeName ];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    //status bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //imagepaper
    _imagePager.delegate=self;
    _imagePager.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
    _imagePager.pageControl.pageIndicatorTintColor = [UIColor blackColor];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    NSLog(@"in dis viewDidLoad of deals tbvc");
    
    
    //flurry track user viewed loction
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",self.locationName,@"location",
                                   nil];
    [Flurry logEvent:@"userViewedLocation" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userViewedLocation" parameters:articleParams];

    

    _locationAdressLabel.text=self.locationAdress;
    
    
    [self viewDidAppear:YES];
    
    //wallet
    //wallet bar button
    UIImage *image2 = [UIImage imageNamed:@"wallet"];
    CGRect frame2 = CGRectMake(0, 0, image2.size.width/3, image2.size.height/2);
    UIButton* button2 = [[UIButton alloc] initWithFrame:frame2];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [button2 setShowsTouchWhenHighlighted:YES];
    [button2 addTarget:self action:@selector(showDealsWallet) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* wallet = [[UIBarButtonItem alloc] initWithCustomView:button2];
    
    
    self.navigationItem.rightBarButtonItem = wallet;
    
    
}

- (void)showDealsWallet{
    if ([PFUser currentUser]) {
    
    //current way
    NSLog(@"the current user is %@",[PFUser currentUser].objectId);
    
    //get users points
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    [query includeKey:@"deals.fromBusiness"];
    
    //start query
    __block TGLViewController*vc;
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            NSArray*deals=[object objectForKey:@"deals"];
            //NSLog(@"the deals are in dealsinWallet %@",deals);
            
            //load the controller now
            UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
            // [aFlowLayout setItemSize:CGSizeMake(400, 400)];
            [aFlowLayout setItemSize:CGSizeMake(320, 463)];
            [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
            vc = [[TGLViewController alloc]initWithCollectionViewLayout:aFlowLayout];
            vc.dealsInWallet=[[NSMutableArray alloc]initWithArray:deals];;
            
            self.navigationController.navigationBarHidden=NO;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else
            NSLog(@"error : %@",error);
    }];
    
    }else{
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];

                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }
}


-(void)getDirections{
    //first create latitude longitude object
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.locationCoordinate.latitude,self.locationCoordinate.longitude);
    
    NSLog(@"the coordinates are %f , %f",coordinate.longitude,coordinate.latitude
          );
    //create MKMapItem out of coordinates
    MKPlacemark* placeMark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
    MKMapItem* destination =  [[MKMapItem alloc] initWithPlacemark:placeMark];
    
    if([destination respondsToSelector:@selector(openInMapsWithLaunchOptions:)])
    {
        //using iOS6 native maps app
        [destination openInMapsWithLaunchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeWalking}];

        //flurry track user wants to go to location
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",self.locationName,@"location",
                                       nil];
        [Flurry logEvent:@"userWantsToNavigateToLocation" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userWantsToNavigateToLocation" parameters:articleParams];

    }
    

    
    
}

-(void)getMoreInfo{
    
    
    //flurry track user wants more info
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",self.locationName,@"location",
                                   nil];
    [Flurry logEvent:@"userWantsMoreInfoAboutLocation" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userWantsMoreInfoAboutLocation" parameters:articleParams];

    
    
    
   MDBrowser* browser = [[MDBrowser alloc] initWithFrame:CGRectMake(0, 75, self.view.bounds.size.width, self.view.bounds.size.height-100)];
    browser.delegate = self;
    [browser ShowInView:self.view AddOverLayToSuperView:YES withAnimationType:MDBrowserPresetationAnimationTypePopUp];
    [browser LoadUrl:[NSURL URLWithString:self.locationMoreInfoURL]];
    [browser setButtonsHidden:NO];
}

-(void)call{
    NSString *phNo = self.locationPhoneNumber;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
        
        //flurry track user wants to call
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",self.locationName,@"location",
                                       nil];
        [Flurry logEvent:@"userWantsToCallLocation" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userWantsToCallLocation" parameters:articleParams];

        
        
        
    } else
    {
        UIAlertView* calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}



- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    

    
    self.title=self.locationName;
    CellIdentifier=@"dealCell";

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSLog(@"memory warning in deals tableview vontrolelr");
    
}

#pragma mark - UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.locationDeals.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    DealTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"DealTableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }

    NSLog(@"the deal name is %@",[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"dealName"]);
    NSLog(@"the deal description is %@",[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"dealDescription"]);
    NSLog(@"the deal quantity is %@",[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"quantity"]);
  
    //deal cost
    cell.pointsRequired=[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"pointsRequired"];
    
    //deal object id
    cell.dealObjectId=[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"objectId"];
    
    //cell t's & c's
    cell.termsAndConditions=[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"termsAndConditions"];
    
    //cell from vc
    cell.fromViewController=self;
    
    
    NSURL*locationImageUrl=[NSURL URLWithString:[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"imageURL"]];
    
    //test OLD WORKING[cell.dealImageView setImageWithURL:locationImageUrl];
    
    APSmartStorage.sharedInstance.parsingBlock = ^(NSData *data, NSURL *url)
    {
        NSLog(@"data is being allocated in the deals vc for an image ! ");
//        return [UIImage imageWithData:data scale:UIScreen.mainScreen.scale];
        return [UIImage imageWithData:data scale:50];

    };
    
    // load object with URL
    [APSmartStorage.sharedInstance loadObjectWithURL:locationImageUrl storeInMemory:NO completion:^(id object, NSError *error)
     {
         // hide progress/activity
         if (error)
         {
             // show error
             NSLog(@"error in downloading the videoThumbnailUrl %@",error);
         }
         else
         {
             // do something with object
             [cell.dealImageView setImage:object];
         }
     }];
    
    
    
    cell.dealTitleDescriptionLabel.text=[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"dealName"];
    
    
    cell.quantityRemainingLabel.text=[NSString stringWithFormat:@"Quantity: %@ ",[[self.locationDeals objectAtIndex:indexPath.row]valueForKey:@"quantity"]];
    
    CALayer *separator = [CALayer layer];
    separator.backgroundColor = [UIColor orangeColor].CGColor;
    separator.frame = CGRectMake(0, cell.frame.size.height, self.view.frame.size.width, 2);
    [cell.layer addSublayer:separator];
    
    
    return cell;

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120.0;
}

#pragma mark - KiImagePager DataSource

- (NSArray *) arrayWithImages{

    /*
     return [NSArray arrayWithObjects:
     @"https://raw.github.com/kimar/tapebooth/master/Screenshots/Screen1.png",
     @"https://raw.github.com/kimar/tapebooth/master/Screenshots/Screen2.png",
     @"https://raw.github.com/kimar/tapebooth/master/Screenshots/Screen3.png",
     @"http://partybyjake.com/pbj/wp-content/uploads/2010/09/cool-duck.jpg",
     @"http://wonderfulengineering.com/wp-content/uploads/2014/03/high-resolution-wallpapers-25.jpg",
     nil];
     */

    NSLog(@"getting used the : arrayWithImageUrlStrings, which are %@",self.locationImageURLsArray);
    
    return self.locationImageURLsArray;

}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image
{
    //return UIViewContentModeScaleAspectFill;
   // return UIViewContentModeRedraw;
    //  return UIViewContentModeScaleAspectFit;
    return UIViewContentModeScaleToFill;
}


- (IBAction)callLocation:(id)sender {
    [self call];
    
}

- (IBAction)moreInfo:(id)sender {
    [self getMoreInfo];
}

- (IBAction)navigateToLocation:(id)sender {
    
    NSString*messageForAlert=[NSString stringWithFormat:@"Navigate To %@ ?",self.locationName];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Direction"
                                                      message:messageForAlert
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"OK", nil];
    message.tag=1;
    [message show];
    
    
}

#pragma mark - alertViewDelation

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if (alertView.tag==1) {
    if([title isEqualToString:@"Cancel"])
    {
        NSLog(@"Cancel was selected.");
    }
    else if([title isEqualToString:@"OK"])
    {
        NSLog(@"OK was selected.");
        [self getDirections];
    }
        
    }else if (alertView.tag==2){

        //already handled by ios7
    }

    
}


-(void)viewDidDisappear:(BOOL)animated{
    _imagePager.dataSource=nil;
    NSLog(@"view did dissapear being called");
    

}
-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"view will dissapear being called");
    /*
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor whiteColor];
     */
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

}




@end
