//
//  homeViewController.h
//  BetMe
//
//  Created by Admin on 6/25/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "RESideMenu.h"
#include "RESideMenu.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Parse/Parse.h>
#import <ALMoviePlayerController.h>
#import <ALMoviePlayerControls.h>
#import "MZRSlideInMenu.h"
#import "couponViewController.h"
#import "SettingsViewController.h"
#import "PBJVideoPlayerController.h"
#import "APFileStorage.h"
#import "HMSideMenu.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "NotificationTableViewController.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


@interface homeViewController : PFQueryTableViewController <ALMoviePlayerControllerDelegate,PBJVideoPlayerControllerDelegate,MZRSlideInMenuDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,FBSDKShareOpenGraphValueContaining,FBSDKSharingDelegate,UITabBarDelegate>{
    
    BOOL hasProfileImageView;
    PBJVideoPlayerController *_videoPlayerController;
    UIImageView *_playButton;
}



//sideMenu
@property (nonatomic, strong) HMSideMenu *sideMenu;


//memory bank reference
@property (strong, nonatomic) APFileStorage *fileStorage;

//nsurlSession
@property (nonatomic,strong) NSURLSession*session;

@property (strong, nonatomic) IBOutlet UIView *betView;


//Properties for table view cell
@property (nonatomic, strong) ALMoviePlayerController *moviePlayer;

@property (nonatomic, strong) AVPlayer* videoPlayer;
@property (nonatomic, strong) AVPlayerLayer * videolayer;



//activity cell
@property (nonatomic,strong)  UIActivityIndicatorView* spinner;

@property (strong, nonatomic) IBOutlet UIButton *notificationButton;

//for video urls
@property (nonatomic,strong) NSMutableArray * videoUrlsArray;

//headerviewCell
@property (weak, nonatomic) IBOutlet UIImageView *headerProfilePictureImageView;



@property (strong, nonatomic) IBOutlet UITabBar *tabBar;


- (IBAction)openNotifications:(id)sender;

@end
