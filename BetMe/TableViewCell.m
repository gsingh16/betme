//
//  TableViewCell.m
//  BetMe
//
//  Created by Admin on 4/14/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import "TableViewCell.h"
#import "DealsTableViewController.h"

@implementation TableViewCell
@synthesize deals=_deals;
@synthesize backgroundImages=_backgroundImages;
@synthesize locationObjectID=_locationObjectID;
@synthesize locationAdress=_locationAdress;
@synthesize locationNameLabel=_locationNameLabel;
@synthesize locationCoordinate=_locationCoordinate;
@synthesize locationMoreInfoURL=_locationMoreInfoURL;
@synthesize locationPhoneNumber=_locationPhoneNumber;
- (void)awakeFromNib
{
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    NSLog(@"did select this one");
    

    
    
}



- (IBAction)goToDeals:(id)sender {
    DealsTableViewController*locationTableViewController=[[DealsTableViewController alloc]initWithNibName:@"DealsTableViewController" bundle:nil];
    locationTableViewController.locationImageURLsArray=_backgroundImages;
    locationTableViewController.locationName=_locationNameLabel.text;
    locationTableViewController.locationAdress=_locationAdress;
    locationTableViewController.locationDistance=_locationDistanceLabel.text;
    locationTableViewController.locationDeals=_deals;
    locationTableViewController.locationCoordinate=_locationCoordinate;
    locationTableViewController.locationMoreInfoURL=_locationMoreInfoURL;
    locationTableViewController.locationPhoneNumber=_locationPhoneNumber;
    
    
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"locationSelected"
     object:locationTableViewController];

    
}
@end
