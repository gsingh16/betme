//
//  parseQueryTableViewController.h
//  BetMe
//
//  Created by Admin on 6/27/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <Parse/Parse.h>
#import "VideoPostTableViewCell.h"
#import "APSmartStorage.h"
#import "FBPictureHelper.h"
#import <ALMoviePlayerController.h>
#import <ALMoviePlayerControls.h>

@interface parseQueryTableViewController : PFQueryTableViewController <UITableViewDelegate,UITableViewDataSource, ALMoviePlayerControllerDelegate>
@property (nonatomic,strong ) NSString* passedTag;
@property (nonatomic,assign ) BOOL usedForMultipleSearch;
//Properties for table view cell
@property (nonatomic, strong) ALMoviePlayerController *moviePlayer;
@end
