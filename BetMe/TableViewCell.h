//
//  TableViewCell.h
//  BetMe
//
//  Created by Admin on 4/14/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
@interface TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *coverImage;
@property (strong, nonatomic) IBOutlet UILabel *locationDistanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationTypeLabel;
@property (strong, nonatomic) IBOutlet UIButton *selectButton;

//REMEMBER TO URL ENCODE///////////////////////////
@property(nonatomic,strong)NSString*locationAdress;
///////////////////////////////////////////////////
@property(nonatomic,strong)PFGeoPoint*locationCoordinate;

@property(nonatomic,strong)NSString*locationMoreInfoURL;
@property(nonatomic,strong)NSString*locationPhoneNumber;
@property(nonatomic,strong)NSString*locationObjectID;
@property(nonatomic,strong)NSArray*backgroundImages;
@property(nonatomic,strong)NSArray*deals;
@property (weak, nonatomic) IBOutlet UILabel *locationNeighborhoodLabel;


//button 
- (IBAction)goToDeals:(id)sender;
@end
