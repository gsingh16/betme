//
//  betmevideoservices.m
//  BetMe
//
//  Created by Admin on 12/27/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//


#import "betMeVideoServices.h"
#import <Parse/Parse.h>
#import "MBProgressHUD.h"
#import "SIAlertView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "GTMHTTPFetcher.h"
#import "GTMHTTPFetcherService.h"
#import "GTMHTTPFetcherLogging.h"
#import "GTMHTTPFetchHistory.h"
#import "GTMHTTPUploadFetcher.h"
#import "Flurry.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#define VIMEO_SECRET @"a0dd320b3f6e1c91d7f3cdcd4668ed6955588395"
#define VIMEO_CONSUMER_KEY @"4ecea6c53ecbf6de9fa541576a8e6f9a7aac215e"
#define VIMEO_BASE_URL @"http://vimeo.com/services/auth/"

#define VIMEO_REQUEST_TOKEN_URL @"http://vimeo.com/oauth/request_token"
#define VIMEO_AUTHORIZATION_URL @"http://vimeo.com/oauth/authorize?permission=write"
#define VIMEO_ACCESS_TOKEN_URL @"http://vimeo.com/oauth/access_token"

#define ACCESS_TOKEN @"da166ecb3f53ffe08590c7180e1cf2ef"
#define ACCESS_TOKEN_SECRET @"2c691d76f0855e077dab2998509ae9d7de875000"


#pragma - mark String Encode
@implementation NSString (NSString_Extended)

- (NSString *)urlEncode {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}
@end





@implementation betMeVideoServices
@synthesize currentWindow=_currentWindow,baseView=_baseView;
@synthesize hud=_hud;
@synthesize defaults=_defaults;
//vimeo URLs
@synthesize completeURI=_completeURI;
@synthesize currentVideoID=_currentVideoID;
@synthesize descriptionForVideo=_descriptionForVideo,keyWordsForVideo=_keyWordsForVideo;

- (id) init
{
    if (self = [super init])
    {

        
        //not needed anymore :9
        //[self vimeoAuth];
        
        


        //retrieve url for video from nsuserdefaults
        _defaults=[NSUserDefaults standardUserDefaults];
        NSURL*videoUrl= [_defaults URLForKey:@"currentVideoUrl"];
        self.videoURL=videoUrl;
        NSData*videoData=[NSData dataWithContentsOfURL:videoUrl];
        self.videoData=videoData;
        
        NSArray *parts = [[videoUrl absoluteString] componentsSeparatedByString:@"/"];
        NSString *filename = [parts objectAtIndex:[parts count]-1];
        self.fileName=filename;
        
        NSLog(@"in the INIT method of betMeVideoServices");
        
    }
    return self;
}



#pragma mark - oauth stuff
          
  - (GTMOAuth2Authentication *)vimeoAuth {
          NSURL *tokenURL = [NSURL URLWithString:@"https://api.vimeo.com"];
          
          // We'll make up an arbitrary redirectURI.  The controller will watch for
          // the server to redirect the web view to this URI, but this URI will not be
          // loaded, so it need not be for any actual web page.
          NSString *redirectURI = @"http://www.google.com/OAuthCallback";
          
          GTMOAuth2Authentication *auth;
      /*
          auth = [GTMOAuth2Authentication authenticationWithServiceProvider:@"Vimeo"
                                                                   tokenURL:tokenURL
                                                                redirectURI:redirectURI
                                                                   clientID:@"Vimeo"
                                                               clientSecret:VIMEO_SECRET;
      
      */
      
          auth.accessToken=@"90937b1dd7a3bba101cb6e1fec59b5ed";
      
      self.signedAuth=auth;
      return auth;
  }


#pragma mark -  vimeo/parse upload

- (void)upload{
    
    if (self.videoData==Nil || self.videoData==NULL) {
        NSLog(@"NO DATA BRO");
        return;
    }else{
        NSLog(@"found data BRO ");
       // [self startUpload];
    }
    
    //show hud
    _baseView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                         0,
                                                         320,
                                                         480)];
    
    _currentWindow = [[[UIApplication sharedApplication] delegate] window];
    [_currentWindow addSubview:_baseView];
    
    
    _hud = [MBProgressHUD showHUDAddedTo:_baseView animated:YES];
    _hud.mode = MBProgressHUDModeDeterminate;
    _hud.labelText = @"Uploading";
    
    
    
    //thumnail
    // Convert to JPEG with 450% quality
    NSData* data = UIImageJPEGRepresentation([self loadThumnbailWithURL:self.videoURL], 0.45f);
    PFFile *imageFile = [PFFile fileWithName:@"thumnbailImage.jpg" data:data];
    //save image
    // Save the image to Parse
    
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"no error");
        }else{
            NSLog(@"error in making image thumnail");
            [Flurry logError:@"errorMakingImageThumbnail" message:error.description error:error];
            [FBSDKAppEvents logEvent:@"errorMakingImageThumbnail" parameters:@{@"error: ":error}];
        }
    }];
    
    //video file/data
    //PFFile*newFIle=[PFFile fileWithData:self.videoData];
    PFFile*newFIle=[PFFile fileWithName:@"video.mp4" data:self.videoData];
    [newFIle saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {

        //if error
        if (error) {
            NSLog(@"error: %@",error);
            [Flurry logError:@"errorSavingVideoFileForUpload" message:error.description error:error];
            [FBSDKAppEvents logEvent:@"errorSavingVideoFileForUpload" parameters:@{@"error: ":error}];


        }
            NSLog(@"saved pffile with metadata fromUser %@ and imagefile %@ and video file %@",[PFUser currentUser],imageFile.description,newFIle.description);
        
            //save rest of object
            PFObject *newPost=[[PFObject alloc]initWithClassName:@"Cuts"];
            [newPost setObject:self.descriptionForVideo forKey:@"description"];
            [newPost setObject:self.keyWordsForVideo forKey:@"ampersandArray"];
            [newPost setObject:[PFUser currentUser] forKey:@"fromUser"];
            [newPost addObject:[PFUser currentUser].objectId forKey:@"likes"];
            //adds user to "like" array [newPost addObject:[PFUser currentUser] forKey:@"likes"];
            [newPost setObject:imageFile forKey:@"videoThumbnailImage"];
            [newPost setObject:newFIle forKey:@"videoFile"];
            [newPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (error) {
                    NSLog(@"error: %@",error);
                    [Flurry logError:@"errorSavingParseObjectForUpload" message:error.description error:error];
                    [FBSDKAppEvents logEvent:@"errorSavingParseObjectForUpload" parameters:@{@"error: ":error}];

                }
                if (succeeded) {
                    NSLog(@"saved the video post obj with object id %@",newPost.objectId);
                    
                    
                    //add description to fist comment
                    PFObject*firstComment=[[PFObject alloc]initWithClassName:@"Comments"];
                    [firstComment setObject:self.descriptionForVideo forKey:@"comment"];
                    [firstComment setObject:[PFUser currentUser] forKey:@"fromUser"];
                    [firstComment setObject:newPost forKey:@"postID"];
                    [firstComment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (error) {
                            NSLog(@"error: %@",error);
                            [Flurry logError:@"errorSavingFirstComment" message:error.description error:error];
                            [FBSDKAppEvents logEvent:@"errorSavingFirstComment" parameters:@{@"error: ":error}];
                            
                        }
                        if (succeeded) {
                            NSLog(@"saved the description as the first comment");
                        }}];
                    
                    
                    //hide hud
                    [_hud hide:YES];
                    [_baseView removeFromSuperview];

                    //nsuserDefaults cleanup
                    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
                    
                    //clean description and tags
                    [defaults setValue:nil forKey:@"currentVideoDescription"];
                    [defaults setObject:nil forKey:@"currentVideoTags"];
                    
                    //Remove any prevouis videos at that path
                    [[NSFileManager defaultManager]  removeItemAtURL:[defaults URLForKey:@"currentVideoUrl"] error:nil];
                    //allow user to upload again by resetting needsToUpload
                    [defaults setBool:NO forKey:@"needsToUpload"];
                    
                    
                    [defaults synchronize];
                    
                    
                    //post notification
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"videoUploadedToParse"
                     object:nil];
                    
                    
                    //award user
                    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Upload Successful" andMessage:@"You earned 400 points! Earn another 100 points to share?"];
                    alertView.messageColor=[UIColor greenColor];
                    
                    
                    //share
                    [alertView addButtonWithTitle:@"Share " type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alertView) {
                        //share code
                        FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
                        FBSDKSharePhoto *photoContent= [[FBSDKSharePhoto alloc]init];
                        [photoContent setImage:[UIImage imageWithData:[NSData dataWithData:UIImageJPEGRepresentation([self loadThumnbailWithURL:self.videoURL], 0.45f)]]];
                        
                        FBSDKShareVideoContent*fbSDKVideo=[[FBSDKShareVideoContent alloc]init];
                        FBSDKShareVideo*theVideo=[[FBSDKShareVideo alloc]init];
                        theVideo.videoURL=self.videoURL;
                        content.video = theVideo;
                        content.previewPhoto=photoContent;
                        [FBSDKShareAPI shareWithContent:content delegate:self];
                        
                    }];
                    
                    //not now
                    [alertView addButtonWithTitle:@"Not Now "
                                             type:SIAlertViewButtonTypeDefault
                                          handler:^(SIAlertView *alert) {
                                              NSLog(@"Button1 Clicked");
                                              //add points to user
                                              //deduct from daily video upload
                                              
                                              PFQuery *query = [PFUser query];
                                              [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                                                  
                                                  if (error) {
                                                      NSLog(@"error fetching user's current points in the betmeVideoServices %@",error);
                                                  }else{
                                                      
                                                      int currentPoints= [[object valueForKeyPath:@"points"]intValue];
                                                      int pointsToBeAdded=400;
                                                      
                                               [[PFUser currentUser]incrementKey:@"points" byAmount:[NSNumber numberWithInt:pointsToBeAdded]];
                                                      
                                              [[PFUser currentUser]incrementKey:@"videosUploadedToday" byAmount:[NSNumber numberWithInt:1]];
                                              
                                              [[PFUser currentUser]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                                  if (!error) {
                                                      NSLog(@"updated users new points completely");
                                                      
                                                      //flurry track user upload Video
                                                      NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                                     [[PFUser currentUser]username] , @"user",
                                                                                     nil];
                                                      [Flurry logEvent:@"userUploadedVideo" withParameters:articleParams];
                                                      [FBSDKAppEvents logEvent:@"userUploadedVideo" parameters:articleParams];

                                                      
                                                  }else{
                                                      NSLog(@"the error in updating the users points for upload is %@,",error);
                                                        [Flurry logError:@"errorUpdatingPointsForUpload" message:error.description error:error];
                                                        [FBSDKAppEvents logEvent:@"errorUpdatingPointsForUpload" parameters:@{@"error:":error}];
    
                                                      
                                                  }
                                              }];
                                              
                                                  }}];
                                          }];

                    
                    
                    
                    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                    
                    [alertView show];
                    
                }
                
                
            }];

    } progressBlock:^(int percentDone) {
        NSLog(@"the percent done is %i",percentDone);

        
        _hud.progress =(float)percentDone/100;
        
        if(_hud.progress==1)
            NSLog(@"finished sending?");


        
    }];
    
 }

#pragma mark- get thumnail image for video
- (UIImage*)loadThumnbailWithURL:(NSURL*)url {
    
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    
    return [[UIImage alloc] initWithCGImage:imgRef];
    
}

#pragma mark-Upload


//step one, authorize
- (void)startUpload {
    
    // OLD API ? >   NSURL *url = [NSURL URLWithString:@"http://vimeo.com/api/rest/v2?format=json&method=vimeo.videos.upload.getQuota"];
    
    NSURL *url = [NSURL URLWithString:@"https://api.vimeo.com/me"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:@"Bearer 90937b1dd7a3bba101cb6e1fec59b5ed" forHTTPHeaderField:@"Authorization"];
    //i think this works
    [[self vimeoAuth] authorizeRequest:request];
    
    
    NSLog(@"The url being used is %@",[request allHTTPHeaderFields]);

    GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    [myFetcher beginFetchWithDelegate:self
                    didFinishSelector:@selector(myFetcher:finishedWithData:error:)];
    self.currentFetcher = myFetcher;
    
}

//actual upload
- (void) myFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *) error {
    if (error != nil) {
        [self handleErrorWithText:nil];
        NSLog(@"error in myFetcher %@", error);
    } else {

        
        NSString *info = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[info dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        NSLog(@"The result in fetcher 1 is : %@",result);

        //quota

        long long quota = [[[[result valueForKeyPath:@"upload_quota"]valueForKey:@"space"]valueForKey:@"free"]longLongValue];

        NSLog(@"the quoata max is %lli",quota);

        //check size here
        
        if ( [self.videoData length] > quota ) {
            [self handleErrorWithText:@"Your Vimeo account quota is exceeded."];
            return;
        }
        
        
        //lets assume we have enough quota
        NSURL *url = [NSURL URLWithString:@"https://api.vimeo.com/me/videos?type=streaming"];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setValue:@"Bearer 90937b1dd7a3bba101cb6e1fec59b5ed" forHTTPHeaderField:@"Authorization"];
        [request setHTTPMethod:@"POST"];
        
        [[self vimeoAuth]authorizeRequest:request];

        GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
        [myFetcher beginFetchWithDelegate:self
                        didFinishSelector:@selector(myFetcher2:finishedWithData:error:)];
        
        self.currentFetcher = myFetcher;
        
    }
}

- (void) myFetcher2:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *) error {
    if (error != nil) {
        [self handleErrorWithText:nil];
        NSLog(@"error %@", error);
    } else {

        NSString *info = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[info dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        NSLog(@"result in fetcher 2 : %@",result);
        
        
        //fail here if neccessary TODO
        
        self.VimeoUploadSecureURL = [result valueForKeyPath:@"upload_link_secure"];
        _completeURI=[result valueForKey:@"complete_uri"];
        self.currentTicketID = [result valueForKeyPath:@"ticket_id"];
        
        NSLog(@"the complete URI is %@",_completeURI);
        NSLog(@"the ticket_id is %@",self.currentTicketID);
        
        

        
        if ( [self.currentTicketID length] == 0 ) {
            [self handleErrorWithText:nil];
            NSLog(@"fail with id length");

            return;
        }
        
        //get video file
        // load the file data
        NSLog(@"file size in fetcher 2 is %i ",[self.videoData length]);
        
        if (self.videoData) {
            NSString *convertedValue = [NSByteCountFormatter stringFromByteCount:[self.videoData length] countStyle:NSByteCountFormatterCountStyleFile];
            NSLog(@"The converted value is %@", convertedValue);
        }
        
        
        //insert endpoint here
        NSURL *url = [NSURL URLWithString:self.VimeoUploadSecureURL];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"PUT"];
        [request setValue:[NSString stringWithFormat:@"%i", [self.videoData length]] forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"video/mp4" forHTTPHeaderField:@"Content-Type"];
        //[request setHTTPBody:[NSData dataWithContentsOfFile:path]];
        [request setHTTPBody:self.videoData];

        NSLog(@"the Url in fetcher 2 is %@",[request allHTTPHeaderFields]);
        
        [[self vimeoAuth]authorizeRequest:request];
        
        GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
        myFetcher.sentDataSelector = @selector(myFetcher:didSendBytes:totalBytesSent:totalBytesExpectedToSend:);
        
        [myFetcher beginFetchWithDelegate:self
                        didFinishSelector:@selector(myFetcher3:finishedWithData:error:)];
        
        self.currentFetcher = myFetcher;
        
        //show hud
        _baseView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                             0,
                                                             320,
                                                             480)];
        
        _currentWindow = [[[UIApplication sharedApplication] delegate] window];
        [_currentWindow addSubview:_baseView];
        
        
        _hud = [MBProgressHUD showHUDAddedTo:_baseView animated:YES];
        _hud.mode = MBProgressHUDModeDeterminate;
        _hud.labelText = @"Uploading";
    }
}

- (void)myFetcher:(GTMHTTPFetcher *)fetcher
     didSendBytes:(NSInteger)bytesSent
   totalBytesSent:(NSInteger)totalBytesSent
totalBytesExpectedToSend:(NSInteger)totalBytesExpectedToSend {
   // NSLog(@"%i / %i", totalBytesSent, totalBytesExpectedToSend);
    NSLog(@"operating in the didSendBytes method with ints like bytes sent %i, total bytes %i",bytesSent,totalBytesSent);
 //proogress bar   [self setProgress:(float)totalBytesSent / (float) totalBytesExpectedToSend];
  // upload stream   self.uploadLabel.text = @"Uploading to Vimeo...";
    _hud.progress = ((float)totalBytesSent / (float) totalBytesExpectedToSend);
    
    if(_hud.progress==1)
        NSLog(@"finished sending?");


    
}

/*
- (void) myFetcher3:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *) error {
    if (error != nil) {
        [self handleErrorWithText:nil];
        NSLog(@"error in fetcher 3 with error: %@",error);
    } else {
        NSString *info = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[info dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        NSLog(@"the info in fetcher 3 is %@ ", result);
        
        //finalize upload
        NSString *requestString = [NSString stringWithFormat:@"http://vimeo.com/api/rest/v2?format=json&method=vimeo.videos.upload.complete&ticket_id=%@&filename=%@", self.currentTicketID, self.fileName];
        
        NSURL *url = [NSURL URLWithString:requestString];
        NSLog(@"the request url in fetcher 3 is %@",url);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [self.signedAuth authorizeRequest:request];
        [self vimeoAuth];

        GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
        [myFetcher beginFetchWithDelegate:self
                        didFinishSelector:@selector(myFetcher4:finishedWithData:error:)];
        
        self.currentFetcher = myFetcher;
    }
}
*/


- (void) myFetcher3:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *) error {

    //hide hud
    [_hud hide:YES];
    [_baseView removeFromSuperview];

    if (error != nil) {
        [self handleErrorWithText:nil];
        NSLog(@"error in fetcher 3 with error: %@",error);
    } else {
        NSString *info = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[info dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        NSLog(@"the info in fetcher 3 is %@ ", result);
       // NSLog(@"the info in fetcher 3 is %@ ", fetcher.responseHeaders);

        //insert endpoint here
        NSURL *url = [NSURL URLWithString:self.VimeoUploadSecureURL];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:[NSString stringWithFormat:@"%i", [self.videoData length]] forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"video/mp4" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"bytes */*" forHTTPHeaderField:@"Content-Range"];

        //NSLog(@"the Url in fetcher 3 is %@",[request allHTTPHeaderFields]);

        [[self vimeoAuth]authorizeRequest:request];
        
        GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
        [myFetcher beginFetchWithDelegate:self
                        didFinishSelector:@selector(myFetcher4:finishedWithData:error:)];
        
        self.currentFetcher = myFetcher;
    }
    
}

        
- (void) myFetcher4:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *) error {
    if (fetcher.responseHeaders == nil) {
        [self handleErrorWithText:nil];
        //probably not a 308
        NSLog(@"error %@",error);
    }
    
    
        //check upload
        NSLog(@"308 error is good %@",error);
        NSLog(@"the data tho: %@",fetcher.responseHeaders);


    //making sure its fully sent
    if([[[fetcher responseHeaders] valueForKey:@"Content-Length"]integerValue]==0){
        NSLog(@"the content length is 0, so its uploaded");
    }
    
//finish upload
    
    //HTTP DELETE to cut wire, and finalize uploade
    NSString*urlPrefix=[NSString stringWithFormat:@"https://api.vimeo.com"];
    NSString*urlParameters=[NSString stringWithFormat:@"%@",_completeURI];
    NSString*fullUrl=[NSString stringWithFormat:@"%@%@",urlPrefix,urlParameters];
    
    NSURL*urlForDeleteMethod=[NSURL URLWithString:fullUrl];
    
    //checking to make sure i have it here
    NSLog(@"the complete URI is %@",_completeURI);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlForDeleteMethod];
    [request setHTTPMethod:@"DELETE"];
    
    [request setValue:@"Bearer 90937b1dd7a3bba101cb6e1fec59b5ed" forHTTPHeaderField:@"Authorization"];

    NSLog(@"the url by the way in fethcer 4 is %@ ",[request URL]);

    
        //auth request
        [[self vimeoAuth]authorizeRequest:request];
    

        GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
        [myFetcher beginFetchWithDelegate:self
                        didFinishSelector:@selector(myFetcher5:finishedWithData:error:)];
        
        self.currentFetcher = myFetcher;

    }
//}


//should recieve video here
- (void) myFetcher5:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *) error {
    if (error != nil) {
        [self handleErrorWithText:nil];
        NSLog(@"error in fetcher 5 %@", error);
        NSString *info = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[info dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        NSLog(@"the result in fetcher 5 is %@",result);
    } else {
        NSString *info = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[info dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        NSLog(@"made it in the fetcher 5 with result %@",result);
        NSLog(@"the url from the headers in fetcher 5 tho: %@",[fetcher.responseHeaders valueForKey:@"Location"]);

        //self.currentVideoID=
        NSString* videoID=[fetcher.responseHeaders valueForKey:@"Location"];
        
        NSRange startRange = [videoID rangeOfString:@"/videos/"];
        
        NSRange searchRange = NSMakeRange(startRange.location , videoID.length);
        [videoID substringWithRange:searchRange];
        
        videoID=[videoID substringFromIndex:8];
        
        _currentVideoID=videoID;
        
        NSLog(@"the current Video Id is %@",_currentVideoID);
        //alert here?
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.vimeo.com/me/videos/"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setValue:@"Bearer 90937b1dd7a3bba101cb6e1fec59b5ed" forHTTPHeaderField:@"Authorization"];
        //[request setHTTPMethod:@"POST"];
        
        [[self vimeoAuth]authorizeRequest:request];
        
        GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
        [myFetcher beginFetchWithDelegate:self
                        didFinishSelector:@selector(myFetcher6:finishedWithData:error:)];
        
        self.currentFetcher = myFetcher;
 
        
        /*
        NSLog(@"UPLOADED SUCCESSFULLY");
        
        
        [_hud hide:YES];
        [_baseView removeFromSuperview];
        //uploaded so fire event to create parse+ facebook object with the URL of the video as a param for video URL
        //*******
        
        
        //change object to self.HTTPFriendlyURL
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"videoUploadedToVimeo"
         object:self.currentVideoID];

        //change nsuserdefaults stuff here too
        */
    }
}


- (void) myFetcher6:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *) error {
    if (error != nil) {
        [self handleErrorWithText:nil];
        NSLog(@"error in myFetcher %@", error);
    } else {
        
        
        NSString *info = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[info dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        
        NSLog(@"The result in fetcher 6 is : %@",result);
        
    //http://player.vimeo.com/external/91632507
       // NSString* hlsURL = [[result valueForKeyPath:@"upload_quota"]valueForKey:@"space"]valueForKey:@"free"]];

        
        
        
        
        }
 }


#pragma mark - sharer delegate

- (void)sharer:	(id<FBSDKSharing>)sharer
didCompleteWithResults:	(NSDictionary *)results{
    
    
}

- (void)sharer:	(id<FBSDKSharing>)sharer
didFailWithError:	(NSError *)error{
    
    NSLog(@"error in sharing, see error: %@",error);
}

- (void) sharerDidCancel:(id<FBSDKSharing>)sharer{
    
}


#pragma mark - handle error

- (void) handleErrorWithText:(NSString *) text {
    
    //notify your views here
    /*
     self.currentFetcher = nil;
     self.isUploading = NO;
     self.progressBar.alpha = 0;
     self.uploadButton.alpha = 1;
     */
}


@end


