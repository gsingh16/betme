//
//  profileViewController.m
//  BetMe
//
//  Created by Admin on 7/6/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import "profileViewController.h"
#import "homeViewController.h"
#import "MainViewController.h"
#import "FlipsideViewController.h"
#include "RESideMenu.h"
#import "RESideMenu.h"
#import "betViewController1.h"
#import "profileViewController.h"
#import "couponViewController.h"
#import "betSelectionViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "CameraViewController.h"
#import "TGLViewController.h"
#import "SIAlertView.h"
//image chache
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import <SDWebImageDownloader.h>
#import "Flurry.h"
#import "FlurryAds.h"


@interface profileViewController () <UIAlertViewDelegate,KIPullToRevealDelegate,MKMapViewDelegate>
{
    NSArray *aLatitudes;
    NSArray *aLongitudes;
    NSArray *aTitles;
    NSArray *videoObjects;
    
    int viewCount;

}


@property  __block UINavigationController *masterNavigationController;
@property (nonatomic,strong) dispatch_block_t menuBlock;
//viewControllers
@property(nonatomic,strong)homeViewController*homeVC;
@property(nonatomic,strong)profileViewController*profileVC;
@property(nonatomic,strong)couponViewController*couponVC;
@property(nonatomic,strong)SettingsViewController*settingsVC;

@end


@implementation profileViewController
@synthesize sideMenu=_sideMenu;
@synthesize facebookID=_facebookID;
@synthesize userImageURL=_userImageURL;
@synthesize profileImageView=_profileImageView;
@synthesize usedForDisplayingCurrentUser=_usedForDisplayingCurrentUser;
@synthesize homeVC=_homeVC,profileVC=_profileVC,couponVC=_couponVC,settingsVC=_settingsVC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

        NSLog(@"in the initwith nibName method");
        
        //setup movie controller
        
        
        //new
        
        
        
        _videoPlayerController = [[PBJVideoPlayerController alloc] init];
        _videoPlayerController.delegate = self;
        
        [self addChildViewController:_videoPlayerController];
        [self.view addSubview:_videoPlayerController.view];
        [_videoPlayerController didMoveToParentViewController:self];
                
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        
        
    }
    return self;
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self addPictureToViewController];

    
    NSLog(@"usedforDisplayingCurrentuser vallue is %hhd",self.usedForDisplayingCurrentUser);

    
    if (self.usedForDisplayingCurrentUser==YES) {
        NSLog(@"the parse user object id is here %@, and the current username is %@",[PFUser currentUser].objectId,[[PFUser currentUser]objectForKey:@"username"]);
        
        //use profile
        NSLog(@"PFUser Current user is looking at their own profile");
        
        self.title = @"Profile";

        UIColor *white = [UIColor whiteColor];
        NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
        [navBarTextAttributes setObject:white forKey:NSForegroundColorAttributeName ];
        self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
        
        //status bar
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        //menu bar button
        UIImage *menuImage = [UIImage imageNamed:@"menu"];
        CGRect menuFrame = CGRectMake(0, 0, menuImage.size.width/2.5, menuImage.size.height/3);
        UIButton* menuButton = [[UIButton alloc] initWithFrame:menuFrame];
        [menuButton setBackgroundImage:menuImage forState:UIControlStateNormal];
        [menuButton setShowsTouchWhenHighlighted:YES];
        [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem* menu = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
        
        self.navigationItem.leftBarButtonItem = menu;
        
        
        //record bar button
        UIImage *image = [UIImage imageNamed:@"notificationIconWhite"];
        CGRect frame = CGRectMake(0, 0, 35, 30);
        UIButton* button = [[UIButton alloc] initWithFrame:frame];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button setShowsTouchWhenHighlighted:YES];
        [button addTarget:self action:@selector(openNotifications) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem* record = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        //wallet bar button
        UIImage *image2 = [UIImage imageNamed:@"wallet"];
        CGRect frame2 = CGRectMake(0, 0, 20, 20);
        UIButton* button2 = [[UIButton alloc] initWithFrame:frame2];
        [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
        [button2 setShowsTouchWhenHighlighted:YES];
        [button2 addTarget:self action:@selector(showDealsWallet) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem* wallet = [[UIBarButtonItem alloc] initWithCustomView:button2];
        
        
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:wallet ,record,nil];
        
        
        
        //record icon //might leave out bc it interferes witht the controller
        /*
        UIButton*recordButton=[[UIButton alloc]initWithFrame:CGRectMake(130, 700, 50, 50)];
        [recordButton setBackgroundImage:[UIImage imageNamed:@"centerVideoIcon"] forState:UIControlStateNormal];
        [recordButton addTarget:self action:@selector(startNewVideo) forControlEvents:UIControlEventTouchDown];
        [self.parentViewController.view addSubview:recordButton];
        
        [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            [recordButton setFrame:CGRectMake(130, self.view.frame.size.height-30, 50, 50)];
            
        } completion:^(BOOL finished) {
            
        }];
        */
        
        
        //color
        self.navigationController.navigationBar.barTintColor = [UIColor orangeColor];
        self.navigationController.navigationBar.translucent=YES;
 
    }else {
        NSLog(@"the parse user object id is %@",self.parseUserObjectID);

        self.title=self.facebookName;
        NSLog(@"the facebookName is %@",self.facebookName);
        
        //add follow button on right bad
        
        //if not following
        //highlight
        //else
        //unfollow
    }
 
    

    
    //map view
    [self.mapView setDelegate:self];
    [self.mapView addAnnotations:[self generateAnnotations]];
    

    if (videoObjects.count==0) {
        //start wait
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        
        [[self queryForTable]findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                videoObjects=objects;
                NSLog(@"the current user being used for the query is %@",self.parseUserObjectID);
                NSLog(@"got the video objects and the count is %lu",(unsigned long)objects.count);
                NSLog(@"retrieved all the video objects for this user");
                
                dispatch_semaphore_signal(sema);
                
                
            }else{
                NSLog(@"the error in fetching objects for the users profile video is %@",error);
            }
        }];
        
        while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop]
             runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0]];
        }
        
    }
    
    
    
    //create movie player
    
    // create a movie player
    self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.moviePlayer.delegate = self; //IMPORTANT!
    self.moviePlayer.scalingMode=MPMovieScalingModeAspectFill;
    // create the controls
    ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:self.moviePlayer style:ALMoviePlayerControlsStyleNone];
    
    // optionally customize the controls here...
    /*
     [movieControls setBarColor:[UIColor colorWithRed:195/255.0 green:29/255.0 blue:29/255.0 alpha:0.5]];
     [movieControls setTimeRemainingDecrements:YES];
     [movieControls setFadeDelay:2.0];
     [movieControls setBarHeight:100.f];
     [movieControls setSeekRate:2.f];
     */
    
    // assign the controls to the movie player
    [self.moviePlayer setControls:movieControls];
    // Going to place in the THumbail View NAO [self.view addSubview:_moviePlayer.view];
    [self.view addSubview:_moviePlayer.view];
    
    
    [_moviePlayer.view.layer setZPosition:0];
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    
    
    //tap
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hadleTapOnPlayer)];
    tap.delegate=self;
    [tap setNumberOfTouchesRequired:1];
    [tap setNumberOfTapsRequired:1];
    //[[tableView cellForRowAtIndexPath:indexPath] addGestureRecognizer:tap];
    [_moviePlayer.view addGestureRecognizer:tap];
    
    
}



#pragma mark- menu buttons

- (void)showMenu
{
    //[self.sideMenuViewController presentMenuViewController];
    MZRSlideInMenu *menu = [[MZRSlideInMenu alloc] init];
    [menu setDelegate:self];
    [menu addMenuItemWithTitle:@"Home"];
    [menu addMenuItemWithTitle:@"Profile"];
    [menu addMenuItemWithTitle:@"Deals"];
    [menu addMenuItemWithTitle:@"Settings"];
    [menu showMenuFromLeft];
}

- (void)openNotifications{
    NotificationTableViewController*notificationTBVC=[[NotificationTableViewController alloc]init];
    notificationTBVC.parseClassName=@"Notifications";
    [self.navigationController presentViewController:notificationTBVC animated:YES completion:nil];
}


- (void)slideInMenu:(MZRSlideInMenu *)menuView didSelectAtIndex:(NSUInteger)index
{
    NSString *buttonTitle = [menuView buttonTitleAtIndex:index];
    
    
    
    switch ((long)index) {
        case 0:
        {
            //0
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if(!_homeVC){
                
                homeViewController*home=[[homeViewController alloc]initWithCoder:nil];
                UINavigationController*homeVC=[[UINavigationController alloc] initWithRootViewController:home];
                _homeVC=homeVC;
                NSLog(@"homeVC is being created");
                
            }
            
            /*
             [self.navigationController setViewControllers:@[_homeVC]];
             [self.navigationController popViewControllerAnimated:YES];
             */
            [self presentViewController:_homeVC animated:YES completion:nil];
            
            
        }
            break;
        case 1:
        {
            
            //1 comment out because already here
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            /*
            if (!_profileVC) {
                NSLog(@"profileVC is being created");
                profileViewController*profile=[[profileViewController alloc] initWithCoder:nil];
                profile.usedForDisplayingCurrentUser=YES;
                profile.facebookName=[[PFUser currentUser]objectForKey:@"displayName"];
                profile.facebookID=[[PFUser currentUser]objectForKey:@"facebookId"];
                profile.parseUserName=[[PFUser currentUser]objectForKey:@"username"];
                profile.parseUserObjectID=[PFUser currentUser].objectId;
                UINavigationController*profileVC=[[UINavigationController alloc] initWithRootViewController:profile];
                _profileVC=profileVC;
                
            }
            */
            
            /*
             [self.navigationController setViewControllers:@[_profileVC]];
             [self.navigationController popViewControllerAnimated:YES];
             */
          //  [self presentViewController:_profileVC animated:YES completion:nil];
            
            
        }
            break;
        case 2:
        {
            //2
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if (!_couponVC) {
                
                _couponVC=[[couponViewController alloc]initWithCoder:nil];
                UINavigationController*couponsVC=[[UINavigationController alloc] initWithRootViewController:_couponVC];
                _couponVC=couponsVC;
                
                NSLog(@"couponsVC is being created");
                
            }
            
            [self presentViewController:_couponVC animated:YES completion:nil];
            // [self.navigationController setViewControllers:@[_couponVC]];
            // [self.navigationController popViewControllerAnimated:YES];
            //[self.navigationController pushViewController:_couponVC animated:YES];
            
        }
            
            break;
        case 3:
        {
            //3
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if (!_settingsVC) {
                SettingsViewController*settings=[[SettingsViewController alloc]initWithCoder:nil];
                UINavigationController*settingsVC=[[UINavigationController alloc] initWithRootViewController:settings];
                _settingsVC=settingsVC;
                NSLog(@"settingsVC is being created");
                
            }
            /*
             [self.navigationController setViewControllers:@[_settingsVC]];
             [self.navigationController popViewControllerAnimated:YES];
             */
            [self presentViewController:_settingsVC animated:YES completion:nil];
            
            
        }
            break;
            
        default:
            
            break;
    }
    
}



- (void)startNewVideo{
    //check for anon
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:^{
                                      NSLog(@"successfully presented home view controller from mainview controller");
                                  }];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to record a new video"];
        [Flurry logEvent:@"User wants to record a new video"];
        
        CameraViewController*betPage=[[CameraViewController alloc]init];
        //    betSelectionViewController*betPage=[[betSelectionViewController alloc]init];
        self.navigationController.navigationBarHidden = NO;
        //    [self.navigationController prese:betPage animated:YES];
        [self.navigationController presentViewController:betPage animated:YES completion:nil];
    }
}






- (void)showDealsWallet{
    //check for anon
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:^{
                                      NSLog(@"successfully presented home view controller from mainview controller");
                                  }];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to see their deal wallet"];
        [Flurry logEvent:@"User wants to see their deal wallet"];
        
        //current way
        NSLog(@"the current user is %@",[PFUser currentUser].objectId);
        
        //get users points
        PFQuery *query = [PFQuery queryWithClassName:@"_User"];
        [query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
        query.cachePolicy = kPFCachePolicyNetworkElseCache;
        [query includeKey:@"deals.fromBusiness"];
        
        
        //start query
        __block TGLViewController*vc;
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                NSArray*deals=[object objectForKey:@"deals"];
                //NSLog(@"the deals are in dealsinWallet %@",deals);
                
                //load the controller now
                UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
                // [aFlowLayout setItemSize:CGSizeMake(400, 400)];
                [aFlowLayout setItemSize:CGSizeMake(320, 463)];
                [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                vc = [[TGLViewController alloc]initWithCollectionViewLayout:aFlowLayout];
                vc.dealsInWallet=[[NSMutableArray alloc]initWithArray:deals];;
                
                
                self.navigationController.navigationBarHidden=NO;
                [self.navigationController pushViewController:vc animated:YES];
                
            }else
                NSLog(@"error : %@",error);
        }];
        
    }
    
    
}

#pragma mark - mapview create annotations
- (NSArray *)generateAnnotations {
    
    PFQuery*checkInsQuery=[PFQuery queryWithClassName:@"checkins"];
    
    if (_usedForDisplayingCurrentUser==NO) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"objectId" equalTo:_parseUserObjectID]; // find user with that object ID
        PFObject *user = [query getFirstObject];
        [checkInsQuery whereKey:@"fromUser" equalTo:user];
    }else{
    [checkInsQuery whereKey:@"fromUser" equalTo:[PFUser currentUser]];
    }
    [checkInsQuery includeKey:@"atLocation"];
    [checkInsQuery includeKey:@"withDeal"];

    checkInsQuery.cachePolicy = kPFCachePolicyNetworkOnly;
    
//    __block NSArray* geoPoints=[[NSArray alloc]init];
    __block NSMutableArray*locationGeoPoints;
    __block NSMutableArray*locationNames;
    __block NSMutableArray*locationImageURLs;
    __block NSMutableArray *annotations;
    
    //start wait
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    [checkInsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        
        if (!error) {
            
            //see if user has no checkins, then show view
            if(objects.count<1){
                self.userHasNoCheckInsView.frame=CGRectMake(40, 80, self.userHasNoCheckInsView.frame.size.width, self.userHasNoCheckInsView.frame.size.height);
                [self.mapView addSubview:self.userHasNoCheckInsView];
            }
            
            
            NSLog(@"the checkins for user %@, are %@",[PFUser currentUser].objectId,objects);
            NSLog(@"the checkins location is %@",[[objects valueForKeyPath:@"atLocation"]valueForKeyPath:@"Location"]);
            NSLog(@"the location name is %@",[[objects valueForKeyPath:@"atLocation"]valueForKeyPath:@"locationName"]);
            NSLog(@"the location image is %@",[[objects valueForKeyPath:@"atLocation"]valueForKeyPath:@"coverPictureURL"]);

           locationGeoPoints=[[NSMutableArray alloc]initWithArray:[[objects valueForKeyPath:@"atLocation"]valueForKeyPath:@"Location"]];
            
            locationNames=[[NSMutableArray alloc]initWithArray:[[objects valueForKeyPath:@"atLocation"]valueForKeyPath:@"locationName"]];
            
            locationImageURLs=[[NSMutableArray alloc]initWithArray:[[objects valueForKeyPath:@"atLocation"]valueForKeyPath:@"coverPictureURL"]];
            
            
            
            NSLog(@"the count of geopoints is %lu",(unsigned long)locationGeoPoints.count);
            
            
            for (int i=0; i<objects.count; i++) {
                
                //add to geopoints arrray
                PFGeoPoint*geopoint=[locationGeoPoints objectAtIndex:i];
                [locationGeoPoints addObject:geopoint];
                //NSLog(@"the longitude is %f and the latitude is %f",geopoint.longitude,geopoint.latitude);
                
                //add to location names array
                [locationNames addObject:[locationNames objectAtIndex:i]];
                
                //add location image url
                [locationImageURLs addObject:[locationImageURLs objectAtIndex:i]];

            }
            //maybe will include used deal later, in next release
            //NSLog(@"pfgeopoint latitude is %f , and longitude is %f", geopoint.latitude,geopoint.longitude );
            //NSLog(@"the locations are %@",locations);

            //change capacity to objects.count
           
            annotations = [[NSMutableArray alloc] initWithCapacity:objects.count];
            
            for (int i=0; i<objects.count; i++) {
                JPSThumbnail *thumbNail = [[JPSThumbnail alloc] init];
                
                thumbNail.image =[UIImage imageNamed:@"foodIcon"];
                thumbNail.title = [locationNames objectAtIndex:i];
                thumbNail.subtitle = @"NYC";
                
                float latitude=[(PFGeoPoint*)[locationGeoPoints objectAtIndex:i]latitude];
                float longitude=[(PFGeoPoint*)[locationGeoPoints objectAtIndex:i]longitude];
                
                thumbNail.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
                
                thumbNail.disclosureBlock = ^{ NSLog(@"selected Location"); };
                
                [annotations addObject:[[JPSThumbnailAnnotation alloc] initWithThumbnail:thumbNail]];
                
                NSLog(@"the annotation added is %@ with latitude %f and longitude %f",thumbNail,latitude,longitude);
                
            }
            
            dispatch_semaphore_signal(sema);

            
        }else{
            NSLog(@"the error in fetching the checkins is %@",error);
        }
    }];

   // before return;
    
    while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
        [[NSRunLoop currentRunLoop]
         runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0]];
    }
     
    return annotations;
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didSelectAnnotationViewInMap:mapView];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation conformsToProtocol:@protocol(JPSThumbnailAnnotationProtocol)]) {
        return [((NSObject<JPSThumbnailAnnotationProtocol> *)annotation) annotationViewInMap:mapView];
    }
    return nil;
}


#pragma mark - get the data for the users video
- (PFQuery *)queryForTable {
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"Cuts"];

    //query against the network by default.
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    
    
    //set limit to 3 for now
    [query setLimit:3];
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (videoObjects.count == 0) {
        //query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByDescending:@"createdAt"];
    //adding user obj
    [query includeKey:@"fromUser"];
    
    if (_usedForDisplayingCurrentUser==YES) {
        NSLog(@"used for displaying current user");

        [query whereKey:@"fromUser" equalTo:[PFUser currentUser]];
    }else{
        NSLog(@"Not being used for displaying current user");
        PFQuery*queryForThisUser=[PFUser query];
        NSLog(@"the current parse user object ID is %@",self.parseUserObjectID);
        [queryForThisUser whereKey:@"objectId" equalTo:self.parseUserObjectID] ;

        //start wait
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        
        __block PFObject*thisUser;
        [queryForThisUser getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {

                thisUser=object;
                [query whereKey:@"fromUser" equalTo:thisUser];
                dispatch_semaphore_signal(sema);
            }else{
                NSLog(@"the error in fething the user query is %@",error);
            }
        }];
        
        //
        NSLog(@"not being used for displaying current user");
        
        while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop]
             runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0]];
        }

    }

    //query where from user is equal to self.currentUser if using to view self, use the one passed in to view another
    return query;
}

-(void)addPictureToViewController{
    
    PAImageView *avatarView = [[PAImageView alloc] initWithFrame:CGRectMake(self.KITableView.frame.size.width/2.5, self.KITableView.frame.origin.y-42.5, _profileImageView.frame.size.width/1.5, _profileImageView.frame.size.height/1.5) backgroundProgressColor:[UIColor whiteColor] progressColor:[UIColor lightGrayColor]];

    UIImageView*appLogo=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"appLogoSmall"]];
    [avatarView updateWithImage:appLogo.image animated:YES];
            [self.KITableView addSubview:avatarView];
    //[_profileImageView setImage:appLogo.image];
    
    /*
    if(!_profileImageView.image){
        
        NSLog(@"downloading from online");
        
        [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *FBuser, NSError *error) {
            if (error) {
                // Handle error
                NSLog(@"the error in the profileVC VIEWDIDLOAD is %@",error);
            }
            else //no error
            {
               // OLD  _userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [FBuser username]];
                //new6-24
                _userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=small",self.facebookID];
                
                
                NSLog(@"The url for the user is %@",_userImageURL);


                PAImageView *avatarView = [[PAImageView alloc] initWithFrame:CGRectMake(self.KITableView.frame.size.width/3, self.KITableView.frame.origin.y-42.5, _profileImageView.frame.size.width, _profileImageView.frame.size.height) backgroundProgressColor:[UIColor whiteColor] progressColor:[UIColor lightGrayColor]];
     
                PAImageView *avatarView = [[PAImageView alloc] initWithFrame:_profileImageView.frame backgroundProgressColor:[UIColor whiteColor] progressColor:[UIColor lightGrayColor]];
     
                
                //changing to logo of app
                UIImageView*appLogo=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"appLogo"]];
                
                
                [self.KITableView addSubview:appLogo];
                // Later
                [avatarView setImageURL:_userImageURL];
                
                
                //UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_userImageURL]]];
               // [_profileImageView setImage:image];
                
            }
            
            
        }];
        
    }else{
        NSLog(@"already have picture");
    }
     */
    
      
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 1)
    {
        // if its logout
        if (buttonIndex==0) {
            
            NSLog(@"hi this is 0");
        }
        
        else if (buttonIndex==1) {
            [PFUser logOut];
            NSLog(@"User logged out!");
            //[_sideMenu hide];
            MainViewController*mainViewController=[[MainViewController alloc]init];
            [self.masterNavigationController removeFromParentViewController];
            [self.masterNavigationController dismissViewControllerAnimated:YES completion:nil];
            
            [self presentViewController:mainViewController animated:YES completion:nil];
        }
        
    }
    else if (alertView.tag == 2)
    {
        NSLog(@"this is 2");
        // do something else
    }
    
    // continue for each alertView
    
}


- (NSString *)documentsPathForFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}


#pragma mark- Buttons on View


- (IBAction)refreshView:(id)sender {
    [self viewDidLoad];
}

- (IBAction)seeFollowing:(id)sender {
    NSLog(@"going to see user's following");
    //see the users folling users
}

- (IBAction)seeFollowers:(id)sender {
    NSLog(@"going to see the user's followers");
    //see who is folling the user 

}

- (IBAction)seeCheckIns:(id)sender {
    NSLog(@"going to see user's check-ins");
    //run a query on class check0ins to see where this user exists in the "fromUser" and display the locations with deals on a map
    

}

- (IBAction)seeVideos:(id)sender {
    NSLog(@"going to see user's videos");
    //run query on Cuts table where this user exists in the "fromUser" and display a tableView to show the tableview

}


#pragma mark - TableView Datasource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //should return parse objects count
//    return [aTitles count];
    
    if (videoObjects.count<1) {
        [tableView addSubview:self.userHasNoVideosView];
    }
    return videoObjects.count;
    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    static NSString *CellIdentifier = @"postTableViewCell";
    VideoPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSLog(@"the cell is null");
        [tableView registerNib:[UINib nibWithNibName:@"videoPostTableViewCellView" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        //[tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"videoPostTableViewCellView"];
        
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    // Configure the cell
    
    //refer to object from array
    PFObject*object=[videoObjects objectAtIndex:indexPath.row];
    
    
    //should use cache!
    cell.postObject=object;
    
    
    //cell object id (to maniplate or delete)
    cell.postObjectID=object.objectId;
    
    //current view controller
    cell.fromViewController=self;
    
    
    //cell video file
    
    NSLog(@"the video file url is %@,",[[object objectForKey:@"videoFile"]valueForKey:@"url"]);
    cell.videoURL=[[object objectForKey:@"videoFile"]valueForKey:@"url"];
    
    
    //cell thumbnail
    
    NSString *videoThumbnailURL= [[object objectForKey:@"videoThumbnailImage"]valueForKey:@"url"];
    NSLog(@"testing to see if i need the pffile or not with this URL! %@",[[object objectForKey:@"videoThumbnailImage"]valueForKey:@"url"]);
    
    cell.videoThumnailImageURL=videoThumbnailURL;
    
    NSLog(@"the thumnbail image is being called and allocated with the url %@",videoThumbnailURL);
    
    /* old
     // setup data parsing block
     
     APSmartStorage.sharedInstance.parsingBlock = ^(NSData *data, NSURL *url)
     {
     NSLog(@"data is being allocated in the home vc for an image ! ");
     return [UIImage imageWithData:data scale:UIScreen.mainScreen.scale];
     };
     
     // load object with URL
     [APSmartStorage.sharedInstance loadObjectWithURL:[NSURL URLWithString:videoThumbnailURL] storeInMemory:YES completion:^(id object, NSError *error)
     {
     // hide progress/activity
     if (error)
     {
     // show error
     NSLog(@"error in downloading the videoThumbnailUrl %@",error);
     }
     else
     {
     // do something with object
     [cell.videoImageView setImage:object];
     }
     }];
     */
    [cell.videoImageView setImageWithURL:[NSURL URLWithString:videoThumbnailURL] placeholderImage:[UIImage imageNamed:@"grey"]];
    
    
    
    //cell description
    cell.descriptionLabel.text = [object objectForKey:@"description"];
    
    
    
    //cell user name
    
    cell.userDisplayName.text = [[object objectForKey:@"fromUser"]objectForKey:@"displayName"];
    
    //cell date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSString*postDate= [dateFormat stringFromDate:[object valueForKey:@"createdAt"]];
    NSLog(@"the date of this post is %@",postDate);
    cell.dateLabel.text=postDate;
    
    //cell profile image
    
    NSURL* fbProfileImageURL=[[FBPictureHelper urlForProfilePictureSizeLargeForFBID:[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"]]absoluteURL];
    NSLog(@"the fbProfileImageUrl is %@",fbProfileImageURL);
    
    //[cell.profilePictureView setImage:[self getProfilePicFromFacebookID:fbProfileImageURL.absoluteString]];
    
    /*old
     // load object with URL
     [APSmartStorage.sharedInstance loadObjectWithURL:fbProfileImageURL storeInMemory:YES completion:^(id object, NSError *error)
     {
     // hide progress/activity
     if (error)
     {
     // show error
     NSLog(@"error is %@",error);
     }
     else
     {
     // do something with object
     [cell.profilePictureView setImage:object];
     
     }
     }];
     */
    
    //new
    [cell.profilePictureView setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];
    cell.profilePictureView.layer.cornerRadius = 27.5;
    cell.profilePictureView.layer.masksToBounds = YES;
    cell.profilePictureView.layer.borderWidth = 1;
    cell.profilePictureView.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    //send fb and parse id to the cell, to overload the profileVC so it is ready
    cell.facebookName=[[object objectForKey:@"fromUser"]objectForKey:@"displayName"];
    cell.facebookID=[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"];
    cell.parseUserName=[[object objectForKey:@"fromUser"]objectForKey:@"username"];
    cell.parseUserObjectID=[[object objectForKey:@"fromUser"]valueForKey:@"objectId"];
    
    NSLog(@"the parseUserObjectID for this user is in home vc  %@  with name %@" ,cell.parseUserObjectID,cell.facebookName);
    
    
    //cell tags
    cell.tagsForTagView=[object objectForKey:@"ampersandArray"];
    
    
    //cell like button id
    cell.likeControl.objectID=cell.postObjectID;
    
    
    BOOL isLikedByUser = [[object objectForKey:@"likes"] containsObject:[PFUser currentUser].objectId];
    NSLog(@"the post has been like by the user: %d",isLikedByUser);
    
    for (NSString* item in [object objectForKey:@"likes"])
    {
        if ([item rangeOfString:[PFUser currentUser].objectId].location != NSNotFound){
            NSLog(@"found object in likes array");
            cell.isLikedByUser=YES;
            [cell.likeButton setTitle:@"Liked👌" forState:UIControlStateNormal];
            [cell.likeButton setUserInteractionEnabled:NO];
            
        }else{
            NSLog(@"did not find object in likes array");
            cell.isLikedByUser=NO;
            [cell.likeButton setTitle:@"Like" forState:UIControlStateNormal];
            [cell.likeButton setUserInteractionEnabled:YES];
            
        }
    }
    
    
    
    // [cell.likeControl setFrame:CGRectMake(cell.likeControl.frame.origin.x, cell.likeControl.frame.origin.y, 85, 45)];
    //[cell.likeControl setLikeControlStyle:FBLikeControlStyleStandard];
    //[cell addSubview:cell.likeControl];
    
    
    //cell disable selecting ui
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"the cell's profile pic view is %@",[[tableView cellForRowAtIndexPath:indexPath]valueForKey:@"profilePictureView"]);
    
    UIImageView*currentCellProfilePictureView=[[tableView cellForRowAtIndexPath:indexPath]valueForKey:@"profilePictureView"];
    
    
    //old[_videoPlayerController stop];
    //new 10-13-14
    [_moviePlayer stop];
    
    
    //if not parse cell
    //NSLog(@"the cell's load more... is %@",[tableView cellForRowAtIndexPath:indexPath].textLabel.text);
    if (![[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"Load more..."]) {
        
        
        
        NSURL*currentCellVideoUrl=[NSURL URLWithString:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoURL"]];
        
        
        //start loading indicator
        
        [self.spinner setCenter:CGPointMake([tableView cellForRowAtIndexPath:indexPath].frame.size.width/2.0, [tableView cellForRowAtIndexPath:indexPath].frame.size.height/2.0)]; // I do this because I'm in landscape mode
        [[tableView cellForRowAtIndexPath:indexPath] addSubview:self.spinner]; // spinner is not visible until started
        
        [self.spinner startAnimating];
        
        
        NSString*fileName=[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoURL"]lastPathComponent];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
        
        NSURL *moveUrl = [NSURL fileURLWithPath:path];
        NSLog(@"the video path is %@",moveUrl);
        
        
        if(![[NSFileManager defaultManager] fileExistsAtPath: path]){
            //if it doesnt exist at the path
            
            // setup data parsing block
            APSmartStorage.sharedInstance.parsingBlock = ^(NSData *data, NSURL *url)
            {
                NSLog(@"the download Url is %@",url);
                return data;
            };
            
            [[APSmartStorage sharedInstance]loadObjectWithURL:currentCellVideoUrl storeInMemory:NO progress:^(NSUInteger percents) {
                //log percent here, might add bool that doesnt allow interuption from the didseelctrow method
                NSLog(@"loading to phone store at %i percent",percents);
                //load uiwebview? then dealloc when done?
                
                
            } completion:^(id object, NSError *error) {
                
                //
                if (!error) {
                    
                    NSLog(@"the object is obtained and is ");
                    //ADD VIDEO FILE TO CELL HERE FROM CACHE! OR REFRENCE!!
                    if(![[NSFileManager defaultManager] fileExistsAtPath: path]){
                        [object writeToFile:path atomically:YES];
                        NSLog(@"file does not exist at path, therefore writing it now");
                    }else{
                        NSLog(@"file does  exist at path, therefore; not writing it");
                    }
                    
                    NSURL *moveUrl = [NSURL fileURLWithPath:path];
                    NSLog(@"the movie path is %@",moveUrl);
                    NSLog(@"the post object id currently being played is %@",_moviePlayer.postObjectID);
                    
                    //   [[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"] addSubview:_moviePlayer.view];
                    _moviePlayer.view.frame = CGRectMake([tableView cellForRowAtIndexPath:indexPath].frame.origin.x, [tableView cellForRowAtIndexPath:indexPath].frame.origin.y+52, [tableView cellForRowAtIndexPath:indexPath].frame.size.width, 300);
                    _playButton.center = self.moviePlayer.view.center;
                    
                    //_moviePlayer = moveUrl.absoluteString;
                    _moviePlayer.contentURL=currentCellVideoUrl;
                    _moviePlayer.postObject=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObject"];
                    _moviePlayer.postObjectID=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObjectID"];
                    NSLog(@"the post object id currently being played is %@",_videoPlayerController.postObjectID);
                    [_moviePlayer play];
                    //[[tableView cellForRowAtIndexPath:indexPath] addSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];
                    
                    
                    
                }else
                    NSLog(@"the error is  %@",error);
                
            }];
            
            
            
        }else{
            //if it does exist at the path
            currentCellVideoUrl=moveUrl;
            NSLog(@"the frame of the cells videoThumbnailImage is x: %f  and the y is: %f  and the width: %f , and the height is %f" , [[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"]frame].origin.x,[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"]frame].origin.y,[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"]frame].size.width,[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"]frame].size.height);
            _moviePlayer.view.frame = CGRectMake([tableView cellForRowAtIndexPath:indexPath].frame.origin.x, [tableView cellForRowAtIndexPath:indexPath].frame.origin.y+52, [tableView cellForRowAtIndexPath:indexPath].frame.size.width, 300);
            
            //[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"] addSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];
            _playButton.center = self.moviePlayer.view.center;
            
            _moviePlayer.contentURL=currentCellVideoUrl;
            _moviePlayer.postObject=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObject"];
            _moviePlayer.postObjectID=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObjectID"];
            NSLog(@"the post object id currently being played is %@",_videoPlayerController.postObjectID);
            // [_moviePlayer.view addSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];
            
            //[[tableView cellForRowAtIndexPath:indexPath] addSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];
            //The Image Cirle Bug     [self.moviePlayer.view insertSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"] atIndex:10];
            //[tableView bringSubviewToFront:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];
            [_moviePlayer play];
            
            
        }
        
        
        //regular excecution
        NSLog(@"the video url bing used is %@",currentCellVideoUrl);
        
    }else{
        NSLog(@"trying to get more videos");
        //self.tableView=nil;
        [_videoPlayerController stop];
        
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
        NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
        [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
        NSLog(@"should load ad");
        
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 469;
    
}




#pragma mark- mpmovieplayer/almovieplayer notifications
- (void)MPMoviePlayerPlaybackStateDidChange:(NSNotification *)notification
{
    //NSLog(@"the playback state is %ld",self.moviePlayer.playbackState);
    //are we currently playing?
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying)
    { //yes->do something as we are playing...
        //stop animating spinner
        [self.spinner stopAnimating];
        NSLog(@"started playing");
    }
    else if (self.moviePlayer.playbackState == 5 )
    {
        
        NSLog(@"videoDidEnd with id %@",self.moviePlayer.postObjectID);
        
        //flurry track user discarded Video
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",self.moviePlayer.postObjectID,@"videoID",
                                       nil];
        [Flurry logEvent:@"userViewedVideo" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userViewedVideo" parameters:articleParams];
        
        viewCount++;
        if (viewCount>6) {
            NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
            [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
            NSLog(@"should load ad");
            viewCount=0;
        }
        
        //log view for parse
        [self.moviePlayer.postObject incrementKey:@"views" byAmount:[NSNumber numberWithInt:1]];
        [self.moviePlayer.postObject saveInBackground];
        
    }
}



#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


-(void)hadleTapOnPlayer{
    NSLog(@"recieved tap from player");
    //  NSLog(@"the playback state is %ld",self.moviePlayer.playbackState);
    
    // _playButton.center = self.moviePlayer.view.center;
    
    //add the button
    //play button
    _playButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playImage"]];
    [self.view addSubview:_playButton];
    
    _playButton.center = self.moviePlayer.view.center;
    //_playButton.center=self.moviePlayer.view.center;
    //[_playButton setFrame:CGRectMake(self.view.frame.size.width/2, self.moviePlayer.view.frame.origin.y, 100, 100)];
    _playButton.hidden=YES;
    
    
    if (self.moviePlayer.playbackState != MPMoviePlaybackStatePaused)
    { //yes->do something as we are playing...
        [self.moviePlayer pause];
        
        NSLog(@"the location of the play button is %f",_playButton.frame.origin.y);
        NSLog(@"the location of the player is %f",_moviePlayer.view.frame.origin.y);
        
        [_playButton setImage:[UIImage imageNamed:@"pauseImage"]];
        _playButton.alpha = .6f;
        _playButton.hidden=NO;
        
        [UIView animateWithDuration:0.35f animations:^{
            _playButton.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
            _playButton.hidden = YES;
        }];
        
        
    }else{
        [self.moviePlayer play];
        
        
        NSLog(@"the playback state is %ld",self.moviePlayer.playbackState);
        
        [_playButton setImage:[UIImage imageNamed:@"playImage"]];
        _playButton.alpha = .6f;
        _playButton.hidden=NO;
        
        [UIView animateWithDuration:0.35f animations:^{
            _playButton.alpha = 0.0f;
        } completion:^(BOOL finished) {
            _playButton.hidden = YES;
        }];
        
    }
    
}

#pragma mark - PBJVideoPlayerControllerDelegate

- (void)videoPlayerReady:(PBJVideoPlayerController *)videoPlayer
{
    //NSLog(@"Max duration of the video: %f", videoPlayer.maxDuration);
}

- (void)videoPlayerPlaybackStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
}

- (void)videoPlayerPlaybackWillStartFromBeginning:(PBJVideoPlayerController *)videoPlayer
{
    /*
     dont need because im not using the play button
     _playButton.alpha = 1.0f;
     _playButton.hidden = NO;
     
     [UIView animateWithDuration:0.1f animations:^{
     _playButton.alpha = 0.0f;
     } completion:^(BOOL finished) {
     _playButton.hidden = YES;
     }];
     */
}

- (void)videoPlayerPlaybackDidEnd:(PBJVideoPlayerController *)videoPlayer
{
    /*
     dont need because im not using the play button
     
     _playButton.hidden = NO;
     
     [UIView animateWithDuration:0.1f animations:^{
     _playButton.alpha = 1.0f;
     } completion:^(BOOL finished) {
     }];
     */
    
    //end all subviews
    NSArray *viewsToRemove = [_videoPlayerController.view subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    
    NSLog(@"videoDidEnd with id %@",videoPlayer.postObjectID);
    
    //flurry track user discarded Video
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",videoPlayer.postObjectID,@"videoID",
                                   nil];
    [Flurry logEvent:@"userViewedVideo" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userViewedVideo" parameters:articleParams];
    
    
    viewCount++;
    if (viewCount>6) {
        NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
        [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
        NSLog(@"should load ad");
        viewCount=0;
    }
    
    //log view for parse
    [videoPlayer.postObject incrementKey:@"views" byAmount:[NSNumber numberWithInt:1]];
    [videoPlayer.postObject saveInBackground];
    
    
    
    
}




#pragma mark - PullToReveal Delegate
- (void) PullToRevealDidSearchFor:(NSString *)searchText
{
    NSLog(@"PullToRevealDidSearchFor: %@", searchText);
}


#pragma mark- cleanup

- (void)didReceiveMemoryWarning
{
    NSLog(@"getting memory warning ~ %s",__PRETTY_FUNCTION__);
    
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    NSLog(@"viewDidUnload");
    
    

     
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSLog(@"viewWillDissaper in profile");
    
    [APSmartStorage.sharedInstance removeAllFromMemory];
    self.tableView=nil;
    self.KITableView=nil;
    videoObjects=nil;
    self.mapView=nil;
    
   // [self.navigationController dismissViewControllerAnimated:NO completion:nil];
  //  [self.navigationController removeFromParentViewController];
  //  [self.navigationController popViewControllerAnimated:NO];
    //[self dismissViewControllerAnimated:NO completion:nil];
   // [self removeFromParentViewController];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDissapear in profile");
   // [self.navigationController popViewControllerAnimated:NO];


    
    
}



@end
