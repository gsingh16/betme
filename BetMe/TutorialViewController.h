//
//  TutorialViewController.h
//  BetMe
//
//  Created by Admin on 8/29/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAIntroView.h"
#import "MLPSpotlight.h"
#import <MediaPlayer/MediaPlayer.h>


@interface TutorialViewController : UIViewController <EAIntroDelegate,UIGestureRecognizerDelegate>

//textOverlay
@property(nonatomic,strong)UILabel*overlayText;
@property(nonatomic,strong)UILabel*bottomOverlayText;

//BetMe Tutorial
@property (strong, nonatomic) IBOutlet UIView *betMeTutorial1;
@property (strong, nonatomic) IBOutlet UIView *betMeTutorial2;
@property (strong, nonatomic) IBOutlet UIView *betMeTutorial3;
@property (strong, nonatomic) IBOutlet UIView *betMeTutorial4;
@property (strong, nonatomic) IBOutlet UIView *betMeTutorial5;



//video tutorial views
@property (strong, nonatomic) IBOutlet UIView *videoTutorialView1;

@property (strong, nonatomic) IBOutlet UIView *videoTutorialView2;
@property (strong, nonatomic) IBOutlet UILabel *videoTutorialPage2Label;


@property (strong, nonatomic) IBOutlet UIView *videoTutorialView3;
@property (strong, nonatomic) IBOutlet UIView *videoTutorialView4;
@property (strong, nonatomic) IBOutlet UIView *videoTutorialView5;

- (IBAction)startVideoTutorial:(id)sender;


//deal tutorial views
@property (strong, nonatomic) IBOutlet UIView *dealTutorialView1;
@property (strong, nonatomic) IBOutlet UIView *dealTutorialView2;
@property (strong, nonatomic) IBOutlet UIView *dealTutorialView3;
@property (strong, nonatomic) IBOutlet UIView *dealTutorialView4;
@property (strong, nonatomic) IBOutlet UIView *dealTutorialView5;
- (IBAction)startDealsTutorial:(id)sender;



//ideas tutorial view
@property (strong, nonatomic) IBOutlet UIView *ideasTutorialView1;
@property (strong, nonatomic) IBOutlet UIView *ideasTutorialView2;
@property (strong, nonatomic) IBOutlet UIView *ideasTutorialView3;

- (IBAction)startIdeasTutorial:(id)sender;



//done button
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
- (IBAction)done:(id)sender;


-(void)showBetMeTutorial;


@end
