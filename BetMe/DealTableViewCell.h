//
//  DealTableViewCell.h
//  BetMe
//
//  Created by Admin on 4/19/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIAlertView.h"
#import <Parse/Parse.h>
@interface DealTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *dealImageView;
@property (strong, nonatomic) IBOutlet UILabel *dealTitleDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *quantityRemainingLabel;
@property (strong, nonatomic) IBOutlet UIButton *claimDealButton;
@property (nonatomic,strong)  NSString* dealObjectId;
@property (nonatomic,strong)  NSString* termsAndConditions;
@property (nonatomic,strong)NSNumber* pointsRequired;

//from vc
@property(nonatomic,strong)UIViewController*fromViewController;

@property(nonatomic,assign)int currentPoints;
- (IBAction)claimDeal:(id)sender;

@end
