//
//  homeViewController.m
//  BetMe
//
//  Created by Admin on 6/25/13.
//  Copyright (c) 2013 BinaryBros. All rights reserved.
//

#import "menuBarView.h"
#import "homeViewController.h"
#import "MainViewController.h"
#import "FlipsideViewController.h"
#import "betViewController1.h"
#import "profileViewController.h"
#import "couponViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "betSelectionViewController.h"
#import "VideoPostTableViewCell.h"
#import "FBPictureHelper.h"
#import "PAImageView.h"
#import "CameraViewController.h"
#import "SIAlertView.h"
//test
#import "TGLViewController.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"

#import "parseQueryTableViewController.h"
#import "MyActivityIndicator.h"

//data chaching
#import "APSmartStorage.h"
#import <NSString+MD5.h>
#import "APFileStorage.h"
#import "NSFileManager+Storage.h"


#import "SettingsViewController.h"
#import "TutorialViewController.h"

//flurry
#import "Flurry.h"
#import "FlurryAds.h"

// fb
#import <FBSDKShareKit/FBSDKShareKit.h>



@interface homeViewController () <UIAlertViewDelegate>{
    int viewCount;
}
@property  __block UINavigationController *masterNavigationController;
@property (nonatomic,strong) dispatch_block_t menuBlock;
//viewControllers
@property(nonatomic,strong)UIViewController*homeVC;
@property(nonatomic,strong)UIViewController*profileVC;
@property(nonatomic,strong)UIViewController*couponVC;
@property(nonatomic,strong)UIViewController*settingsVC;

@property(nonatomic,strong)UIButton*recordButton;



@end



@implementation homeViewController
@synthesize sideMenu=_sideMenu;
@synthesize homeVC=_homeVC,profileVC=_profileVC,couponVC=_couponVC,settingsVC=_settingsVC;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        NSLog(@"made it to initWithCoder in home vc with currrent user %@",[[PFUser currentUser]username]);
        // The className to query on
        //self.parseClassName = @"bets";
        
        self.parseClassName = @"Cuts";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"betDescription";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 5;
        
    }
    return self;
}




#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //title
    //self.title = @"Home";
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homeIconSmallWhite"]];
    UIColor *white = [UIColor whiteColor];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:white forKey:NSForegroundColorAttributeName ];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    //status bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //menu bar button
    UIImage *menuImage = [UIImage imageNamed:@"menu"];
    CGRect menuFrame = CGRectMake(0, 0, menuImage.size.width/2.5, menuImage.size.height/3);
    UIButton* menuButton = [[UIButton alloc] initWithFrame:menuFrame];
    [menuButton setBackgroundImage:menuImage forState:UIControlStateNormal];
    [menuButton setShowsTouchWhenHighlighted:YES];
    [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* menu = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menu;
    
    
    //record bar button
    UIImage *image = [UIImage imageNamed:@"notificationIconWhite"];
    CGRect frame = CGRectMake(0, 0, 35, 30);
    UIButton* button = [[UIButton alloc] initWithFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setShowsTouchWhenHighlighted:YES];
    [button addTarget:self action:@selector(openNotifications:) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* record = [[UIBarButtonItem alloc] initWithCustomView:button];

    //wallet bar button
    UIImage *image2 = [UIImage imageNamed:@"wallet"];
    CGRect frame2 = CGRectMake(0, 0, 20, 20);
    UIButton* button2 = [[UIButton alloc] initWithFrame:frame2];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [button2 setShowsTouchWhenHighlighted:YES];
    [button2 addTarget:self action:@selector(showDealsWallet) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* wallet = [[UIBarButtonItem alloc] initWithCustomView:button2];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:wallet ,record,nil];
    
    
    //color
    self.navigationController.navigationBar.barTintColor = [UIColor orangeColor];
    self.navigationController.navigationBar.translucent=YES;
    
    //disable ui
    //self.tableView.userInteractionEnabled = NO;
    

    //set viewcount to 0
    viewCount=0;

    
    
    //observers for notifications

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"profilePictureSelected"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"tagSelected"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MPMoviePlayerPlaybackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEvent:)
                                                 name:@"userWantsToSharePost"
                                               object:nil];
    
    //set max count for memory
   // APSmartStorage.sharedInstance.maxObjectCount = 10;
    
    
    
    //setup movie controller
    
    
    //new
    /*
    _videoPlayerController = [[PBJVideoPlayerController alloc] init];
    _videoPlayerController.delegate = self;
    
    [self addChildViewController:_videoPlayerController];
    [self.view addSubview:_videoPlayerController.view];
    //[_videoPlayerController didMoveToParentViewController:self];
    
    self.view.layer.zPosition=5;
    _videolayer.zPosition=-5;
     NSLog(@"the z index of self.view is %f",self.view.layer.zPosition);

    _playButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play_button"]];
    */
    
    
    //_movieplayer way
    
    // create a movie player
    self.moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.moviePlayer.delegate = self; //IMPORTANT!
    self.moviePlayer.scalingMode=MPMovieScalingModeAspectFill;
    // create the controls
    ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:self.moviePlayer style:ALMoviePlayerControlsStyleNone];
    
    // optionally customize the controls here...
    /*
     [movieControls setBarColor:[UIColor colorWithRed:195/255.0 green:29/255.0 blue:29/255.0 alpha:0.5]];
     [movieControls setTimeRemainingDecrements:YES];
     [movieControls setFadeDelay:2.0];
     [movieControls setBarHeight:100.f];
     [movieControls setSeekRate:2.f];
     */
    
    // assign the controls to the movie player
    [self.moviePlayer setControls:movieControls];
    // Going to place in the THumbail View NAO [self.view addSubview:_moviePlayer.view];
    [self.view addSubview:_moviePlayer.view];
    

    [_moviePlayer.view.layer setZPosition:0];
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    

    
    //tap
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hadleTapOnPlayer)];
    tap.delegate=self;
    [tap setNumberOfTouchesRequired:1];
    [tap setNumberOfTapsRequired:1];
    //[[tableView cellForRowAtIndexPath:indexPath] addGestureRecognizer:tap];
    [_moviePlayer.view addGestureRecognizer:tap];
    
    
    
    NSLog(@"viewDidLoad in Home Vc");
    
    
    //add notification by uiview animation
   // [self.tableView addSubview:self.notificationButton];
    //[self.view addSubview:self.notificationButton];
    UIButton *but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    but.frame= CGRectMake(200, 15, 30, 30);
    [but setTitle:@"Ok" forState:UIControlStateNormal];
    [but addTarget:self action:@selector(openNotifications:) forControlEvents:UIControlEventTouchUpInside];
   // [self.view addSubview:but];
    
    
    self.recordButton=[[UIButton alloc]initWithFrame:CGRectMake(130, 700, 50, 50)];
    [self.recordButton setBackgroundImage:[UIImage imageNamed:@"centerVideoIcon"] forState:UIControlStateNormal];
    [self.recordButton addTarget:self action:@selector(startNewVideo) forControlEvents:UIControlEventTouchDown];
    [self.parentViewController.view addSubview:self.recordButton];

    [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [self.recordButton setFrame:CGRectMake(130, self.view.frame.size.height-30, 50, 50)];
        
    } completion:^(BOOL finished) {
        
    }];
     

    
}






- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    


    
    
}



#pragma mark- menu buttons

- (void)showMenu
{
    //[self.sideMenuViewController presentMenuViewController];
    MZRSlideInMenu *menu = [[MZRSlideInMenu alloc] init];
    [menu setDelegate:self];
    [menu addMenuItemWithTitle:@"Home"];
    [menu addMenuItemWithTitle:@"Profile"];
    //[menu addMenuItemWithTitle:@"Explore"];
    [menu addMenuItemWithTitle:@"Deals"];
    [menu addMenuItemWithTitle:@"Settings"];
    [menu showMenuFromLeft];
}

- (void)slideInMenu:(MZRSlideInMenu *)menuView didSelectAtIndex:(NSUInteger)index
{
    NSString *buttonTitle = [menuView buttonTitleAtIndex:index];
    
    

    switch ((long)index) {
        case 0:
        {
            //0
            //do nothing since it is here
            NSLog(@"%@(%ld) tapped in viewcontroller %@",buttonTitle, (long)index, [self presentingViewController] );
            /*
            NSLog(@"%@(%ld) tapped in viewcontroller %@",buttonTitle, (long)index, [self presentedViewController] );

            
            if(!_homeVC){
                
            homeViewController*home=[[homeViewController alloc]initWithCoder:nil];
            UINavigationController*homeVC=[[UINavigationController alloc] initWithRootViewController:home];
            _homeVC=homeVC;
            NSLog(@"homeVC is being created");
                
            }
            
            [self presentViewController:_homeVC animated:YES completion:nil];



           */
        }
            break;
            
        case 1:
        {
            //1
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if ([PFUser currentUser]==NULL) {
                NSLog(@"current user is anon %@",[PFUser currentUser]);
                SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
                [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
                [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
                
                //yes
                [alertView addButtonWithTitle:@"Ok"
                                         type:SIAlertViewButtonTypeDestructive
                                      handler:^(SIAlertView *alert) {
                                          NSLog(@"Button1 Clicked");
                                          [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                          [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                          //yes
                                          //analytics
                                          MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                          // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                          [self presentViewController:main animated:YES completion:^{
                                              NSLog(@"successfully presented home view controller from mainview controller");
                                          }];
                                          
                                          
                                          
                                      }];
                
                [alertView addButtonWithTitle:@"Cancel"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          NSLog(@"Button2 Clicked");
                                          //analytics
                                      }];
                
                alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                
                [alertView show];
            }else{
            if (!_profileVC) {
                NSLog(@"profileVC is being created");
                [FBSDKAppEvents logEvent:@"User Wants to see their profile"];
                [Flurry logEvent:@"User Wants to see their profile"];
                
                profileViewController*profile=[[profileViewController alloc] initWithCoder:nil];
                profile.usedForDisplayingCurrentUser=YES;
                profile.facebookName=[[PFUser currentUser]objectForKey:@"displayName"];
                profile.facebookID=[[PFUser currentUser]objectForKey:@"facebookId"];
                profile.parseUserName=[[PFUser currentUser]objectForKey:@"username"];
                profile.parseUserObjectID=[PFUser currentUser].objectId;
                UINavigationController*profileVC=[[UINavigationController alloc] initWithRootViewController:profile];
                _profileVC=profileVC;
                
            }


            [self presentViewController:_profileVC animated:YES completion:nil];
            }
            
        }
            break;
    
        case 2:
        {
            //2
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if (!_couponVC) {
                
                _couponVC=[[couponViewController alloc]initWithCoder:nil];
                UINavigationController*couponsVC=[[UINavigationController alloc] initWithRootViewController:_couponVC];
                _couponVC=couponsVC;
                
                NSLog(@"couponsVC is being created");

            }
        
            [self presentViewController:_couponVC animated:YES completion:nil];
           // [self.navigationController setViewControllers:@[_couponVC]];
           // [self.navigationController popViewControllerAnimated:YES];
            //[self.navigationController pushViewController:_couponVC animated:YES];
            
        }
            
            break;
        case 3:
        {
            //3
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if (!_settingsVC) {
                SettingsViewController*settings=[[SettingsViewController alloc]initWithCoder:nil];
                UINavigationController*settingsVC=[[UINavigationController alloc] initWithRootViewController:settings];
                _settingsVC=settingsVC;
                NSLog(@"settingsVC is being created");

            }
            /*
            [self.navigationController setViewControllers:@[_settingsVC]];
            [self.navigationController popViewControllerAnimated:YES];
            */
            [self presentViewController:_settingsVC animated:YES completion:nil];


        }
            break;
            
        default:

            break;
    }
    
}

#pragma mark- new video
- (void)startNewVideo{
    //check for anon
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:^{
                                      NSLog(@"successfully presented home view controller from mainview controller");
                                  }];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to record a new video"];
        [Flurry logEvent:@"User wants to record a new video"];
        
        
    CameraViewController*betPage=[[CameraViewController alloc]init];
//    betSelectionViewController*betPage=[[betSelectionViewController alloc]init];
    self.navigationController.navigationBarHidden = NO;
//    [self.navigationController prese:betPage animated:YES];
    [self.navigationController presentViewController:betPage animated:YES completion:nil];
    }
}






- (void)showDealsWallet{
    //check for anon
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:^{
                                      NSLog(@"successfully presented home view controller from mainview controller");
                                  }];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
    
    //current way
    NSLog(@"the current user is %@",[PFUser currentUser].objectId);
    
        [FBSDKAppEvents logEvent:@"User wants to see their deal wallet"];
        [Flurry logEvent:@"User wants to see their deal wallet"];
        
        
    //get users points
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    [query includeKey:@"deals.fromBusiness"];
    
    
    //start query
    __block TGLViewController*vc;
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            NSArray*deals=[object objectForKey:@"deals"];
            //NSLog(@"the deals are in dealsinWallet %@",deals);
            
            //load the controller now
            UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
            // [aFlowLayout setItemSize:CGSizeMake(400, 400)];
            [aFlowLayout setItemSize:CGSizeMake(320, 463)];
            [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
            vc = [[TGLViewController alloc]initWithCollectionViewLayout:aFlowLayout];
            vc.dealsInWallet=[[NSMutableArray alloc]initWithArray:deals];;
            
            
            self.navigationController.navigationBarHidden=NO;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else
            NSLog(@"error : %@",error);
    }];
        
    }
    

}



#pragma mark- cleanup 

- (void)didReceiveMemoryWarning
{
    NSLog(@"getting memory warning ~ %s",__PRETTY_FUNCTION__);

    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    NSLog(@"viewDidUnload");
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSLog(@"viewWillDissaper in homevc");
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [self.recordButton setFrame:CGRectMake(130, self.view.frame.size.height+90, 50, 50)];
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDissapear in home vc");
    
    [APSmartStorage.sharedInstance removeAllFromMemory];
    self.tableView=nil;

}




#pragma mark - PFQueryTableViewController

- (void)objectsWillLoad {
    //self.tableView.dataSource=nil;
    [super objectsWillLoad];
    // This method is called before a PFQuery is fired to get more objects
    
    //should use! -->[self clear];
    if (self.objects.count>1) {
     //   [self clear];
        [APSmartStorage.sharedInstance removeAllFromMemory];
    }

    
    NSLog(@"all the objects in home vc are %lu",(unsigned long)self.objects.count) ;

}

- (void)objectsDidLoad:(NSError *)error {

    [super objectsDidLoad:error];
    // This method is called every time objects are loaded from Parse via the PFQuery

}



// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Cuts"];
    // If Pull To Refresh is enabled, query against the network by default.
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    [query orderByDescending:@"createdAt"];
    //adding user obj
    [query includeKey:@"fromUser"];
    
    //limit count
    [query setLimit:5];
    
    return query;
}


#pragma mark - Table view delegate

// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the textKey in the object,
// and the imageView being the imageKey in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    NSLog(@"somethings going on");
    
    
    static NSString *CellIdentifier = @"postTableViewCell";
    VideoPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSLog(@"the cell is null");
        [tableView registerNib:[UINib nibWithNibName:@"videoPostTableViewCellView" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        //[tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"videoPostTableViewCellView"];

        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
    }

    // Configure the cell
    
    //should use cache!
    cell.postObject=object;

    
    //cell object id (to maniplate or delete)
    cell.postObjectID=object.objectId;

    //current view controller
    cell.fromViewController=self;
    
    
    //cell video file

        NSLog(@"the video file url is %@,",[[object objectForKey:@"videoFile"]valueForKey:@"url"]);
        cell.videoURL=[[object objectForKey:@"videoFile"]valueForKey:@"url"];


    //cell thumbnail
    
        NSString *videoThumbnailURL= [[object objectForKey:@"videoThumbnailImage"]valueForKey:@"url"];
        NSLog(@"testing to see if i need the pffile or not with this URL! %@",[[object objectForKey:@"videoThumbnailImage"]valueForKey:@"url"]);
    
        cell.videoThumnailImageURL=videoThumbnailURL;

    NSLog(@"the thumnbail image is being called and allocated with the url %@",videoThumbnailURL);
    

    [cell.videoImageView sd_setImageWithURL:[NSURL URLWithString:videoThumbnailURL] placeholderImage:[UIImage imageNamed:@"grey"]];
    

    
    //cell description
       cell.descriptionLabel.text = [object objectForKey:@"description"];


    
    //cell user name

        cell.userDisplayName.text = [[object objectForKey:@"fromUser"]objectForKey:@"displayName"];

    //cell date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSString*postDate= [dateFormat stringFromDate:[object valueForKey:@"createdAt"]];
    NSLog(@"the date of this post is %@",postDate);
    cell.dateLabel.text=postDate;
    
    //cell profile image
    NSURL* fbProfileImageURL=[[FBPictureHelper urlForProfilePictureSizeLargeForFBID:[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"]]absoluteURL];
    NSLog(@"the fbProfileImageUrl is %@",fbProfileImageURL);
    

    
    //new
    [cell.profilePictureView sd_setImageWithURL:fbProfileImageURL placeholderImage:[UIImage imageNamed:@"grey"]];
    cell.profilePictureView.layer.cornerRadius = 27.5;
    cell.profilePictureView.layer.masksToBounds = YES;
    cell.profilePictureView.layer.borderWidth = 1;
    cell.profilePictureView.layer.borderColor = [[UIColor orangeColor] CGColor];

    //share button
    cell.shareButton.layer.cornerRadius = 2.5;
    cell.shareButton.layer.masksToBounds = YES;
    
    //comment button
    cell.commentButton.layer.cornerRadius = 2.5;
    cell.commentButton.layer.masksToBounds = YES;
    
    
    //send fb and parse id to the cell, to overload the profileVC so it is ready
    cell.facebookName=[[object objectForKey:@"fromUser"]objectForKey:@"displayName"];
    cell.facebookID=[[object objectForKey:@"fromUser"]objectForKey:@"facebookId"];
    cell.parseUserName=[[object objectForKey:@"fromUser"]objectForKey:@"username"];
    cell.parseUserObjectID=[[object objectForKey:@"fromUser"]valueForKey:@"objectId"];

    NSLog(@"the parseUserObjectID for this user is in home vc  %@  with name %@" ,cell.parseUserObjectID,cell.facebookName);
    

    //cell tags
    cell.tagsForTagView=[object objectForKey:@"ampersandArray"];

    //cell author
    cell.postAuthor=[object objectForKey:@"fromUser"];
    
    //cell like button id
//    cell.likeControl.objectID=cell.postObjectID;
    // THIS WAS THE ONLY ONE NEEDED cell.likeControl.objectID=cell.videoURL;
  //  [cell.likeControl setLikeControlStyle:FBLikeControlStyleStandard];
    //[cell.likeControl setObjectType:FBSDKLikeObjectTypeUnknown];
        //Might need new comment button here SMH

    //like button property
    
    
    if ([PFUser currentUser]) {
    
     BOOL isLikedByUser = [[object objectForKey:@"likes"] containsObject:[PFUser currentUser].objectId];
     NSLog(@"the post has been like by the user: %d",isLikedByUser);

    for (NSString* item in [object objectForKey:@"likes"])
    {
        if ([item rangeOfString:[PFUser currentUser].objectId].location != NSNotFound){
            NSLog(@"found object in likes array");
            cell.isLikedByUser=YES;
            [cell.likeButton setTitle:@"Liked👌" forState:UIControlStateNormal];
            [cell.likeButton setUserInteractionEnabled:NO];

            }else{
            NSLog(@"did not find object in likes array");
            cell.isLikedByUser=NO;
            [cell.likeButton setTitle:@"Like" forState:UIControlStateNormal];
            [cell.likeButton setUserInteractionEnabled:YES];

            }
    }
    
    }else
        NSLog(@"User is anon, can not retrieve like data");
    
    //cell disable selecting ui
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"a cell is ending display");
//    cell=nil;
    //close menu
   // [self.sideMenu close];
    [_videoPlayerController stop];

    
}

//footer


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    /*
    UITabBar *myTabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, tableView.frame.size.height, 320, 50)];
    myTabBar.delegate=self;
    
    NSMutableArray *tabBarItems = [[NSMutableArray alloc] init];
    
    
    UITabBarItem *tabBarItem1 = [[UITabBarItem alloc] initWithTitle:@"Notifications" image:[UIImage imageNamed:@"notificationIconSmallDark"] tag:0];
    UITabBarItem *tabBarItem2 = [[UITabBarItem alloc] initWithTitle:@"Record" image:[UIImage imageNamed:@"recordIconMediumDark"] tag:1];
    UITabBarItem *tabBarItem3 = [[UITabBarItem alloc] initWithTitle:@"Wallet" image:[UIImage imageNamed:@"walletIconSmall"] tag:2];
    
    
    
    
    [tabBarItems addObject:tabBarItem1];
    [tabBarItems addObject:tabBarItem2];
    [tabBarItems addObject:tabBarItem3];
    
    myTabBar.items = tabBarItems;
    myTabBar.selectedItem = [tabBarItems objectAtIndex:0];
    
    return myTabBar;
    */
    
    
    
    UIView*bottomView=[[UIView alloc]initWithFrame:CGRectMake(0, tableView.frame.size.height, 320, 50)];
    [bottomView setBackgroundColor:[UIColor orangeColor]];
  

    
    
    
    return bottomView;
    
    
    
    //UIButton*recordButton=[[UIButton alloc]initWithFrame:CGRectMake(130, 400, 128, 87)];
    //[recordButton setBackgroundImage:[UIImage imageNamed:@"recordIconOrange"] forState:UIControlStateNormal];
    
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    
    //changig to 0 so not using
    return 25;
}

#pragma play video


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    


    
    //old[_videoPlayerController stop];
    //new 10-13-14
    [_moviePlayer stop];
    

    //if not parse cell
    //NSLog(@"the cell's load more... is %@",[tableView cellForRowAtIndexPath:indexPath].textLabel.text);
    if (![[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"Load more..."]) {
        
        NSLog(@"the cell's profile pic view is %@",[[tableView cellForRowAtIndexPath:indexPath]valueForKey:@"profilePictureView"]);
        
        UIImageView*currentCellProfilePictureView=[[tableView cellForRowAtIndexPath:indexPath]valueForKey:@"profilePictureView"];

        
        NSURL*currentCellVideoUrl=[NSURL URLWithString:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoURL"]];
        
        
        //start loading indicator
        
        [self.spinner setCenter:CGPointMake([tableView cellForRowAtIndexPath:indexPath].frame.size.width/2.0, [tableView cellForRowAtIndexPath:indexPath].frame.size.height/2.0)]; // I do this because I'm in landscape mode
        [[tableView cellForRowAtIndexPath:indexPath] addSubview:self.spinner]; // spinner is not visible until started
        
        [self.spinner startAnimating];

        
        NSString*fileName=[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoURL"]lastPathComponent];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
        
        NSURL *moveUrl = [NSURL fileURLWithPath:path];
        NSLog(@"the video path is %@",moveUrl);

        
        if(![[NSFileManager defaultManager] fileExistsAtPath: path]){
        //if it doesnt exist at the path

        // setup data parsing block
        APSmartStorage.sharedInstance.parsingBlock = ^(NSData *data, NSURL *url)
        {
            NSLog(@"the download Url is %@",url);
            return data;
        };
            
        [[APSmartStorage sharedInstance]loadObjectWithURL:currentCellVideoUrl storeInMemory:NO progress:^(NSUInteger percents) {
            //log percent here, might add bool that doesnt allow interuption from the didseelctrow method
            NSLog(@"loading to phone store at %i percent",percents);
            //load uiwebview? then dealloc when done?
            
            
        } completion:^(id object, NSError *error) {
   
            //
            if (!error) {
           
                NSLog(@"the object is obtained and is ");
                //ADD VIDEO FILE TO CELL HERE FROM CACHE! OR REFRENCE!!
                if(![[NSFileManager defaultManager] fileExistsAtPath: path]){
                    [object writeToFile:path atomically:YES];
                    NSLog(@"file does not exist at path, therefore writing it now");
                }else{
                    NSLog(@"file does  exist at path, therefore; not writing it");
                }
                
                NSURL *moveUrl = [NSURL fileURLWithPath:path];
                NSLog(@"the movie path is %@",moveUrl);
                NSLog(@"the post object id currently being played is %@",_moviePlayer.postObjectID);
 
             //   [[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"] addSubview:_moviePlayer.view];
                _moviePlayer.view.frame = CGRectMake([tableView cellForRowAtIndexPath:indexPath].frame.origin.x, [tableView cellForRowAtIndexPath:indexPath].frame.origin.y+55, [tableView cellForRowAtIndexPath:indexPath].frame.size.width, 300);
                _playButton.center = self.moviePlayer.view.center;

                //_moviePlayer = moveUrl.absoluteString;
                _moviePlayer.contentURL=currentCellVideoUrl;
                _moviePlayer.postObject=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObject"];
                _moviePlayer.postObjectID=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObjectID"];
                NSLog(@"the post object id currently being played is %@",_videoPlayerController.postObjectID);
                [_moviePlayer play];
                //[[tableView cellForRowAtIndexPath:indexPath] addSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];


                
            }else
                NSLog(@"the error is  %@",error);
            
        }];


            
        }else{
            //if it does exist at the path
            currentCellVideoUrl=moveUrl;
            NSLog(@"the frame of the cells videoThumbnailImage is x: %f  and the y is: %f  and the width: %f , and the height is %f" , [[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"]frame].origin.x,[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"]frame].origin.y,[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"]frame].size.width,[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"]frame].size.height);
            _moviePlayer.view.frame = CGRectMake([tableView cellForRowAtIndexPath:indexPath].frame.origin.x, [tableView cellForRowAtIndexPath:indexPath].frame.origin.y+55, [tableView cellForRowAtIndexPath:indexPath].frame.size.width, 300);
            
            //[[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"videoImageView"] addSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];
            _playButton.center = self.moviePlayer.view.center;

            _moviePlayer.contentURL=currentCellVideoUrl;
            _moviePlayer.postObject=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObject"];
            _moviePlayer.postObjectID=[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"postObjectID"];
            NSLog(@"the post object id currently being played is %@",_videoPlayerController.postObjectID);
           // [_moviePlayer.view addSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];

            //[[tableView cellForRowAtIndexPath:indexPath] addSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];
       //The Image Cirle Bug     [self.moviePlayer.view insertSubview:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"] atIndex:10];
            //[tableView bringSubviewToFront:[[tableView cellForRowAtIndexPath:indexPath]valueForKeyPath:@"profilePictureView"]];
            [_moviePlayer play];


        }
        
        
        //regular excecution
        NSLog(@"the video url bing used is %@",currentCellVideoUrl);

    }else{
        NSLog(@"trying to get more videos");
        //self.tableView=nil;
       // [_videoPlayerController stop];
        [self loadNextPage];
        

        
    }

}





- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 453;
    
}

#pragma mark- get fb picture for user


-(UIImage*)getProfilePicFromFacebookID:(NSString*)fbID{
    
    // this gets a large profile image url for user 'dasmer'. You could  use the User's Facebook ID instead of the username
    
    //start the wait program
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    
    __block UIImage *image;
    NSURL *url = [FBPictureHelper urlForProfilePictureSizeLargeForFBID:fbID];


     [SDWebImageDownloader.sharedDownloader downloadImageWithURL:url
                                                    options:0
                                                   progress:^(NSInteger receivedSize, NSInteger expectedSize)
 {
     // progression tracking code
 }
                                                  completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
 {
     if (image && finished)
     {
         // do something with image
         
     }
 }];
    
    
    NSLog(@"going to return the image now");
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    return image;
    
}



#pragma mark- get thumnail image for video
- (UIImage*)loadThumnbailWithURL:(NSURL*)url {
    
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    
    return [[UIImage alloc] initWithCGImage:imgRef];
    
}



#pragma mark- notification 
- (void)receiveEvent:(NSNotification *)notification {
    // handle event
    
    
    if ([notification.name isEqual:@"profilePictureSelected"]) {
        //clean up memory
        //[APSmartStorage.sharedInstance removeAllFromMemory];
        
        //push to profile vc
        if([self.navigationController.topViewController isKindOfClass:[homeViewController class]]) {

        NSLog(@"recieved event in homeViewController  and the object is %@",notification.object);
        [self.navigationController pushViewController:notification.object animated:YES];
  
            //flurry track wants to see profile
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [[PFUser currentUser]username] , @"user",
                                           nil];
            [Flurry logEvent:@"userWantsToSeeProfile" withParameters:articleParams];
            [FBSDKAppEvents logEvent:@"userWantsToSeeProfile" parameters:articleParams];

            
            
        }
        
        
    }else if ([notification.name isEqual:@"tagSelected"]){
        //flurry track wants to see profile
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user", notification.object,@"hashtag",
                                       nil];
        [Flurry logEvent:@"userWantsToSimilarHashTags" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userWantsToSimilarHashTags" parameters:articleParams];

        parseQueryTableViewController*pqtvc=[[parseQueryTableViewController alloc]initWithCoder:nil];
        pqtvc.passedTag=notification.object;
        pqtvc.usedForMultipleSearch=NO;
        
        if([self.navigationController.topViewController isKindOfClass:[homeViewController class]]) {

        [self.navigationController pushViewController:pqtvc animated:YES];
            NSLog(@"pushing new view controller");
        }
    }else if ([notification.name isEqual:@"userWantsToSharePost"]){


        
        [FBSDKShareDialog showFromViewController:self
                                     withContent:notification.object
                                        delegate:self];
        
        
       // [self presentViewController:notification.object animated:YES completion:^{
            NSLog(@"share sheet has been presented.");
        

    }
    
}

#pragma mark- fbsdk messenger delegate
-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    NSLog(@"sharer did complete with results %@",results);
}
-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    NSLog(@"sharer did fail with error %@",error);
}
-(void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    NSLog(@"sharer did cancel");
}

#pragma mark- mpmovieplayer/almovieplayer notifications
- (void)MPMoviePlayerPlaybackStateDidChange:(NSNotification *)notification
{
    //NSLog(@"the playback state is %ld",self.moviePlayer.playbackState);
    //are we currently playing?
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying)
    { //yes->do something as we are playing...
        //stop animating spinner
        [self.spinner stopAnimating];
        NSLog(@"started playing");
    }
    else if (self.moviePlayer.playbackState == 5 )
    {
        
        NSLog(@"videoDidEnd with id %@",self.moviePlayer.postObjectID);
        
        //flurry track user discarded Video
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[PFUser currentUser]username] , @"user",self.moviePlayer.postObjectID,@"videoID",
                                       nil];
        [Flurry logEvent:@"userViewedVideo" withParameters:articleParams];
        [FBSDKAppEvents logEvent:@"userViewedVideo" parameters:articleParams];

        viewCount++;
        if (viewCount>6) {
            NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
            [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
            NSLog(@"should load ad");
            viewCount=0;
        }
        
        //log view for parse
        [self.moviePlayer.postObject incrementKey:@"views" byAmount:[NSNumber numberWithInt:1]];
        [self.moviePlayer.postObject saveInBackground];
    
    }
}


#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


-(void)hadleTapOnPlayer{
    

    
    NSLog(@"recieved tap from player");
  //  NSLog(@"the playback state is %ld",self.moviePlayer.playbackState);
    
   // _playButton.center = self.moviePlayer.view.center;
    
    //add the button
    //play button
    _playButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playImage"]];
    [self.view addSubview:_playButton];

    _playButton.center = self.moviePlayer.view.center;
    //_playButton.center=self.moviePlayer.view.center;
    //[_playButton setFrame:CGRectMake(self.view.frame.size.width/2, self.moviePlayer.view.frame.origin.y, 100, 100)];
    _playButton.hidden=YES;
    
    
    if (self.moviePlayer.playbackState != MPMoviePlaybackStatePaused)
    { //yes->do something as we are playing...
        [self.moviePlayer pause];
        
        NSLog(@"the location of the play button is %f",_playButton.frame.origin.y);
        NSLog(@"the location of the player is %f",_moviePlayer.view.frame.origin.y);

        [_playButton setImage:[UIImage imageNamed:@"pauseImage"]];
        _playButton.alpha = .6f;
        _playButton.hidden=NO;
        
        [UIView animateWithDuration:0.35f animations:^{
            _playButton.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
            _playButton.hidden = YES;
        }];
        
        
    }else{
        [self.moviePlayer play];
        

        NSLog(@"the playback state is %ld",self.moviePlayer.playbackState);
        
        [_playButton setImage:[UIImage imageNamed:@"playImage"]];
        _playButton.alpha = .6f;
        _playButton.hidden=NO;
        
        [UIView animateWithDuration:0.35f animations:^{
            _playButton.alpha = 0.0f;
        } completion:^(BOOL finished) {
            _playButton.hidden = YES;
        }];

    }
    
}

#pragma mark - PBJVideoPlayerControllerDelegate

- (void)videoPlayerReady:(PBJVideoPlayerController *)videoPlayer
{
    //NSLog(@"Max duration of the video: %f", videoPlayer.maxDuration);
}

- (void)videoPlayerPlaybackStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
}

- (void)videoPlayerPlaybackWillStartFromBeginning:(PBJVideoPlayerController *)videoPlayer
{
    /*
     dont need because im not using the play button
    _playButton.alpha = 1.0f;
    _playButton.hidden = NO;
    
    [UIView animateWithDuration:0.1f animations:^{
        _playButton.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _playButton.hidden = YES;
    }];
     */
}

- (void)videoPlayerPlaybackDidEnd:(PBJVideoPlayerController *)videoPlayer
{
    /*
     dont need because im not using the play button

    _playButton.hidden = NO;
    
    [UIView animateWithDuration:0.1f animations:^{
        _playButton.alpha = 1.0f;
    } completion:^(BOOL finished) {
    }];
     */
    
    //end all subviews
    NSArray *viewsToRemove = [_videoPlayerController.view subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    
    NSLog(@"videoDidEnd with id %@",videoPlayer.postObjectID);
    
    //flurry track user discarded Video
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",videoPlayer.postObjectID,@"videoID",
                                   nil];
    [Flurry logEvent:@"userViewedVideo" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userViewedVideo" parameters:articleParams];

    
    viewCount++;
    if (viewCount>6) {
        NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
        [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
        NSLog(@"should load ad");
        viewCount=0;
    }
    
    //log view for parse
    [videoPlayer.postObject incrementKey:@"views" byAmount:[NSNumber numberWithInt:1]];
    [videoPlayer.postObject saveInBackground];
    
    
    
    
}





- (IBAction)openNotifications:(id)sender {
    NotificationTableViewController*notificationTBVC=[[NotificationTableViewController alloc]init];
    notificationTBVC.parseClassName=@"Notifications";
    [self presentViewController:notificationTBVC animated:YES completion:nil];
}
@end
