//
//  SettingsViewController.m
//  BetMe
//
//  Created by Admin on 8/3/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import "SettingsViewController.h"
#import "betSelectionViewController.h"
#import <Parse/Parse.h>
#include "RESideMenu.h"
#import "RESideMenu.h"
#import "TGLViewController.h"
#import "homeViewController.h"
#import "MainViewController.h"
#import "MDBrowser.h"
#import "IQFeedbackView.h"
#import "SIAlertView.h"
#import "TutorialViewController.h"
#import "Flurry.h"
#import "CameraViewController.h"
#import "SIAlertView.h"
@interface SettingsViewController () <MDBrowserAnimator,BrowserViewDelegate>
//viewControllers
@property(nonatomic,strong)homeViewController*homeVC;
@property(nonatomic,strong)profileViewController*profileVC;
@property(nonatomic,strong)couponViewController*couponVC;
@property(nonatomic,strong)SettingsViewController*settingsVC;


@end

@implementation SettingsViewController
@synthesize homeVC=_homeVC,profileVC=_profileVC,couponVC=_couponVC,settingsVC=_settingsVC;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"logging the viewDidLoad in the settings vc");;
    [self loadNavigationButtons];
    
    NSMutableArray * buttonsArray=[[NSMutableArray alloc]init];
    
    
    self.feedBackButton=[KHFlatButton buttonWithFrame:self.feedBackButton.frame withTitle:@"Feedback" backgroundColor:[UIColor orangeColor]];
    [buttonsArray addObject:self.feedBackButton];
    
    self.termsAndConditionsButton=[KHFlatButton buttonWithFrame:self.termsAndConditionsButton.frame withTitle:@"Terms and Conditions" backgroundColor:[UIColor orangeColor]];
    [buttonsArray addObject:self.termsAndConditionsButton];

    self.privacyPolicyButton=[KHFlatButton buttonWithFrame:self.privacyPolicyButton.frame withTitle:@"Privacy Policy" backgroundColor:[UIColor orangeColor]];
    [buttonsArray addObject:self.privacyPolicyButton];

    self.attributionButton=[KHFlatButton buttonWithFrame:self.attributionButton.frame withTitle:@"Attribution" backgroundColor:[UIColor orangeColor]];
    [buttonsArray addObject:self.attributionButton];

    self.tutorialsButton=[KHFlatButton buttonWithFrame:self.tutorialsButton.frame withTitle:@"Tutorials" backgroundColor:[UIColor orangeColor]];
    [buttonsArray addObject:self.tutorialsButton];

    self.logOutButton=[KHFlatButton buttonWithFrame:self.logOutButton.frame withTitle:@"Log Out" backgroundColor:[UIColor orangeColor]];
    [buttonsArray addObject:self.logOutButton];
    
    
    [self.feedBackButton addTarget:self action:@selector(feedback:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.termsAndConditionsButton addTarget:self action:@selector(termsAndConditions:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.privacyPolicyButton addTarget:self action:@selector(privacyPolicy:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.attributionButton addTarget:self action:@selector(attribution:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.tutorialsButton addTarget:self action:@selector(tutorials:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.logOutButton addTarget:self action:@selector(logOut:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [buttonsArray enumerateObjectsUsingBlock:^(UIView* object, NSUInteger idx, BOOL *stop) {
        // do something with object
        object.layer.cornerRadius = 20.0;
        object.layer.masksToBounds = YES;
        [self.view addSubview:object];
    }];
    
    
}

-(void)loadNavigationButtons{
    self.title = @"Settings";
    
    UIColor *white = [UIColor whiteColor];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:white forKey:NSForegroundColorAttributeName ];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    //status bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    //menu bar button
    UIImage *menuImage = [UIImage imageNamed:@"menu"];
    CGRect menuFrame = CGRectMake(0, 0, menuImage.size.width/2.5, menuImage.size.height/3);
    UIButton* menuButton = [[UIButton alloc] initWithFrame:menuFrame];
    [menuButton setBackgroundImage:menuImage forState:UIControlStateNormal];
    [menuButton setShowsTouchWhenHighlighted:YES];
    [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* menu = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menu;
    
    
    //record bar button
    UIImage *image = [UIImage imageNamed:@"notificationIconWhite"];
    CGRect frame = CGRectMake(0, 0, 35, 30);
    UIButton* button = [[UIButton alloc] initWithFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setShowsTouchWhenHighlighted:YES];
    [button addTarget:self action:@selector(openNotifications) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* record = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    //wallet bar button
    UIImage *image2 = [UIImage imageNamed:@"wallet"];
    CGRect frame2 = CGRectMake(0, 0, 20, 20);
    UIButton* button2 = [[UIButton alloc] initWithFrame:frame2];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [button2 setShowsTouchWhenHighlighted:YES];
    [button2 addTarget:self action:@selector(showDealsWallet) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem* wallet = [[UIBarButtonItem alloc] initWithCustomView:button2];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:wallet ,record,nil];
    
    //color
    self.navigationController.navigationBar.barTintColor = [UIColor orangeColor];
    self.navigationController.navigationBar.translucent=YES;
}

#pragma mark- menu buttons

- (void)showMenu
{
    //[self.sideMenuViewController presentMenuViewController];
    MZRSlideInMenu *menu = [[MZRSlideInMenu alloc] init];
    [menu setDelegate:self];
    [menu addMenuItemWithTitle:@"Home"];
    [menu addMenuItemWithTitle:@"Profile"];
    [menu addMenuItemWithTitle:@"Deals"];
    [menu addMenuItemWithTitle:@"Settings"];
    [menu showMenuFromLeft];
}



- (void)slideInMenu:(MZRSlideInMenu *)menuView didSelectAtIndex:(NSUInteger)index
{
    NSString *buttonTitle = [menuView buttonTitleAtIndex:index];
    
    
    
    switch ((long)index) {
        case 0:
        {
            //0
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if(!_homeVC){
                
                homeViewController*home=[[homeViewController alloc]initWithCoder:nil];
                UINavigationController*homeVC=[[UINavigationController alloc] initWithRootViewController:home];
                _homeVC=homeVC;
                NSLog(@"homeVC is being created");
                
            }
            
            /*
             [self.navigationController setViewControllers:@[_homeVC]];
             [self.navigationController popViewControllerAnimated:YES];
             */
            [self presentViewController:_homeVC animated:YES completion:nil];
            
            
        }
            break;
            
             case 1:
             {
                 
                 if ([PFUser currentUser]==NULL) {
                     NSLog(@"current user is anon %@",[PFUser currentUser]);
                     SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
                     [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
                     [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
                     
                     //yes
                     [alertView addButtonWithTitle:@"Ok"
                                              type:SIAlertViewButtonTypeDestructive
                                           handler:^(SIAlertView *alert) {
                                               NSLog(@"Button1 Clicked");
                                               [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                               [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                               //yes
                                               //analytics
                                               MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                               // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                               [self presentViewController:main animated:YES completion:^{
                                                   NSLog(@"successfully presented home view controller from mainview controller");
                                               }];
                                               
                                               
                                               
                                           }];
                     
                     [alertView addButtonWithTitle:@"Cancel"
                                              type:SIAlertViewButtonTypeDefault
                                           handler:^(SIAlertView *alert) {
                                               NSLog(@"Button2 Clicked");
                                               //analytics
                                           }];
                     
                     alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                     
                     [alertView show];
                 }else{
                     if (!_profileVC) {
                         
                         [FBSDKAppEvents logEvent:@"User Wants to see their profile"];
                         [Flurry logEvent:@"User Wants to see their profile"];
                         
                         NSLog(@"profileVC is being created");
                         profileViewController*profile=[[profileViewController alloc] initWithCoder:nil];
                         profile.usedForDisplayingCurrentUser=YES;
                         profile.facebookName=[[PFUser currentUser]objectForKey:@"displayName"];
                         profile.facebookID=[[PFUser currentUser]objectForKey:@"facebookId"];
                         profile.parseUserName=[[PFUser currentUser]objectForKey:@"username"];
                         profile.parseUserObjectID=[PFUser currentUser].objectId;
                         UINavigationController*profileVC=[[UINavigationController alloc] initWithRootViewController:profile];
                         _profileVC=profileVC;
                         
                     }
                     
                     
                     [self presentViewController:_profileVC animated:YES completion:nil];
                 }
             }
             break;
            
        case 2:
        {
            //2
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            
            if (!_couponVC) {
                
                [FBSDKAppEvents logEvent:@"User Wants to see the deals page"];
                [Flurry logEvent:@"User Wants to see the deals page"];
                
                _couponVC=[[couponViewController alloc]initWithCoder:nil];
                UINavigationController*couponsVC=[[UINavigationController alloc] initWithRootViewController:_couponVC];
                _couponVC=couponsVC;
                
                NSLog(@"couponsVC is being created");
                
            }
            
            [self presentViewController:_couponVC animated:YES completion:nil];
            // [self.navigationController setViewControllers:@[_couponVC]];
            // [self.navigationController popViewControllerAnimated:YES];
            //[self.navigationController pushViewController:_couponVC animated:YES];
            
        }
            
            break;
        case 3:
        {
            //3
            NSLog(@"%@(%ld) tapped",buttonTitle, (long)index);
            /*
            
            if (!_settingsVC) {
                SettingsViewController*settings=[[SettingsViewController alloc]initWithCoder:nil];
                UINavigationController*settingsVC=[[UINavigationController alloc] initWithRootViewController:settings];
                _settingsVC=settingsVC;
                NSLog(@"settingsVC is being created");
                
            }

            [self presentViewController:_settingsVC animated:YES completion:nil];
            
            */
            
        }
            break;
            
        default:
            
            break;
    }
    
}



- (void)openNotifications{
    NotificationTableViewController*notificationTBVC=[[NotificationTableViewController alloc]init];
    notificationTBVC.parseClassName=@"Notifications";
    [self presentViewController:notificationTBVC animated:YES completion:nil];
}

- (void)startNewVideo{
    //check for anon
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:^{
                                      NSLog(@"successfully presented home view controller from mainview controller");
                                  }];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to record a new video"];
        [Flurry logEvent:@"User wants to record a new video"];
        CameraViewController*betPage=[[CameraViewController alloc]init];
        //    betSelectionViewController*betPage=[[betSelectionViewController alloc]init];
        self.navigationController.navigationBarHidden = NO;
        //    [self.navigationController prese:betPage animated:YES];
        [self.navigationController presentViewController:betPage animated:YES completion:nil];
    }
}






- (void)showDealsWallet{
    //check for anon
    if ([PFUser currentUser]==NULL) {
        NSLog(@"current user is anon %@",[PFUser currentUser]);
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Locked" andMessage:@"To use this feature, please login with Facebook"];
        [FBSDKAppEvents logEvent:@"User Tried to View profile while logged in Anonymously "];
        [Flurry logEvent:@"User Tried to View profile while logged in Anonymously"];
        
        //yes
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button1 Clicked");
                                  [FBSDKAppEvents logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  [Flurry logEvent:@"User Wants to log in with facebook from being anonymously"];
                                  //yes
                                  //analytics
                                  MainViewController*main=[[MainViewController alloc]initWithCoder:Nil];
                                  // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:main];
                                  [self presentViewController:main animated:YES completion:^{
                                      NSLog(@"successfully presented home view controller from mainview controller");
                                  }];
                                  
                                  
                                  
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  NSLog(@"Button2 Clicked");
                                  //analytics
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
    }else{
        
        [FBSDKAppEvents logEvent:@"User wants to see their deal wallet"];
        [Flurry logEvent:@"User wants to see their deal wallet"];
        
        //current way
        NSLog(@"the current user is %@",[PFUser currentUser].objectId);
        
        //get users points
        PFQuery *query = [PFQuery queryWithClassName:@"_User"];
        [query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
        query.cachePolicy = kPFCachePolicyNetworkElseCache;
        [query includeKey:@"deals.fromBusiness"];
        
        
        //start query
        __block TGLViewController*vc;
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                NSArray*deals=[object objectForKey:@"deals"];
                //NSLog(@"the deals are in dealsinWallet %@",deals);
                
                //load the controller now
                UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
                // [aFlowLayout setItemSize:CGSizeMake(400, 400)];
                [aFlowLayout setItemSize:CGSizeMake(320, 463)];
                [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                vc = [[TGLViewController alloc]initWithCollectionViewLayout:aFlowLayout];
                vc.dealsInWallet=[[NSMutableArray alloc]initWithArray:deals];;
                
                
                self.navigationController.navigationBarHidden=NO;
                [self.navigationController pushViewController:vc animated:YES];
                
            }else
                NSLog(@"error : %@",error);
        }];
        
    }
    
    
}


#pragma mark- buttons



- (IBAction)feedback:(id)sender {
    NSLog(@"user selected feedback button");
    
    IQFeedbackView *feedback = [[IQFeedbackView alloc] initWithTitle:@"Feedback" message:nil image:nil cancelButtonTitle:@"Cancel" doneButtonTitle:@"Send"];
    [feedback setCanAddImage:NO];
    [feedback setCanEditText:YES];
    
    [feedback showInViewController:self completionHandler:^(BOOL isCancel, NSString *message, UIImage *image) {
        NSLog(@"The message is %@ with length %lu and isCancel %hhd",message,(unsigned long)[message length],isCancel);
        
        if (isCancel==0 && [message length]>0 ) {
        
        PFObject*feedbackPost=[PFObject objectWithClassName:@"feedback"];
        [feedbackPost setObject:[PFUser currentUser] forKey:@"fromUser"];
        [feedbackPost setValue:message forKey:@"message"];

        [feedbackPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                //alert thanks
                
                SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Success" andMessage:@"Thank you for your feedback! "];
                alertView.messageColor=[UIColor greenColor];
                //yes
                [alertView addButtonWithTitle:@"Ok"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert)
                 {
                     //flurry track submitted feedback
                     NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                    [[PFUser currentUser]username] , @"user",
                                                    nil];
                     [Flurry logEvent:@"userSubmittedFeedback" withParameters:articleParams];
                     [FBSDKAppEvents logEvent:@"userSubmittedFeedback" parameters:articleParams];

                 }];
                
                [alertView show];
                
            }else{
                NSLog(@"the error in saving the feedback post is %@",error);
                //analytics
                [Flurry logError:@"errorSavingFeedBack" message:@"???" error:error];
                
                
            }
        }];
            
        }else
        {
            NSLog(@"the user cancelled feedback post");
        }
        
        
        [feedback dismiss];
    }];
    

}

- (IBAction)termsAndConditions:(id)sender {
    NSLog(@"user selected terms and conditions button");
    MDBrowser* browser = [[MDBrowser alloc] initWithFrame:CGRectMake(0, 75, self.view.bounds.size.width, self.view.bounds.size.height-100)];
    browser.delegate = self;
    [browser ShowInView:self.view AddOverLayToSuperView:YES withAnimationType:MDBrowserPresetationAnimationTypePopUp];
    [browser LoadUrl:[NSURL URLWithString:@"https://s3-us-west-2.amazonaws.com/binarybros.legal/termsAndConditions.html"]];
    [browser setButtonsHidden:YES];
    
    //flurry track viewed terms of use
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",
                                   nil];
    [Flurry logEvent:@"userViewedTermsOfUse" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userViewedTermsOfUse" parameters:articleParams];

    

}

- (IBAction)privacyPolicy:(id)sender {
    NSLog(@"user selected privacy policy button");
    MDBrowser* browser = [[MDBrowser alloc] initWithFrame:CGRectMake(0, 75, self.view.bounds.size.width, self.view.bounds.size.height-100)];
    browser.delegate = self;
    [browser ShowInView:self.view AddOverLayToSuperView:YES withAnimationType:MDBrowserPresetationAnimationTypePopUp];
    [browser LoadUrl:[NSURL URLWithString:@"https://s3-us-west-2.amazonaws.com/binarybros.legal/privacyPolicy.html"]];
    [browser setButtonsHidden:YES];
    
    //flurry track viewed privacy policy
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",
                                   nil];
    [Flurry logEvent:@"userViewedPrivacyPolicy" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userViewedPrivacyPolicy" parameters:articleParams];

    
}

- (IBAction)attribution:(id)sender {
    NSLog(@"user selected attribution button");
    
    MDBrowser* browser = [[MDBrowser alloc] initWithFrame:CGRectMake(0, 75, self.view.bounds.size.width, self.view.bounds.size.height-100)];
    browser.delegate = self;
    [browser ShowInView:self.view AddOverLayToSuperView:YES withAnimationType:MDBrowserPresetationAnimationTypePopUp];
    [browser LoadUrl:[NSURL URLWithString:@"https://s3-us-west-2.amazonaws.com/binarybros.legal/Attribution.html"]];
    [browser setButtonsHidden:YES];

}

- (IBAction)tutorials:(id)sender {
    NSLog(@"user selected tutorials button");
    TutorialViewController*tutorialsVC=[[TutorialViewController alloc]init];
    [self.navigationController pushViewController:tutorialsVC animated:YES];


}

- (IBAction)logOut:(id)sender {
    NSLog(@"User logged out!");
    
    
    
    //user logged out
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[PFUser currentUser]username] , @"user",
                                   nil];
    [Flurry logEvent:@"userLoggedOut" withParameters:articleParams];
    [FBSDKAppEvents logEvent:@"userLoggedOut" parameters:articleParams];

    
    
    [PFUser logOut];
    
     MainViewController*mainViewController=[[MainViewController alloc]init];
    //[self.navigationController removeFromParentViewController];
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];

    //[self removeFromParentViewController];
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self presentViewController:mainViewController animated:YES completion:nil];
    
    

}




#pragma mark- cleanup

- (void)didReceiveMemoryWarning
{
    NSLog(@"getting memory warning ~ %s",__PRETTY_FUNCTION__);
    
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    NSLog(@"viewDidUnload");
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSLog(@"viewWillDissaper");
    [self.navigationController removeFromParentViewController];

    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDissapear");
    
    [APSmartStorage.sharedInstance removeAllFromMemory];

    
}


@end
