/*
 Copyright 2013 Scott Logic Ltd
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#import "SCViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "SCShapeView.h"
#import <Parse/Parse.h>
#import "SIAlertView.h"
#import "Flurry.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface SCViewController () <AVCaptureMetadataOutputObjectsDelegate> {
    AVCaptureVideoPreviewLayer *_previewLayer;
    SCShapeView *_boundingBox;
    NSTimer *_boxHideTimer;
    UILabel *_decodedMessage;
}
@end

@implementation SCViewController

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Create a new AVCaptureSession
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    // Want the normal device

    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    
    
    
    if ([session canAddInput: input])
    {
        [session addInput: input];
    }else{
        NSLog(@"the error in adding input is %@",error);
    }
    
    /*
    if(input) {
        // Add the input to the session
        [session addInput:input];
    } else {
        NSLog(@"error: %@", error);
        return;
    }
    */
    
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
    // Have to add the output before setting metadata types
    [session addOutput:output];
    // What different things can we register to recognise?
    NSLog(@"%@", [output availableMetadataObjectTypes]);
    // We're only interested in QR Codes
    [output setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    // This VC is the delegate. Please call us on the main queue
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    // Display on screen
    _previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _previewLayer.bounds = self.view.bounds;
    _previewLayer.position = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    [self.view.layer addSublayer:_previewLayer];
    
    
    // Add the view to draw the bounding box for the UIView
    _boundingBox = [[SCShapeView alloc] initWithFrame:self.view.bounds];
    _boundingBox.backgroundColor = [UIColor clearColor];
    _boundingBox.hidden = YES;
    [self.view addSubview:_boundingBox];
    
    // Add a label to display the resultant message
    _decodedMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds) - 75, CGRectGetWidth(self.view.bounds), 75)];
    _decodedMessage.numberOfLines = 0;
    _decodedMessage.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.9];
    _decodedMessage.textColor = [UIColor darkGrayColor];
    _decodedMessage.textAlignment = NSTextAlignmentCenter;
    _decodedMessage.text=@"Please Scan QR code to Redeem the offer";
    [self.view addSubview:_decodedMessage];
    
    // Start the AVSession running
    [session startRunning];
    

}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            // Transform the meta-data coordinates to screen coords
            AVMetadataMachineReadableCodeObject *transformed = (AVMetadataMachineReadableCodeObject *)[_previewLayer transformedMetadataObjectForMetadataObject:metadata];
            // Update the frame on the _boundingBox view, and show it
            _boundingBox.frame = transformed.bounds;
            _boundingBox.hidden = NO;
            // Now convert the corners array into CGPoints in the coordinate system
            //  of the bounding box itself
            NSArray *translatedCorners = [self translatePoints:transformed.corners
                                                      fromView:self.view
                                                        toView:_boundingBox];
            
            // Set the corners array
            _boundingBox.corners = translatedCorners;
            
            // Update the view with the decoded text
            _decodedMessage.text = [transformed stringValue];
            
            if ([_decodedMessage.text isEqualToString:self.smallBusinessID]) {
                [self scannedCorrectQR];
                return;
                
            }else{
                [self scannedIncorrectQR];
            }
            //log contents
            NSLog(@"the contents are %@",transformed);
            
            // Start the timer which will hide the overlay
            [self startOverlayHideTimer];
        }
    }
}


#pragma mark - scanned methods
-(void)scannedCorrectQR{
    //show alert
    NZAlertView *alert = [[NZAlertView alloc] initWithStyle:NZAlertStyleSuccess
                          //statusBarColor:[UIColor purpleColor]
                                                      title:@"Got it!"
                                                    message:@"Successfully Scanned QR; \n Please show this to the merchant \n ENJOY! -BinaryBros \n (This message will dissapear in 15 seconds)"
                                                   delegate:self];
    
    [alert setStatusBarColor:[UIColor colorWithRed:0.897 green:0.324 blue:0.044 alpha:1.000]];
    //alert.screenBlurLevel = 1;
    //[alert setTextAlignment:NSTextAlignmentCenter];
    
    alert.alertDuration=20.0;

        [alert showWithCompletion:^{
            //returning to prevoius vc after alert
            [self.navigationController popViewControllerAnimated:YES];
            
            //used succesfully;
            //prompt YES! and show small businness that this is good
   
            NSLog(@"right coupon");
            
            //nsnotification and also check-in with the deal
            PFObject*checkIn=[PFObject objectWithClassName:@"checkins"];
            [checkIn setObject:[PFUser currentUser] forKey:@"fromUser"];
            
            [checkIn setObject:self.smallBusinessIDPointer forKey:@"atLocation"];
            [checkIn setObject:self.dealIDPointer forKey:@"withDeal"];
            
            [checkIn saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    NSLog(@"saved the checkin! ");
                    
                    //flurry track user used deal, and checked in
                    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [[PFUser currentUser]username] , @"user",self.dealIDPointer,@"deal",self.smallBusinessID,@"atLocation",
                                                   nil];
                    [Flurry logEvent:@"userUsedDeal" withParameters:articleParams];
                    [FBSDKAppEvents logEvent:@"userUsedDeal" parameters:articleParams];

                    
                    
                    
                }else{
                    NSLog(@"error is %@",error);
                }
            }];
            
            //analytics
            
            //offer to share
            //if small business has facebook.... "This location is on facebook!" share a checking, spread good word and get an extra 200 points!!!
            [self shareToFaceBook];
            
            //offer to share on fb post? all together ; solving the problem above; i just got a deal from _location using the app
            
            
        }];
}

-(void)shareToFaceBook{
    
    
    //Sharing changed on facebook 4-14-15 due to new sdk
    
    /*
     
     
     
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Spread The Love" andMessage:@"Share on Facebook for an extra 200 points! (This also REALLY helps the local merchant!) "];
    alertView.messageColor=[UIColor blueColor];
    //yes
    [alertView addButtonWithTitle:@"Yes"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"user wants to share");

                              
                              
                              
                              NSString*shareString=[NSString stringWithFormat:@"I Just Checked In at %@! \n Checkout: %@  \n - Discover great local merchants via BetMe, available for iOS \n www.BetMe.NYC ",self.locationName,self.locationShareURL];


                              [FBRequestConnection startForPostStatusUpdate:shareString
                                                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                              if (!error) {
                                                                  // Status update posted successfully to Facebook
                                                                  //add points to user
                                                                  //share to fb
                                                                  //analytics
                                                                  
                                                                  int currentPoints=[[[PFUser currentUser]objectForKey:@"points"]intValue];
                                                                  int pointsToBeAdded=200;
                                                                  int newValueForPoints=currentPoints+pointsToBeAdded;
                                                                  [[PFUser currentUser]setObject:[NSNumber numberWithInt:newValueForPoints] forKey:@"points"];
                                                                  
                                                                  [[PFUser currentUser]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                                                      if (!error) {
                                                                          NSLog(@"updated users new points completely");
                                                                          [self removeDealFromUser];
                                                                          
                                                                          //flurry track user used deal, and checked in
                                                                          NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         [[PFUser currentUser]username] , @"user",self.dealIDPointer,@"deal",self.smallBusinessID,@"atLocation",
                                                                                                         nil];
                                                                          [Flurry logEvent:@"userSharedCheckinToFaceBook" withParameters:articleParams];
                                                                          [FBSDKAppEvents logEvent:@"userSharedCheckinToFaceBook" parameters:articleParams];


                                                                          
                                                                      }else
                                                                          NSLog(@"the error in updating the users points for upload is %@,",error);
                                                                  }];
                                                                  
                                                                  NSLog(@"result: %@", result);
                                                              } else {
                                                                  // An error occurred, we need to handle the error
                                                                  // See: https://developers.facebook.com/docs/ios/errors
                                                                  NSLog(@"%@", error.description);
                                                              }
                                                          }];
                              
                          }];
    
    [alertView addButtonWithTitle:@"No"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"User Did not want to share");
                              //analytics
                              [self removeDealFromUser];
                              
                              
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];

                               */
    
    
}

-(void)scannedIncorrectQR{

    NSLog(@"wrong qr");
    
    //show alert
    NZAlertView *alert = [[NZAlertView alloc] initWithStyle:NZAlertStyleError
                          //statusBarColor:[UIColor purpleColor]
                                                      title:@"Try again"
                                                    message:@"That QR code didn't match up, Try again"
                                                   delegate:self];
    
    [alert setStatusBarColor:[UIColor colorWithRed:0.897 green:0.324 blue:0.044 alpha:1.000]];
    //alert.screenBlurLevel = 1;
    //[alert setTextAlignment:NSTextAlignmentCenter];
    
    alert.alertDuration=2.0;
    
    [alert showWithCompletion:^{
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    
    

}

-(void)removeDealFromUser{
    
    //remove from user's wallet.......(pointer) and nsotification? that will delete that object
    //add to user
    [[PFUser currentUser]removeObject:[PFObject objectWithoutDataWithClassName:@"deals" objectId:self.dealID]forKey:@"deals"];
    
    [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"the error in saving the user's data after deal consumption is %@",error);
        }else{
            NSLog(@"succesfully removed the deal from the user");
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"userUsedDeal" object:self.relativeIndexPath];
            });
        }
    }];
    
}

#pragma mark - NZAlertViewDelegate

- (void)willPresentNZAlertView:(NZAlertView *)alertView
{
    NSLog(@"%s\n\t alert will present", __PRETTY_FUNCTION__);
}

- (void)didPresentNZAlertView:(NZAlertView *)alertView
{
    NSLog(@"%s\n\t alert did present", __PRETTY_FUNCTION__);
}

- (void)NZAlertViewWillDismiss:(NZAlertView *)alertView
{
    NSLog(@"%s\n\t alert will dismiss", __PRETTY_FUNCTION__);
}

- (void)NZAlertViewDidDismiss:(NZAlertView *)alertView
{
    NSLog(@"%s\n\t alert did dismiss", __PRETTY_FUNCTION__);


}

#pragma mark - Utility Methods
- (void)startOverlayHideTimer
{
    // Cancel it if we're already running
    if(_boxHideTimer) {
        [_boxHideTimer invalidate];
    }
    
    // Restart it to hide the overlay when it fires
    _boxHideTimer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                                     target:self
                                                   selector:@selector(removeBoundingBox:)
                                                   userInfo:nil
                                                    repeats:NO];
}

- (void)removeBoundingBox:(id)sender
{
    // Hide the box and remove the decoded text
    _boundingBox.hidden = YES;
    _decodedMessage.text = @"";
}

- (NSArray *)translatePoints:(NSArray *)points fromView:(UIView *)fromView toView:(UIView *)toView
{
    NSMutableArray *translatedPoints = [NSMutableArray new];

    // The points are provided in a dictionary with keys X and Y
    for (NSDictionary *point in points) {
        // Let's turn them into CGPoints
        CGPoint pointValue = CGPointMake([point[@"X"] floatValue], [point[@"Y"] floatValue]);
        // Now translate from one view to the other
        CGPoint translatedPoint = [fromView convertPoint:pointValue toView:toView];
        // Box them up and add to the array
        [translatedPoints addObject:[NSValue valueWithCGPoint:translatedPoint]];
    }
    
    return [translatedPoints copy];
}


@end
