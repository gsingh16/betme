//
//  TGLCollectionViewCell.h
//  BetMe
//
//  Created by Admin on 5/8/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


@interface TGLCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *roundedView;
@property (strong, nonatomic) IBOutlet UITextView *nameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dealImage;

@property (strong, nonatomic) IBOutlet UITextView *dealTermsAndConditions;

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) UIColor *color;


//for qr
@property(nonatomic,strong) NSString * dealID;
@property(nonatomic,strong) NSString * smallBusinessID;
@property(nonatomic,strong) NSString * dealIDPointer;
@property(nonatomic,strong) PFObject * smallBusinessIDPointer;

@property(nonatomic,strong) NSString * locationName;
@property(nonatomic,strong) NSString * locationShareURL;

@property(nonatomic,strong)NSDictionary*cellDetailsDict;

@property(nonatomic,strong)NSIndexPath* relativeIndexPath;

@end
