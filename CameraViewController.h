//
//  CameraViewController.h
//  BetMe
//
//  Created by Admin on 2/26/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AVCaptureManager.h"
#import "NZAlertView.h"
#import "NZAlertViewDelegate.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CMTime.h>
#import "YIPopupTextView.h"
#import "ALMoviePlayerController.h"
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>


@interface CameraViewController : UIViewController <AVCaptureManagerDelegate,ALMoviePlayerControllerDelegate,UITextViewDelegate,NZAlertViewDelegate,FBSDKAppInviteDialogDelegate>{
    AVAssetExportSession *exporter;

}
@property (strong, nonatomic) IBOutlet UIButton *switchButton;
@property (strong, nonatomic) IBOutlet UIButton *flashButton;
@property (strong, nonatomic) IBOutlet UIView *camView;
@property (strong, nonatomic) IBOutlet UIButton *xButton;
//should upload pop-up view
@property (strong, nonatomic) IBOutlet UIView *shouldUploadPopUpView;
//replay video and buttons
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIView *descriptionInputView;

- (IBAction)uploadVideo:(id)sender;
- (IBAction)discardVideo:(id)sender;

- (IBAction)switchFlashMode:(id)sender;

- (IBAction)dismissViewController:(id)sender;

@property(nonatomic,strong)MPMoviePlayerViewController*movieController;

//from crop video
- (void)exportDidFinish:(AVAssetExportSession*)session;
- (void)observeValueForKeyPath:(NSString*) path ofObject:(id)object change:(NSDictionary*)change context:(void*)context;

@end
