//
//  main.m
//  essvideoshareiostest
//
//  Created by Matthias Gansrigler on 26.03.12.
//  Copyright (c) 2012 Eternal Storms Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESSAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESSAppDelegate class]));
	}
}
