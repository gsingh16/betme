//
//  CameraViewController.m
//  BetMe
//
//  Created by Admin on 2/26/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//

#import "CameraViewController.h"
#import <Parse/Parse.h>
#import "SIAlertView.h"
#import <Flurry.h>
#import "FlurryAds.h"
//for hud
#import "AVCaptureManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ALMoviePlayerController.h"
#import "ALMoviePlayerControls.h"
#import "ALButton.h"
#import "ALAirplayView.h"
#import <AVCaptureManager.h>
#import <AVFoundation/AVFoundation.h>
#import "betMeVideoServices.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@interface CameraViewController ()
<AVCaptureManagerDelegate,ALAirplayViewDelegate,ALMoviePlayerControllerDelegate,YIPopupTextViewDelegate>
{
    NSTimeInterval startTime;
    BOOL isNeededToSave;
    BOOL userSetDescription;
    YIPopupTextView* popupTextView;
}

@property (nonatomic, strong) AVCaptureManager *captureManager;
@property (nonatomic, assign) NSTimer *timer;
@property (nonatomic, strong) UIImage *recStartImage;
@property (nonatomic, strong) UIImage *recStopImage;
@property (nonatomic, strong) UIImage *outerImage1;
@property (nonatomic, strong) UIImage *outerImage2;
@property (nonatomic,assign) BOOL isFrontFacing;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UISegmentedControl *fpsControl;
@property (nonatomic, weak) IBOutlet UIButton *recBtn;
@property (nonatomic, weak) IBOutlet UIImageView *outerImageView;
@property(nonatomic,strong)NSMutableArray*tags;
@property(nonatomic,strong) NSString* betDescription;

@property(nonatomic,strong)NSURL*videoPathUrl;
@property (nonatomic, strong) ALMoviePlayerController *moviePlayer;

@end


@implementation CameraViewController
@synthesize betDescription=_betDescription;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNotification:)
                                                 name:@"recievedBetDescription"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNotification:)
                                                 name:@"exportDidFinish"
                                               object:nil];
    
    
    
    //initial checkk
    [self initialCheck];
    
    
    self.captureManager = [[AVCaptureManager alloc] initWithPreviewView:self.camView];
    self.captureManager.desiredFPS=15.0;
    self.captureManager.delegate = self;
 
    /*
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleDoubleTap:)];
    tapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:tapGesture];
    */
    
    // Setup images for the Shutter Button
    UIImage *image;
    image = [UIImage imageNamed:@"ShutterButtonStart"];
    self.recStartImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.recBtn setImage:self.recStartImage
                 forState:UIControlStateNormal];
    
    image = [UIImage imageNamed:@"ShutterButtonStop"];
    self.recStopImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    [self.recBtn setTintColor:[UIColor colorWithRed:245./255.
                                              green:51./255.
                                               blue:51./255.
                                              alpha:1.0]];
    self.outerImage1 = [UIImage imageNamed:@"outer1"];
    self.outerImage2 = [UIImage imageNamed:@"outer2"];
    self.outerImageView.image = self.outerImage1;
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    

}

-(void)initialCheck{
    
    
    PFQuery *query = [PFUser query];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        if (!error) {

        int videosUploadedToday= [[object valueForKeyPath:@"videosUploadedToday"]intValue];
        if (videosUploadedToday >= 2 ) {
            NSLog(@"reched video upload limit for the day :( with count %i",videosUploadedToday);
            //show that they have to wait until midnight
            
            //animate timer saying this much until you can upload again..
            
            //commenting out but should display pop-up saying you have a video uploading please wait
            SIAlertView*uploadLimitReachedAlert=[[SIAlertView alloc]initWithTitle:@"Daily Limit Reached" andMessage:@"You reached your daily limit of 2, please wait until after midnight, to take any more videos"];
            
            [uploadLimitReachedAlert addButtonWithTitle:@"Ok"
                                                   type:SIAlertViewButtonTypeDestructive
                                                handler:^(SIAlertView *alert) {
                                                    [self.navigationController popViewControllerAnimated:YES];
                                                    
                                                    NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
                                                    [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
                                                    
                                                    NSLog(@"should load ad");
                                                    
                                                    [self dismissViewController:nil];
                                                }];
            [uploadLimitReachedAlert show];
            
            return ;
            
        }else{
            NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
            BOOL needsToUpload=[defaults boolForKey:@"needsToUpload"];
            
            if (needsToUpload==YES)
            [self showPopUpView];
        }
        
        
        }else{
            NSLog(@"error fetching user's videosuploadedToday");
            [Flurry logError:@"errorFetchingUsersVideoUploadedTodayCount" message:error.description error:error];
            [FBSDKAppEvents logEvent:@"errorFetchingUsersVideoUploadedTodayCount" parameters:@{@"error: ":error}];

            
            }
    }];
    
}

#pragma mark - show popup view
-(void)showPopUpView{

        NSLog(@"current video waiting in q");
        self.shouldUploadPopUpView.center = CGPointMake(self.view.frame.size.width/2.0,
                                                        self.view.frame.size.height/2.0);
        [self.navigationController.view addSubview:self.shouldUploadPopUpView];
        
        [self.view addSubview:self.shouldUploadPopUpView];
        [self playTheSavedVideo];
        
        //set usersetdescription to no
        userSetDescription=NO;
        
        ////test
        BOOL editable = YES;
        
        popupTextView =
        [[YIPopupTextView alloc] initWithPlaceHolder:@"Enter Your Description:"
                                            maxCount:128
                                         buttonStyle:YIPopupTextViewButtonStyleRightCancel];
        popupTextView.delegate = self;
        popupTextView.caretShiftGestureEnabled = YES;       // default = NO. using YISwipeShiftCaret is recommended.
        popupTextView.text = @"";
        popupTextView.editable = editable;                  // set editable=NO to show without keyboard
        //popupTextView.topUIBarMargin=180;
        //popupTextView.bottomUIBarMargin=350;
        //[popupTextView showInViewController:self];
        [popupTextView showInView:self.descriptionInputView];
        NSLog(@"the current frame and location is X: %f and y: %f",popupTextView.frame.origin.x,popupTextView.frame.origin.y);
        
        
        //hide the start button
        // self.startButton.hidden=YES;

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


// =============================================================================
#pragma mark - Gesture Handler

- (void)handleDoubleTap:(UITapGestureRecognizer *)sender {
    
    [self.captureManager toggleContentsGravity];
}


// =============================================================================
#pragma mark - Private


- (void)saveRecordedFile:(NSURL *)recordedFile {
    
   // [SVProgressHUD showWithStatus:@"Saving..."
     //                    maskType:SVProgressHUDMaskTypeGradient];
    
    
    
   //my way 4-8-14  >>>>>>>
//    [self saveVideo:recordedFile withName:@"firstVideo"];

    //5-1-14 cooler way
    //new way from http://www.one-dreamer.com/cropping-video-square-like-vine-and-instagram-in-xcode.html
    [self CropVideoSquareWithURL:recordedFile];
    
    /*
    //newer way 8 2 14
    [self saveVideoWithCompletionBlockWithURL:recordedFile usingBlock:^(BOOL success) {
        if (success) {
            NSLog(@"saved video!");
        }
    }];
     */
}



#pragma mark - crop video to square 
- (void) CropVideoSquareWithURL:(NSURL*)url{
    
    //load our movie Asset
    AVAsset *asset = [AVAsset assetWithURL:url];
    
    //create an avassetrack with our asset
    AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    //create a video composition and preset some settings
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    //fps? 5-15-14 12:32pm
    videoComposition.frameDuration = CMTimeMake(1, 30);
    //here we are setting its render size to its height x height (Square)
    videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.height);
    
    NSLog(@"the render height is %f, and the width is %f",videoComposition.renderSize.height,videoComposition.renderSize.width);
    
    //create a video instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30));
 

    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    
    

 
    
    //Here we shift the viewing square up to the TOP of the video so we only see the top
   // CGAffineTransform t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, 0 );
    
    CGAffineTransform t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height,-(clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height)/2);

    NSLog(@"the size cropped is height %f, width %f",clipVideoTrack.naturalSize.height,-((clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height)) /2);
    
    //Use this code if you want the viewing square to be in the middle of the video
    //CGAffineTransform t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, -(clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height) /2 );
    
    //Make sure the square is portrait
    CGAffineTransform t2 = CGAffineTransformRotate(t1, M_PI_2);
    
    CGAffineTransform finalTransform = t2;
    [transformer setTransform:finalTransform atTime:kCMTimeZero];
    
    //add the transformer layer instructions, then add to video composition
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
  
    
    //Create an Export Path to store the cropped video
    NSString * documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *exportPath = [documentsPath stringByAppendingFormat:@"/CroppedVideo.mp4"];
    NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
    
    //Remove any prevouis videos at that path
    [[NSFileManager defaultManager]  removeItemAtURL:exportUrl error:nil];
    
    //Export
    exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetMediumQuality] ;
    exporter.videoComposition = videoComposition;
    exporter.outputURL = exportUrl;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
	exporter.shouldOptimizeForNetworkUse=YES;
    
    
	[exporter exportAsynchronouslyWithCompletionHandler:^
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             //Call when finished
             [self exportDidFinish:exporter];
         });
     }];
}


- (void)exportDidFinish:(AVAssetExportSession*)session
{
    //Play the New Cropped video
    NSURL *outputURL = session.outputURL;
    //AVURLAsset* asset = [AVURLAsset URLAssetWithURL:outputURL options:nil];
    //AVPlayerItem * newPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
    
    self.videoPathUrl=outputURL;
    
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    [defaults setURL:self.videoPathUrl forKey:@"currentVideoUrl"];
    [defaults setBool:YES forKey:@"needsToUpload"];
    [defaults synchronize];
    
    
    NSLog(@"the video path in the exportDidFinish: method is %@",self.videoPathUrl);
    
    //also nsnotify betselectionviewcontroller to dismiss
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"exportDidFinish"
     object:nil];
    
}


/* not using
//used for compression completion handler
- (void)exportDidFinishWithURL:(NSURL*)url
{
    //Play the New Cropped video
    NSURL *outputURL = url;
    //AVURLAsset* asset = [AVURLAsset URLAssetWithURL:outputURL options:nil];
    //AVPlayerItem * newPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
    
    self.videoPathUrl=outputURL;
    
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    [defaults setURL:self.videoPathUrl forKey:@"currentVideoUrl"];
    [defaults setBool:YES forKey:@"needsToUpload"];
    [defaults synchronize];
    
    
    NSLog(@"the video path in the exportDidFinishWithUrl: method is %@",self.videoPathUrl);
    
    //also nsnotify betselectionviewcontroller to dismiss
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"camViewDismissed"
     object:nil];
    
}
*/
#pragma save video crop and compress? 
- (void) saveVideoWithCompletionBlockWithURL:(NSURL*)fromUrl usingBlock:(void (^)(BOOL))completion
{
    
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
        // 2 - Video track
        AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                            preferredTrackID:kCMPersistentTrackID_Invalid];
        AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                            preferredTrackID:kCMPersistentTrackID_Invalid];
        __block CMTime time = kCMTimeZero;
        __block CGAffineTransform translate;
        __block CGSize size;
        
    
            AVAsset *asset = [AVAsset assetWithURL:fromUrl];
            AVAssetTrack *videoAssetTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
            [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
                                ofTrack:videoAssetTrack atTime:time error:nil];
            
            [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
                                ofTrack:[[asset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:time error:nil];

    
    if(true)
    {//Square
        size = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.height);
        
        translate = CGAffineTransformMakeTranslation(-0, 0);
        CGAffineTransform newTransform = CGAffineTransformConcat(translate, videoAssetTrack.preferredTransform);
        [videoTrack setPreferredTransform:newTransform];
    }
    
    
    
    
            time = CMTimeAdd(time, asset.duration);
    
        
        AVMutableVideoCompositionInstruction *vtemp = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        vtemp.timeRange = CMTimeRangeMake(kCMTimeZero, time);
        NSLog(@"\nInstruction vtemp's time range is %f %f", CMTimeGetSeconds( vtemp.timeRange.start),
              CMTimeGetSeconds(vtemp.timeRange.duration));
        
        // Also tried videoCompositionLayerInstructionWithAssetTrack:compositionVideoTrack
        AVMutableVideoCompositionLayerInstruction *vLayerInstruction = [AVMutableVideoCompositionLayerInstruction
                                                                        videoCompositionLayerInstructionWithAssetTrack:videoTrack];
        
        
        [vLayerInstruction setTransform:videoTrack.preferredTransform atTime:kCMTimeZero];
        vtemp.layerInstructions = @[vLayerInstruction];
        
        AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
        videoComposition.renderSize = size;
    
    //trying with 15 !
        videoComposition.frameDuration = CMTimeMake(1,15);
        videoComposition.instructions = @[vtemp];
        
        // 4 - Get path
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path =  [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
        NSURL *url = [NSURL fileURLWithPath:path];
        
        // 5 - Create exporter
        exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                              presetName:AVAssetExportPresetHighestQuality];
        exporter.outputURL = url;
        exporter.outputFileType = AVFileTypeQuickTimeMovie;
        exporter.shouldOptimizeForNetworkUse = YES;
        exporter.videoComposition = videoComposition;
        
        //self.exportProgressBarTimer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self.delegate selector:@selector(updateProgress) userInfo:nil repeats:YES];
        
        __block id weakSelf = self;
        
        [exporter exportAsynchronouslyWithCompletionHandler:^{
            NSLog (@"i is in your block, exportin. status is %ld",(long)exporter.status);
            dispatch_async(dispatch_get_main_queue(), ^{
                // OLD[weakSelf exportDidFinish:exporter];
                [weakSelf convertVideoToLowQuailtyWithInputURL:fromUrl outputURL:url handler:^(AVAssetExportSession * session  ) {
                    [self exportDidFinish:session];
                }];
            });
        }];
    
}

/*
-(void)exportDidFinish:(AVAssetExportSession*)session withCompletionBlock:(void(^)(BOOL success))completion {
    exporter = nil;
    
    __block id weakSelf = self;
    //delete stored pieces
    [self.assets enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(AVAsset *asset, NSUInteger idx, BOOL *stop) {
        
        NSURL *fileURL = nil;
        if ([asset isKindOfClass:AVURLAsset.class])
        {
            AVURLAsset *urlAsset = (AVURLAsset*)asset;
            fileURL = urlAsset.URL;
        }
        
        if (fileURL)
            [weakSelf removeFile:fileURL];
    }];
    
    [self.assets removeAllObjects];
    [self.delegate removeProgress];
    
    if (session.status == AVAssetExportSessionStatusCompleted) {
        NSURL *outputURL = session.outputURL;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
            [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                //delete file from documents after saving to camera roll
                [weakSelf removeFile:outputURL];
                
                if (error) {
                    completion (NO);
                } else {
                    completion (YES);
                }
            }];
        }
    }
    [self.assets removeAllObjects];
}
*/




- (void)observeValueForKeyPath:(NSString*) path ofObject:(id)object change:(NSDictionary*)change context:(void*)context
{
    //if (mPlayer.status == AVPlayerStatusReadyToPlay) {
        //[self.mPlaybackView setPlayer:self.mPlayer];
        //[self.mPlayer play];
    //}
}




// =============================================================================
#pragma mark - Timer Handler

- (void)timerHandler:(NSTimer *)timer {
    
    NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval recorded = current - startTime;
    
    self.statusLabel.text = [NSString stringWithFormat:@"%.2f", recorded];
    
    if (recorded>10.1) {
        
        NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
        [self timeLimitReached];
    }
}



// =============================================================================
#pragma mark - AVCaptureManagerDeleagte

- (void)didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL error:(NSError *)error {
    
    if (error) {
        NSLog(@"error:%@", error);
        return;
    }
    
    if (isNeededToSave) {
        NSLog(@"Needs to save");
        return;
    }
    
    
    
    
    
    [self saveRecordedFile:outputFileURL];
   // NSLog(@"this is the formal delegate with the url %@",outputFileURL);
}


// =============================================================================
#pragma mark - IBAction

- (IBAction)recButtonTapped:(id)sender {
    
    // REC START
    if (!self.captureManager.isRecording) {
        
        // change UI
        [self.recBtn setImage:self.recStopImage
                     forState:UIControlStateNormal];
        self.fpsControl.enabled = NO;
        
        //prevents stopping video (making it continuous)
        self.recBtn.enabled=NO;
        
        self.switchButton.hidden=YES;
        
        //hide the x
        self.xButton.hidden=YES;
        
        // timer start
        startTime = [[NSDate date] timeIntervalSince1970];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                      target:self
                                                    selector:@selector(timerHandler:)
                                                    userInfo:nil
                                                     repeats:YES];
        
        [self.captureManager startRecording];
    }
    // REC STOP
    else {
        
        isNeededToSave = YES;
        NSLog(@"needs to save REC STOP");
        [self.captureManager stopRecording];
        
        [self.timer invalidate];
        self.timer = nil;
        
        // change UI
        [self.recBtn setImage:self.recStartImage
                     forState:UIControlStateNormal];
        self.fpsControl.enabled = YES;
        self.switchButton.hidden=NO;

    }
}

//- (IBAction)retakeButtonTapped:(id)sender {
//
//    isNeededToSave = NO;
//    [self.captureManager stopRecording];
//
//    [self.timer invalidate];
//    self.timer = nil;
//
//    self.statusLabel.text = nil;
//}

- (IBAction)fpsChanged:(UISegmentedControl *)sender {
    
    // Switch FPS
    
    CGFloat desiredFps = 0.0;;
    switch (self.fpsControl.selectedSegmentIndex) {
        case 0:
        default:
        {
            break;
        }
        case 1:
            desiredFps = 60.0;
            break;
        case 2:
            desiredFps = 120.0;
            break;
    }
    
    
   // [SVProgressHUD showWithStatus:@"Switching..."
     //                    maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        if (desiredFps > 0.0) {
            [self.captureManager switchFormatWithDesiredFPS:desiredFps];
        }
        else {
            [self.captureManager resetFormat];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (desiredFps > 30.0) {
                self.outerImageView.image = self.outerImage2;
            }
            else {
                self.outerImageView.image = self.outerImage1;
            }
       //     [SVProgressHUD dismiss];
        });
    });
}


- (void) toggleFlashlight
{
    
    // check if flashlight available
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (device.torchMode == AVCaptureTorchModeOff)
            {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                //torchIsOn = YES;
                [self.flashButton setImage:[UIImage imageNamed:@"flashing_on"]forState:UIControlStateNormal];
                [self.flashButton setTitle:@"on" forState:UIControlStateNormal];

                ;
                

            }
            else
            {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
                // torchIsOn = NO;
                [self.flashButton setTitle:@"off" forState:UIControlStateNormal];
                [device unlockForConfiguration];}
        }
    } }


- (IBAction)switchCamera:(id)sender {
    //dont record if switiching cameras
if (!self.captureManager.isRecording) {
    
    if (!self.isFrontFacing) {
        [self.captureManager switchCamera:YES];
        self.isFrontFacing=YES;}
    else{
        [self.captureManager switchCamera:NO];
        self.isFrontFacing=NO;}
   }
    
}

- (IBAction)switchFlashMode:(id)sender {
    [self toggleFlashlight];
    
}

- (IBAction)dismissViewController:(id)sender {

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self dismissViewControllerAnimated:YES completion:nil];

}



#pragma mark- Saving video

//testing

-(void)timeLimitReached{
    
    //after time limit expires: show View for uploading, if no internet connection, then will a
    
    // doesnt really matter isNeededToSave = YES;
    [self.captureManager stopRecording];
    [self.timer invalidate];
    self.timer = nil;
    
    // change UI
    [self.recBtn setImage:self.recStartImage
                 forState:UIControlStateNormal];
    self.fpsControl.enabled = YES;
    self.switchButton.hidden=NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
   // [self dismissViewControllerAnimated:YES completion:Nil];
    //show the popup
    //if internet is good then show popup with loading
    


}

#pragma Upload And Discard Video
- (IBAction)uploadVideo:(id)sender {
    NSLog(@"should UploadVideo");
    
    if (_betDescription.length > 0) {

    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Confirm" andMessage:@"Are you sure you would like to upload? "];
    //yes
    [alertView addButtonWithTitle:@"Yes"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button1 Clicked");
                              //yes
                              [self.moviePlayer stop];
                              self.moviePlayer=nil;
                              [self.shouldUploadPopUpView removeFromSuperview];
                              
                              betMeVideoServices*videoService=[[betMeVideoServices alloc]init];
                              //should be from nsuserdefaults
                              
                              NSLog(@"the tags in the upload video are %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentVideoTags"]);
                              
                              videoService.descriptionForVideo=[[NSUserDefaults standardUserDefaults]valueForKey:@"currentVideoDescription"];
                              videoService.keyWordsForVideo=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentVideoTags"];
                              NSLog(@"the descsription for the video is %@",videoService.descriptionForVideo);
                              [videoService upload];
                              
                              [self dismissViewController:nil];
                             // [self.navigationController popViewControllerAnimated:YES];
                              
                              
                              //ask to share here for extra points
                              
                              
                          }];
    
    [alertView addButtonWithTitle:@"No"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button2 Clicked");
                              
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
        
    }else{
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:@"Please enter a description"];
        [alertView addButtonWithTitle:@"Ok" type:SIAlertViewButtonTypeDefault handler:nil];
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];

    }
    
    
    
}

- (IBAction)discardVideo:(id)sender {
    NSLog(@"should discard video");
    
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Confirm" andMessage:@"Are you sure you would like to discard this video?? "];
    //yes
    [alertView addButtonWithTitle:@"Yes"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              
                              //nsuserDefaults cleanup
                              NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
                              
                              
                              //clean description and tags
                              [defaults setValue:nil forKey:@"currentVideoDescription"];
                              [defaults setObject:nil forKey:@"currentVideoTags"];
                              
                              //Remove any prevouis videos at that path
                              [[NSFileManager defaultManager]  removeItemAtURL:[defaults URLForKey:@"currentVideoUrl"] error:nil];
                              //allow user to upload again by resetting needsToUpload
                              [defaults setBool:NO forKey:@"needsToUpload"];
                              
                              
                              [defaults synchronize];
                              
                              [self.moviePlayer stop];
                              self.moviePlayer=nil;
                              [self.shouldUploadPopUpView removeFromSuperview];
                              
                              
                              SIAlertView *alertView2 = [[SIAlertView alloc] initWithTitle:@"Video Discarded, you lost 300 points😰" andMessage:@"Invite friends to earn back 100 points?"];
                              //yes
                              
                              alertView2.messageColor=[UIColor redColor];
                              
                              
                              [alertView2 addButtonWithTitle:@"Not Now"
                                                        type:SIAlertViewButtonTypeDefault
                                                     handler:^(SIAlertView *alert) {
                                                         NSLog(@"Button1 Clicked");
                                                         //deduct points and deduct from daily limit
                                                         
                                                         PFQuery *query = [PFUser query];
                                                         [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                                                             
                                                             if (error) {
                                                                 NSLog(@"error fetching user's current points in the betselection vc %@",error);
                                                                 [self.navigationController popViewControllerAnimated:YES];
                                                             }else{
                                                                 
                                                                 
                                                                 int currentPoints=[[[PFUser currentUser]objectForKey:@"points"]intValue];
                                                                 int pointsToBeSubtracted=300;
                                                                 int newValueForPoints=currentPoints-pointsToBeSubtracted;
                                                                 [[PFUser currentUser]setObject:[NSNumber numberWithInt:newValueForPoints] forKey:@"points"];
                                                                 [[PFUser currentUser]incrementKey:@"videosUploadedToday" byAmount:[NSNumber numberWithInt:1]];
                                                                 
                                                                 [[PFUser currentUser]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                                                     if (!error) {
                                                                         NSLog(@"updated users new points completely for discard");
                                                                         
                                                                         //flurry track user discarded Video
                                                                         NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                        [[PFUser currentUser]username] , @"user",
                                                                                                        nil];
                                                                         [Flurry logEvent:@"userDiscardedVideo" withParameters:articleParams];
                                                                         [FBSDKAppEvents logEvent:@"userDiscardedVideo" parameters:articleParams];
                                                                         
                                                                         
                                                                         NSString *adSpaceName = @"INTERSTITIAL_MAIN_VIEW";
                                                                         [FlurryAds fetchAndDisplayAdForSpace:adSpaceName view:self.view size:FULLSCREEN];
                                                                         [self dismissViewController:nil];
                                                                         
                                                                         
                                                                     }else{
                                                                         NSLog(@"the error in updating the users points for discard is %@,",error);
                                                                         [Flurry logError:@"errorUpdatingPointsForDiscard" message:error.description error:error];
                                                                         [FBSDKAppEvents logEvent:@"errorUpdatingPointsForDiscard" parameters:@{@"error: ":error}];
                                                                         
                                                                     }
                                                                     
                                                                 }];
                                                             }}];
                                                         
                                                     }];
                              
                              
                              [alertView2 addButtonWithTitle:@"Ok" type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alert) {
                                  
                                  FBSDKAppInviteContent * content =[[FBSDKAppInviteContent alloc] initWithAppLinkURL:[NSURL URLWithString:@"https://fb.me/720752971368052"]];
                                  //optionally set previewImageURL
                                  content.previewImageURL = [NSURL URLWithString:@"https://s3.amazonaws.com/webresources.binarybros.betme/BimmeTheBunnyLarge1.jpg"];
                                  
                                  // present the dialog. Assumes self implements protocol `FBSDKAppInviteDialogDelegate`
                                  [FBSDKAppInviteDialog showWithContent:content
                                                               delegate:self];
                                  
                              }];
                              
                              
                              alertView2.transitionStyle = SIAlertViewTransitionStyleBounce;
                              
                              [alertView2 show];
                              

                              
                              
                          }];
    
    [alertView addButtonWithTitle:@"No" type:SIAlertViewButtonTypeDefault handler:nil];
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    
     
    



}


#pragma remove image
//removing an image

- (void)removeVideo:(NSString*)fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mov", fileName]];
    
    [fileManager removeItemAtPath: fullPath error:NULL];
    
    NSLog(@"video removed");
    
}

#pragma compression
#pragma mark - convert to low quality

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(AVAssetExportSession*))handler
{
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *session = [[AVAssetExportSession alloc] initWithAsset: urlAsset presetName:AVAssetExportPresetMediumQuality];
    session.outputURL = outputURL;
    session.outputFileType = AVFileTypeQuickTimeMovie;
    session.shouldOptimizeForNetworkUse=YES;
    [session exportAsynchronouslyWithCompletionHandler:^(void)
     {
         handler(session);
         
     }];
}
#pragma mark - play video
- (void)playTheSavedVideo{
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    NSURL*currentVideoUrl=[defaults URLForKey:@"currentVideoUrl"];
    NSLog(@"the currentVideoUrl in the play method is %@",currentVideoUrl);
    
    
    
    // create a movie player
    self.moviePlayer = [[ALMoviePlayerController alloc]initWithContentURL:currentVideoUrl];
    self.moviePlayer.delegate = self; //IMPORTANT!
    
    
    // create the controls
    ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:self.moviePlayer style:ALMoviePlayerControlsStyleNone];
    
    
    // optionally customize the controls here...
    
    [movieControls setBarColor:[UIColor colorWithRed:195/255.0 green:29/255.0 blue:29/255.0 alpha:0.5]];
    [movieControls setTimeRemainingDecrements:NO];
    [movieControls setFadeDelay:0.1];
    [movieControls setBarHeight:1.0f];
    [movieControls setSeekRate:0.f];
    
    
    //frame
    [self.moviePlayer setFrame:self.videoView.frame];
    
    // assign the controls to the  player
    [self.moviePlayer setControls:movieControls];
    
    
    //set content
    [self.moviePlayer setContentURL:currentVideoUrl];
    
    //stretch video
    self.moviePlayer.view.clipsToBounds = YES;
    [self.moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
    
    //replay
    self.moviePlayer.repeatMode = MPMovieRepeatModeOne;
    
    // add movie player to your view
    [self.shouldUploadPopUpView addSubview:self.moviePlayer.view];
    
    
    
}
#pragma mark - UITextViewDelegates
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"textViewDidBeginEditing:");
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    NSLog(@"textViewShouldEndEditing:");
    //update for parse obj
    [[NSNotificationCenter defaultCenter]postNotificationName:@"recievedBetDescription" object:textView];
    
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    [defaults setValue:textView.text forKey:@"currentVideoDescription"];
    self.tags=[self checkForHashtagwithText:textView.text];
    [defaults setObject:self.tags forKey:@"currentVideoTags"];
    [defaults synchronize];
    
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"textViewDidEndEditing:");
    //store values for nsuserdefaults

    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    if (textView.text.length + text.length > 128){
        if (location != NSNotFound){
            [textView resignFirstResponder];
            NSLog(@"exceeded 128 char");
        }
        return NO;
    }
    else if (location != NSNotFound){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    NSLog(@"textViewDidChange: with the length %i",textView.text.length);
    
    if (textView.text.length > 128){
        NSLog(@"exceeded the length");
    }
    
    
}
- (void)textViewDidChangeSelection:(UITextView *)textView{
    NSLog(@"textViewDidChangeSelection:");
}

#pragma mark - Check for hashtags

- (NSMutableArray*) checkForHashtagwithText:(NSString*)text{
    NSMutableArray*hashtags=[[NSMutableArray alloc]init];
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)];
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:1];
        NSString* word = [text substringWithRange:wordRange];
        NSLog(@"Found tag %@", word);
        [hashtags addObject:word];
    }
    
    return hashtags;
    
    /*
     hashtag = YES;
     
     if (mention &amp; hashtag &amp; comment) {
     complete = YES;
     }
     */
    
}


#pragma mark - YIPopupTextView delegate

- (void)popupTextView:(YIPopupTextView*)textView willDismissWithText:(NSString*)text cancelled:(BOOL)cancelled{
    if (cancelled==YES) {
        NSLog(@"the description was cancelled should clear");
        userSetDescription=NO;
        _betDescription=@"";

    }else{
        NSLog(@"the description was accepted");
        
    }
    
}
- (void)popupTextView:(YIPopupTextView*)textView didDismissWithText:(NSString*)text cancelled:(BOOL)cancelled{
    
    if (cancelled==YES) {
        NSLog(@"the description was cancelled should clear");
        userSetDescription=NO;
        _betDescription=@"";
    }else{
        NSLog(@"the description was accepted");
        
    }
}




#pragma mark- notifications

- (void) receivedNotification:(NSNotification *) notification
{
    
    
    if ([[notification name] isEqualToString:@"recievedBetDescription"]){
        NSLog (@"Successfully received the bet description %@",(NSString *)[notification.object valueForKey:@"text"]);
        NSLog (@"Successfully received the object frame %@",[notification.object valueForKey:@"frame"]);
        
        //set bool here
        userSetDescription=YES;
        
        _betDescription=(NSString *)[notification.object valueForKey:@"text"];
        
        
    }
    if ([[notification name] isEqualToString:@"exportDidFinish"]){

        [self showPopUpView];

    }
}


#pragma mark- app invite delegate
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results{
    NSLog(@"User did complete app invite with result %@",results);
    //should reward user
    
    if (![[results valueForKey:@"completionGesture"]isEqualToString:@"cancel"]) {
    
    PFQuery *query = [PFUser query];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        if (error) {
            NSLog(@"error fetching user's current points in the betmeVideoServices %@",error);
        }else{
            
            int currentPoints= [[object valueForKeyPath:@"points"]intValue];
            int pointsToBeAdded=150;
            
            [[PFUser currentUser]incrementKey:@"points" byAmount:[NSNumber numberWithInt:pointsToBeAdded]];
            
            
            [[PFUser currentUser]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    NSLog(@"updated users new points completely");
                    //flurry track user upload Video
                    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [[PFUser currentUser]username] , @"user",results,@"result",
                                                   nil];
                    [Flurry logEvent:@"userCompletedAppInvite" withParameters:articleParams];
                    [FBSDKAppEvents logEvent:@"userCompletedAppInvite" parameters:articleParams];
                    [self dismissViewControllerAnimated:YES completion:nil];

                    
                }else{
                    NSLog(@"the error in updating the users points for upload is %@,",error);
                    [Flurry logError:@"errorUpdatingPointsForAppInvite" message:error.description error:error];
                    [FBSDKAppEvents logEvent:@"errorUpdatingPointsForAppInvite" parameters:@{@"error:":error}];
                    [self dismissViewControllerAnimated:YES completion:nil];

                    
                }
            }];
            
        }}];
    
    }else{
        NSLog(@"User did complete app invite with result %@",results);
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
    
}


- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error{
    NSLog(@"User failed to  complete app invite with error %@",error);
    
    [Flurry logError:@"userFailedToCompleteAppInvite" message:error.description error:error];
    [FBSDKAppEvents logEvent:@"userFailedToCompleteAppInvite" parameters:@{@"error:":error}];
    [self dismissViewControllerAnimated:YES completion:nil];

    
}



@end
