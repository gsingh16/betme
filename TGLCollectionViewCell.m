//
//  TGLCollectionViewCell.m
//  BetMe
//
//  Created by Admin on 5/8/14.
//  Copyright (c) 2014 BinaryBros. All rights reserved.
//


#import <QuartzCore/QuartzCore.h>
#import "TGLCollectionViewCell.h"

@interface TGLCollectionViewCell ()



@end

@implementation TGLCollectionViewCell
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.opaque = NO;
    self.backgroundColor = [UIColor clearColor];
    
    self.roundedView.backgroundColor = self.color;
    
    self.roundedView.layer.cornerRadius = 10.0;
    self.roundedView.layer.borderWidth = 1.0;
    self.roundedView.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.nameLabel.text = self.title;
    
    

    
}

#pragma mark - Accessors

- (void)setTitle:(NSString *)title {
    
    _title = [title copy];
    
    self.nameLabel.text = self.title;
}

- (void)setColor:(UIColor *)color {
    
    _color = [color copy];
    
    self.roundedView.backgroundColor = self.color;
}

#pragma mark - Methods

- (void)setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    
    self.roundedView.layer.borderColor = self.selected ? [UIColor whiteColor].CGColor : [UIColor blackColor].CGColor;
    
    NSLog(@"selected this one");
    
}


- (IBAction)useDeal:(id)sender {
    NSLog(@"use deal?");
    //ask user if they want to use, they have to be at the location, to scan the code
    
    
    //if scan = good, show thing to SB then [index card]
    //post notifiicatoin
    

    /*
           self.cellDetailsDict=[NSDictionary dictionaryWithObjectsAndKeys:self.dealID,@"dealId", self.smallBusinessID,@"smallBusinessID", self.smallBusinessIDPointer, @"smallBusinessIDPointer",self.dealIDPointer,@"dealIdPointer" ,self.locationName,"fromLocationName",self.locationShareURL,"shareURL", nil];
    */

               self.cellDetailsDict=[NSDictionary dictionaryWithObjectsAndKeys:self.dealID,@"dealId", self.smallBusinessID,@"smallBusinessID", self.smallBusinessIDPointer, @"smallBusinessIDPointer",self.dealIDPointer,@"dealIdPointer",self.locationName,@"fromLocationName",self.locationShareURL,@"shareURL", self.relativeIndexPath,@"relativeIndexPath", nil];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"userSelectedUseDeal" object:self.cellDetailsDict];
    
    NSLog(@"the dict being passed is %@",self.cellDetailsDict);
    
}

@end

