//
//  FBPictureHelper.h
//  FacebookPicture
//
//  Created by dasmer on 1/2/14.
//  Copyright (c) 2014 Columbia University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBPictureHelper : NSObject



// gets url of large facebook image for fbid/username specified
+ (NSURL *) urlForProfilePictureSizeLargeForFBID: (NSString *)facebookID;

// gets url of normal facebook image for fbid/username specified
+ (NSURL *) urlForProfilePictureSizeNormalForFBID: (NSString *)facebookID;

// gets url of small facebook image for fbid/username specified

+ (NSURL *) urlForProfilePictureSizeSmallForFBID: (NSString *)facebookID;

// gets url of square facebook image for fbid/username specified
+ (NSURL *) urlForProfilePictureSizeSquareForFBID: (NSString *)facebookID;

// gets url of the image that has a height closest to pixel height specified for fbid/username specified
+ (NSURL *) urlForProfilePictureWithHeightClosestTo:(NSInteger) pixels ForFBID: (NSString *)facebookID;

// gets url of the image that has a width closest to pixel width specified for fbid/username specified
+ (NSURL *) urlForProfilePictureWithWidthClosestTo:(NSInteger) pixels ForFBID: (NSString *)facebookID;


//gets url of the normal sized cover photo for fbid/username specified
+ (void) requestURLForCoverPhotoSizeNormalForFBID:(NSString *) facebookID completionHandler: (void(^)(NSURL *url, NSError *error)) completion;

@end
